from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
import os
import pandas as pd
from pathlib import Path
HOME_PATH = str(Path.home())

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

True_False_choice = [('True', 'TRUE'), ('False', 'FALSE')]
class StarQuarterSelectForm(forms.Form):
    StarID = forms.CharField(label = 'StarID')
    QuarterNumber = forms.CharField(label = 'QuarterNumber') # , widget=forms.Select(choices=Quarters)

class MSAP1_Form(forms.Form):
    def __init__(self, QuarterNumber, *args, **kwargs):
        super(MSAP1_Form, self).__init__(*args, **kwargs)
        CheckerBoxChoice = []
        #print("The QuarterNumber is: ", QuarterNumber)
        for i in range(1,int(QuarterNumber)+1):
            CheckerBoxChoice.append((str(i), str('Q') + str(i)))
        self.fields['FlareQuarterSelect'] = forms.MultipleChoiceField(label='FlareQuarterSelect', required=False, widget=forms.CheckboxSelectMultiple, choices=CheckerBoxChoice)
        self.fields['TransitQuarterSelect'] = forms.MultipleChoiceField(label='TransitQuarterSelect', required=False, widget=forms.CheckboxSelectMultiple, choices=CheckerBoxChoice)

False_True_choice = [('False', 'FALSE'), ('True', 'TRUE')]
class MSAP3_MSAP4_MSAP5_Form(forms.Form):
    Oscillations = forms.CharField(label='Oscillations', widget = forms.Select(choices = True_False_choice))
    Peak_Bagging_Flag = forms.CharField(label='Peak_Bagging_Flag', widget = forms.Select(choices = True_False_choice))
    Mixed_Modes_Flag = forms.CharField(label='Mixed_Modes_Flag', widget = forms.Select(choices = False_True_choice))

class MSAP4_Form(forms.Form):
    SpotModelling_Flag = forms.CharField(label='SpotModelling_Flag', widget = forms.Select(choices = True_False_choice))
    Rotation_Period = forms.CharField(label='RotationPeriod', widget = forms.Select(choices = True_False_choice))
    Activity_Level = forms.CharField(label='ActivityLevel', widget = forms.Select(choices = True_False_choice))
    Logg_Granulation = forms.CharField(label='LoggGranulation', widget = forms.Select(choices = True_False_choice))

class MSAP5_Form(forms.Form):
    Sofesticated_Method_Flag_P_Modes = forms.CharField(label='Sofesticated_Method_Flag_P_Modes', widget = forms.Select(choices = True_False_choice))
    Inversion_Flag = forms.CharField(label='Inversion_Flag', widget = forms.Select(choices = True_False_choice))
    Interpolation_Flag_P_Modes = forms.CharField(label='Interpolation_Flag_P_Modes', widget = forms.Select(choices = True_False_choice))
    Sofesticated_Method_Flag_Mixed_Modes = forms.CharField(label='Sofesticated_Method_Flag_Mixed_Modes', widget = forms.Select(choices = True_False_choice))
    Interpolation_Flag_Mixed_Modes = forms.CharField(label='Interpolation_Flag_Mixed_Modes', widget = forms.Select(choices = True_False_choice))

class MSAP5_Form_Mixed_Modes_False(forms.Form):
    Sofesticated_Method_Flag_P_Modes = forms.CharField(label='Sofesticated_Method_Flag_P_Modes', widget = forms.Select(choices = True_False_choice))
    Inversion_Flag = forms.CharField(label='Inversion_Flag', widget = forms.Select(choices = True_False_choice))
    Interpolation_Flag_P_Modes = forms.CharField(label='Interpolation_Flag_P_Modes', widget = forms.Select(choices = True_False_choice))
