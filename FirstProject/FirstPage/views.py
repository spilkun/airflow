from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
from .forms import CreateUserForm, StarQuarterSelectForm, MSAP1_Form, MSAP3_MSAP4_MSAP5_Form, MSAP4_Form, MSAP5_Form, MSAP5_Form_Mixed_Modes_False
from subprocess import Popen #To start the Airflow initiator Scripts
import pandas as pd #to read input and produce output
import os
from pathlib import Path
import webbrowser
HOME_PATH = str(Path.home())

def Home(request):
    if request.method == 'POST':
        username = request.POST.get('username') #get the username and password
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password) #authenticating the user
         #check if user already exists
        if user is not None:
            login(request, user)
            return redirect('StarQuarterSelection') #The first page after logging in
        else:
            messages.info(request, 'username or password is incorrect')
    context = {}
    return render(request, 'FirstPage/Home.html', context)

def logoutUser(request):
    logout(request)
    return redirect('Home')

def Register(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        #print(form)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was successfully created for ' + user)
            return redirect('Home') #once registration is complete user goes to Login Page
    context = {'form':form}
    return render(request, 'FirstPage/Register.html', context)

def ForgotPassword(request):
    context = {}
    return render(request, 'FirstPage/ForgotPassword.html')

@login_required(login_url = 'Home')
def StarQuarterSelection(request):
    form = StarQuarterSelectForm()
    #print("The Home page form Has been entered ", request.method)
    if request.method == 'POST':
        #print("The Home page form Request method was equal to POST ")
        form = StarQuarterSelectForm(request.POST)
        #Postgreshook_File_Path = os.path.join(HOME_PATH, "airflow", "dags", "PostgresHook.py")
        if form.is_valid():
            #print(form)
            # print("The Home page form is valid Successfully")
            StarID = form.cleaned_data.get('StarID')
            QuarterNumber = form.cleaned_data.get('QuarterNumber')
            """Storing user inputs as an environmental variable"""
            os.environ['StarID'] = StarID
            os.environ['QuarterNumber'] = QuarterNumber
            ExportPath = os.path.join(HOME_PATH, 'userdirectory')

            #form = MSAP1_Form()
            return redirect('MSAP1')
    return render(request, 'FirstPage/StarQuarterSelection.html', {'form':form})#

@login_required(login_url = 'Home')
def MSAP1(request):
    #form = MSAP1_Form()
    if request.method == 'GET':
        """we read the Quarter Number"""
        Quarter = int(os.environ["QuarterNumber"])
        #print("Entering MSAP1 Page GET: Quarter ", Quarter)
        form = MSAP1_Form(Quarter)
        return render(request, 'FirstPage/MSAP1.html', {'form':form})
    if request.method == 'POST':
        #print("MSAP1 Page Request Method was Posted")
        """Read the Quarter number to produce the Flares.csv"""
        Quarter = int(os.environ["QuarterNumber"])
        form = MSAP1_Form(Quarter, request.POST)#
        #os.system("gnome-terminal -- /bin/sh -c 'echo PostgreshookStart; python /home/spilkun/airflow/dags/PostgresHook.py; exec bash'") #
        if form.is_valid():
            print("The MSAP1 form is valid successfully ")
            #Flares Modelling Select
            FlareQuarterSelect = form.cleaned_data.get('FlareQuarterSelect')
            #Transit Modelling Select
            TransitQuarterSelect = form.cleaned_data.get('TransitQuarterSelect')

            FlareDataTable = pd.DataFrame({'FlareQuarterSelectionData':FlareQuarterSelect})
            FlareDataPath = os.path.join(HOME_PATH, 'userdirectory', 'Flares.csv')#HOME_PATH
            FlareDataTable.to_csv(path_or_buf = FlareDataPath, header=False, index=False)

            TransitDataTable = pd.DataFrame({'TransitQuarterSelectData':TransitQuarterSelect})
            TransitDataPath = os.path.join(HOME_PATH, 'userdirectory', 'Transit.csv')
            TransitDataTable.to_csv(path_or_buf = TransitDataPath, header=False, index=False)

            return redirect("MSAP4")#render(request, 'FirstPage/MSAP3.html', {'form':form})
    return render(request, 'FirstPage/MSAP1.html', {'form':form})

@login_required(login_url = 'Home')
def MSAP3(request):
    form = MSAP3_MSAP4_MSAP5_Form()
    print("Entering MSAP3 Page")
    if request.method == 'POST':
        print("MSAP3 Page Request Method was Posted")
        form = MSAP3_MSAP4_MSAP5_Form(request.POST)
        if form.is_valid():
            print("The MSAP3 form is valid successfully")
            #Oscillations flag
            Oscillations = form.cleaned_data.get('Oscillations')
            #Peak Bagging Flag
            Peak_Bagging_Flag = form.cleaned_data.get('Peak_Bagging_Flag')
            """IDP_128_PEAK_BAGGING_FLAG"""
            #Mixed_Modes  flag
            Mixed_Modes_Flag = form.cleaned_data.get('Mixed_Modes_Flag')
            PeakBaggingFlagPath = os.path.join(HOME_PATH, 'userdirectory', 'IDP_128_PEAK_BAGGING_FLAG.csv')
            os.environ['Oscillations'] = Oscillations
            os.environ['Peak_Bagging_Flag'] = Peak_Bagging_Flag
            os.environ['Mixed_Modes_Flag'] = Mixed_Modes_Flag
            AirflowLink = '0.0.0.0:8080/'
            webbrowser.open_new_tab(AirflowLink)#Starts Airflow
            if Peak_Bagging_Flag == "True" and Mixed_Modes_Flag == "True":
                return redirect("MSAP5")
            elif Peak_Bagging_Flag == "True" and Mixed_Modes_Flag == "False":
                return redirect("MSAP5_Mixed_Modes_False")
            else:
                """Creating the user inputs into csv file"""
                UserInputPath = os.path.join(HOME_PATH, 'userdirectory', 'UserInputs.csv')
                UserInputData = pd.DataFrame({'QuarterNumber':[str(os.environ["QuarterNumber"])],
                                'StarID':[str(os.environ["StarID"])],
                                'Oscillations' :[str(os.environ["Oscillations"])],
                                'Peak_Bagging_Flag' :[str(os.environ["Peak_Bagging_Flag"])],
                                'Mixed_Modes_Flag' :[str(os.environ["Mixed_Modes_Flag"])],
                                'SpotModelling_Flag' :[str(os.environ["SpotModelling_Flag"])],
                                'RotationPeriod' :[str(os.environ["RotationPeriod"])],
                                'ActivityLevel' :[str(os.environ["ActivityLevel"])],
                                'LoggGranulation' :[str(os.environ["LoggGranulation"])]})
                UserInputData.to_csv(path_or_buf = UserInputPath, header=True, index=False)
                return redirect("datapipelines")
    return render(request, 'FirstPage/MSAP3.html', {'form':form})

@login_required(login_url = 'Home')
def MSAP4(request):
    form = MSAP4_Form()
    print("Entering MSAP4 page.")
    if request.method == 'POST':
        print("The MSAP4 page form Request method was equal to POST ")
        form = MSAP4_Form(request.POST)
        if form.is_valid():
            #print(form)
            print("The MSAP4 page form is valid Successfully")
            #Spot Modelling Flag
            SpotModelling_Flag = form.cleaned_data.get('SpotModelling_Flag')
            RotationPeriod = form.cleaned_data.get('Rotation_Period')
            ActivityLevel = form.cleaned_data.get('Activity_Level')
            LoggGranulation = form.cleaned_data.get('Logg_Granulation')
            os.environ['SpotModelling_Flag'] = SpotModelling_Flag
            os.environ['RotationPeriod'] = RotationPeriod
            os.environ['ActivityLevel'] = ActivityLevel
            os.environ['LoggGranulation'] = LoggGranulation
            return redirect('MSAP3')
    return render(request, 'FirstPage/MSAP4.html', {'form':form})#

@login_required(login_url = 'Home')
def MSAP5(request):
    form = MSAP5_Form()
    print("Entering MSAP5 page.")
    if request.method == 'POST':
        #print("Method is successfully posted: Mixed Modes ", Mixed_Modes)
        form = MSAP5_Form(request.POST)#
        if form.is_valid():
            #print(form)
            #print("The MSAP5 page form is valid Successfully")
            #Getting the user input data
            Sofesticated_Method_P_Modes = form.cleaned_data.get('Sofesticated_Method_Flag_P_Modes')
            InversionFlag = form.cleaned_data.get('Inversion_Flag')
            Interpolation_P_Modes = form.cleaned_data.get('Interpolation_Flag_P_Modes')
            Sofesticated_Method_Mixed_Modes = form.cleaned_data.get('Sofesticated_Method_Flag_Mixed_Modes')
            Interpolation_Mixed_Modes = form.cleaned_data.get('Interpolation_Flag_Mixed_Modes')

            os.environ['Sofesticated_Method_P_Modes'] = Sofesticated_Method_P_Modes
            os.environ['InversionFlag'] = InversionFlag
            os.environ['Interpolation_P_Modes'] = Interpolation_P_Modes
            os.environ['Sofesticated_Method_Mixed_Modes'] = Sofesticated_Method_Mixed_Modes
            os.environ['Interpolation_Mixed_Modes'] = Interpolation_Mixed_Modes
            """Creating the user inputs into csv file"""
            UserInputPath = os.path.join(HOME_PATH, 'userdirectory', 'UserInputs.csv')
            UserInputData = pd.DataFrame({'QuarterNumber':[str(os.environ["QuarterNumber"])],
                            'StarID':[str(os.environ["StarID"])],
                            'Oscillations' :[str(os.environ["Oscillations"])],
                            'Peak_Bagging_Flag' :[str(os.environ["Peak_Bagging_Flag"])],
                            'Mixed_Modes_Flag' :[str(os.environ["Mixed_Modes_Flag"])],
                            'SpotModelling_Flag' :[str(os.environ["SpotModelling_Flag"])],
                            'RotationPeriod' :[str(os.environ["RotationPeriod"])],
                            'ActivityLevel' :[str(os.environ["ActivityLevel"])],
                            'LoggGranulation' :[str(os.environ["LoggGranulation"])],
                            'Sofesticated_Method_P_Modes' :[str(os.environ["Sofesticated_Method_P_Modes"])],
                            'InversionFlag' :[str(os.environ["InversionFlag"])],
                            'Interpolation_P_Modes' :[str(os.environ["Interpolation_P_Modes"])],
                            'Sofesticated_Method_Mixed_Modes' :[str(os.environ["Sofesticated_Method_Mixed_Modes"])],
                            'Interpolation_Mixed_Modes' :[str(os.environ["Interpolation_Mixed_Modes"])]})
            UserInputData.to_csv(path_or_buf = UserInputPath, header=True, index=False)
            return redirect('datapipelines')
    return render(request, 'FirstPage/MSAP5.html', {'form':form})

@login_required(login_url = 'Home')
def MSAP5_Mixed_Modes_False(request):
    form = MSAP5_Form_Mixed_Modes_False()
    print("Entering MSAP5 page.")
    if request.method == 'POST':
        #print("Method is successfully posted: Mixed Modes ", Mixed_Modes)
        form = MSAP5_Form_Mixed_Modes_False(request.POST)#
        if form.is_valid():
            #print(form)
            #Getting the user input data
            Sofesticated_Method_P_Modes = form.cleaned_data.get('Sofesticated_Method_Flag_P_Modes')
            InversionFlag = form.cleaned_data.get('Inversion_Flag')
            Interpolation_P_Modes = form.cleaned_data.get('Interpolation_Flag_P_Modes')

            os.environ['Sofesticated_Method_P_Modes'] = Sofesticated_Method_P_Modes
            os.environ['InversionFlag'] = InversionFlag
            os.environ['Interpolation_P_Modes'] = Interpolation_P_Modes
            """Creating the user inputs into csv file"""
            UserInputPath = os.path.join(HOME_PATH, 'userdirectory', 'UserInputs.csv')
            UserInputData = pd.DataFrame({'QuarterNumber':[str(os.environ["QuarterNumber"])],
                            'StarID':[str(os.environ["StarID"])],
                            'Oscillations' :[str(os.environ["Oscillations"])],
                            'Peak_Bagging_Flag' :[str(os.environ["Peak_Bagging_Flag"])],
                            'Mixed_Modes_Flag' :[str(os.environ["Mixed_Modes_Flag"])],
                            'SpotModelling_Flag' :[str(os.environ["SpotModelling_Flag"])],
                            'RotationPeriod' :[str(os.environ["RotationPeriod"])],
                            'ActivityLevel' :[str(os.environ["ActivityLevel"])],
                            'LoggGranulation' :[str(os.environ["LoggGranulation"])],
                            'Sofesticated_Method_P_Modes' :[str(os.environ["Sofesticated_Method_P_Modes"])],
                            'InversionFlag' :[str(os.environ["InversionFlag"])],
                            'Interpolation_P_Modes' :[str(os.environ["Interpolation_P_Modes"])]})
            UserInputData.to_csv(path_or_buf = UserInputPath, header=True, index=False)
            return redirect('datapipelines')
    return render(request, 'FirstPage/MSAP5_Mixed_Modes_False.html', {'form':form})

@login_required(login_url = 'Home')
def datapipelines(request):
    context = {}
    UserInputPath = os.path.join(HOME_PATH, 'userdirectory', 'UserInputs.csv')
    UserFlags = pd.read_csv(UserInputPath)
    ret = []
    for flagName in UserFlags.columns:
        ret.append(flagName + ': ' + str(UserFlags.loc[0, flagName]))
    return render(request, 'FirstPage/datapipelines.html', {'uflags': ret})

