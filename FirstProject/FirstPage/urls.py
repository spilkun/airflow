from django.urls import path
from django.conf.urls import url
from django.contrib import admin
from FirstPage import views

urlpatterns = [
    path('', views.Home, name = "Home"),
    url(r'Register/', views.Register, name = "Register"),
    url(r'ForgotPassword/', views.ForgotPassword, name = "ForgotPassword"),
    url(r'logout/', views.logoutUser, name = "logout"),
    url(r'StarQuarterSelection/', views.StarQuarterSelection, name = "StarQuarterSelection"),
    url(r'MSAP1/', views.MSAP1, name = "MSAP1"),
    url(r'MSAP3/', views.MSAP3, name = "MSAP3"),
    url(r'MSAP4/', views.MSAP4, name = "MSAP4"),
    url(r'MSAP5/', views.MSAP5, name = "MSAP5"),
    url(r'MSAP5_Mixed_Modes_False/', views.MSAP5_Mixed_Modes_False, name = "MSAP5_Mixed_Modes_False"),
    url(r'datapipelines/', views.datapipelines, name = "datapipelines"),
]
