"""importing libraries"""
import pandas as pd
import os
import texttable
from pathlib import Path
from datetime import datetime
import Operations
"""Define Home directory path"""
HOME_DIR = str(Path.home())
"""Defining the path where Outputs will be Stored"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
FilePaths = pd.read_csv(os.path.join(UserDirectoryPath, 'FilePaths.csv'), delimiter = ',')
InputsPath = FilePaths.loc[0, 'InputsPath']
OutputPath = FilePaths.loc[0, 'OutputPath']
MandatoryInputsPath = FilePaths.loc[0, 'MandatoryInputsPath']
GeneratedOutputsPath = FilePaths.loc[0, 'GeneratedOutputs_Path']
"""We use the below Date and Time for the timeStamp"""
now = datetime.now()
dt_string = now.strftime("%B_%dMandatoryInputsPath_%Y_%H_%M")
"""Defining the path to the folder where all outputs are stored, this is based on
the date and time at which the data pipeline was run"""
FolderPathForReport = FilePaths.loc[0, 'FolderPathForReport']
ReportName = FilePaths.loc[0, 'ReportName']
"""Defining the Paths for the different inputs"""
"""Reading the User input Parameter, Oscillations"""
UserInputsFilePath = os.path.join(UserDirectoryPath, 'UserInputs.csv')#'airflow',
UserInputsData = pd.read_csv(UserInputsFilePath, index_col=None)
OscillationDetectionFlag = UserInputsData.loc[0, 'Oscillations']
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
class MSAP3:
    """The class defined is used to decribe the MSAP3 data pipeline, the different functions defined below
    represent the tasks performed in the different operators of the pipeline,
    in the current version of the code, we use these functions to produce a report"""
    def Start_MSAP3():
        """We generate 2 reports one for the overall work flow and one which is specific to MSAP3 data pipeline,
        thus in certain information is repeated in both the reports"""
        now = datetime.now()
        """Creating a report for MSAP3, and writing information in it"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3_temp.txt"), "a")
        file.write("\n\n")
        file.write(".\n*** --- Start of Report - MSAP3 --- ***\n\n")
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP3 is initialized.\n")
        file.close()
        """writing the content described above in the overall SAS workflow report,
        since MSAP3 and MSAP4 occur parallely, write all the information of MSAP3 in a child report,
        and later copy this content into the SAS Overall workflow report, the child report is deleted once the
        information is copied."""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp.txt"))

    """The function below performs Preparatory Data Check, PDP-A and PDP-C"""
    def PrepDataCheck_MSAP3():
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"), "a")
        file.write("\n.\nInputs Check: Preparatory Data Check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write(".\nTime Stamp: " + str(timeStamp) + "\n")
        """Extracting the path to the Preparatory Data"""
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_VSINI_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_VSINI_SPECTROSCOPY.csv'))
        PDP_C_122_M_H_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SPECTROSCOPY.csv'))
        """If Mandatory inputs are not present then raise Airflow exceptions to quit the data pipeline"""
        if PDP_A_122_PREP_OBS_DATA_Truth == "False":
            file.write("PDP_A_122_PREP_OBS_DATA is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            raise ValueError("PDP_A_122_PREP_OBS_DATA is absent, Data Pipeline failed.")
        if PDP_A_122_PREP_OBS_METADATA_Truth == "False":
            file.write("PDP_A_122_PREP_OBS_METADATA is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            raise ValueError("PDP_A_122_PREP_OBS_METADATA is absent, Data Pipeline failed.")
        if PDP_C_122_TEFF_SAPP_Truth == "False":
            file.write("PDP_C_122_TEFF_SAPP is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            raise ValueError("PDP_C_122_TEFF_SAPP is absent, Data Pipeline failed.")
        if PDP_C_125_MASS_CLASSICAL_Truth == "False":
            file.write("PDP_C_125_MASS_CLASSICAL is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            raise ValueError("PDP_C_125_MASS_CLASSICAL is absent, Data Pipeline failed.")
        if PDP_C_125_RADIUS_CLASSICAL_Truth == "False":
            file.write("PDP_C_125_RADIUS_CLASSICAL is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            raise ValueError("PDP_C_125_RADIUS_CLASSICAL is absent, Data Pipeline failed.")
        if PDP_C_122_VSINI_SPECTROSCOPY_Truth == "False":
            file.write("PDP_C_122_VSINI_SPECTROSCOPY is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            raise ValueError("PDP_C_122_VSINI_SPECTROSCOPY is absent, Data Pipeline failed.")
        if PDP_C_122_M_H_SPECTROSCOPY_Truth == "False":
            file.write("PDP_C_122_M_H_SPECTROSCOPY is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
            raise ValueError("PDP_C_122_M_H_SPECTROSCOPY is absent, Data Pipeline failed.")
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t","t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_A_122_PREP_OBS_DATA", "PDP_A_122_PREP_OBS_METADATA",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_125_MASS_CLASSICAL",
                            "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_VSINI_SPECTROSCOPY",
                            "PDP_C_122_[M_H]_SPECTROSCOPY"],
                            [str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth),
                            str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_VSINI_SPECTROSCOPY_Truth),
                            str(PDP_C_122_M_H_SPECTROSCOPY_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj2.draw())
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_A.txt"))
    """The function below performs Intermediate Data Products Check"""
    def IDP_dataCheck_MSAP3():
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_B.txt"), "a")
        file.write("\n.\nInputs Check: Intermediate Data Products.\n")
        file.write("\nThese are the outputs produced from MSAP1.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        IDP_128_AARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC.csv'))
        IDP_128_AARLC_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC_METADATA.csv'))

        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_128_AARLC", "IDP_128_AARLC_METADATA"],
                            [str(IDP_128_AARLC_Truth), str(IDP_128_AARLC_METADATA_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write("Inputs Check: Reading outputs from MSAP1.\n")
        file.write(tableObj2.draw())
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_B.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_B.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_B.txt"))
    """The function below performs Data Products3 check"""
    def DP3_dataCheck_MSAP3():
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_C.txt"), "a")
        file.write("\n.\nInputs Check: DP3 or PDP_A_128_OSC_TRAINING_SE check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        QuarterNumber = int(UserInputsData.loc[0, 'QuarterNumber'])
        DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ.csv'))
        DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
        DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        PDP_A_128_OSC_TRAINING_SE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_128_OSC_TRAINING_SE.csv'))

        if QuarterNumber == 1:
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"

        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT",
                            "DP3_128_OSC_FREQ_METADATA", "PDP_A_128_OSC_TRAINING_SE"],
                            [str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth),
                            str(DP3_128_OSC_FREQ_METADATA_Truth), str(PDP_A_128_OSC_TRAINING_SE_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj2.draw())
        file.write("\nInput Checks Completed\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_C.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_C.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP3_temp_branch_C.txt"))
    """If oscillations are present the below function is executed"""
    def DETECTION_OF_OSCILLATIONS_01():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_DETECTION_OF_OSCILLATIONS_01 = {'DETECTION_OF_OSCILLATIONS_01':['True']}
        Data_DETECTION_OF_OSCILLATIONS_01 = pd.DataFrame(Variable_DETECTION_OF_OSCILLATIONS_01)
        Data_DETECTION_OF_OSCILLATIONS_01.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP3', 'DETECTION_OF_OSCILLATIONS_01.csv'), header=True, index=False)
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\n.\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Detection for Oscillations.\n")
        file.write("Reading the inputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_128_DETECTION_METRICS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_DETECTION_METRICS.csv')
        IDP_128_DETECTION_METRICS_out = DP4PQRead.to_csv(Path_IDP_128_DETECTION_METRICS, index = False)

        Path_IDP_128_MULTIDETECTION_METRICS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_MULTIDETECTION_METRICS.csv')
        IDP_128_MULTIDETECTION_METRICS_out = DP4PQRead.to_csv(Path_IDP_128_MULTIDETECTION_METRICS, index = False)

        Path_IDP_128_AARPS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_AARPS.csv')
        IDP_128_AARPS_out = DP4PQRead.to_csv(Path_IDP_128_AARPS, index = False)

        Path_IDP_128_AARPS_METADATA = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_AARPS_METADATA.csv')
        IDP_128_AARPS_METADATA_out = DP4PQRead.to_csv(Path_IDP_128_AARPS_METADATA, index = False)

        IDP_128_AARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC.csv'))
        IDP_128_AARLC_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC_METADATA.csv'))
        IDP_128_DetectionFlag = OscillationDetectionFlag
        IDP_128_DETECTION_METRICS_Truth = os.path.isfile(Path_IDP_128_DETECTION_METRICS)
        IDP_128_MULTIDETECTION_METRICS_Truth = os.path.isfile(Path_IDP_128_MULTIDETECTION_METRICS)
        IDP_128_AARPS_Truth = os.path.isfile(Path_IDP_128_AARPS)
        IDP_128_AARPS_METADATA_Truth = os.path.isfile(Path_IDP_128_AARPS_METADATA)

        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_VSINI_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_VSINI_SPECTROSCOPY.csv'))
        PDP_C_122_M_H_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SPECTROSCOPY.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_AARLC", "IDP_128_AARLC_METADATA", "PDP_A_122_PREP_OBS_DATA", "PDP_A_122_PREP_OBS_METADATA", "PDP_C_122_TEFF_SAPP", "PDP_C_125_MASS_CLASSICAL", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_VSINI_SPECTROSCOPY", "PDP_C_122_M_H_SPECTROSCOPY"],
                            [str(IDP_128_AARLC_Truth), str(IDP_128_AARLC_METADATA_Truth), str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth), str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_VSINI_SPECTROSCOPY_Truth), str(PDP_C_122_M_H_SPECTROSCOPY_Truth)]
                            ])
        """We will now write in the file created above in the IF condition defined"""
        file.write("The inputs available are:\n \n")
        file.write(tableObj.draw())
        file.write("\nThe Intermediate Data Products produced are:\n \n")
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_128_DetectionFlag", "IDP_128_DETECTION_METRICS", "IDP_128_MULTIDETECTION_METRICS", "IDP_128_AARPS", "IDP_128_AARPS_METADATA"],
                            [str(IDP_128_DetectionFlag), str(IDP_128_DETECTION_METRICS_Truth), str(IDP_128_MULTIDETECTION_METRICS_Truth), str(IDP_128_AARPS_Truth), str(IDP_128_AARPS_METADATA_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.close()
    """If IDP_125_Detection_Flag was false the below function is executed"""
    def IDP_125_DETECTION_FLAG_MSAP3_False():
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write(".\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("IDP_128_DETECTION_FLAG is set to False.\n")
        file.close()
    """If IDP_125_Detection_Flag is True the below function is executed"""
    def GlobalParametersExtraction02_MSAP3():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GlobalParametersExtraction02_MSAP3 = {'GlobalParametersExtraction02_MSAP3':['True']}
        Data_GlobalParametersExtraction02_MSAP3 = pd.DataFrame(Variable_GlobalParametersExtraction02_MSAP3)
        Data_GlobalParametersExtraction02_MSAP3.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP3', 'GlobalParametersExtraction02_MSAP3.csv'), header=True, index=False)

        """Checking for inputs"""
        IDP_128_AARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC.csv'))
        IDP_128_AARLC_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC_METADATA.csv'))

        Path_IDP_128_DETECTION_METRICS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_DETECTION_METRICS.csv')
        Path_IDP_128_MULTIDETECTION_METRICS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_MULTIDETECTION_METRICS.csv')
        Path_IDP_128_AARPS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_AARPS.csv')
        Path_IDP_128_AARPS_METADATA = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_AARPS_METADATA.csv')

        IDP_128_DetectionFlag = OscillationDetectionFlag
        IDP_128_DETECTION_METRICS_Truth = os.path.isfile(Path_IDP_128_DETECTION_METRICS)
        IDP_128_MULTIDETECTION_METRICS_Truth = os.path.isfile(Path_IDP_128_MULTIDETECTION_METRICS)
        IDP_128_AARPS_Truth = os.path.isfile(Path_IDP_128_AARPS)
        IDP_128_AARPS_METADATA_Truth = os.path.isfile(Path_IDP_128_AARPS_METADATA)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write(".\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("IDP_128_DETECTION_FLAG is set to True.\n")
        file.write("Performing the Global Parameters Extraction.\n")
        file.write("Reading the inputs.\n")
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_AARLC", "IDP_128_AARLC_METADATA", "IDP_128_DetectionFlag", "IDP_128_DETECTION_METRICS", "IDP_128_MULTIDETECTION_METRICS", "IDP_128_AARPS", "IDP_128_AARPS_METADATA"],
                            [str(IDP_128_AARLC_Truth), str(IDP_128_AARLC_METADATA_Truth), str(IDP_128_DetectionFlag), str(IDP_128_DETECTION_METRICS_Truth), str(IDP_128_MULTIDETECTION_METRICS_Truth), str(IDP_128_AARPS_Truth), str(IDP_128_AARPS_METADATA_Truth)]
                            ])
        """We will now write in the file created above in the IF condition defined"""
        file.write("The inputs available are:\n \n")
        file.write(tableObj.draw())
        """Producing the Outputs"""
        file.write("\nThe inputs have been successfully read. \n")
        file.write("Producing Outputs. \n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))
        Path_DP3_128_DELTA_NU_AV = os.path.join(FolderPathForReport, 'MSAP3', 'DP3_128_DELTA_NU_AV.csv')
        DP3_128_DELTA_NU_AV_out = DP4PQRead.to_csv(Path_DP3_128_DELTA_NU_AV, index = False)
        Path_DP3_128_DELTA_NU_AV_METADATA = os.path.join(FolderPathForReport,'MSAP3', 'DP3_128_DELTA_NU_AV_METADATA.csv')
        DP3_128_DELTA_NU_AV_METADATA_out = DP4PQRead.to_csv(Path_DP3_128_DELTA_NU_AV_METADATA, index = False)
        Path_DP3_128_NU_MAX = os.path.join(FolderPathForReport,'MSAP3', 'DP3_128_NU_MAX.csv')
        DP3_128_NU_MAX_out = DP4PQRead.to_csv(Path_DP3_128_NU_MAX, index = False)
        Path_DP3_128_NU_MAX_METADATA = os.path.join(FolderPathForReport, 'MSAP3', 'DP3_128_NU_MAX_METADATA.csv')
        DP3_128_NU_MAX_METADATA_out = DP4PQRead.to_csv(Path_DP3_128_NU_MAX_METADATA, index = False)
        Path_IDP_128_FREQUENCY_RANGE = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_FREQUENCY_RANGE.csv')
        IDP_128_FREQUENCY_RANGE_out = DP4PQRead.to_csv(Path_IDP_128_FREQUENCY_RANGE, index = False)
        file.write("Producing DP3_128_DELTA_NU_AV. \n")
        file.write("Producing DP3_128_DELTA_NU_AV_METADATA. \n")
        file.write("Producing DP3_128_NU_MAX. \n")
        file.write("Producing DP3_128_NU_MAX_METADATA. \n")
        file.write("Producing IDP_128_FREQUENCY_RANGE. \n")
        DP3_128_DELTA_NU_AV_Truth = os.path.isfile(Path_DP3_128_DELTA_NU_AV)
        DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(Path_DP3_128_DELTA_NU_AV_METADATA)
        DP3_128_NU_MAX_Truth = os.path.isfile(Path_DP3_128_NU_MAX)
        DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(Path_DP3_128_NU_MAX_METADATA)
        IDP_128_FREQUENCY_RANGE_Truth = os.path.isfile(Path_IDP_128_FREQUENCY_RANGE)
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "IDP_128_FREQUENCY_RANGE"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(IDP_128_FREQUENCY_RANGE_Truth)]
                            ])
        file.write("\nThe Outputs produced are:\n \n")
        file.write(tableObj2.draw())
        """write the same content in the Overall SAS workflow child report"""
        Main_Report_file = open(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), "a")
        Main_Report_file.write("\nIDP_128_DETECTION FLAG was set to True, hence Global Parameter Extraction is performed.\n")
        Main_Report_file.write("Global Parameter Extraction from MSAP3 produces the following DP3's and IDP_128_FREQUENCY_RANGE as shown below:\n")
        Main_Report_file.write("\n")
        Main_Report_file.write(tableObj2.draw())
        Main_Report_file.close()
        file.close()
    """The below function prepares data for Peak Bagging"""
    def PREPARATION_FOR_PEAK_BAGGING_MSAP3_03():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_PREPARATION_FOR_PEAK_BAGGING_MSAP3_03 = {'PREPARATION_FOR_PEAK_BAGGING_MSAP3_03':['True']}
        Data_PREPARATION_FOR_PEAK_BAGGING_MSAP3_03 = pd.DataFrame(Variable_PREPARATION_FOR_PEAK_BAGGING_MSAP3_03)
        Data_PREPARATION_FOR_PEAK_BAGGING_MSAP3_03.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP3', 'PREPARATION_FOR_PEAK_BAGGING_MSAP3_03.csv'), header=True, index=False)

        """Checking for Inputs"""
        Path_IDP_128_DETECTION_METRICS = os.path.join(FolderPathForReport,'MSAP3', 'IDP_128_DETECTION_METRICS.csv')
        Path_IDP_128_MULTIDETECTION_METRICS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_MULTIDETECTION_METRICS.csv')
        Path_IDP_128_AARPS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_AARPS.csv')
        Path_IDP_128_AARPS_METADATA = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_AARPS_METADATA.csv')
        IDP_128_DetectionFlag = OscillationDetectionFlag

        IDP_128_DETECTION_METRICS_Truth = os.path.isfile(Path_IDP_128_DETECTION_METRICS)
        IDP_128_MULTIDETECTION_METRICS_Truth = os.path.isfile(Path_IDP_128_MULTIDETECTION_METRICS)
        IDP_128_AARPS_Truth = os.path.isfile(Path_IDP_128_AARPS)
        IDP_128_AARPS_METADATA_Truth = os.path.isfile(Path_IDP_128_AARPS_METADATA)

        IDP_128_AARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC.csv'))
        IDP_128_AARLC_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC_METADATA.csv'))

        QuarterNumber = int(UserInputsData.loc[0, 'QuarterNumber'])
        if QuarterNumber == 1:
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv') )
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))

        PDP_A_128_OSC_TRAINING_SE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_128_OSC_TRAINING_SE.csv'))
        Path_DP3_128_DELTA_NU_AV = os.path.join(FolderPathForReport, 'MSAP3', 'DP3_128_DELTA_NU_AV.csv')
        Path_DP3_128_DELTA_NU_AV_METADATA = os.path.join(FolderPathForReport, 'MSAP3', 'DP3_128_DELTA_NU_AV_METADATA.csv')
        Path_DP3_128_NU_MAX = os.path.join(FolderPathForReport, 'MSAP3', 'DP3_128_NU_MAX.csv')
        Path_DP3_128_NU_MAX_METADATA = os.path.join(FolderPathForReport, 'MSAP3', 'DP3_128_NU_MAX_METADATA.csv')
        Path_IDP_128_FREQUENCY_RANGE = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_FREQUENCY_RANGE.csv')

        DP3_128_DELTA_NU_AV_Truth = os.path.isfile(Path_DP3_128_DELTA_NU_AV)
        DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(Path_DP3_128_DELTA_NU_AV_METADATA)
        DP3_128_NU_MAX_Truth = os.path.isfile(Path_DP3_128_NU_MAX)
        DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(Path_DP3_128_NU_MAX_METADATA)
        IDP_128_FREQUENCY_RANGE_Truth = os.path.isfile(Path_IDP_128_FREQUENCY_RANGE)

        """Writing in the file"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), "a")
        file.write("\n.\nPreparing for Peak Bagging:\n")
        file.write("Reading the Inputs:\n")
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_DETECTION_METRICS", "IDP_128_MULTIDETECTION_METRICS", "IDP_128_AARPS", "IDP_128_AARPS_METADATA",
                            "IDP_128_DetectionFlag", "IDP_128_AARLC", "IDP_128_AARLC_METADATA", "DP3_128_OSC_FREQ",
                            "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA", "PDP_A_128_OSC_TRAINING_SE", "DP3_128_DELTA_NU_AV",
                            "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "IDP_128_FREQUENCY_RANGE"],
                            [str(IDP_128_DETECTION_METRICS_Truth), str(IDP_128_MULTIDETECTION_METRICS_Truth), str(IDP_128_AARPS_Truth), str(IDP_128_AARPS_METADATA_Truth),
                            str(IDP_128_DetectionFlag), str(IDP_128_AARLC_Truth), str(IDP_128_AARLC_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth),
                            str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth), str(PDP_A_128_OSC_TRAINING_SE_Truth), str(DP3_128_DELTA_NU_AV_Truth),
                            str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(IDP_128_FREQUENCY_RANGE_Truth)
                            ]
                            ])
        file.write(tableObj.draw())
        file.write("\nProducing the Outputs:\n")
        file.write("The Outputs produced are.\n")
        IDP_128_PEAK_BAGGING_FLAG = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))
        Path_IDP_128_BACKGROUND_FIT = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_BACKGROUND_FIT.csv')
        IDP_128_BACKGROUND_FIT_out = DP4PQRead.to_csv(Path_IDP_128_BACKGROUND_FIT, index = False)
        Path_IDP_128_FREQUENCIES_FIRST_GUESS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_FREQUENCIES_FIRST_GUESS.csv')
        IDP_128_FREQUENCIES_FIRST_GUESS_out = DP4PQRead.to_csv(Path_IDP_128_FREQUENCIES_FIRST_GUESS, index = False)
        Path_IDP_128_FREQUENCIES_FIRST_GUESS_METADATA = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_FREQUENCIES_FIRST_GUESS_METADATA.csv')
        IDP_128_FREQUENCIES_FIRST_GUESS_METADATA_out = DP4PQRead.to_csv(Path_IDP_128_FREQUENCIES_FIRST_GUESS_METADATA, index = False)
        Path_IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS.csv')
        IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS_out = DP4PQRead.to_csv(Path_IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS, index = False)
        Path_IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS.csv')
        IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS_out = DP4PQRead.to_csv(Path_IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS, index = False)
        Path_IDP_128_SPLITTING_FIRST_GUESS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_SPLITTING_FIRST_GUESS.csv')
        IDP_128_SPLITTING_FIRST_GUESS_out = DP4PQRead.to_csv(Path_IDP_128_SPLITTING_FIRST_GUESS, index = False)

        IDP_128_BACKGROUND_FIT_Truth = os.path.isfile(Path_IDP_128_BACKGROUND_FIT)
        IDP_128_FREQUENCIES_FIRST_GUESS_Truth = os.path.isfile(Path_IDP_128_FREQUENCIES_FIRST_GUESS)
        IDP_128_FREQUENCIES_FIRST_GUESS_METADATA_Truth = os.path.isfile(Path_IDP_128_FREQUENCIES_FIRST_GUESS_METADATA)
        IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS_Truth = os.path.isfile(Path_IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS)
        IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS_Truth = os.path.isfile(Path_IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS)
        IDP_128_SPLITTING_FIRST_GUESS_Truth = os.path.isfile(Path_IDP_128_SPLITTING_FIRST_GUESS)

        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=150)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_128_PEAK_BAGGING_FLAG", "IDP_128_BACKGROUND_FIT", "IDP_128_FREQUENCIES_FIRST_GUESS", "IDP_128_FREQUENCIES_FIRST_GUESS_METADATA",
                            "IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS", "IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS", "IDP_128_SPLITTING_FIRST_GUESS"],
                            [str(IDP_128_PEAK_BAGGING_FLAG), str(IDP_128_BACKGROUND_FIT_Truth), str(IDP_128_FREQUENCIES_FIRST_GUESS_Truth), str(IDP_128_FREQUENCIES_FIRST_GUESS_METADATA_Truth),
                            str(IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS_Truth), str(IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS_Truth), str(IDP_128_SPLITTING_FIRST_GUESS_Truth)                        ]
                            ])
        file.write(tableObj2.draw())
        file.close()
    """If Peak bagging is true the below function is executed"""
    def PEAK_BAGGING_MSAP3_04():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_PEAK_BAGGING_MSAP3_04 = {'PEAK_BAGGING_MSAP3_04':['True']}
        Data_PEAK_BAGGING_MSAP3_04 = pd.DataFrame(Variable_PEAK_BAGGING_MSAP3_04)
        Data_PEAK_BAGGING_MSAP3_04.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP3', 'PEAK_BAGGING_MSAP3_04.csv'), header=True, index=False)

        """Checking for Inputs"""
        IDP_128_AARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC.csv'))
        IDP_128_AARLC_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP1_Output_Path'], 'IDP_128_AARLC_METADATA.csv'))

        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_VSINI_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_VSINI_SPECTROSCOPY.csv'))
        PDP_C_122_M_H_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SPECTROSCOPY.csv'))

        Path_IDP_128_FREQUENCIES_FIRST_GUESS = os.path.join(FolderPathForReport, 'MSAP3','IDP_128_FREQUENCIES_FIRST_GUESS.csv')
        Path_IDP_128_FREQUENCIES_FIRST_GUESS_METADATA = os.path.join(FolderPathForReport, 'MSAP3','IDP_128_FREQUENCIES_FIRST_GUESS_METADATA.csv')
        Path_IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS = os.path.join(FolderPathForReport, 'MSAP3','IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS.csv')
        Path_IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS = os.path.join(FolderPathForReport,'MSAP3', 'IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS.csv')
        Path_IDP_128_SPLITTING_FIRST_GUESS = os.path.join(FolderPathForReport, 'MSAP3','IDP_128_SPLITTING_FIRST_GUESS.csv')

        IDP_128_FREQUENCIES_FIRST_GUESS_Truth = os.path.isfile(Path_IDP_128_FREQUENCIES_FIRST_GUESS)
        IDP_128_FREQUENCIES_FIRST_GUESS_METADATA_Truth = os.path.isfile(Path_IDP_128_FREQUENCIES_FIRST_GUESS_METADATA)
        IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS_Truth = os.path.isfile(Path_IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS)
        IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS_Truth = os.path.isfile(Path_IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS)
        IDP_128_SPLITTING_FIRST_GUESS_Truth = os.path.isfile(Path_IDP_128_SPLITTING_FIRST_GUESS)
        """Writing in the file"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), "a")
        file.write("\n.\nPeak Bagging Flag was set to True:\n")
        file.write("Reading the Inputs:\n")
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_AARLC", "IDP_128_AARLC_METADATA", "PDP_A_122_PREP_OBS_DATA", "PDP_A_122_PREP_OBS_METADATA",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_125_MASS_CLASSICAL", "PDP_C_122_VSINI_SPECTROSCOPY", "PDP_C_122_M_H_SPECTROSCOPY",
                            "PDP_C_125_RADIUS_CLASSICAL", "IDP_128_FREQUENCIES_FIRST_GUESS", "IDP_128_FREQUENCIES_FIRST_GUESS_METADATA", "IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS",
                            "IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS", "IDP_128_SPLITTING_FIRST_GUESS"],
                            [str(IDP_128_AARLC_Truth), str(IDP_128_AARLC_METADATA_Truth), str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(PDP_C_122_VSINI_SPECTROSCOPY_Truth), str(PDP_C_122_M_H_SPECTROSCOPY_Truth),
                            str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(IDP_128_FREQUENCIES_FIRST_GUESS_Truth), str(IDP_128_FREQUENCIES_FIRST_GUESS_METADATA_Truth), str(IDP_128_OSC_MODE_HEIGHTS_FIRST_GUESS_Truth),
                            str(IDP_128_OSC_MODE_WIDTHS_FIRST_GUESS_Truth), str(IDP_128_SPLITTING_FIRST_GUESS_Truth)
                            ]
                            ])
        file.write(tableObj.draw())
        file.write("\nProducing DP3 Outputs:\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))
        DP3_128_OSC_FREQ_out = DP4PQRead.to_csv(os.path.join(FilePaths.loc[0, 'FolderPathForReport'], 'MSAP3', 'DP3_128_OSC_FREQ.csv'), index = False)
        DP3_128_OSC_FREQ_COVMAT_out = DP4PQRead.to_csv(os.path.join(FilePaths.loc[0, 'FolderPathForReport'], 'MSAP3', 'DP3_128_OSC_FREQ_COVMAT.csv'), index = False)
        DP3_128_OSC_FREQ_METADATA_out = DP4PQRead.to_csv(os.path.join(FilePaths.loc[0, 'FolderPathForReport'], 'MSAP3', 'DP3_128_OSC_FREQ_METADATA.csv'), index = False)

        DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ.csv'))
        DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
        DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(IDP_128_AARLC_Truth), str(IDP_128_AARLC_METADATA_Truth), str(PDP_A_122_PREP_OBS_DATA_Truth)
                            ]
                            ])
        file.write(tableObj2.draw())
        """write the same content in the Overall SAS workflow child report"""
        Main_Report_file = open(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), "a")
        file.write("\nPeak Bagging Flag was set to True:\n")
        Main_Report_file.write("\nPeak Bagging Flag was set to True:\n")
        Main_Report_file.write("\nPeak Bagging from MSAP3 produces the following DP3's:\n")
        Main_Report_file.write("\n")
        Main_Report_file.write(tableObj2.draw())
        Main_Report_file.close()
        file.write("\nProducing Intermediate Data Products Outputs:\n")

        Path_IDP_128_QUALITY_METADATA = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_QUALITY_METADATA.csv')
        IDP_128_QUALITY_METADATA_out = DP4PQRead.to_csv(Path_IDP_128_QUALITY_METADATA, index = False)
        Path_IDP_128_COVMAT_ALL_PARAMETERS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_COVMAT_ALL_PARAMETERS.csv')
        IDP_128_COVMAT_ALL_PARAMETERS_out = DP4PQRead.to_csv(Path_IDP_128_COVMAT_ALL_PARAMETERS, index = False)
        Path_IDP_128_SPLITTINGS = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_SPLITTINGS.csv')
        IDP_128_SPLITTINGS_out = DP4PQRead.to_csv(Path_IDP_128_SPLITTINGS, index = False)
        Path_IDP_128_INCLINATION_ANGLE_PEAKBAGGING = os.path.join(FolderPathForReport, 'MSAP3', 'IDP_128_INCLINATION_ANGLE_PEAKBAGGING.csv')
        IDP_128_INCLINATION_ANGLE_PEAKBAGGING_out = DP4PQRead.to_csv(Path_IDP_128_INCLINATION_ANGLE_PEAKBAGGING, index = False)

        IDP_128_QUALITY_METADATA_Truth = os.path.isfile(Path_IDP_128_QUALITY_METADATA)
        IDP_128_COVMAT_ALL_PARAMETERS_Truth = os.path.isfile(Path_IDP_128_COVMAT_ALL_PARAMETERS)
        IDP_128_SPLITTINGS_Truth = os.path.isfile(Path_IDP_128_SPLITTINGS)
        IDP_128_INCLINATION_ANGLE_PEAKBAGGING_Truth = os.path.isfile(Path_IDP_128_INCLINATION_ANGLE_PEAKBAGGING)
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_128_QUALITY_METADATA", "IDP_128_COVMAT_ALL_PARAMETERS", "IDP_128_SPLITTINGS", "IDP_128_INCLINATION_ANGLE_PEAKBAGGING"],
                            [str(IDP_128_QUALITY_METADATA_Truth), str(IDP_128_COVMAT_ALL_PARAMETERS_Truth), str(IDP_128_SPLITTINGS_Truth), str(IDP_128_INCLINATION_ANGLE_PEAKBAGGING_Truth)
                            ]
                            ])
        file.write(tableObj3.draw())
        file.write("\nProducing Additional Data Products Outputs:\n")

        Path_ADP_128_BACKGROUND_BESTFIT = os.path.join(FolderPathForReport, 'MSAP3', 'ADP_128_BACKGROUND_BESTFIT.csv')
        ADP_128_BACKGROUND_BESTFIT_out = DP4PQRead.to_csv(Path_ADP_128_BACKGROUND_BESTFIT, index = False)
        Path_ADP_128_MODE_HEIGHTS = os.path.join(FolderPathForReport, 'MSAP3','ADP_128_MODE_HEIGHTS.csv')
        ADP_128_MODE_HEIGHTS_out = DP4PQRead.to_csv(Path_ADP_128_MODE_HEIGHTS, index = False)
        Path_ADP_128_MODE_WIDTHS = os.path.join(FolderPathForReport, 'MSAP3','ADP_128_MODE_WIDTHS.csv')
        ADP_128_MODE_WIDTHS_out = DP4PQRead.to_csv(Path_ADP_128_MODE_WIDTHS, index = False)
        Path_ADP_128_MODE_POWERS = os.path.join(FolderPathForReport, 'MSAP3','ADP_128_MODE_POWERS.csv')
        ADP_128_MODE_POWERS_out = DP4PQRead.to_csv(Path_ADP_128_MODE_POWERS, index = False)
        Path_ADP_128_PEAK_ASSYMETRIES = os.path.join(FolderPathForReport, 'MSAP3','ADP_128_PEAK_ASSYMETRIES.csv')
        ADP_128_PEAK_ASSYMETRIES_out = DP4PQRead.to_csv(Path_ADP_128_PEAK_ASSYMETRIES, index = False)
        Path_ADP_128_SPLITTING_ASSYMETRIES = os.path.join(FolderPathForReport, 'MSAP3','ADP_128_SPLITTING_ASSYMETRIES.csv')
        ADP_128_SPLITTING_ASSYMETRIES_out = DP4PQRead.to_csv(Path_ADP_128_SPLITTING_ASSYMETRIES, index = False)

        ADP_128_BACKGROUND_BESTFIT_Truth = os.path.isfile(Path_ADP_128_BACKGROUND_BESTFIT)
        ADP_128_MODE_HEIGHTS_Truth = os.path.isfile(Path_ADP_128_MODE_HEIGHTS)
        ADP_128_MODE_WIDTHS_Truth = os.path.isfile(Path_ADP_128_MODE_WIDTHS)
        ADP_128_MODE_POWERS_Truth = os.path.isfile(Path_ADP_128_MODE_POWERS)
        ADP_128_PEAK_ASSYMETRIES_Truth = os.path.isfile(Path_ADP_128_PEAK_ASSYMETRIES)
        ADP_128_SPLITTING_ASSYMETRIES_Truth = os.path.isfile(Path_ADP_128_SPLITTING_ASSYMETRIES)
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=140)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["ADP_128_BACKGROUND_BESTFIT", "ADP_128_MODE_HEIGHTS", "ADP_128_MODE_WIDTHS", "ADP_128_MODE_POWERS", "ADP_128_PEAK_ASSYMETRIES", "ADP_128_SPLITTING_ASSYMETRIES"],
                            [str(ADP_128_BACKGROUND_BESTFIT_Truth), str(ADP_128_MODE_HEIGHTS_Truth), str(ADP_128_MODE_WIDTHS_Truth), str(ADP_128_MODE_POWERS_Truth), str(ADP_128_PEAK_ASSYMETRIES_Truth), str(ADP_128_SPLITTING_ASSYMETRIES_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.write("\nPeak Bagging True Completed\n")
    """If Peak Bagging is False the below function is executed"""
    def PEAK_BAGGING_FLAG_FALSE_MSAP3():
        """Writing in the file"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), "a")
        file.write("\n.\nPeak Bagging was set to False:\n")
        file.write("Moving to the End of MSAP3:\n")
        file.close()
    """This is the end of the report"""
    def Stop_MSAP3():
        """For each function that was described above, we check if that function was run or not by reading the respective .csv files,
        we later delete these files"""
        Process_01 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport,'MSAP3', 'DETECTION_OF_OSCILLATIONS_01.csv'))
        Process_02 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport,'MSAP3', 'GlobalParametersExtraction02_MSAP3.csv'))
        Process_03 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport,'MSAP3', 'PREPARATION_FOR_PEAK_BAGGING_MSAP3_03.csv'))
        Process_04 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport,'MSAP3', 'PEAK_BAGGING_MSAP3_04.csv'))
        """Writing in the report file for MSAP3"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP3.txt"), "a")
        file.write("\n.\nMSAP3 Completed:\n")
        file.write("***--- End of MSAP3 ---***\n")
        """write the operations summary content in the Overall SAS workflow child report"""
        Main_Report_file = open(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"), "a")
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c", "c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t", "t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b", "b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DETECTION_OF_OSCILLATIONS_01", "GlobalParametersExtraction02_MSAP3",
                            "PREPARATION_FOR_PEAK_BAGGING_MSAP3_03", "PEAK_BAGGING_MSAP3_04"],
                            [str(Process_01), str(Process_02),
                            str(Process_03), str(Process_04)]
                            ])
        """write the table content in the Overall SAS workflow child report"""
        Main_Report_file.write("\n")
        Main_Report_file.write("\nThe summary of all the processes in MSAP3 is:\n")
        Main_Report_file.write(tableObj.draw())
        Main_Report_file.write("\nMSAP3 completed.\n")
        Main_Report_file.write("\n---**---MSAP3-END---**---\n")
        Main_Report_file.close()
        """Copy the contents from the "Main_Report_MSAP3.txt" to the Main Report file Generated,
        we do this to get the information continously since operations in airflow are performed parallely"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Main_Report_MSAP3.txt"))

if __name__ == '__Start_MSAP3__':
    Start_MSAP3()
if __name__ == '__PrepDataCheck_MSAP3__':
    PrepDataCheck_MSAP3()
if __name__ == '__IDP_dataCheck_MSAP3__':
    IDP_dataCheck_MSAP3()
if __name__ == '__DP3_dataCheck_MSAP3__':
    DP3_dataCheck_MSAP3()
if __name__ == '__DP3_dataCheck_MSAP3__':
    DP3_dataCheck_MSAP3()
if __name__ == '__DETECTION_OF_OSCILLATIONS_01__':
    DETECTION_OF_OSCILLATIONS_01()
if __name__ == '__IDP_125_DETECTION_FLAG_MSAP3_False__':
    IDP_125_DETECTION_FLAG_MSAP3_False()
if __name__ == '__GlobalParametersExtraction02_MSAP3__':
    GlobalParametersExtraction02_MSAP3()
if __name__ == '__PREPARATION_FOR_PEAK_BAGGING_MSAP3_03__':
    PREPARATION_FOR_PEAK_BAGGING_MSAP3_03()
if __name__ == '__PEAK_BAGGING_MSAP3_04__':
    PEAK_BAGGING_MSAP3_04()
if __name__ == '__PEAK_BAGGING_FLAG_FALSE_MSAP3__':
    PEAK_BAGGING_FLAG_FALSE_MSAP3()
if __name__ == '__Stop_MSAP3__':
    Stop_MSAP3()
