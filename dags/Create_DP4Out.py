"""Importing libraries"""
import pandas as pd
import os
import texttable
from pathlib import Path
from datetime import datetime
import Operations
"""Define the home directory"""
HOME_DIR = str(Path.home())
"""We define the path for the input and output file location"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
FilePaths = pd.read_csv(os.path.join(UserDirectoryPath, 'FilePaths.csv'), delimiter = ',')
InputsPath = FilePaths.loc[0, 'InputsPath']#os.path.join(HOME_DIR, 'userdirectory', 'inputs')#'airflow',
OutputPath = FilePaths.loc[0, 'OutputPath']#os.path.join(HOME_DIR, 'userdirectory', 'output')#'airflow',
GeneratedOutputsPath = FilePaths.loc[0, 'GeneratedOutputs_Path']#os.path.join(OutputPath, 'GeneratedOutputs')#'airflow',
"""Defining User inputs file"""
UserInputsFilePath = os.path.join(HOME_DIR, 'userdirectory', 'UserInputs.csv')#'airflow',
UserInputsData = pd.read_csv(UserInputsFilePath, index_col=None)
"""We use the below Date and Time for the timeStamp"""
now = datetime.now()
dt_string = now.strftime("%B_%d_%Y_%H_%M")
"""Reading the name of the overall SAS data pipeline report name which is based on
the date and time at which the data pipeline was run"""
F = open(os.path.join(GeneratedOutputsPath, "ReportID.txt"), "r")
ReportName = F.readline()
F.close()
"""Reading the name of the overall SAS data pipeline folder name which is based on
the date and time at which the data pipeline was run"""
F2 = open(os.path.join(GeneratedOutputsPath, "FolderName.txt"), "r")
FolderName = F2.readline()
F2.close()
"""Defining the path to the folder where all outputs are stored, this is based on
the date and time at which the data pipeline was run"""
FolderPathForReport = os.path.join(GeneratedOutputsPath, FolderName)
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
class MSAP4:
    """The class defined is used to decribe the MSAP4 data pipeline, the different functions defined below
    represent the tasks performed in the different operators of the pipeline,
    in the current version of the code, we use these functions to produce a report"""
    def Start_MSAP4():
        """We generate 2 reports one for the overall work flow and one which is specific to MSAP4 data pipeline,
        thus in certain information is repeated in both the reports"""
        now = datetime.now()
        """Creating a report for MSAP4, and writing information in it"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"), "a")
        file.write("\n.\n")
        file.write("*** --- Start of Report - MSAP4 --- ***\n\n")
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP4 is initialized.\n")
        file.close()
        """writing the content described above in the overall SAS workflow report,
        since MSAP3 and MSAP4 occur parallely, write all the information of MSAP4 in a child report,
        and later copy this content into the SAS Overall workflow report, the child report is deleted once the
        information is copied."""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))

    """The function below checks for IDP inputs"""
    def IntermediateDataProductsCheck_MSAP4():
        file = open(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_A.txt"), "a")
        file.write("\n.\nInputs Check: Intermediate Data Products Check.\n")
        file.write("Inputs Check: The following Intermediate Data Products are available.\n")
        file.write("Inputs Check: These inputs are produced from MSAP1 datapipeline.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Using outputs from MSAP1.\n")
        IDP_128_AARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'IDP_128_AARLC.csv'))
        IDP_128_AARLC_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'IDP_128_AARLC_METADATA.csv'))
        IDP_123_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_VARLC.csv'))
        IDP_123_BINNED_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_BINNED_VARLC.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=100)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_AARLC", "IDP_128_AARLC_METADATA", "IDP_123_VARLC",
                            "IDP_123_BINNED_VARLC"],
                            [str(IDP_128_AARLC_Truth), str(IDP_128_AARLC_METADATA_Truth), str(IDP_123_VARLC_Truth),
                            str(IDP_123_BINNED_VARLC_Truth)]
                            ])
        file.write(tableObj.draw())
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_A.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_A.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_A.txt"))

    """The function below checks for DP4 inputs"""
    def DP4PQCheck_MSAP4():
        file = open(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_B.txt"), "a")
        file.write("\n.\nInputs Check: Data Products 4 of Previous Quarter Check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        DP4PQ_Truth = os.path.isfile(FilePaths.loc[0, 'DP4_Path'])
        """If QuarterNumber is 1 then DP4 outputs are not produced"""
        QuarterNumber = UserInputsData.loc[0, 'QuarterNumber']
        if QuarterNumber == 1:
            DP4PQ_Truth == "False"
        if DP4PQ_Truth ==True:
            file.write("MSAP4: DP4 of Previous quarter file exists.\n")
        else:
            file.write("MSAP4: DP4 of Previous quarter file does not exist.\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_B.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_B.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_B.txt"))

    """The function below checks for PDP inputs"""
    def PDP_Check_MSAP4():
        file = open(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"), "a")
        file.write("\n.\nInputs Check: Preparatory Data Products Check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        PDP_123_B_CONVECTION_PRIORS_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_123_B_CONVECTION_PRIORS.csv'))
        PDP_123_B_DIFFROT_PRIORS_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_123_B_DIFFROT_PRIORS.csv'))
        PDP_123_B_ACTIVITY_PRIORS_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_123_B_ACTIVITY_PRIORS.csv'))
        """If Mandatory inputs are not present then raise Airflow exceptions to quit the data pipeline"""
        if PDP_123_B_CONVECTION_PRIORS_Truth == "False":
            file.write("PDP_123_B_CONVECTION_PRIORS is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            raise ValueError("PDP_123_B_CONVECTION_PRIORS is absent, Data Pipeline failed.")
        if PDP_123_B_DIFFROT_PRIORS_Truth == "False":
            file.write("PDP_123_B_DIFFROT_PRIORS is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            raise ValueError("PDP_123_B_DIFFROT_PRIORS is absent, Data Pipeline failed.")
        if PDP_123_B_ACTIVITY_PRIORS_Truth == "False":
            file.write("PDP_123_B_ACTIVITY_PRIORS is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
            raise ValueError("PDP_123_B_ACTIVITY_PRIORS is absent, Data Pipeline failed.")
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=90)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_123_B_CONVECTION_PRIORS", "PDP_123_B_DIFFROT_PRIORS", "PDP_123_B_ACTIVITY_PRIORS"],
                            [str(PDP_123_B_CONVECTION_PRIORS_Truth), str(PDP_123_B_DIFFROT_PRIORS_Truth), str(PDP_123_B_ACTIVITY_PRIORS_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nInputs Check Completed.\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
        # Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4"), os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp_Branch_C.txt"))
    """The function below performs fourier analysis"""
    def FourierAnalysis01_MSAP4():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_FourierAnalysis01_MSAP4 = {'FourierAnalysis01_MSAP4':['True']}
        Data_FourierAnalysis01_MSAP4 = pd.DataFrame(Variable_FourierAnalysis01_MSAP4)
        Data_FourierAnalysis01_MSAP4.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP4', 'FourierAnalysis01_MSAP4.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), "a")
        file.write("\n.\nMSAP4: FourierAnalysis01 initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The Inputs available are as below:\n")
        file.write("Using outputs from MSAP1.\n")

        Truth_IDP_128_AARLC = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'IDP_128_AARLC.csv'))
        Truth_IDP_128_AARLC_METADATA = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'IDP_128_AARLC_METADATA.csv'))
        Truth_IDP_123_VARLC = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_VARLC.csv'))
        Truth_IDP_123_BINNED_VARLC = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_BINNED_VARLC.csv'))
        Truth_DP4PQ = os.path.isfile(FilePaths.loc[0, 'DP4_Path'])
        """Create texttable object"""
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_AARLC", "IDP_128_AARLC_METADATA", "IDP_123_VARLC", "IDP_123_BINNED_VARLC", "DP4_PreviousQuarter"],
                            [str(Truth_IDP_128_AARLC), str(Truth_IDP_128_AARLC_METADATA), str(Truth_IDP_123_VARLC), str(Truth_IDP_123_BINNED_VARLC), str(Truth_DP4PQ)]
                            ])
        file.write(tableObj.draw())
        file.write("\nProducing Outputs.\n")
        file.write("The Outputs produced are:\n")
        file.write("The DP4 produced are:\n")

        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_DP4_123_HARVEY1_AMPLITUDE = os.path.join(FolderPathForReport,'MSAP4', 'DP4_123_HARVEY1_AMPLITUDE.csv')
        DP4_123_HARVEY1_AMPLITUDE_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY1_AMPLITUDE, index = False)
        Path_DP4_123_HARVEY1_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_TIME.csv')
        DP4_123_HARVEY1_TIME_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY1_TIME, index = False)
        Path_DP4_123_HARVEY1_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_EXPONENT.csv')
        DP4_123_HARVEY1_EXPONENT_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY1_EXPONENT, index = False)

        Path_DP4_123_HARVEY2_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_AMPLITUDE.csv')
        DP4_123_HARVEY2_AMPLITUDE_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY2_AMPLITUDE, index = False)
        Path_DP4_123_HARVEY2_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_TIME.csv')
        DP4_123_HARVEY2_TIME_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY2_TIME, index = False)
        Path_DP4_123_HARVEY2_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_EXPONENT.csv')
        DP4_123_HARVEY2_EXPONENT_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY2_EXPONENT, index = False)

        Path_DP4_123_HARVEY3_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_AMPLITUDE.csv')
        DP4_123_HARVEY3_AMPLITUDE_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY3_AMPLITUDE, index = False)
        Path_DP4_123_HARVEY3_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_TIME.csv')
        DP4_123_HARVEY3_TIME_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY3_TIME, index = False)
        Path_DP4_123_HARVEY3_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_EXPONENT.csv')
        DP4_123_HARVEY3_EXPONENT_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY3_EXPONENT, index = False)

        Path_DP4_123_WHITE_NOISE_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_WHITE_NOISE_FOURIER.csv')
        DP4_123_WHITE_NOISE_FOURIER_out = DP4PQRead.to_csv(Path_DP4_123_WHITE_NOISE_FOURIER, index = False)

        Path_DP4_123_HARVEY_METADATA = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY_METADATA.csv')
        DP4_123_HARVEY_METADATA_out = DP4PQRead.to_csv(Path_DP4_123_HARVEY_METADATA, index = False)

        Truth_DP4_123_HARVEY1_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY1_AMPLITUDE)
        Truth_DP4_123_HARVEY1_TIME = os.path.isfile(Path_DP4_123_HARVEY1_TIME)
        Truth_DP4_123_HARVEY1_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY1_EXPONENT)

        Truth_DP4_123_HARVEY2_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY2_AMPLITUDE)
        Truth_DP4_123_HARVEY2_TIME = os.path.isfile(Path_DP4_123_HARVEY2_TIME)
        Truth_DP4_123_HARVEY2_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY2_EXPONENT)

        Truth_DP4_123_HARVEY3_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY3_AMPLITUDE)
        Truth_DP4_123_HARVEY3_TIME = os.path.isfile(Path_DP4_123_HARVEY3_TIME)
        Truth_DP4_123_HARVEY3_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY3_EXPONENT)

        Truth_DP4_123_WHITE_NOISE_FOURIER = os.path.isfile(Path_DP4_123_WHITE_NOISE_FOURIER)
        Truth_DP4_123_HARVEY_METADATA = os.path.isfile(Path_DP4_123_HARVEY_METADATA)

        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP4_123_HARVEY1_AMPLITUDE", "DP4_123_HARVEY1_TIME", "DP4_123_HARVEY1_EXPONENT", "DP4_123_HARVEY2_AMPLITUDE", "DP4_123_HARVEY2_TIME", "DP4_123_HARVEY2_EXPONENT", "DP4_123_HARVEY3_AMPLITUDE", "DP4_123_HARVEY3_TIME", "DP4_123_HARVEY3_EXPONENT", "DP4_123_WHITE_NOISE_FOURIER", "DP4_123_HARVEY_METADATA"],
                            [str(Truth_DP4_123_HARVEY1_AMPLITUDE), str(Truth_DP4_123_HARVEY1_TIME), str(Truth_DP4_123_HARVEY1_EXPONENT), str(Truth_DP4_123_HARVEY2_AMPLITUDE), str(Truth_DP4_123_HARVEY2_TIME), str(Truth_DP4_123_HARVEY2_EXPONENT), str(Truth_DP4_123_HARVEY3_AMPLITUDE), str(Truth_DP4_123_HARVEY3_TIME), str(Truth_DP4_123_HARVEY3_EXPONENT), str(Truth_DP4_123_WHITE_NOISE_FOURIER), str(Truth_DP4_123_HARVEY_METADATA)]
                            ])
        file.write(tableObj2.draw())
        file.write("\n.\nThe following Intermediate Data Products are produced:\n")
        Path_IDP_123_PROT_POWERSPECTRUM = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_POWERSPECTRUM.csv')
        IDP_123_PROT_POWERSPECTRUM_out = DP4PQRead.to_csv(Path_IDP_123_PROT_POWERSPECTRUM, index = False)
        Path_IDP_123_PROT_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_FOURIER.csv')
        IDP_123_PROT_FOURIER_out = DP4PQRead.to_csv(Path_IDP_123_PROT_FOURIER, index = False)
        Path_IDP_123_DIFF_ROT_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DIFF_ROT_FOURIER.csv')
        IDP_123_DIFF_ROT_FOURIER_out = DP4PQRead.to_csv(Path_IDP_123_DIFF_ROT_FOURIER, index = False)

        Truth_IDP_123_PROT_POWERSPECTRUM = os.path.isfile(Path_IDP_123_PROT_POWERSPECTRUM)
        Truth_IDP_123_PROT_FOURIER = os.path.isfile(Path_IDP_123_PROT_FOURIER)
        Truth_IDP_123_DIFF_ROT_FOURIER = os.path.isfile(Path_IDP_123_DIFF_ROT_FOURIER)

        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_123_PROT_POWERSPECTRUM", "IDP_123_PROT_FOURIER", "IDP_123_DIFF_ROT_FOURIER"],
                            [str(Truth_IDP_123_PROT_POWERSPECTRUM), str(Truth_IDP_123_PROT_FOURIER), str(Truth_IDP_123_DIFF_ROT_FOURIER)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nFourier Analysis Completed\n")
        file.close()
    def TimeSeriesAnalysis02_MSAP4():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_TimeSeriesAnalysis02_MSAP4 = {'TimeSeriesAnalysis02_MSAP4':['True']}
        Data_TimeSeriesAnalysis02_MSAP4 = pd.DataFrame(Variable_TimeSeriesAnalysis02_MSAP4)
        Data_TimeSeriesAnalysis02_MSAP4.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP4', 'TimeSeriesAnalysis02_MSAP4.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), "a")
        file.write("\n.\nMSAP4: Time Series Analysis initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The Inputs available are as below:\n")
        Truth_IDP_128_AARLC = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'IDP_128_AARLC.csv'))
        Truth_IDP_128_AARLC_METADATA = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'IDP_128_AARLC_METADATA.csv'))
        Truth_IDP_123_VARLC = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_VARLC.csv'))
        Truth_IDP_123_BINNED_VARLC = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_BINNED_VARLC.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_AARLC", "IDP_128_AARLC_METADATA", "IDP_123_VARLC", "IDP_123_BINNED_VARLC"],
                            [str(Truth_IDP_128_AARLC), str(Truth_IDP_128_AARLC_METADATA), str(Truth_IDP_123_VARLC), str(Truth_IDP_123_BINNED_VARLC)]
                            ])
        file.write(tableObj.draw())
        file.write("\n.\nThe following Intermediate Data Products are produced:\n")

        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))

        Path_IDP_123_PROT_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_TIMESERIES.csv')
        IDP_123_PROT_TIMESERIES_out = DP4PQRead.to_csv(Path_IDP_123_PROT_TIMESERIES, index = False)

        Path_IDP_123_DIFF_ROT_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DIFF_ROT_TIMESERIES.csv')
        IDP_123_DIFF_ROT_TIMESERIES_out = DP4PQRead.to_csv(Path_IDP_123_DIFF_ROT_TIMESERIES, index = False)

        Path_IDP_123_PCYCLE_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PCYCLE_TIMESERIES.csv')
        IDP_123_PCYCLE_TIMESERIES_out = DP4PQRead.to_csv(Path_IDP_123_PCYCLE_TIMESERIES, index = False)

        Path_IDP_123_FLICKERING_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_FLICKERING_AMPLITUDE.csv')
        IDP_123_FLICKERING_AMPLITUDE_out = DP4PQRead.to_csv(Path_IDP_123_FLICKERING_AMPLITUDE, index = False)

        Truth_IDP_123_PROT_TIMESERIES = os.path.isfile(Path_IDP_123_PROT_TIMESERIES)
        Truth_IDP_123_DIFF_ROT_TIMESERIES = os.path.isfile(Path_IDP_123_DIFF_ROT_TIMESERIES)
        Truth_IDP_123_PCYCLE_TIMESERIES = os.path.isfile(Path_IDP_123_PCYCLE_TIMESERIES)
        Truth_IDP_123_FLICKERING_AMPLITUDE = os.path.isfile(Path_IDP_123_FLICKERING_AMPLITUDE)

        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_123_PROT_TIMESERIES", "IDP_123_DIFF_ROT_TIMESERIES", "IDP_123_PCYCLE_TIMESERIES", "IDP_123_FLICKERING_AMPLITUDE"],
                            [str(Truth_IDP_123_PROT_TIMESERIES), str(Truth_IDP_123_DIFF_ROT_TIMESERIES), str(Truth_IDP_123_PCYCLE_TIMESERIES), str(Truth_IDP_123_FLICKERING_AMPLITUDE)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nTime Series Analysis Completed\n")
        file.close()
    def RotationPeriodDetermination03_MSAP4():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_RotationPeriodDetermination03_MSAP4 = {'RotationPeriodDetermination03_MSAP4':['True']}
        Data_RotationPeriodDetermination03_MSAP4 = pd.DataFrame(Variable_RotationPeriodDetermination03_MSAP4)
        Data_RotationPeriodDetermination03_MSAP4.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP4', 'RotationPeriodDetermination03_MSAP4.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), "a")
        file.write("\n.\nMSAP4: Rotation Period Determination initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The Inputs available are as below:\n")
        file.write("The DP4's and IDP's available are as below:\n")
        Path_DP4_123_HARVEY1_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_AMPLITUDE.csv')
        Path_DP4_123_HARVEY1_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_TIME.csv')
        Path_DP4_123_HARVEY1_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_EXPONENT.csv')
        Path_DP4_123_HARVEY2_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_AMPLITUDE.csv')
        Path_DP4_123_HARVEY2_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_TIME.csv')
        Path_DP4_123_HARVEY2_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_EXPONENT.csv')
        Path_DP4_123_HARVEY3_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_AMPLITUDE.csv')
        Path_DP4_123_HARVEY3_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_TIME.csv')
        Path_DP4_123_HARVEY3_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_EXPONENT.csv')
        Path_DP4_123_WHITE_NOISE_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_WHITE_NOISE_FOURIER.csv')
        Path_DP4_123_HARVEY_METADATA = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY_METADATA.csv')
        Path_IDP_123_PROT_POWERSPECTRUM = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_POWERSPECTRUM.csv')
        Path_IDP_123_PROT_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_FOURIER.csv')
        Path_IDP_123_DIFF_ROT_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DIFF_ROT_FOURIER.csv')
        Path_IDP_123_PROT_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_TIMESERIES.csv')
        Path_IDP_123_DIFF_ROT_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DIFF_ROT_TIMESERIES.csv')

        Truth_DP4_123_HARVEY1_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY1_AMPLITUDE)
        Truth_DP4_123_HARVEY1_TIME = os.path.isfile(Path_DP4_123_HARVEY1_TIME)
        Truth_DP4_123_HARVEY1_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY1_EXPONENT)
        Truth_DP4_123_HARVEY2_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY2_AMPLITUDE)
        Truth_DP4_123_HARVEY2_TIME = os.path.isfile(Path_DP4_123_HARVEY2_TIME)
        Truth_DP4_123_HARVEY2_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY2_EXPONENT)
        Truth_DP4_123_HARVEY3_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY3_AMPLITUDE)
        Truth_DP4_123_HARVEY3_TIME = os.path.isfile(Path_DP4_123_HARVEY3_TIME)
        Truth_DP4_123_HARVEY3_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY3_EXPONENT)
        Truth_DP4_123_WHITE_NOISE_FOURIER = os.path.isfile(Path_DP4_123_WHITE_NOISE_FOURIER)
        Truth_DP4_123_HARVEY_METADATA = os.path.isfile(Path_DP4_123_HARVEY_METADATA)
        Truth_IDP_123_PROT_POWERSPECTRUM = os.path.isfile(Path_IDP_123_PROT_POWERSPECTRUM)
        Truth_IDP_123_PROT_FOURIER = os.path.isfile(Path_IDP_123_PROT_FOURIER)
        Truth_IDP_123_DIFF_ROT_FOURIER = os.path.isfile(Path_IDP_123_DIFF_ROT_FOURIER)
        Truth_IDP_123_PROT_TIMESERIES = os.path.isfile(Path_IDP_123_PROT_TIMESERIES)
        Truth_IDP_123_DIFF_ROT_TIMESERIES = os.path.isfile(Path_IDP_123_DIFF_ROT_TIMESERIES)
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_HARVEY1_AMPLITUDE", "DP4_123_HARVEY1_TIME", "DP4_123_HARVEY1_EXPONENT", "DP4_123_HARVEY2_AMPLITUDE",
                            "DP4_123_HARVEY2_TIME", "DP4_123_HARVEY2_EXPONENT", "DP4_123_HARVEY3_AMPLITUDE", "DP4_123_HARVEY3_TIME",
                            "DP4_123_HARVEY3_EXPONENT", "DP4_123_WHITE_NOISE_FOURIER", "DP4_123_HARVEY_METADATA", "IDP_123_PROT_POWERSPECTRUM",
                            "IDP_123_PROT_FOURIER", "IDP_123_DIFF_ROT_FOURIER", "IDP_123_PROT_TIMESERIES", "IDP_123_DIFF_ROT_TIMESERIES"
                            ],
                            [str(Truth_DP4_123_HARVEY1_AMPLITUDE),str(Truth_DP4_123_HARVEY1_TIME), str(Truth_DP4_123_HARVEY1_EXPONENT), str(Truth_DP4_123_HARVEY2_AMPLITUDE),
                            str(Truth_DP4_123_HARVEY2_TIME), str(Truth_DP4_123_HARVEY2_EXPONENT), str(Truth_DP4_123_HARVEY3_AMPLITUDE), str(Truth_DP4_123_HARVEY3_TIME),
                            str(Truth_DP4_123_HARVEY3_EXPONENT), str(Truth_DP4_123_WHITE_NOISE_FOURIER), str(Truth_DP4_123_HARVEY_METADATA), str(Truth_IDP_123_PROT_POWERSPECTRUM),
                            str(Truth_IDP_123_PROT_FOURIER), str(Truth_IDP_123_DIFF_ROT_FOURIER), str(Truth_IDP_123_PROT_TIMESERIES), str(Truth_IDP_123_DIFF_ROT_TIMESERIES)]
                            ])
        file.write(tableObj.draw())
        file.write("\nReading the Preparatory Data and DP4 of Previous Quarter:\n")

        Truth_DP4PQ = os.path.isfile(FilePaths.loc[0, 'DP4_Path'])
        Truth_PDP_123_B_DIFFROT_PRIORS = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_123_B_DIFFROT_PRIORS.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP4 Previous Quarter", "PDP_123_B_DIFFROT_PRIORS"],
                            [str(Truth_DP4PQ), str(Truth_PDP_123_B_DIFFROT_PRIORS)]
                            ])
        file.write(tableObj2.draw())
        file.write("\n.\nThe following Intermediate Data Products are produced:\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))

        """Check for Rotation Period in UserInputsData  and produce outputs accordingly"""
        UserInputsData = pd.read_csv(os.path.join(HOME_DIR, 'userdirectory','UserInputs.csv'), delimiter=',')
        RotationPeriod = UserInputsData.loc[0, "RotationPeriod"]

        if bool(RotationPeriod) == False:
            Truth_IDP_123__PROT_NOSPOT = "False"
            Truth_IDP_123_DELTA_PROT_NOSPOT = "False"
        else:
            Path_IDP_123_PROT_NOSPOT = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_NOSPOT.csv')
            IDP_123_PROT_NOSPOT_out = DP4PQRead.to_csv(Path_IDP_123_PROT_NOSPOT, index = False)
            Path_IDP_123_DELTA_PROT_NOSPOT = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DELTA_PROT_NOSPOT.csv')
            IDP_123_DELTA_PROT_NOSPOT_out = DP4PQRead.to_csv(Path_IDP_123_DELTA_PROT_NOSPOT, index = False)
            Truth_IDP_123__PROT_NOSPOT = os.path.isfile(Path_IDP_123_PROT_NOSPOT)
            Truth_IDP_123_DELTA_PROT_NOSPOT = os.path.isfile(Path_IDP_123_DELTA_PROT_NOSPOT)
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_123_PROT_NOSPOT", "IDP_123_DELTA_PROT_NOSPOT"],
                            [str(Truth_IDP_123__PROT_NOSPOT), str(Truth_IDP_123_DELTA_PROT_NOSPOT)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nRotation Period Determination Completed\n")
        file.close()
    def GravityFromFlicker_FliPer04_MSAP4():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GravityFromFlicker_FliPer04_MSAP4 = {'GravityFromFlicker_FliPer04_MSAP4':['True']}
        Data_GravityFromFlicker_FliPer04_MSAP4 = pd.DataFrame(Variable_GravityFromFlicker_FliPer04_MSAP4)
        Data_GravityFromFlicker_FliPer04_MSAP4.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP4', 'GravityFromFlicker_FliPer04_MSAP4.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), "a")
        file.write("\n.\nMSAP4: Gravity From Flicker/FliPer initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The Inputs available are as below:\n")
        Path_DP4_123_HARVEY1_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_AMPLITUDE.csv')
        Path_DP4_123_HARVEY1_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_TIME.csv')
        Path_DP4_123_HARVEY1_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_EXPONENT.csv')
        Path_DP4_123_HARVEY2_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_AMPLITUDE.csv')
        Path_DP4_123_HARVEY2_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_TIME.csv')
        Path_DP4_123_HARVEY2_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_EXPONENT.csv')
        Path_DP4_123_HARVEY3_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_AMPLITUDE.csv')
        Path_DP4_123_HARVEY3_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_TIME.csv')
        Path_DP4_123_HARVEY3_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_EXPONENT.csv')
        Path_DP4_123_WHITE_NOISE_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_WHITE_NOISE_FOURIER.csv')
        Path_DP4_123_HARVEY_METADATA = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY_METADATA.csv')
        Path_IDP_123_FLICKERING_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_FLICKERING_AMPLITUDE.csv')

        Truth_DP4_123_HARVEY1_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY1_AMPLITUDE)
        Truth_DP4_123_HARVEY1_TIME = os.path.isfile(Path_DP4_123_HARVEY1_TIME)
        Truth_DP4_123_HARVEY1_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY1_EXPONENT)
        Truth_DP4_123_HARVEY2_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY2_AMPLITUDE)
        Truth_DP4_123_HARVEY2_TIME = os.path.isfile(Path_DP4_123_HARVEY2_TIME)
        Truth_DP4_123_HARVEY2_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY2_EXPONENT)
        Truth_DP4_123_HARVEY3_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY3_AMPLITUDE)
        Truth_DP4_123_HARVEY3_TIME = os.path.isfile(Path_DP4_123_HARVEY3_TIME)
        Truth_DP4_123_HARVEY3_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY3_EXPONENT)
        Truth_DP4_123_WHITE_NOISE_FOURIER = os.path.isfile(Path_DP4_123_WHITE_NOISE_FOURIER)
        Truth_DP4_123_HARVEY_METADATA = os.path.isfile(Path_DP4_123_HARVEY_METADATA)
        Truth_IDP_123_FLICKERING_AMPLITUDE = os.path.isfile(Path_IDP_123_FLICKERING_AMPLITUDE)
        Truth_PDP_123_B_CONVECTION_PRIORS = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_123_B_CONVECTION_PRIORS.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_HARVEY1_AMPLITUDE", "DP4_123_HARVEY1_TIME", "DP4_123_HARVEY1_EXPONENT", "DP4_123_HARVEY2_AMPLITUDE", "DP4_123_HARVEY2_TIME",
                            "DP4_123_HARVEY2_EXPONENT", "DP4_123_HARVEY3_AMPLITUDE", "DP4_123_HARVEY3_TIME", "DP4_123_HARVEY3_EXPONENT", "DP4_123_WHITE_NOISE_FOURIER",
                            "DP4_123_HARVEY_METADATA", "IDP_123_FLICKERING_AMPLITUDE", "PDP_123_B_CONVECTION_PRIORS"],
                            [str(Truth_DP4_123_HARVEY1_AMPLITUDE), str(Truth_DP4_123_HARVEY1_TIME), str(Truth_DP4_123_HARVEY1_EXPONENT), str(Truth_DP4_123_HARVEY2_AMPLITUDE), str(Truth_DP4_123_HARVEY2_TIME),
                            str(Truth_DP4_123_HARVEY2_EXPONENT), str(Truth_DP4_123_HARVEY3_AMPLITUDE), str(Truth_DP4_123_HARVEY3_TIME), str(Truth_DP4_123_HARVEY3_EXPONENT), str(Truth_DP4_123_WHITE_NOISE_FOURIER),
                            str(Truth_DP4_123_HARVEY_METADATA), str(Truth_IDP_123_FLICKERING_AMPLITUDE), str(Truth_PDP_123_B_CONVECTION_PRIORS)]
                            ])
        file.write(tableObj.draw())
        file.write("\nThe following Intermediate Data Product is produced:\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))
        UserInputsData = pd.read_csv(os.path.join(HOME_DIR, 'userdirectory','UserInputs.csv'), delimiter=',')
        LoggGranulation = UserInputsData.loc[0, "LoggGranulation"]
        """if LoggGranulation is False then IDP_123_LOGG_VARLC.csv is not produced"""
        if bool(LoggGranulation) == False:
            Truth_IDP_123_LOGG_VARLC = "False"
        else:
            Path_IDP_123_LOGG_VARLC = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_LOGG_VARLC.csv')
            IDP_123_LOGG_VARLC_out = DP4PQRead.to_csv(Path_IDP_123_LOGG_VARLC, index = False)
            Truth_IDP_123_LOGG_VARLC = os.path.isfile(Path_IDP_123_LOGG_VARLC)
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_123_LOGG_VARLC"],
                            [str(Truth_IDP_123_LOGG_VARLC)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nGravity From Flicker/FliPer Completed\n")
        file.close()
    def SpotModelling05():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_SpotModelling05 = {'SpotModelling05':['True']}
        Data_SpotModelling05 = pd.DataFrame(Variable_SpotModelling05)
        Data_SpotModelling05.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP4', 'SpotModelling05.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"), "a")
        file.write("\n.\nMSAP4: SpotModelling was set to true.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The Inputs available are as below:\n")
        file.write("The different DP4 available are:\n")
        Path_DP4_123_HARVEY1_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_AMPLITUDE.csv')
        Path_DP4_123_HARVEY1_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_TIME.csv')
        Path_DP4_123_HARVEY1_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_EXPONENT.csv')
        Path_DP4_123_HARVEY2_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_AMPLITUDE.csv')
        Path_DP4_123_HARVEY2_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_TIME.csv')
        Path_DP4_123_HARVEY2_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_EXPONENT.csv')
        Path_DP4_123_HARVEY3_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_AMPLITUDE.csv')
        Path_DP4_123_HARVEY3_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_TIME.csv')
        Path_DP4_123_HARVEY3_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_EXPONENT.csv')
        Path_DP4_123_WHITE_NOISE_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'MSAP4', 'DP4_123_WHITE_NOISE_FOURIER.csv')
        Path_DP4_123_HARVEY_METADATA = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY_METADATA.csv')

        Truth_DP4_123_HARVEY1_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY1_AMPLITUDE)
        Truth_DP4_123_HARVEY1_TIME = os.path.isfile(Path_DP4_123_HARVEY1_TIME)
        Truth_DP4_123_HARVEY1_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY1_EXPONENT)
        Truth_DP4_123_HARVEY2_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY2_AMPLITUDE)
        Truth_DP4_123_HARVEY2_TIME = os.path.isfile(Path_DP4_123_HARVEY2_TIME)
        Truth_DP4_123_HARVEY2_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY2_EXPONENT)
        Truth_DP4_123_HARVEY3_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY3_AMPLITUDE)
        Truth_DP4_123_HARVEY3_TIME = os.path.isfile(Path_DP4_123_HARVEY3_TIME)
        Truth_DP4_123_HARVEY3_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY3_EXPONENT)
        Truth_DP4_123_WHITE_NOISE_FOURIER = os.path.isfile(Path_DP4_123_WHITE_NOISE_FOURIER)
        Truth_DP4_123_HARVEY_METADATA = os.path.isfile(Path_DP4_123_HARVEY_METADATA)
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_HARVEY1_AMPLITUDE", "DP4_123_HARVEY1_TIME", "DP4_123_HARVEY1_EXPONENT", "DP4_123_HARVEY2_AMPLITUDE", "DP4_123_HARVEY2_TIME",
                            "DP4_123_HARVEY2_EXPONENT", "DP4_123_HARVEY3_AMPLITUDE", "DP4_123_HARVEY3_TIME", "DP4_123_HARVEY3_EXPONENT", "DP4_123_WHITE_NOISE_FOURIER",
                            "DP4_123_HARVEY_METADATA"],
                            [str(Truth_DP4_123_HARVEY1_AMPLITUDE), str(Truth_DP4_123_HARVEY1_TIME), str(Truth_DP4_123_HARVEY1_EXPONENT), str(Truth_DP4_123_HARVEY2_AMPLITUDE), str(Truth_DP4_123_HARVEY2_TIME),
                            str(Truth_DP4_123_HARVEY2_EXPONENT), str(Truth_DP4_123_HARVEY3_AMPLITUDE), str(Truth_DP4_123_HARVEY3_TIME), str(Truth_DP4_123_HARVEY3_EXPONENT), str(Truth_DP4_123_WHITE_NOISE_FOURIER),
                            str(Truth_DP4_123_HARVEY_METADATA)]
                            ])
        file.write(tableObj.draw())
        file.write("\nThe different Intermediate Data Products available are:\n")

        Path_IDP_123_PROT_POWERSPECTRUM = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_POWERSPECTRUM.csv')
        Path_IDP_123_PROT_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_FOURIER.csv')
        Path_IDP_123_DIFF_ROT_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DIFF_ROT_FOURIER.csv')
        Path_IDP_123_PROT_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_TIMESERIES.csv')
        Path_IDP_123_DIFF_ROT_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DIFF_ROT_TIMESERIES.csv')
        Path_IDP_123_PCYCLE_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PCYCLE_TIMESERIES.csv')
        Path_IDP_123_FLICKERING_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_FLICKERING_AMPLITUDE.csv')
        Path_IDP_123_PROT_NOSPOT = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_NOSPOT.csv')
        Path_IDP_123_DELTA_PROT_NOSPOT = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DELTA_PROT_NOSPOT.csv')
        Path_IDP_123_LOGG_VARLC = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_LOGG_VARLC.csv')

        Truth_IDP_123_PROT_POWERSPECTRUM = os.path.isfile(Path_IDP_123_PROT_POWERSPECTRUM)
        Truth_IDP_123_PROT_FOURIER = os.path.isfile(Path_IDP_123_PROT_FOURIER)
        Truth_IDP_123_DIFF_ROT_FOURIER = os.path.isfile(Path_IDP_123_DIFF_ROT_FOURIER)
        Truth_IDP_123_PROT_TIMESERIES = os.path.isfile(Path_IDP_123_PROT_TIMESERIES)
        Truth_IDP_123_DIFF_ROT_TIMESERIES = os.path.isfile(Path_IDP_123_DIFF_ROT_TIMESERIES)
        Truth_IDP_123_PCYCLE_TIMESERIES = os.path.isfile(Path_IDP_123_PCYCLE_TIMESERIES)
        Truth_IDP_123_FLICKERING_AMPLITUDE = os.path.isfile(Path_IDP_123_FLICKERING_AMPLITUDE)
        Truth_IDP_123__PROT_NOSPOT = os.path.isfile(Path_IDP_123_PROT_NOSPOT)
        Truth_IDP_123_DELTA_PROT_NOSPOT = os.path.isfile(Path_IDP_123_DELTA_PROT_NOSPOT)
        Truth_IDP_123_LOGG_VARLC = os.path.isfile(Path_IDP_123_LOGG_VARLC)

        """Create texttable object"""
        tableObj3 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c","c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t","t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b","b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_123_PROT_POWERSPECTRUM", "IDP_123_PROT_FOURIER", "IDP_123_DIFF_ROT_FOURIER",
                            "IDP_123_PROT_TIMESERIES", "IDP_123_DIFF_ROT_TIMESERIES", "IDP_123_PCYCLE_TIMESERIES", "IDP_123_FLICKERING_AMPLITUDE",
                            "IDP_123_PROT_NOSPOT", "IDP_123_DELTA_PROT_NOSPOT", "IDP_123_LOGG_VARLC"],
                            [str(Truth_IDP_123_PROT_POWERSPECTRUM), str(Truth_IDP_123_PROT_FOURIER), str(Truth_IDP_123_DIFF_ROT_FOURIER),
                            str(Truth_IDP_123_PROT_TIMESERIES), str(Truth_IDP_123_DIFF_ROT_TIMESERIES), str(Truth_IDP_123_PCYCLE_TIMESERIES), str(Truth_IDP_123_FLICKERING_AMPLITUDE),
                            str(Truth_IDP_123__PROT_NOSPOT), str(Truth_IDP_123_DELTA_PROT_NOSPOT), str(Truth_IDP_123_LOGG_VARLC)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nThe new Data Products for spot modelling available are:\n")
        PathDP1 = os.path.join(InputsPath, "MandatoryInputs", "DP1", "DP1.csv")
        PathDP2 = os.path.join(InputsPath, "NonMandatoryInputs", "DP2", "DP2.csv")
        Path_DP4_123_PROT = os.path.join(InputsPath, "NonMandatoryInputs", "DP4PQ", "DP4_123_PROT.csv")
        Path_DP4_123_DELTAPROT = os.path.join(InputsPath, "NonMandatoryInputs", "DP4PQ", "DP4_123_DELTAPROT.csv")
        Path_DP5_125_MASS = os.path.join(InputsPath, "NonMandatoryInputs", "DP5PQ", "DP5_125_MASS.csv")
        Path_DP5_125_MASS_METADATA = os.path.join(InputsPath, "NonMandatoryInputs", "DP5PQ", "DP5_125_MASS_METADATA.csv")
        Path_DP5_125_RADIUS = os.path.join(InputsPath, "NonMandatoryInputs", "DP5PQ", "DP5_125_RADIUS.csv")
        Path_DP5_125_RADIUS_METADATA = os.path.join(InputsPath, "NonMandatoryInputs", "DP5PQ", "DP5_125_RADIUS_METADATA.csv")

        Truth_DP1 = os.path.isfile(PathDP1)
        Truth_DP2 = os.path.isfile(PathDP2)
        Truth_DP4_123_PROT = os.path.isfile(Path_DP4_123_PROT)
        Truth_DP4_123_DELTAPROT = os.path.isfile(Path_DP4_123_DELTAPROT)
        Truth_DP5_125_MASS = os.path.isfile(Path_DP5_125_MASS)
        Truth_DP5_125_MASS_METADATA = os.path.isfile(Path_DP5_125_MASS_METADATA)
        Truth_DP5_125_RADIUS = os.path.isfile(Path_DP5_125_RADIUS)
        Truth_DP5_125_RADIUS_MET = os.path.isfile(Path_DP5_125_RADIUS_METADATA)

        """Create texttable object"""
        tableObj3 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c","c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t","t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b","b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["DP1", "DP2 (Transit Model)", "DP4_123_PROT",
                            "DP4_123_DELTAPROT", "DP5_125_MASS", "DP5_125_MASS_METADATA", "DP5_125_RADIUS",
                            "DP5_125_RADIUS_MET"],
                            [str(Truth_DP1), str(Truth_DP2), str(Truth_DP4_123_PROT),
                            str(Truth_DP4_123_DELTAPROT), str(Truth_DP5_125_MASS), str(Truth_DP5_125_MASS_METADATA), str(Truth_DP5_125_RADIUS),
                            str(Truth_DP5_125_RADIUS_MET)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nThe new Intermediate Data Products for spot modelling available are:\n")

        Path_IDP_128_INCLINATION_ANGLE_PEAKBAGGING = os.path.join(OutputPath, "GeneratedOutputs", "MSAP4", "IDP_128_INCLINATION_ANGLE_PEAKBAGGING.csv")
        Path_IDP_128_OSC_SPLITTING = os.path.join(OutputPath, "GeneratedOutputs", "MSAP4", "IDP_128_OSC_SPLITTING.csv")
        Path_IDP_122_VSINI_INCLINATION = os.path.join(OutputPath, "GeneratedOutputs", "MSAP4", "IDP_122_VSINI_INCLINATION.csv")

        Truth_IDP_128_INCLINATION_ANGLE_PEAKBAGGING = os.path.isfile(Path_IDP_128_INCLINATION_ANGLE_PEAKBAGGING)
        Truth_IDP_128_OSC_SPLITTING = os.path.isfile(Path_IDP_128_OSC_SPLITTING)
        Truth_IDP_122_VSINI_INCLINATION = os.path.isfile(Path_IDP_122_VSINI_INCLINATION)
        Truth_IDP_123_VARLC = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_VARLC.csv'))
        Truth_IDP_123_BINNED_VARLC = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_BINNED_VARLC.csv'))
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c","c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t","t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b","b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_128_INCLINATION_ANGLE_PEAKBAGGING", "IDP_128_OSC_SPLITTING", "IDP_122_VSINI_INCLINATION",
                            "IDP_123_VARLC", "IDP_123_BINNED_VARLC"],
                            [str(Truth_IDP_128_INCLINATION_ANGLE_PEAKBAGGING), str(Truth_IDP_128_OSC_SPLITTING), str(Truth_IDP_122_VSINI_INCLINATION),
                            str(Truth_IDP_123_VARLC), str(Truth_IDP_123_BINNED_VARLC)]
                            ])
        file.write(tableObj4.draw())
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        file = open(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"), "a")
        file.write("\n.\nThe following Intermediate Data Products are produced:\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))

        Path_IDP_123_SPOT_PROT = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_SPOT_PROT.csv')
        IDP_123_SPOT_PROT_out = DP4PQRead.to_csv(Path_IDP_123_SPOT_PROT, index = False)
        Path_IDP_123_SPOT_AREA = os.path.join(FolderPathForReport,  'MSAP4','IDP_123_SPOT_AREA.csv')
        IDP_123_SPOT_AREA_out = DP4PQRead.to_csv(Path_IDP_123_SPOT_AREA, index = False)

        Truth_IDP_123_SPOT_PROT = os.path.isfile(Path_IDP_123_SPOT_PROT)
        Truth_IDP_123_SPOT_AREA = os.path.isfile(Path_IDP_123_SPOT_AREA)
        """Create texttable object"""
        tableObj4 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_123_SPOT_PROT", "IDP_123_SPOT_AREA"],
                            [str(Truth_IDP_123_SPOT_PROT), str(Truth_IDP_123_SPOT_AREA)]
                            ])
        file.write(tableObj4.draw())
        file.write("\nThe following Additional Data Product is produced:\n")
        Path_ADP_123_SPOT_MODEL = os.path.join(FolderPathForReport, 'MSAP4', 'ADP_123_SPOT_MODEL.csv')
        ADP_123_SPOT_MODEL_out = DP4PQRead.to_csv(Path_ADP_123_SPOT_MODEL, index = False)
        Truth_ADP_123_SPOT_MODEL = os.path.isfile(Path_ADP_123_SPOT_MODEL)
        """Create texttable object"""
        tableObj5 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj5.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj5.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj5.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj5.add_rows([["ADP_123_SPOT_MODEL"],
                            [str(Truth_ADP_123_SPOT_MODEL)]
                            ])
        file.write(tableObj5.draw())
        file.write("\nSpot Modelling Completed\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        # Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4"), os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))

    def SpotModelling05_False_MSAP4():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_SpotModelling05_MSAP4 = {'SpotModelling05_MSAP4':['False']}
        Data_SpotModelling05_MSAP4 = pd.DataFrame(Variable_SpotModelling05_MSAP4)
        Data_SpotModelling05_MSAP4.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP4', 'SpotModelling05_MSAP4.csv'), header=True, index=False)
        file = open(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), "a")
        file.write("\n.\nMSAP4: SpotModelling was set to False.\n")
        file.write("Spot Modelling Completed\n")
        file.close()

    def FixRotationPeriod07_MSAP4():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_FixRotationPeriod07_MSAP4 = {'FixRotationPeriod07_MSAP4':['True']}
        Data_FixRotationPeriod07_MSAP4 = pd.DataFrame(Variable_FixRotationPeriod07_MSAP4)
        Data_FixRotationPeriod07_MSAP4.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP4', 'FixRotationPeriod07_MSAP4.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), "a")
        file.write("\n.\nMSAP4: Fix Rotation Period.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The following Intermediate Data Products are fed as Inputs.\n")
        Path_IDP_123_PROT_NOSPOT = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PROT_NOSPOT.csv')
        Path_IDP_123_DELTA_PROT_NOSPOT = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_DELTA_PROT_NOSPOT.csv')
        Truth_IDP_123__PROT_NOSPOT = os.path.isfile(Path_IDP_123_PROT_NOSPOT)
        Truth_IDP_123_DELTA_PROT_NOSPOT = os.path.isfile(Path_IDP_123_DELTA_PROT_NOSPOT)
        Path_IDP_123_SPOT_PROT = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_SPOT_PROT.csv')
        Truth_IDP_123_SPOT_PROT = os.path.isfile(Path_IDP_123_SPOT_PROT)
        """Create texttable object"""
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_123_PROT_NOSPOT", "IDP_123_DELTA_PROT_NOSPOT", "IDP_123_SPOT_PROT"],
                            [str(Truth_IDP_123__PROT_NOSPOT), str(Truth_IDP_123_DELTA_PROT_NOSPOT), str(Truth_IDP_123_SPOT_PROT)]
                            ])
        file.write(tableObj.draw())
        file.write("\nThe following Outputs are produced.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))

        UserInputsData = pd.read_csv(os.path.join(HOME_DIR, 'userdirectory','UserInputs.csv'), delimiter=',')
        RotationPeriod = UserInputsData.loc[0, "RotationPeriod"]
        """if RotationPeriod is False then DP4 outputs are not produced"""
        if bool(RotationPeriod) == False:
            Truth_DP4_123_PROT = "False"
            Truth_DP4_123_PROT_METADATA = "False"
            Truth_DP4_123_DELTAPROT = "False"
            Truth_DP4_123_DELTAPROT_METADATA = "False"
        else:
            Path_DP4_123_PROT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_PROT.csv')
            DP4_123_PROT_out = DP4PQRead.to_csv(Path_DP4_123_PROT, index = False)
            Path_DP4_123_PROT_METADATA = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_PROT_METADATA.csv')
            DP4_123_PROT_METADATA_out = DP4PQRead.to_csv(Path_DP4_123_PROT_METADATA, index = False)
            Path_DP4_123_DELTAPROT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_DELTAPROT.csv')
            DP4_123_DELTAPROT_out = DP4PQRead.to_csv(Path_DP4_123_DELTAPROT, index = False)
            Path_DP4_123_DELTAPROT_METADATA = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_DELTAPROT_METADATA.csv')
            DP4_123_DELTAPROT_METADATA_out = DP4PQRead.to_csv(Path_DP4_123_DELTAPROT_METADATA, index = False)

            Truth_DP4_123_PROT = os.path.isfile(Path_DP4_123_PROT)
            Truth_DP4_123_PROT_METADATA = os.path.isfile(Path_DP4_123_PROT_METADATA)
            Truth_DP4_123_DELTAPROT = os.path.isfile(Path_DP4_123_DELTAPROT)
            Truth_DP4_123_DELTAPROT_METADATA = os.path.isfile(Path_DP4_123_DELTAPROT_METADATA)

        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP4_123_PROT", "DP4_123_PROT_METADATA", "DP4_123_DELTAPROT", "DP4_123_DELTAPROT_METADATA"],
                            [str(Truth_DP4_123_PROT), str(Truth_DP4_123_PROT_METADATA), str(Truth_DP4_123_DELTAPROT), str(Truth_DP4_123_DELTAPROT_METADATA)]
                            ])
        file.write(tableObj2.draw())
        Main_Report_file = open(os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"), "a")
        Main_Report_file.write("\nThe following DP4 products were produced by Fix Rotation Period 07 operation.\n")
        Main_Report_file.write(tableObj2.draw())
        file.write("\nFix Rotation Period Completed\n")
        file.close()
        Main_Report_file.close()
    def ActivityCycleDetermination06_MSAP4():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_ActivityCycleDetermination06_MSAP4 = {'ActivityCycleDetermination06_MSAP4':['True']}
        Data_ActivityCycleDetermination06_MSAP4 = pd.DataFrame(Variable_ActivityCycleDetermination06_MSAP4)
        Data_ActivityCycleDetermination06_MSAP4.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP4', 'ActivityCycleDetermination06_MSAP4.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"), "a")
        file.write("\n.\nMSAP4: Activity Cycle Determination Initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")

        Path_DP4_123_HARVEY1_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_AMPLITUDE.csv')
        Path_DP4_123_HARVEY1_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_TIME.csv')
        Path_DP4_123_HARVEY1_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY1_EXPONENT.csv')
        Path_DP4_123_HARVEY2_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_AMPLITUDE.csv')
        Path_DP4_123_HARVEY2_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_TIME.csv')
        Path_DP4_123_HARVEY2_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY2_EXPONENT.csv')
        Path_DP4_123_HARVEY3_AMPLITUDE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_AMPLITUDE.csv')
        Path_DP4_123_HARVEY3_TIME = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_TIME.csv')
        Path_DP4_123_HARVEY3_EXPONENT = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY3_EXPONENT.csv')
        Path_DP4_123_WHITE_NOISE_FOURIER = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_WHITE_NOISE_FOURIER.csv')
        Path_DP4_123_HARVEY_METADATA = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_HARVEY_METADATA.csv')
        Path_IDP_123_PCYCLE_TIMESERIES = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_PCYCLE_TIMESERIES.csv')
        Path_IDP_123_SPOT_AREA = os.path.join(FolderPathForReport, 'MSAP4', 'IDP_123_SPOT_AREA.csv')

        Truth_DP4_123_HARVEY1_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY1_AMPLITUDE)
        Truth_DP4_123_HARVEY1_TIME = os.path.isfile(Path_DP4_123_HARVEY1_TIME)
        Truth_DP4_123_HARVEY1_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY1_EXPONENT)
        Truth_DP4_123_HARVEY2_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY2_AMPLITUDE)
        Truth_DP4_123_HARVEY2_TIME = os.path.isfile(Path_DP4_123_HARVEY2_TIME)
        Truth_DP4_123_HARVEY2_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY2_EXPONENT)
        Truth_DP4_123_HARVEY3_AMPLITUDE = os.path.isfile(Path_DP4_123_HARVEY3_AMPLITUDE)
        Truth_DP4_123_HARVEY3_TIME = os.path.isfile(Path_DP4_123_HARVEY3_TIME)
        Truth_DP4_123_HARVEY3_EXPONENT = os.path.isfile(Path_DP4_123_HARVEY3_EXPONENT)
        Truth_DP4_123_WHITE_NOISE_FOURIER = os.path.isfile(Path_DP4_123_WHITE_NOISE_FOURIER)
        Truth_DP4_123_HARVEY_METADATA = os.path.isfile(Path_DP4_123_HARVEY_METADATA)
        Truth_DP4PQ = os.path.isfile(FilePaths.loc[0, 'DP4_Path'])
        Truth_IDP_123_PCYCLE_TIMESERIES = os.path.isfile(Path_IDP_123_PCYCLE_TIMESERIES)
        Truth_IDP_123_SPOT_AREA = os.path.isfile(Path_IDP_123_SPOT_AREA)
        Truth_PDP_123_B_ACTIVITY_PRIORS = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_123_B_ACTIVITY_PRIORS.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c",])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t","t", "t", "t", "t", "t", "t", "t","t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b","b", "b", "b", "b", "b", "b", "b","b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["DP4_123_HARVEY1_AMPLITUDE", "DP4_123_HARVEY1_TIME", "DP4_123_HARVEY1_EXPONENT", "DP4_123_HARVEY2_AMPLITUDE", "DP4_123_HARVEY2_TIME",
                            "DP4_123_HARVEY2_EXPONENT", "DP4_123_HARVEY3_AMPLITUDE", "DP4_123_HARVEY3_TIME", "DP4_123_HARVEY3_EXPONENT", "DP4_123_WHITE_NOISE_FOURIER",
                            "DP4_123_HARVEY_METADATA", "DP4 Previous Quarter", "IDP_123_PCYCLE_TIMESERIES", "IDP_123_SPOT_AREA", "PDP_123_B_ACTIVITY_PRIORS"],
                            [str(Truth_DP4_123_HARVEY1_AMPLITUDE), str(Truth_DP4_123_HARVEY1_TIME), str(Truth_DP4_123_HARVEY1_EXPONENT), str(Truth_DP4_123_HARVEY2_AMPLITUDE), str(Truth_DP4_123_HARVEY2_TIME),
                            str(Truth_DP4_123_HARVEY2_EXPONENT), str(Truth_DP4_123_HARVEY3_AMPLITUDE), str(Truth_DP4_123_HARVEY3_TIME), str(Truth_DP4_123_HARVEY3_EXPONENT), str(Truth_DP4_123_WHITE_NOISE_FOURIER),
                            str(Truth_DP4_123_HARVEY_METADATA), str(Truth_DP4PQ), str(Truth_IDP_123_PCYCLE_TIMESERIES), str(Truth_IDP_123_SPOT_AREA), str(Truth_PDP_123_B_ACTIVITY_PRIORS)]
                            ])
        file.write(tableObj3.draw())
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))

        file = open(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"), "a")
        file.write("\n.\nThe following Data Products are produced as Outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputsPath,'Corrected_Light_Curves.csv'))
        UserInputsData = pd.read_csv(os.path.join(HOME_DIR, 'userdirectory','UserInputs.csv'), delimiter=',')
        ActivityLevel = UserInputsData.loc[0, "ActivityLevel"]
        """if ActivityLevel is False then DP4 outputs are not produced"""
        if bool(ActivityLevel) == False:
            Truth_DP4_123_PCYCLE = "False"
            Truth_DP4_123_PCYCLE_METADATA = "False"
        else:
            Path_DP4_123_PCYCLE = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_PCYCLE.csv')
            DP4_123_PCYCLE_out = DP4PQRead.to_csv(Path_DP4_123_PCYCLE, index = False)
            Path_DP4_123_PCYCLE_METADATA = os.path.join(FolderPathForReport, 'MSAP4', 'DP4_123_PCYCLE_METADATA.csv')
            DP4_123_PCYCLE_METADATA_out = DP4PQRead.to_csv(Path_DP4_123_PCYCLE_METADATA, index = False)

            Truth_DP4_123_PCYCLE = os.path.isfile(Path_DP4_123_PCYCLE)
            Truth_DP4_123_PCYCLE_METADATA = os.path.isfile(Path_DP4_123_PCYCLE_METADATA)

        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP4_123_PCYCLE", "DP4_123_PCYCLE_METADATA"],
                            [str(Truth_DP4_123_PCYCLE), str(Truth_DP4_123_PCYCLE_METADATA)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nActivity cycle determination completed\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        # Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Main_Report_MSAP4"), os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP4_temp.txt"))

    def Stop_MSAP4():
        """For each function that was described above, we check if that function was run or not by reading the respective .csv files,
        we later delete these files"""
        Process_01 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP4', 'FourierAnalysis01_MSAP4.csv'))
        Process_02 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP4', 'TimeSeriesAnalysis02_MSAP4.csv'))
        Process_03 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP4', 'RotationPeriodDetermination03_MSAP4.csv'))
        Process_04 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP4', 'GravityFromFlicker_FliPer04_MSAP4.csv'))
        Process_05 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP4', 'SpotModelling05.csv'))
        Process_06 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP4', 'ActivityCycleDetermination06_MSAP4.csv'))
        Process_07 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP4', 'FixRotationPeriod07_MSAP4.csv'))

        """Writing the results in the report specific to MSAP4"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP4.txt"), "a")
        file.write("\n.\nMSAP4 Completed:\n")
        file.write("***--- End of Report ---***\n")
        file.close()
        """Writing the operations summary on SAS overall workflow child report"""
        Main_Report_file = open(os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"), "a")
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b", "b", "b","b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["FourierAnalysis01", "TimeSeriesAnalysis02", "RotationPeriodDetermination03",
                            "GravityFromFlicker_FliPer04", "SpotModelling05", "ActivityCycleDetermination06",
                            "FixRotationPeriod07"],
                            [str(Process_01), str(Process_02), str(Process_03),
                            str(Process_04), str(Process_05), str(Process_06),
                            str(Process_07)]
                            ])
        """Write the Table in the Report"""
        Main_Report_file.write("\n")
        Main_Report_file.write("\nThe summary of all the processes in MSAP4 is:\n")
        Main_Report_file.write(tableObj.draw())
        Main_Report_file.write("\nMSAP4 completed.\n")
        Main_Report_file.write("\n---**---MSAP4-END---**---\n")
        Main_Report_file.close()
        """Copy the information from child report to parent overall SAS workflow report and delete the child report"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"))

if __name__ == '__Start_MSAP4__':
    Start_MSAP4()
if __name__ == '__IntermediateDataProductsCheck_MSAP4__':
    IntermediateDataProductsCheck_MSAP4()
if __name__ == '__DP4PQCheck_MSAP4__':
    DP4PQCheck_MSAP4()
if __name__ == '__PDP_Check_MSAP4__':
    PDP_Check_MSAP4()
if __name__ == '__FourierAnalysis01_MSAP4__':
    FourierAnalysis01_MSAP4()
if __name__ == '__TimeSeriesAnalysis02_MSAP4__':
    TimeSeriesAnalysis02_MSAP4()
if __name__ == '__RotationPeriodDetermination03_MSAP4__':
    RotationPeriodDetermination03_MSAP4()
if __name__ == '__GravityFromFlicker_FliPer04_MSAP4__':
    GravityFromFlicker_FliPer04_MSAP4()
if __name__ == '__SpotModelling05__':
    SpotModelling05()
if __name__ == '__SpotModelling05_False_MSAP4__':
    SpotModelling05_False_MSAP4()
if __name__ == '__FixRotationPeriod07_MSAP4__':
    FixRotationPeriod07_MSAP4()
if __name__ == '__ActivityCycleDetermination06_MSAP4__':
    ActivityCycleDetermination06_MSAP4()
if __name__ == '__Stop_MSAP4__':
    Stop_MSAP4()
