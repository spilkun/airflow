"""import libraries"""
import os
from pathlib import Path
from datetime import datetime
import pandas as pd
from sys import stdout
import Operations
"""To define the Tables"""
import texttable
HOME_DIR = str(Path.home())
"""Defining the path where Outputs will be Stored"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
FilePaths = pd.read_csv(os.path.join(UserDirectoryPath, 'FilePaths.csv'), delimiter = ',')
"""Defining the paths where files need to be searched"""
PathToDir = FilePaths.loc[0,'GeneratedOutputs_Path'] #'airflow',
"""Writing Report_ File name to be stored based on time"""
now = datetime.now()
dt_string = now.strftime("%B_%d_%Y_%H_%M")
"""We will read the contents of the file created above"""
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
def start():
    """Writing Report_ File name to be stored based on time"""
    now = datetime.now()
    dt_string = now.strftime("%B_%d_%Y_%H_%M")
    """This code writes the data in the report file and notes down the folder name too"""
    F = open(os.path.join(PathToDir, "ReportID.txt"), "w")
    F.write("Report" + dt_string + ".txt")
    F.close()
    F2 = open(os.path.join(PathToDir, "FolderName.txt"), "w")
    F2.write(dt_string)
    F2.close()
    """This codes reads the name of the report file and the folder name"""
    F = open(os.path.join(PathToDir, "ReportID.txt"), "r")
    ReportName = F.readline()
    F.close()
    F2 = open(os.path.join(PathToDir, "FolderName.txt"), "r")
    FolderName = F2.readline()
    F2.close()
    FolderPathForReport = os.path.join(PathToDir, FolderName)
    """Creating the respective Folders where the generated outputs will be stored"""
    os.mkdir(FolderPathForReport)
    os.mkdir(os.path.join(FolderPathForReport, 'MSAP1'))
    os.mkdir(os.path.join(FolderPathForReport, 'MSAP2'))
    os.mkdir(os.path.join(FolderPathForReport, 'MSAP3'))
    os.mkdir(os.path.join(FolderPathForReport, 'MSAP4'))
    os.mkdir(os.path.join(FolderPathForReport, 'MSAP5'))
    os.mkdir(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1'))
    os.mkdir(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2'))
    os.mkdir(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3'))
    """Creating a file containing all the paths"""
    """Defining important paths"""
    UserDirectory_Path = os.path.join(HOME_DIR, 'userdirectory')
    InputsPath = os.path.join(UserDirectory_Path, 'inputs')
    FilePaths = os.path.join(HOME_DIR, 'userdirectory', 'FilePaths.csv')
    GeneratedOutputs_Path = os.path.join(UserDirectory_Path, 'output', 'GeneratedOutputs')
    FilePathsData = pd.DataFrame({"ReportName":[ReportName], "FolderPathForReport":[FolderPathForReport],
                    "UserDirectory_Path":[UserDirectory_Path], "InputsPath" : [os.path.join(UserDirectory_Path, 'inputs')],
                    "OutputPath" : [os.path.join(UserDirectory_Path, 'output')], "MandatoryInputsPath" : [os.path.join(InputsPath, 'MandatoryInputs')],
                    "NonMandatoryInputsPath" : [os.path.join(InputsPath, 'NonMandatoryInputs')], "GeneratedOutputs_Path":[os.path.join(UserDirectory_Path, 'output', 'GeneratedOutputs')],
                    "MSAP2_Input_Path": [os.path.join(GeneratedOutputs_Path, 'MSAP2')], "MSAP3_Input_Path": [os.path.join(GeneratedOutputs_Path, 'MSAP3')],
                    "MSAP4_Input_Path": [os.path.join(GeneratedOutputs_Path, 'MSAP4')],
                    "MSAP1_Output_Path":[os.path.join(FolderPathForReport, 'MSAP1')], "MSAP2_Output_Path":[os.path.join(FolderPathForReport, 'MSAP2')],
                    "MSAP3_Output_Path":[os.path.join(FolderPathForReport, 'MSAP3')], "MSAP4_Output_Path":[os.path.join(FolderPathForReport, 'MSAP4')],
                    "MSAP5_Output_Path":[os.path.join(FolderPathForReport, 'MSAP5')], "MSAP5_Part1_Output_Path":[os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1')],
                    "MSAP5_Part2_Output_Path":[os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2')], "MSAP5_Part3_Output_Path":[os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3')],
                    "PreparatoryData_Path":[os.path.join(InputsPath, 'MandatoryInputs', 'PreparatoryData')], "DP1_Path": [os.path.join(InputsPath, 'MandatoryInputs', 'DP1')],
                    "DP2_Path":[os.path.join(InputsPath, 'NonMandatoryInputs', 'DP2')], "DP3_Path": [os.path.join(InputsPath, 'NonMandatoryInputs', 'DP3')],
                    "DP4_Path":[os.path.join(InputsPath, 'NonMandatoryInputs', 'DP4PQ')], "DP5_Path":[os.path.join(InputsPath, 'NonMandatoryInputs', 'DP5PQ')],
                    "StellarModelsRelativeFrequencies_Path":[os.path.join(InputsPath, 'MandatoryInputs', 'StellarModelsandRelativeFrequencies')]
                    })
    FilePathsData.to_csv(path_or_buf = FilePaths, header=True, index=False)
    """Reading the Paths from the FilePaths.csv file"""
    UserInputFilePath = os.path.join(FilePathsData.loc[0, 'UserDirectory_Path'], 'UserInputs.csv')
    UserInputData = pd.read_csv(UserInputFilePath, index_col=None)

    QuarterNumber = UserInputData.loc[0, 'QuarterNumber']
    RotationPeriod = UserInputData.loc[0, 'RotationPeriod']
    ActivityLevel = UserInputData.loc[0, 'ActivityLevel']
    LoggGranulation = UserInputData.loc[0, 'LoggGranulation']
    OscillationDetectionFlag = UserInputData.loc[0, 'Oscillations']
    IDP_128_PEAK_BAGGING_FLAG = UserInputData.loc[0, 'Peak_Bagging_Flag']
    SpotModelling = UserInputData.loc[0, "SpotModelling_Flag"]
    IDP_128_QUALITY_METADATA_Flag = UserInputData.loc[0, "Peak_Bagging_Flag"]
    Mixed_Modes_Flag = UserInputData.loc[0, "Mixed_Modes_Flag"]
    """Create texttable object"""
    tableObj = texttable.Texttable(max_width=190)
    """Set column alignment (center alignment)"""
    tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
    """Set datatype of each column (text type)"""
    tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
    """Adjust columns"""
    tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
    """Insert rows in the table"""
    tableObj.add_rows([["QuarterNumber", "Rotation_Period", "Activity_Level", "Logg_Granulation",
                        "Oscillations", "IDP_128_PEAK_BAGGING_FLAG", "SPOTMODELLING_FLAG",
                        "IDP_128_QUALITY_METADATA", "Mixed_Modes"],
                        [str(QuarterNumber), str(RotationPeriod), str(ActivityLevel), str(LoggGranulation),
                        str(OscillationDetectionFlag), str(IDP_128_PEAK_BAGGING_FLAG), str(SpotModelling),
                        str(IDP_128_QUALITY_METADATA_Flag), str(Mixed_Modes_Flag)]
                        ])
    if "Sofesticated_Method_P_Modes" in UserInputData.columns:
        Sofesticated_Method_P_Modes_Flag = UserInputData.loc[0, "Sofesticated_Method_P_Modes"]
        Interpolation_P_Modes_Flag = UserInputData.loc[0, "Interpolation_P_Modes"]
        Inversion_Flag = UserInputData.loc[0, "InversionFlag"]
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["QuarterNumber", "Rotation_Period", "Activity_Level", "Logg_Granulation",
                            "Oscillations", "IDP_128_PEAK_BAGGING_FLAG", "SPOTMODELLING_FLAG",
                            "IDP_128_QUALITY_METADATA", "Mixed_Modes", "Sofesticated_Method_Mixed_Modes", "Interpolation_Mixed_Modes",
                            "Inversion"],
                            [str(QuarterNumber), str(RotationPeriod), str(ActivityLevel), str(LoggGranulation),
                            str(OscillationDetectionFlag), str(IDP_128_PEAK_BAGGING_FLAG), str(SpotModelling),
                            str(IDP_128_QUALITY_METADATA_Flag), str(Mixed_Modes_Flag), str(Sofesticated_Method_P_Modes_Flag), str(Interpolation_P_Modes_Flag),
                            str(Inversion_Flag)]
                            ])
    if "Sofesticated_Method_Mixed_Modes" in UserInputData.columns:
        Sofesticated_Method_Mixed_Modes_Flag = UserInputData.loc[0, "Sofesticated_Method_Mixed_Modes"]
        Interpolation_Mixed_Modes_Flag = UserInputData.loc[0, "Interpolation_Mixed_Modes"]

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["QuarterNumber", "Rotation_Period", "Activity_Level", "Logg_Granulation",
                            "Oscillations", "IDP_128_PEAK_BAGGING_FLAG", "SPOTMODELLING_FLAG",
                            "IDP_128_QUALITY_METADATA", "Mixed_Modes", "Sofesticated_Method_Mixed_Modes", "Interpolation_Mixed_Modes",
                            "Inversion", "Sofesticated_Method_Mixed_Modes", "Interpolation_Mixed_Modes"],
                            [str(QuarterNumber), str(RotationPeriod), str(ActivityLevel), str(LoggGranulation),
                            str(OscillationDetectionFlag), str(IDP_128_PEAK_BAGGING_FLAG), str(SpotModelling),
                            str(IDP_128_QUALITY_METADATA_Flag), str(Mixed_Modes_Flag), str(Sofesticated_Method_P_Modes_Flag), str(Interpolation_P_Modes_Flag),
                            str(Inversion_Flag), str(Sofesticated_Method_Mixed_Modes_Flag), str(Interpolation_Mixed_Modes_Flag)]
                            ])
    file = open(os.path.join(FolderPathForReport, ReportName), "a")
    file.write("\n\n")
    file.write("DAG has been initialized.\n")
    file.write("The user selected parameters are:\n \n")
    file.write(tableObj.draw())
    file.write("\n\n The different operators will now be executed.\n")
    file.close()

"""The Function below is executed if MSAP3 and MSAP4 are Completed"""
def MSAP3_MSAP4_Completed():
    """Reading the Paths from the FilePaths.csv file"""
    FolderPathForReport = FilePaths.loc[0, 'FolderPathForReport']
    ReportName = FilePaths.loc[0, 'ReportName']
    file = open(os.path.join(FolderPathForReport, ReportName), "a")
    file.write("\n\n MSAP3 and MSAP4 Successfully completed.\n")
    file.close()

"""The Function below is executed if OscillationDetection or LoggGranulation is set to False"""
def IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2():
    """Reading the Paths from the FilePaths.csv file"""
    FolderPathForReport = FilePaths.loc[0, 'FolderPathForReport']
    ReportName = FilePaths.loc[0, 'ReportName']
    ClassicStellarParametersPath = os.path.join(FolderPathForReport, 'ClassicStellarParameters.csv')
    ClassicStellarParametersTruth = os.path.isfile(ClassicStellarParametersPath)
    file = open(os.path.join(FolderPathForReport, ReportName), "a")
    file.write("\n\nMSAP2.\n")
    file.write("OscillationDetection or LoggGranulation is set to False.\n")
    file.write("Classic Stellar Parameters can not be created.\n")
    """Create texttable object"""
    tableObj = texttable.Texttable()
    """Set column alignment (center alignment)"""
    tableObj.set_cols_align(["c"])
    """Set datatype of each column (text type)"""
    tableObj.set_cols_dtype(["t"])
    """Adjust columns"""
    tableObj.set_cols_valign(["b"])
    """Insert rows in the table"""
    tableObj.add_rows([["ClassicStellarParameters.csv"],
                        [str(ClassicStellarParametersTruth)]
                        ])
    file.write(tableObj.draw())
    file.write("\nMSAP2 Completed.\n")
    file.close()

"""The Function deletes the Report ID so that the latest file can be read during next DAG run"""
def Stop():
    """Reading the Paths from the FilePaths.csv file"""
    FolderPathForReport = FilePaths.loc[0, 'FolderPathForReport']
    ReportName = FilePaths.loc[0, 'ReportName']
    UserDirectoryPath = FilePaths.loc[0, 'UserDirectory_Path']
    """Defining Paths for various Data Product Outputs"""
    MSAP3_Output_Path = FilePaths.loc[0, 'MSAP3_Output_Path']
    MSAP4_Output_Path = FilePaths.loc[0, 'MSAP4_Output_Path']
    MSAP5_Part3_Output_Path = FilePaths.loc[0, 'MSAP5_Part3_Output_Path']
    """Checking if the Data Products generated exist"""
    """Checking for outputs from MSAP3"""
    DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(MSAP3_Output_Path, "DP3_128_DELTA_NU_AV.csv"))
    DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(MSAP3_Output_Path, "DP3_128_DELTA_NU_AV_METADATA.csv"))
    DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(MSAP3_Output_Path, "DP3_128_NU_MAX.csv"))
    DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(MSAP3_Output_Path, "DP3_128_NU_MAX_METADATA.csv"))
    DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(MSAP3_Output_Path, "DP3_128_OSC_FREQ.csv"))
    DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(MSAP3_Output_Path, "DP3_128_OSC_FREQ_COVMAT.csv"))
    DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(MSAP3_Output_Path, "DP3_128_OSC_FREQ_METADATA.csv"))
    """Create texttable object"""
    tableObj = texttable.Texttable(max_width=160)
    """Set column alignment (center alignment)"""
    tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
    """Set datatype of each column (text type)"""
    tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
    """Adjust columns"""
    tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
    """Insert rows in the table"""
    tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX",
                        "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT",
                        "DP3_128_OSC_FREQ_METADATA"],
                        [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth),
                        str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth),
                        str(DP3_128_OSC_FREQ_METADATA_Truth)]
                        ])
    """Checking for outputs from MSAP4"""
    DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY1_AMPLITUDE.csv"))
    DP4_123_HARVEY1_TIME_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY1_TIME.csv"))
    DP4_123_HARVEY1_EXPONENT_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY1_EXPONENT.csv"))
    DP4_123_HARVEY2_AMPLITUDE_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY2_AMPLITUDE.csv"))
    DP4_123_HARVEY2_TIME_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY2_TIME.csv"))
    DP4_123_HARVEY2_EXPONENT_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY2_EXPONENT.csv"))
    DP4_123_HARVEY3_AMPLITUDE_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY3_AMPLITUDE.csv"))
    DP4_123_HARVEY3_TIME_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY3_TIME.csv"))
    DP4_123_HARVEY3_EXPONENT_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY3_EXPONENT.csv"))
    DP4_123_WHITE_NOISE_FOURIER_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_WHITE_NOISE_FOURIER.csv"))
    DP4_123_HARVEY_METADATA_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_HARVEY_METADATA.csv"))

    """Create texttable object"""
    tableObj2 = texttable.Texttable(max_width=190)
    """Set column alignment (center alignment)"""
    tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
    """Set datatype of each column (text type)"""
    tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
    """Adjust columns"""
    tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
    """Insert rows in the table"""
    tableObj2.add_rows([["DP4_123_HARVEY1_AMPLITUDE", "DP4_123_HARVEY1_TIME", "DP4_123_HARVEY1_EXPONENT",
                        "DP4_123_HARVEY2_AMPLITUDE", "DP4_123_HARVEY2_TIME", "DP4_123_HARVEY2_EXPONENT",
                        "DP4_123_HARVEY3_AMPLITUDE", "DP4_123_HARVEY3_TIME", "DP4_123_HARVEY3_EXPONENT",
                        "DP4_123_WHITE_NOISE_FOURIER", "DP4_123_HARVEY_METADATA"],
                        [str(DP4_123_HARVEY1_AMPLITUDE_Truth), str(DP4_123_HARVEY1_TIME_Truth), str(DP4_123_HARVEY1_EXPONENT_Truth),
                        str(DP4_123_HARVEY2_AMPLITUDE_Truth), str(DP4_123_HARVEY2_TIME_Truth), str(DP4_123_HARVEY2_EXPONENT_Truth),
                        str(DP4_123_HARVEY3_AMPLITUDE_Truth), str(DP4_123_HARVEY3_TIME_Truth), str(DP4_123_HARVEY3_EXPONENT_Truth),
                        str(DP4_123_WHITE_NOISE_FOURIER_Truth), str(DP4_123_HARVEY_METADATA_Truth)]
                        ])
    """Checking for outputs from MSAP4"""
    DP4_123_PROT_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_PROT.csv"))
    DP4_123_PROT_METADATA_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_PROT_METADATA.csv"))
    DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_DELTAPROT.csv"))
    DP4_123_DELTAPROT_METADATA_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_DELTAPROT_METADATA.csv"))
    DP4_123_PCYCLE_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_PCYCLE.csv"))
    DP4_123_PCYCLE_METADATA_Truth = os.path.isfile(os.path.join(MSAP4_Output_Path, "DP4_123_PCYCLE_METADATA.csv"))

    """Create texttable object"""
    tableObj3 = texttable.Texttable(max_width=160)
    """Set column alignment (center alignment)"""
    tableObj3.set_cols_align(["c", "c", "c", "c", "c", "c"])
    """Set datatype of each column (text type)"""
    tableObj3.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
    """Adjust columns"""
    tableObj3.set_cols_valign(["b", "b", "b", "b", "b", "b"])
    """Insert rows in the table"""
    tableObj3.add_rows([["DP4_123_PROT", "DP4_123_PROT_METADATA", "DP4_123_DELTAPROT",
                        "DP4_123_DELTAPROT_METADATA", "DP4_123_PCYCLE", "DP4_123_PCYCLE_METADATA"],
                        [str(DP4_123_PROT_Truth), str(DP4_123_PROT_METADATA_Truth), str(DP4_123_DELTAPROT_Truth),
                        str(DP4_123_DELTAPROT_METADATA_Truth), str(DP4_123_PCYCLE_Truth), str(DP4_123_PCYCLE_METADATA_Truth)]
                        ])
    """Checking for outputs from MSAP5"""
    DP5_125_MASS_Truth = os.path.isfile(os.path.join(MSAP5_Part3_Output_Path, "DP5_125_MASS.csv"))
    DP5_125_MASS_METADATA_Truth = os.path.isfile(os.path.join(MSAP5_Part3_Output_Path, "DP5_125_MASS_METADATA.csv"))
    DP5_125_RADIUS_Truth = os.path.isfile(os.path.join(MSAP5_Part3_Output_Path, "DP5_125_RADIUS.csv"))
    DP5_125_RADIUS_METADATA_Truth = os.path.isfile(os.path.join(MSAP5_Part3_Output_Path, "DP5_125_RADIUS_METADATA.csv"))
    DP5_125_AGE_Truth = os.path.isfile(os.path.join(MSAP5_Part3_Output_Path, "DP5_125_AGE.csv"))
    DP5_125_AGE_METADATA_Truth = os.path.isfile(os.path.join(MSAP5_Part3_Output_Path, "DP5_125_AGE_METADATA.csv"))

    """Create texttable object"""
    tableObj4 = texttable.Texttable(max_width=140)
    """Set column alignment (center alignment)"""
    tableObj4.set_cols_align(["c", "c", "c", "c", "c", "c"])
    """Set datatype of each column (text type)"""
    tableObj4.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
    """Adjust columns"""
    tableObj4.set_cols_valign(["b", "b", "b", "b", "b", "b"])
    """Insert rows in the table"""
    tableObj4.add_rows([["DP5_125_MASS", "DP5_125_MASS_METADATA", "DP5_125_RADIUS",
                        "DP5_125_RADIUS_METADATA", "DP5_125_AGE", "DP5_125_AGE_METADATA"],
                        [str(DP5_125_MASS_Truth), str(DP5_125_MASS_METADATA_Truth), str(DP5_125_RADIUS_Truth),
                        str(DP5_125_RADIUS_METADATA_Truth), str(DP5_125_AGE_Truth), str(DP5_125_AGE_METADATA_Truth)]
                        ])

    file = open(os.path.join(FolderPathForReport, ReportName), "a")
    file.write("\nThe list of the Data Products produced from the pipeline are as below: \n")
    file.write(tableObj.draw())
    file.write("\nfurther,\n")
    file.write(tableObj2.draw())
    file.write("\nfurther,\n")
    file.write(tableObj3.draw())
    file.write("\nfurther,\n")
    file.write(tableObj4.draw())
    file.write("\nThe SAS Overall workflow has thus been completed: \n")
    file.write("\n**---**---END-OF-REPORT---**---**\n")
    file.close()
    """Check if the following files exist and remove them if they exist"""
    Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Corrected_Light_Curves.csv'))
    Operations_Class.RemoveFile(os.path.join(UserDirectoryPath, 'Transit.csv'))
    Operations_Class.RemoveFile(os.path.join(UserDirectoryPath, 'Flares.csv'))
    Operations_Class.RemoveFile(os.path.join(UserDirectoryPath, 'Flares2.csv'))
    Operations_Class.RemoveFile(os.path.join(UserDirectoryPath, 'Transit2.csv'))
"""The code below calls the respective Functions"""
if __name__ == '__start__':
    start()
if __name__ == '__MSAP3_MSAP4_Completed__':
    LoggGranulation_False()
if __name__ == '__IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2__':
    IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2()
if __name__ == '__Stop__':
    Stop()
