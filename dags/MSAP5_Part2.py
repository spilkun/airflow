"""Importing libraries"""
import pandas as pd
import os
import texttable
from pathlib import Path
from datetime import datetime
import Operations
"""Define the home directory"""
HOME_DIR = str(Path.home())
"""We define the path for the input and output file location"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
FilePaths = pd.read_csv(os.path.join(UserDirectoryPath, 'FilePaths.csv'), delimiter = ',')
"""Defining the path to the folder where all outputs are stored, this is based on
the date and time at which the data pipeline was run"""
FolderPathForReport = FilePaths.loc[0, 'FolderPathForReport']
"""Defining User inputs file"""
UserInputsFilePath = os.path.join(HOME_DIR, 'userdirectory', 'UserInputs.csv')#'airflow',
UserInputsData = pd.read_csv(UserInputsFilePath, index_col=None)
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
class MSAP5_Part2_class:
    """The class defined is used to decribe the MSAP5_Part2 data pipeline, the different functions defined below
    represent the tasks performed in the different operators of the pipeline,
    in the current version of the code, we use these functions to produce a report"""
    def Start_MSAP5_2():
        """We generate one report for the overall work flow, one which is specific to overall MSAP5 and one specific to MSAP5 Part2 data pipeline,
        thus in certain information is repeated in both the reports"""

        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_MSAP5_2_MRA_determination_from_Non_Seismic_data = {'MSAP5_2_MRA_determination_from_Non_Seismic_data':['True']}
        Data_MSAP5_2_MRA_determination_from_Non_Seismic_data = pd.DataFrame(Variable_MSAP5_2_MRA_determination_from_Non_Seismic_data)
        Data_MSAP5_2_MRA_determination_from_Non_Seismic_data.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_2_MRA_determination_from_Non_Seismic_data.csv'), header=True, index=False)

        Variable_Start_MSAP5_2 = {'Start_MSAP5_2':['True']}
        Data_Start_MSAP5_2 = pd.DataFrame(Variable_Start_MSAP5_2)
        Data_Start_MSAP5_2.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'Start_MSAP5_2.csv'), header=True, index=False)
        """Writing the content in the temporary report specific to MSAP5 Part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\n.\n")
        file.write("\n_____*__*_____\n")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2 MRA Determination From Non Seismic Data initialized.\n")
        file.close()
        """Writing the content in the report specific to MSAP5 Part2"""
        """Writing the content in the child report of Overall SAS workflow"""
        """Writing the content in the report specific to Overall MSAP5"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
        #Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))

    """The function below performs the input checks"""
    def InputsCheck_MSAP5_2():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_InputsCheck_MSAP5_2 = {'InputsCheck_MSAP5_2':['True']}
        Data_InputsCheck_MSAP5_2 = pd.DataFrame(Variable_InputsCheck_MSAP5_2)
        Data_InputsCheck_MSAP5_2.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'InputsCheck_MSAP5_2.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Writing the content in the temporary report specific to MSAP5 Part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: Inputs check initialized.\n")
        file.write("\nMSAP5-2: Checking for DP4.\n")
        file.write("MSAP5-2: The following DP4 products exist.\n")
        """If RotationPeriod is False then the following DP4 outputs are not produced"""
        RotationPeriod = UserInputsData.loc[0, 'RotationPeriod']
        if bool(RotationPeriod) == False:
            DP4_123_PROT_Truth = "False"
            DP4_123_PROT_METADATA_Truth = "False"
            DP4_123_DELTAPROT_Truth = "False"
            DP4_123_DELTAPROT_METADATA_Truth = "False"
        else:
            DP4_123_PROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PROT.csv'))
            DP4_123_PROT_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PROT_METADATA.csv'))
            DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_DELTAPROT.csv'))
            DP4_123_DELTAPROT_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_DELTAPROT_METADATA.csv'))
        """If ActivityLevel is False then the following DP4 outputs are not produced"""
        ActivityLevel = UserInputsData.loc[0, 'ActivityLevel']
        if bool(ActivityLevel) == False:
            DP4_123_PCYCLE_Truth = "False"
            DP4_123_PCYCLE_METADATA_Truth = "False"
        else:
            DP4_123_PCYCLE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PCYCLE.csv'))
            DP4_123_PCYCLE_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PCYCLE_METADATA.csv'))

        DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_HARVEY1_AMPLITUDE.csv'))
        DP4_123_HARVEY_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_HARVEY_METADATA.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_PCYCLE", "DP4_123_PCYCLE_METADATA", "DP4_123_HARVEY1_AMPLITUDE", "DP4_123_HARVEY_METADATA",
                            "DP4_123_PROT", "DP4_123_PROT_METADATA", "DP4_123_DELTAPROT", "DP4_123_DELTAPROT_METADATA"],
                            [str(DP4_123_PCYCLE_Truth), str(DP4_123_PCYCLE_METADATA_Truth), str(DP4_123_HARVEY1_AMPLITUDE_Truth), str(DP4_123_HARVEY_METADATA_Truth),
                            str(DP4_123_PROT_Truth), str(DP4_123_PROT_METADATA_Truth), str(DP4_123_DELTAPROT_Truth), str(DP4_123_DELTAPROT_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-2: Checking for Preparatory Data.\n")
        file.write("MSAP5-2: The following Preparatory Data products exist.\n")

        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122__M_H__SAPP.csv'))
        """If Mandatory inputs are not present then raise Airflow exceptions to quit the data pipeline"""
        if PDP_C_125_RADIUS_CLASSICAL_Truth == "False":
            file.write("PDP_C_125_RADIUS_CLASSICAL is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            #Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            raise ValueError("PDP_C_125_RADIUS_CLASSICAL is absent, Data Pipeline failed.")
        if PDP_C_125_AGE_CLASSICAL_Truth == "False":
            file.write("PDP_C_125_AGE_CLASSICAL is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            #Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            raise ValueError("PDP_C_125_AGE_CLASSICAL is absent, Data Pipeline failed.")
        if PDP_C_125_MASS_CLASSICAL_Truth == "False":
            file.write("PDP_C_125_MASS_CLASSICAL is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            #Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            raise ValueError("PDP_C_125_MASS_CLASSICAL is absent, Data Pipeline failed.")
        if PDP_C_122_TEFF_SAPP_Truth == "False":
            file.write("PDP_C_122_TEFF_SAPP is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            #Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            raise ValueError("PDP_C_122_TEFF_SAPP is absent, Data Pipeline failed.")
        if PDP_C_122_M_H_SAPP_Truth == "False":
            file.write("PDP_C_122_M_H_SAPP is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            #Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            raise ValueError("PDP_C_122_M_H_SAPP is absent, Data Pipeline failed.")
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=180)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_125_RADIUS_CLASSICAL", "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL", "PDP_C_122_TEFF_SAPP",
                            "PDP_C_122_[M_H]_SAPP", "IDP_122_TEFF_SAPP", "IDP_122_[M_H]_SAPP"],
                            [str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(PDP_C_122_TEFF_SAPP_Truth),
                            str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-2: The availability of the other inputs used in MSAP Part 2 is as shown below.\n")

        DP5PQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP5_Path'], 'DP5PQ.csv'))
        DP2_RHO_TRANSIT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP2_Path'], 'DP2_RHO_TRANSIT.csv'))
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        if PDP_A_122_PREP_OBS_DATA_Truth == "False":
            file.write("PDP_A_122_PREP_OBS_DATA is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            #Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
            raise ValueError("PDP_A_122_PREP_OBS_DATA is absent, Data Pipeline failed.")
        IDP_123_LOGG_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_123_LOGG_VARLC.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["DP5PQ", "DP2_RHO_TRANSIT", "PDP_A_122_PREP_OBS_DATA", "IDP_123_LOGG_VARLC"],
                            [str(DP5PQ_Truth), str(DP2_RHO_TRANSIT_Truth), str(PDP_A_122_PREP_OBS_DATA_Truth), str(IDP_123_LOGG_VARLC_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-2: Input checks completed.\n")
        file.close()
        """Writing the content in the report specific to MSAP5 Part2"""
        """Writing the content in the Overall SAS workflow child report"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
        #Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_temp.txt"))

    """The function below performs the Gyrochronocology operation"""
    def Gyrochronocology_21():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Gyrochronocology_21 = {'Gyrochronocology_21':['True']}
        Data_Gyrochronocology_21 = pd.DataFrame(Variable_Gyrochronocology_21)
        Data_Gyrochronocology_21.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'Gyrochronocology_21.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """writing the content described below in the report specific to MSAP5 part 2,
        since in MSAP5 Part 2 datapipeline, we have 4 branches being executed parallely, write the information of each branch of MSAP5 part1 in a child report,
        and later copy this content into the report specific to MSAP5 Part1, the child report is deleted once the
        information is copied."""
        """Write the contents into the first child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_01.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: DP4_123_PROT.csv and DP4_123_PROT_METADATA.csv exists.\n")
        file.write("\nMSAP5-2: Gyrochronocology is initiated.\n")
        file.write("MSAP5-2: Checking inputs.\n")
        file.write("MSAP5-2: The following DP4 inputs exist.\n")
        """If RotationPeriod is False then the following DP4 outputs are not produced"""
        RotationPeriod = UserInputsData.loc[0, 'RotationPeriod']
        if bool(RotationPeriod) == False:
            DP4_123_PROT_Truth = "False"
            DP4_123_PROT_METADATA_Truth = "False"
            DP4_123_DELTAPROT_Truth = "False"
            DP4_123_DELTAPROT_METADATA_Truth = "False"
        else:
            DP4_123_PROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PROT.csv'))
            DP4_123_PROT_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PROT_METADATA.csv'))
            DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_DELTAPROT.csv'))
            DP4_123_DELTAPROT_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_DELTAPROT_METADATA.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_PROT", "DP4_123_PROT_METADATA", "DP4_123_DELTAPROT", "DP4_123_DELTAPROT_METADATA"],
                            [str(DP4_123_PROT_Truth), str(DP4_123_PROT_METADATA_Truth), str(DP4_123_DELTAPROT_Truth), str(DP4_123_DELTAPROT_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-2: The following PDP and IDP inputs exist.\n")
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        DP5PQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP5_Path'], 'DP5PQ.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122__M_H__SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c","c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_125_RADIUS_CLASSICAL", "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL", "DP5PQ_Truth",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_122_M_H_SAPP", "IDP_122_TEFF_SAPP", "IDP_122_M_H_SAPP"],
                            [str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(DP5PQ_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-2: Gyrochronocology Input check completed.\n")
        file.write("\nMSAP5-2: Producing the output IDP_125_AGE_GYRO.csv.\n")
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        """Producing the Output"""
        Path_IDP_125_AGE_GYRO = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_AGE_GYRO.csv')
        IDP_125_AGE_GYRO_out = DP4PQRead.to_csv(Path_IDP_125_AGE_GYRO, index = False)
        file.write("\nMSAP5-2: Output IDP_125_AGE_GYRO.csv is available.\n")
        IDP_125_AGE_GYRO_Truth = os.path.isfile(Path_IDP_125_AGE_GYRO)
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_125_AGE_GYRO"],
                            [str(IDP_125_AGE_GYRO_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs the completion operation from Gyrochronocology"""
    def End_From_MSAP5_Part2_21():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_From_MSAP5_Part2_21 = {'End_From_MSAP5_Part2_21':['True']}
        Data_End_From_MSAP5_Part2_21 = pd.DataFrame(Variable_End_From_MSAP5_Part2_21)
        Data_End_From_MSAP5_Part2_21.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'End_From_MSAP5_Part2_21.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the first child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_01.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: Gyrochronocology is completed.\n")
        file.close()
    """The function below performs the Activity age determination"""
    def Activity_Age_22():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Activity_Age_22 = {'Activity_Age_22':['True']}
        Data_Activity_Age_22 = pd.DataFrame(Variable_Activity_Age_22)
        Data_Activity_Age_22.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'Activity_Age_22.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the second child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_02.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("\nMSAP5-2: Activity Age is initiated.\n")
        file.write("MSAP5-2: DP4_123_HARVEY1_AMPLITUDE.csv exists.\n")
        file.write("MSAP5-2: Checking inputs.\n")
        file.write("MSAP5-2: The following DP4 inputs exist.\n")
        """If ActivityLevel is False then the following DP4 outputs are not produced"""
        ActivityLevel = UserInputsData.loc[0, 'ActivityLevel']
        if bool(ActivityLevel) == False:
            DP4_123_PCYCLE_Truth = "False"
            DP4_123_PCYCLE_METADATA_Truth = "False"
        else:
            DP4_123_PCYCLE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PCYCLE.csv'))
            DP4_123_PCYCLE_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PCYCLE_METADATA.csv'))

        DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_HARVEY1_AMPLITUDE.csv'))
        DP4_123_HARVEY_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_HARVEY_METADATA.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_PCYCLE", "DP4_123_PCYCLE_METADATA", "DP4_123_HARVEY1_AMPLITUDE", "DP4_123_HARVEY_METADATA"],
                            [str(DP4_123_PCYCLE_Truth), str(DP4_123_PCYCLE_METADATA_Truth), str(DP4_123_HARVEY1_AMPLITUDE_Truth), str(DP4_123_HARVEY_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-2: The following PDP and IDP inputs exist.\n")
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        DP5PQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP5_Path'], 'DP5PQ.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122__M_H__SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c","c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_125_RADIUS_CLASSICAL", "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL", "DP5PQ_Truth",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_122_M_H_SAPP", "IDP_122_TEFF_SAPP", "IDP_122_M_H_SAPP"],
                            [str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(DP5PQ_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-2: Activity Age Input check completed.\n")
        file.write("\nMSAP5-2: Producing the output IDP_125_AGE_ACTIVITY.csv.\n")
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        """Producing the Output"""
        Path_IDP_125_AGE_ACTIVITY = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_AGE_ACTIVITY.csv')
        IDP_125_AGE_ACTIVITY_out = DP4PQRead.to_csv(Path_IDP_125_AGE_ACTIVITY, index = False)
        file.write("\nMSAP5-2: Output IDP_125_AGE_ACTIVITY.csv is available.\n")
        IDP_125_AGE_ACTIVITY_Truth = os.path.isfile(Path_IDP_125_AGE_ACTIVITY)
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_125_AGE_ACTIVITY"],
                            [str(IDP_125_AGE_ACTIVITY_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs completion operation from the Activity age determination"""
    def End_From_MSAP5_Part2_22():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_From_MSAP5_Part2_22 = {'End_From_MSAP5_Part2_22':['True']}
        Data_End_From_MSAP5_Part2_22 = pd.DataFrame(Variable_End_From_MSAP5_Part2_22)
        Data_End_From_MSAP5_Part2_22.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'End_From_MSAP5_Part2_22.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the second child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_02.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: Activity age completed.\n")
        file.close()
    """The function below performs completion operation if IDP_123_LOGG_VARLC.csv does not exist"""
    def End_From_MSAP5_Part2_23_24():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_From_MSAP5_Part2_23_24 = {'End_From_MSAP5_Part2_23_24':['True']}
        Data_End_From_MSAP5_Part2_23_24 = pd.DataFrame(Variable_End_From_MSAP5_Part2_23_24)
        Data_End_From_MSAP5_Part2_23_24.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'End_From_MSAP5_Part2_23_24.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the third child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_03.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: File IDP_123_LOGG_VARLC.csv does not exist.\n")
        file.close()
    """The function below performs compute M from logg or flick R phot operation"""
    def ComputeMfromLOGG_Flick_R_Phot_23():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_ComputeMfromLOGG_Flick_R_Phot_23 = {'ComputeMfromLOGG_Flick_R_Phot_23':['True']}
        Data_ComputeMfromLOGG_Flick_R_Phot_23 = pd.DataFrame(Variable_ComputeMfromLOGG_Flick_R_Phot_23)
        Data_ComputeMfromLOGG_Flick_R_Phot_23.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'ComputeMfromLOGG_Flick_R_Phot_23.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the third child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_03.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("\nMSAP5-2: Compute M from LOGG_Flick and R_Phot is initialized.\n")
        file.write("MSAP5-2: File IDP_123_LOGG_VARLC.csv exists.\n")

        file.write("MSAP5-2: Reading the inputs.\n")
        file.write("MSAP5-2: The following PDP and IDP inputs are read.\n")
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        DP5PQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP5_Path'], 'DP5PQ.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122__M_H__SAPP.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c","c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_C_125_RADIUS_CLASSICAL", "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL", "DP5PQ_Truth",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_122_M_H_SAPP", "IDP_122_TEFF_SAPP", "IDP_122_M_H_SAPP"],
                            [str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(DP5PQ_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-2: The following inputs are further read.\n")
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        """If LoggGranulation is False then IDP_123_LOGG_VARLC_Truth is not produced"""
        LoggGranulation = UserInputsData.loc[0, 'LoggGranulation']
        if bool(LoggGranulation) == False:
            IDP_123_LOGG_VARLC_Truth = "False"
        else:
            IDP_123_LOGG_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_123_LOGG_VARLC.csv'))

        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_A_122_PREP_OBS_DATA", "IDP_123_LOGG_VARLC"],
                            [str(PDP_A_122_PREP_OBS_DATA_Truth), str(IDP_123_LOGG_VARLC_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-2: Compute M from LOGG Flick R Phot Input check completed.\n")
        file.write("\nMSAP5-2: Producing the outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        """Producing the Output"""
        Path_IDP_125_MASS_GRANULATION = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_MASS_GRANULATION.csv')
        IDP_125_MASS_GRANULATION_out = DP4PQRead.to_csv(Path_IDP_125_MASS_GRANULATION, index = False)
        file.write("MSAP5-2: Producing IDP_125_MASS_GRANULATION.csv.\n")
        file.write("MSAP5-2: IDP_125_MASS_GRANULATION.csv is available.\n")
        IDP_125_MASS_GRANULATION_Truth = os.path.isfile(Path_IDP_125_MASS_GRANULATION)
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_125_MASS_GRANULATION"],
                            [str(IDP_125_MASS_GRANULATION_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs grid based modelling using classical code operation"""
    def GridBasedModellingUsingClassicalCode_24():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GridBasedModellingUsingClassicalCode_24 = {'GridBasedModellingUsingClassicalCode_24':['True']}
        Data_GridBasedModellingUsingClassicalCode_24 = pd.DataFrame(Variable_GridBasedModellingUsingClassicalCode_24)
        Data_GridBasedModellingUsingClassicalCode_24.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'GridBasedModellingUsingClassicalCode_24.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the third child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_03.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: Grid Based Modelling using Classical Code is initialized.\n")
        file.write("MSAP5-2: File IDP_123_LOGG_VARLC.csv exists.\n")
        file.write("MSAP5-2: Reading the inputs.\n")
        file.write("MSAP5-2: The following PDP and IDP inputs are read.\n")
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        DP5PQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP5_Path'], 'DP5PQ.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122__M_H__SAPP.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c","c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_C_125_RADIUS_CLASSICAL", "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL", "DP5PQ_Truth",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_122_M_H_SAPP", "IDP_122_TEFF_SAPP", "IDP_122_M_H_SAPP"],
                            [str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(DP5PQ_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-2: The following inputs are further read.\n")
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        """If LoggGranulation is False then IDP_123_LOGG_VARLC_Truth is not produced"""
        LoggGranulation = UserInputsData.loc[0, 'LoggGranulation']
        if bool(LoggGranulation) == False:
            IDP_123_LOGG_VARLC_Truth = "False"
        else:
            IDP_123_LOGG_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_123_LOGG_VARLC.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_A_122_PREP_OBS_DATA", "IDP_123_LOGG_VARLC"],
                            [str(PDP_A_122_PREP_OBS_DATA_Truth), str(IDP_123_LOGG_VARLC_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-2: Grid based modelling with classical code Input check completed.\n")
        file.write("\nMSAP5-2: Producing the outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        """Producing the Output"""
        Path_IDP_125_MASS_GRANULATION_CGBM = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_MASS_GRANULATION_CGBM.csv')
        IDP_125_MASS_GRANULATION_CGBM_out = DP4PQRead.to_csv(Path_IDP_125_MASS_GRANULATION_CGBM, index = False)
        Path_IDP_125_RADIUS_GRANULATION_CGBM = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_RADIUS_GRANULATION_CGBM.csv')
        IDP_125_RADIUS_GRANULATION_CGBM_out = DP4PQRead.to_csv(Path_IDP_125_RADIUS_GRANULATION_CGBM, index = False)
        Path_IDP_125_AGE_GRANULATION_CGBM = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_AGE_GRANULATION_CGBM.csv')
        IDP_125_AGE_GRANULATION_CGBM_out = DP4PQRead.to_csv(Path_IDP_125_AGE_GRANULATION_CGBM, index = False)
        file.write("MSAP5-2: Producing the following outpute.\n")
        IDP_125_MASS_GRANULATION_CGBM_Truth = os.path.isfile(Path_IDP_125_MASS_GRANULATION_CGBM)
        IDP_125_RADIUS_GRANULATION_CGBM_Truth = os.path.isfile(Path_IDP_125_RADIUS_GRANULATION_CGBM)
        IDP_125_AGE_GRANULATION_CGBM_Truth = os.path.isfile(Path_IDP_125_AGE_GRANULATION_CGBM)
        """Create texttable object"""
        tableObj3 = texttable.Texttable(max_width=80)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c","c","c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t","t","t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b","b","b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_125_MASS_GRANULATION_CGBM", "IDP_125_RADIUS_GRANULATION_CGBM", "IDP_125_AGE_GRANULATION_CGBM"],
                            [str(IDP_125_MASS_GRANULATION_CGBM_Truth), str(IDP_125_RADIUS_GRANULATION_CGBM_Truth), str(IDP_125_AGE_GRANULATION_CGBM_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs completion operation from operators 23 and 24"""
    def End2_From_MSAP5_Part2_23_24():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End2_From_MSAP5_Part2_23_24 = {'End2_From_MSAP5_Part2_23_24':['True']}
        Data_End2_From_MSAP5_Part2_23_24 = pd.DataFrame(Variable_End2_From_MSAP5_Part2_23_24)
        Data_End2_From_MSAP5_Part2_23_24.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'End2_From_MSAP5_Part2_23_24.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the third child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_03.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: Grid Based Modelling using Classical Code is completed.\n")
        file.write("MSAP5-2: Compute M from LOGG_Flick and R_Phot is completed.\n")
        file.close()
    """The function below performs completion operation if DP2_RHO_TRANSIT.csv does not exist"""
    def End_From_MSAP5_Part2_25_26():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_From_MSAP5_Part2_25_26 = {'End_From_MSAP5_Part2_25_26':['True']}
        Data_End_From_MSAP5_Part2_25_26 = pd.DataFrame(Variable_End_From_MSAP5_Part2_25_26)
        Data_End_From_MSAP5_Part2_25_26.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'End_From_MSAP5_Part2_25_26.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the fourth child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_04.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: The file DP2_RHO_TRANSIT.csv does not exist.\n")
        file.close()
    """The function below performs compute M from rho transit or R phot operation"""
    def ComputeMfromRHO_Transit_R_Phot_25():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_ComputeMfromRHO_Transit_R_Phot_25 = {'ComputeMfromRHO_Transit_R_Phot_25':['True']}
        Data_ComputeMfromRHO_Transit_R_Phot_25 = pd.DataFrame(Variable_ComputeMfromRHO_Transit_R_Phot_25)
        Data_ComputeMfromRHO_Transit_R_Phot_25.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'ComputeMfromRHO_Transit_R_Phot_25.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the fourth child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_04.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: The file DP2_RHO_TRANSIT.csv exists.\n")
        file.write("MSAP5-2: Compute M from RHO transit and R_Phot is initialized.\n")
        file.write("MSAP5-2: Reading the inputs.\n")
        file.write("MSAP5-2: The following PDP and IDP inputs are read.\n")
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        DP5PQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP5_Path'], 'DP5PQ.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122__M_H__SAPP.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c","c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_C_125_RADIUS_CLASSICAL", "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL", "DP5PQ_Truth",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_122_M_H_SAPP", "IDP_122_TEFF_SAPP", "IDP_122_M_H_SAPP"],
                            [str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(DP5PQ_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-2: The following inputs are further read.\n")
        DP2_RHO_TRANSIT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP2_Path'], 'DP2_RHO_TRANSIT.csv'))
        IDP_123_LOGG_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_123_LOGG_VARLC.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c","c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t","t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b","b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP2_RHO_TRANSIT", "IDP_123_LOGG_VARLC"],
                            [str(DP2_RHO_TRANSIT_Truth), str(IDP_123_LOGG_VARLC_Truth)]
                            ])
        file.write(tableObj2.draw())
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        """Producing the Output"""
        Path_IDP_125_MASS_RHO_TRANSIT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_MASS_RHO_TRANSIT.csv')
        IDP_125_MASS_RHO_TRANSIT_out = DP4PQRead.to_csv(Path_IDP_125_MASS_RHO_TRANSIT, index = False)
        file.write("\nMSAP5-2: Compute M from RHO transit R Phot Input check completed.")
        file.write("\nMSAP5-2: Producing IDP_125_MASS_RHO_TRANSIT.csv.\n")
        IDP_125_MASS_RHO_TRANSIT_Truth = os.path.isfile(Path_IDP_125_MASS_RHO_TRANSIT)
        file.write("MSAP5-2: IDP_125_MASS_RHO_TRANSIT.csv produced.\n")
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_125_MASS_RHO_TRANSIT"],
                            [str(IDP_125_MASS_RHO_TRANSIT_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs grid based modelling using classical code operation"""
    def GridBasedModellingUsingClassicalCode_26():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GridBasedModellingUsingClassicalCode_26 = {'GridBasedModellingUsingClassicalCode_26':['True']}
        Data_GridBasedModellingUsingClassicalCode_26 = pd.DataFrame(Variable_GridBasedModellingUsingClassicalCode_26)
        Data_GridBasedModellingUsingClassicalCode_26.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'GridBasedModellingUsingClassicalCode_26.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the fourth child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_04.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: The file DP2_RHO_TRANSIT.csv exists.\n")
        file.write("MSAP5-2: Grid Based Modelling using classical code is initialized.\n")
        file.write("MSAP5-2: Reading the inputs.\n")
        file.write("MSAP5-2: The following PDP and IDP inputs are read.\n")
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        DP5PQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP5_Path'], 'DP5PQ.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_122__M_H__SAPP.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c","c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_C_125_RADIUS_CLASSICAL", "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL", "DP5PQ_Truth",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_122_M_H_SAPP", "IDP_122_TEFF_SAPP", "IDP_122_M_H_SAPP"],
                            [str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(DP5PQ_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-2: The following inputs are further read.\n")
        DP2_RHO_TRANSIT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP2_Path'], 'DP2_RHO_TRANSIT.csv'))
        IDP_123_LOGG_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'IDP_123_LOGG_VARLC.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c","c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t","t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b","b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP2_RHO_TRANSIT", "IDP_123_LOGG_VARLC"],
                            [str(DP2_RHO_TRANSIT_Truth), str(IDP_123_LOGG_VARLC_Truth)]
                            ])
        file.write(tableObj2.draw())
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        file.write("\nMSAP5-2: Grid based modelling using classical code input check completed.")
        """Producing the Output"""
        Path_IDP_125_MASS_RHO_TRANSIT_CGBM = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_MASS_RHO_TRANSIT_CGBM.csv')
        IDP_125_MASS_RHO_TRANSIT_CGBM_out = DP4PQRead.to_csv(Path_IDP_125_MASS_RHO_TRANSIT_CGBM, index = False)
        Path_IDP_125_RADIUS_RHO_TRANSIT_CGBM = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_RADIUS_RHO_TRANSIT_CGBM.csv')
        IDP_125_RADIUS_RHO_TRANSIT_CGBM_out = DP4PQRead.to_csv(Path_IDP_125_RADIUS_RHO_TRANSIT_CGBM, index = False)
        Path_IDP_125_AGE_RHO_TRANSIT_CGBM = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'IDP_125_AGE_RHO_TRANSIT_CGBM.csv')
        IDP_125_AGE_RHO_TRANSIT_CGBM_out = DP4PQRead.to_csv(Path_IDP_125_AGE_RHO_TRANSIT_CGBM, index = False)
        file.write("MSAP5-2: Producing the following outpute.\n")
        IDP_125_MASS_RHO_TRANSIT_CGBM_Truth = os.path.isfile(Path_IDP_125_MASS_RHO_TRANSIT_CGBM)
        IDP_125_RADIUS_RHO_TRANSIT_CGBM_Truth = os.path.isfile(Path_IDP_125_RADIUS_RHO_TRANSIT_CGBM)
        IDP_125_AGE_RHO_TRANSIT_CGBM_Truth = os.path.isfile(Path_IDP_125_AGE_RHO_TRANSIT_CGBM)
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c","c","c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t","t","t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b","b","b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_125_MASS_RHO_TRANSIT_CGBM", "IDP_125_RADIUS_RHO_TRANSIT_CGBM", "IDP_125_AGE_RHO_TRANSIT_CGBM"],
                            [str(IDP_125_MASS_RHO_TRANSIT_CGBM_Truth), str(IDP_125_RADIUS_RHO_TRANSIT_CGBM_Truth), str(IDP_125_AGE_RHO_TRANSIT_CGBM_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs completion operation from operators 25 and 26"""
    def End2_From_MSAP5_Part2_25_26():
        Variable_End2_From_MSAP5_Part2_25_26 = {'End2_From_MSAP5_Part2_25_26':['True']}
        Data_End2_From_MSAP5_Part2_25_26 = pd.DataFrame(Variable_End2_From_MSAP5_Part2_25_26)
        Data_End2_From_MSAP5_Part2_25_26.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'End2_From_MSAP5_Part2_25_26.csv'), header=True, index=False)
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the contents into the fourth child branch of report specific to MSAP5 part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2_04.txt"), "a")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-2: Grid Based Modelling using Classical Code is completed.\n")
        file.write("MSAP5-2: Compute M from RHO Transit and R_Phot is completed.\n")
        file.close()
    """The function below performs completion operation of MSAP5 part 2 data pipeline"""
    def Stop_MSAP5_2():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Stop_MSAP5_2 = {'Stop_MSAP5_2':['True']}
        Data_Stop_MSAP5_2 = pd.DataFrame(Variable_Stop_MSAP5_2)
        Data_Stop_MSAP5_2.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'Stop_MSAP5_2.csv'), header=True, index=False)
        """For each function that is described, we check if that function was run or not by reading the respective .csv files,
        we later delete these files"""
        Process_01 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'Start_MSAP5_2.csv'))
        Process_02 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'InputsCheck_MSAP5_2.csv'))
        Process_03 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'Gyrochronocology_21.csv'))
        Process_04 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'End_From_MSAP5_Part2_21.csv'))
        Process_05 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'Activity_Age_22.csv'))
        Process_06 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'End_From_MSAP5_Part2_22.csv'))
        Process_07 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'End_From_MSAP5_Part2_23_24.csv'))
        Process_08 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'End_From_MSAP5_Part2_25_26.csv'))
        Process_09 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'ComputeMfromRHO_Transit_R_Phot_25.csv'))
        Process_10 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'ComputeMfromLOGG_Flick_R_Phot_23.csv'))
        Process_11 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'GridBasedModellingUsingClassicalCode_24.csv'))
        Process_12 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'End2_From_MSAP5_Part2_23_24.csv'))
        Process_13 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'GridBasedModellingUsingClassicalCode_26.csv'))
        Process_14 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'End2_From_MSAP5_Part2_25_26.csv'))
        Process_15 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part2', 'Stop_MSAP5_2.csv'))

        """Copying each of the child branch report into the main report specific to MSAP5 part2 and deleting the child report"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part2.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_01.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP5_Part2_01.txt'))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part2.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_02.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP5_Part2_02.txt'))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part2.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_03.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP5_Part2_03.txt'))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part2.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part2_04.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP5_Part2_04.txt'))

        """Writing the operations summary on Main report"""
        tableObj = texttable.Texttable(max_width=140)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c", "c","c", "c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t", "t","t", "t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b", "b","b", "b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([["Gyrochronocology_21", "Activity_Age_22", "ComputeMfromRHO_Transit_R_Phot_25",
                            "ComputeMfromLOGG_Flick_R_Phot_23", "GridBasedModellingUsingClassicalCode_24",
                            "GridBasedModellingUsingClassicalCode_26"],
                            [str(Process_03), str(Process_05), str(Process_09),
                            str(Process_10), str(Process_11), str(Process_13)]
                            ])
        """Write the Table in the Report"""
        Main_Report_file_MSAP5_Part2 = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"), "a")
        Main_Report_file_MSAP5_Part2.write("\n")
        Main_Report_file_MSAP5_Part2.write("\nThe summary of all the processes in MSAP5 Part 2 is:\n")
        Main_Report_file_MSAP5_Part2.write(tableObj.draw())
        Main_Report_file_MSAP5_Part2.write("\nMSAP5 Part 2 completed.\n")
        Main_Report_file_MSAP5_Part2.write("\n---**---MSAP5-Part2-END---**---\n")
        Main_Report_file_MSAP5_Part2.close()
        """Writing the operations summary on Main report for SAS overall workflow"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part2.txt"))
        """Copying the contents from child report to the SAS overall workflow report"""
        if os.path.isfile(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part2.txt')) == True:
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part2.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part2.txt'))
        else:
            print("MainReportfile_MSAP5_Part2.txt does not exist")

if __name__ == '__Start_MSAP5_2__':
    Start_MSAP5_2()
if __name__ == '__InputsCheck_MSAP5_2__':
    InputsCheck_MSAP5_2()
if __name__ == '__Gyrochronocology_21__':
    Gyrochronocology_21()
if __name__ == '__End_From_MSAP5_Part2_21__':
    End_From_MSAP5_Part2_21()
if __name__ == '__Activity_Age_22__':
    Activity_Age_22()
if __name__ == '__End_From_MSAP5_Part2_22__':
    End_From_MSAP5_Part2_22()
if __name__ == '__End_From_MSAP5_Part2_23_24__':
    End_From_MSAP5_Part2_23_24()
if __name__ == '__ComputeMfromLOGG_Flick_R_Phot_23__':
    ComputeMfromLOGG_Flick_R_Phot_23()
if __name__ == '__GridBasedModellingUsingClassicalCode_24__':
    GridBasedModellingUsingClassicalCode_24()
if __name__ == '__End2_From_MSAP5_Part2_23_24__':
    End2_From_MSAP5_Part2_23_24()
if __name__ == '__End_From_MSAP5_Part2_25_26__':
    End_From_MSAP5_Part2_25_26()
if __name__ == '__ComputeMfromRHO_Transit_R_Phot_25__':
    ComputeMfromRHO_Transit_R_Phot_25()
if __name__ == '__GridBasedModellingUsingClassicalCode_26__':
    GridBasedModellingUsingClassicalCode_26()
if __name__ == '__End2_From_MSAP5_Part2_25_26__':
    End2_From_MSAP5_Part2_25_26()
if __name__ == '__Stop_MSAP5_2__':
    Stop_MSAP5_2()
