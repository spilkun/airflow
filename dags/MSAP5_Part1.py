"""Importing libraries"""
import pandas as pd
import os
import texttable
from pathlib import Path
from datetime import datetime
import Operations
"""Define the home directory"""
HOME_DIR = str(Path.home())
"""We define the path for the input and output file location"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
FilePaths = pd.read_csv(os.path.join(UserDirectoryPath, 'FilePaths.csv'), delimiter = ',')
"""We define the path for the input and output file location"""
InputsPath = FilePaths.loc[0, 'InputsPath']
GeneratedOutputs_Path = FilePaths.loc[0, 'GeneratedOutputs_Path']
"""Defining the path to the folder where all outputs are stored, this is based on
the date and time at which the data pipeline was run"""
FolderPathForReport = FilePaths.loc[0, 'FolderPathForReport']
"""We use the below Date and Time for the timeStamp"""
now = datetime.now()
dt_string = now.strftime("%B_%d_%Y_%H_%M")
"""Defining User inputs file"""
UserInputsFilePath = os.path.join(HOME_DIR, 'userdirectory', 'UserInputs.csv')#'airflow',
UserInputsData = pd.read_csv(UserInputsFilePath, index_col=None)
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
class MSAP5_Part1_class:
    """The class defined is used to decribe the MSAP5_Part1 data pipeline, the different functions defined below
    represent the tasks performed in the different operators of the pipeline,
    in the current version of the code, we use these functions to produce a report"""
    def Start_MSAP5_1():
        """We generate one report for the overall work flow, one which is specific to overall MSAP5 and one specific to MSAP5 Part1 data pipeline,
        thus in certain information is repeated in both the reports"""

        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_MSAP5_1_MRA_Determination_From_Seismic_Data = {'MSAP5_1_MRA_Determination_From_Seismic_Data':['True']}
        Data_MSAP5_1_MRA_Determination_From_Seismic_Data = pd.DataFrame(Variable_MSAP5_1_MRA_Determination_From_Seismic_Data)
        Data_MSAP5_1_MRA_Determination_From_Seismic_Data.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part1', 'MSAP5_1_MRA_Determination_From_Seismic_Data.csv'), header=True, index=False)

        Variable_Start_MSAP5_1 = {'Start_MSAP5_1':['True']}
        Data_Start_MSAP5_1 = pd.DataFrame(Variable_Start_MSAP5_1)
        Data_Start_MSAP5_1.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part2', 'Start_MSAP5_1.csv'), header=True, index=False)
        """Writing the content in the report to a temporary file"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\n\n")
        file.write("_____*__*_____\n")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("IDP_128_DETECTION_FLAG is set to True.\n")
        file.write("MSAP5-1 MRA Determination From Seismic Data initialized.\n")
        file.close()
        """Writing the content in the report specific to MSAP5 Part1"""
        """Writing the content in the report Overall SAS workflow"""
        """Writing the content in the report specific to Overall MSAP5"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_Overall_MSAP5.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))

    """Performing input checks"""
    def InputsCheck_MSAP5_1():
        """Writing the content in the report specific to MSAP5 Part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("\nMSAP5-1: Inputs Check Initialized.\n")
        file.write("MSAP5-1: DP3 Check Initialized.\n")
        file.write("MSAP5-1: DP3 Files available are as below.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: DP3 Check Completed.\n")
        file.write("MSAP5-1: PDP-C and IDP check.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__ALPHA_FE__SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """If Mandatory inputs are not present then raise Airflow exceptions to quit the data pipeline"""
        if PDP_C_122_TEFF_SAPP_Truth == "False":
            file.write("PDP_C_122_TEFF_SAPP is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            raise ValueError("PDP_C_122_TEFF_SAPP is absent, Data Pipeline failed.")
        if PDP_C_122_M_H_SAPP_Truth == "False":
            file.write("PDP_C_122_M_H_SAPP is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            raise ValueError("PDP_C_122_M_H_SAPP is absent, Data Pipeline failed.")
        if PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth == "False":
            file.write("PDP_C_122_ALPHA_FE_SPECTROSCOPY is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            raise ValueError("PDP_C_122_ALPHA_FE_SPECTROSCOPY is absent, Data Pipeline failed.")
        if PDP_C_122_L_SED_Truth == "False":
            file.write("PDP_C_122_L_SED is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            raise ValueError("PDP_C_122_L_SED is absent, Data Pipeline failed.")
        if PDP_C_125_RADIUS_CLASSICAL_Truth == "False":
            file.write("PDP_C_125_RADIUS_CLASSICAL is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            raise ValueError("PDP_C_125_RADIUS_CLASSICAL is absent, Data Pipeline failed.")
        if PDP_C_122_COVMAT_SAPP_Truth == "False":
            file.write("PDP_C_122_COVMAT_SAPP is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            raise ValueError("PDP_C_122_COVMAT_SAPP is absent, Data Pipeline failed.")
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "PDP_C_122_[M_H]_SAPP", "IDP_122_[M_H]_SAPP", "PDP_C_122_[ALPHA_FE]_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY_Truth", "PDP_C_122_L_SED", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_COVMAT_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth), str(PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth), str(PDP_C_122_L_SED_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_COVMAT_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: PDP-C and IDP Check Completed.\n")
        file.write("MSAP5-1: PDP-B Check Initiated.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """If Mandatory inputs are not present then raise Airflow exceptions to quit the data pipeline"""
        if PDP_B_121_MOD_OSC_FREQ_Truth == "False":
            file.write("PDP_B_121_MOD_OSC_FREQ is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            raise ValueError("PDP_B_121_MOD_OSC_FREQ is absent, Data Pipeline failed.")
        if PDP_B_121_MOD_EVOL_Truth == "False":
            file.write("PDP_B_121_MOD_EVOL is absent, Data Pipeline failed.")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
            raise ValueError("PDP_B_121_MOD_EVOL is absent, Data Pipeline failed.")
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-1: PDP-B Check Completed.\n")
        file.write("MSAP5-1: Input Check Completed.\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_temp.txt"))
    """The function below performs scaling laws operation"""
    def ScalingLaws_12():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_ScalingLaws_12 = {'ScalingLaws_12':['True']}
        Data_ScalingLaws_12 = pd.DataFrame(Variable_ScalingLaws_12)
        Data_ScalingLaws_12.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'ScalingLaws_12.csv'), header=True, index=False)
        """writing the content described below in the report specific to MSAP5 part 1,
        since in MSAP5 Part 1 datapipeline, we have 5 branches being executed parallely, write the information of each branch of MSAP5 part1 in a child report,
        and later copy this content into the report specific to MSAP5 Part1, the child report is deleted once the
        information is copied."""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_01.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-1: Scaling Laws initialized.\n")
        file.write("MSAP5-1: Reading Inputs.\n")
        file.write("MSAP5-1: Reading DP3 Inputs.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=100)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA","DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth),str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: Reading DP3 Inputs Completed.\n")
        file.write("MSAP5-1: Reading PDP-C and IDP Inputs Initiated.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: Generating Outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_MASS_SCALING_ONLY = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_MASS_SCALING_ONLY.csv')
        IDP_124_MASS_SCALING_ONLY_out = DP4PQRead.to_csv(Path_IDP_124_MASS_SCALING_ONLY, index = False)
        Path_IDP_124_RADIUS_SCALING_ONLY = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_SCALING_ONLY.csv')
        IDP_124_RADIUS_SCALING_ONLY_out = DP4PQRead.to_csv(Path_IDP_124_RADIUS_SCALING_ONLY, index = False)
        Path_IDP_124_LOGG_SCALING_ONLY = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_SCALING_ONLY.csv')
        IDP_124_LOGG_SCALING_ONLY_out = DP4PQRead.to_csv(Path_IDP_124_LOGG_SCALING_ONLY, index = False)
        file.write("MSAP5-1: The Generated Outputs are.\n")
        IDP_124_MASS_SCALING_ONLY_Truth = os.path.isfile(Path_IDP_124_MASS_SCALING_ONLY)
        IDP_124_RADIUS_SCALING_ONLY_Truth = os.path.isfile(Path_IDP_124_RADIUS_SCALING_ONLY)
        IDP_124_LOGG_SCALING_ONLY_Truth = os.path.isfile(Path_IDP_124_LOGG_SCALING_ONLY)
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_124_MASS_SCALING_ONLY", "IDP_124_RADIUS_SCALING_ONLY", "IDP_124_LOGG_SCALING_ONLY"],
                            [str(IDP_124_MASS_SCALING_ONLY_Truth), str(IDP_124_RADIUS_SCALING_ONLY_Truth), str(IDP_124_LOGG_SCALING_ONLY_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs scaling laws completion operation"""
    def ScalingLaws_12_End():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_ScalingLaws_12_End = {'ScalingLaws_12_End':['True']}
        Data_ScalingLaws_12_End = pd.DataFrame(Variable_ScalingLaws_12_End)
        Data_ScalingLaws_12_End.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part1', 'ScalingLaws_12_End.csv'), header=True, index=False)
        """Write the contents into the first child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_01.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-1: Scaling Laws Completed.\n")
        file.close()
    """The function below performs Global Parameters direct method operation"""
    def GlobalParametersDirectMethod_11():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GlobalParametersDirectMethod_11 = {'GlobalParametersDirectMethod_11':['True']}
        Data_GlobalParametersDirectMethod_11 = pd.DataFrame(Variable_GlobalParametersDirectMethod_11)
        Data_GlobalParametersDirectMethod_11.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GlobalParametersDirectMethod_11.csv'), header=True, index=False)
        """Write the contents into the second child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_02.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-1: Global Parameters Direct Method initialized.\n")
        file.write("MSAP5-1: Reading Inputs.\n")
        file.write("MSAP5-1: Reading DP3 Inputs.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=100)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: DP3 Check Completed.\n")
        file.write("MSAP5-1: PDP-C and IDP check.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__ALPHA_FE__SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "PDP_C_122_[M_H]_SAPP", "IDP_122_[M_H]_SAPP", "PDP_C_122_[ALPHA_FE]_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY_Truth", "PDP_C_122_L_SED", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_COVMAT_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth), str(PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth), str(PDP_C_122_L_SED_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_COVMAT_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: PDP-C and IDP Check Completed.\n")
        file.write("MSAP5-1: PDP-B Check Initiated.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-1: PDP-B Check Completed.\n")
        file.write("MSAP5-1: Input Check Completed.\n")
        file.write("MSAP5-1: Generating outputs from Global Parameters Direct Method.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_MASS_SCALING_GRIDS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_MASS_SCALING_GRIDS.csv')
        IDP_124_MASS_SCALING_GRIDS_out = DP4PQRead.to_csv(Path_IDP_124_MASS_SCALING_GRIDS, index = False)
        Path_IDP_124_RADIUS_SCALING_GRIDS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_SCALING_GRIDS.csv')
        IDP_124_RADIUS_SCALING_GRIDS_out = DP4PQRead.to_csv(Path_IDP_124_RADIUS_SCALING_GRIDS, index = False)
        Path_IDP_124_AGE_SCALING_GRIDS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_SCALING_GRIDS.csv')
        IDP_124_AGE_SCALING_GRIDS_out = DP4PQRead.to_csv(Path_IDP_124_AGE_SCALING_GRIDS, index = False)
        Path_IDP_124_LOGG_SCALING_GRIDS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_SCALING_GRIDS.csv')
        IDP_124_LOGG_SCALING_GRIDS_out = DP4PQRead.to_csv(Path_IDP_124_LOGG_SCALING_GRIDS, index = False)
        Path_IDP_124_DENSITY_SCALING_GRIDS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_SCALING_GRIDS.csv')
        IDP_124_DENSITY_SCALING_GRIDS_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_SCALING_GRIDS, index = False)
        Path_IDP_124_METADATA_SCALING_GRIDS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_SCALING_GRIDS.csv')
        IDP_124_METADATA_SCALING_GRIDS_out = DP4PQRead.to_csv(Path_IDP_124_METADATA_SCALING_GRIDS, index = False)

        IDP_124_MASS_SCALING_GRIDS_Truth = os.path.isfile(Path_IDP_124_MASS_SCALING_GRIDS)
        IDP_124_RADIUS_SCALING_GRIDS_Truth = os.path.isfile(Path_IDP_124_RADIUS_SCALING_GRIDS)
        IDP_124_AGE_SCALING_GRIDS_Truth = os.path.isfile(Path_IDP_124_AGE_SCALING_GRIDS)
        IDP_124_LOGG_SCALING_GRIDS_Truth = os.path.isfile(Path_IDP_124_LOGG_SCALING_GRIDS)
        IDP_124_DENSITY_SCALING_GRIDS_Truth = os.path.isfile(Path_IDP_124_DENSITY_SCALING_GRIDS)
        IDP_124_METADATA_SCALING_GRIDS_Truth = os.path.isfile(Path_IDP_124_METADATA_SCALING_GRIDS)
        file.write("MSAP5-1: The Generated outputs from Global Parameters Direct Method are.\n")
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=130)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_MASS_SCALING_GRIDS", "IDP_124_RADIUS_SCALING_GRIDS", "IDP_124_AGE_SCALING_GRIDS", "IDP_124_LOGG_SCALING_GRIDS", "IDP_124_DENSITY_SCALING_GRIDS", "IDP_124_METADATA_SCALING_GRIDS"],
                            [str(IDP_124_MASS_SCALING_GRIDS_Truth), str(IDP_124_RADIUS_SCALING_GRIDS_Truth), str(IDP_124_AGE_SCALING_GRIDS_Truth), str(IDP_124_LOGG_SCALING_GRIDS_Truth), str(IDP_124_DENSITY_SCALING_GRIDS_Truth), str(IDP_124_METADATA_SCALING_GRIDS_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.close()
    """The function below performs Global Parameters direct method completion operation"""
    def GlobalParametersDirectMethod_11_End():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GlobalParametersDirectMethod_11_End = {'GlobalParametersDirectMethod_11_End':['True']}
        Data_GlobalParametersDirectMethod_11_End = pd.DataFrame(Variable_GlobalParametersDirectMethod_11_End)
        Data_GlobalParametersDirectMethod_11_End.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GlobalParametersDirectMethod_11_End.csv'), header=True, index=False)
        """Write the contents into the second child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_02.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Global Parameters Direct Method completed.\n")
        file.close()
    """The function below performs Global Parameters direct method completion operation"""
    def GridBasedInferenceMixedModes_13():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GridBasedInferenceMixedModes_13 = {'GridBasedInferenceMixedModes_13':['True']}
        Data_GridBasedInferenceMixedModes_13 = pd.DataFrame(Variable_GridBasedInferenceMixedModes_13)
        Data_GridBasedInferenceMixedModes_13.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceMixedModes_13.csv'), header=True, index=False)
        """Write the contents into the fourth child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_04.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Mixed Modes was set to True.\n")
        file.write("Grid Based Inference Mixed Modes intiated.\n")
        file.write("Reading Inputs.\n")
        file.write("MSAP5-1: DP3 Check Initialized.\n")
        file.write("MSAP5-1: DP3 Files available are as below.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=150)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: DP3 Check Completed.\n")
        file.write("MSAP5-1: PDP-C and IDP check.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__ALPHA_FE__SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "PDP_C_122_[M_H]_SAPP", "IDP_122_[M_H]_SAPP", "PDP_C_122_[ALPHA_FE]_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY_Truth", "PDP_C_122_L_SED", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_COVMAT_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth), str(PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth), str(PDP_C_122_L_SED_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_COVMAT_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: PDP-C and IDP Check Completed.\n")
        file.write("MSAP5-1: PDP-B Check Initiated.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-1: PDP-B Check Completed.\n")
        file.write("MSAP5-1: Input Check Completed.\n")
        file.write("Generating Outputs for Grid Based Inference mixed modes.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_MASS_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_MASS_GRID_MIXED.csv')
        IDP_124_MASS_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_MASS_GRID_MIXED, index = False)
        Path_IDP_124_RADIUS_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_GRID_MIXED.csv')
        IDP_124_RADIUS_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_RADIUS_GRID_MIXED, index = False)
        Path_IDP_124_AGE_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_GRID_MIXED.csv')
        IDP_124_AGE_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_AGE_GRID_MIXED, index = False)
        Path_IDP_124_LOGG_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_GRID_MIXED.csv')
        IDP_124_LOGG_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_LOGG_GRID_MIXED, index = False)
        Path_IDP_124_TEFF_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_TEFF_GRID_MIXED.csv')
        IDP_124_TEFF_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_TEFF_GRID_MIXED, index = False)
        Path_IDP_124_L_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_L_GRID_MIXED.csv')
        IDP_124_L_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_L_GRID_MIXED, index = False)
        Path_IDP_124_DENSITY_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_GRID_MIXED.csv')
        IDP_124_DENSITY_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_GRID_MIXED, index = False)
        Path_IDP_124_METADATA_REFMOD_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_REFMOD_GRID_MIXED.csv')
        IDP_124_METADATA_REFMOD_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_METADATA_REFMOD_GRID_MIXED, index = False)
        IDP_124_MASS_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_MASS_GRID_MIXED)
        IDP_124_RADIUS_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_RADIUS_GRID_MIXED)
        IDP_124_AGE_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_AGE_GRID_MIXED)
        IDP_124_LOGG_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_LOGG_GRID_MIXED)
        IDP_124_TEFF_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_TEFF_GRID_MIXED)
        IDP_124_L_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_L_GRID_MIXED)
        IDP_124_DENSITY_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_DENSITY_GRID_MIXED)
        IDP_124_METADATA_REFMOD_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_METADATA_REFMOD_GRID_MIXED)
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t","t", "t", "t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_MASS_GRID_MIXED", "IDP_124_RADIUS_GRID_MIXED", "IDP_124_AGE_GRID_MIXED", "IDP_124_LOGG_GRID_MIXED", "IDP_124_TEFF_GRID_MIXED", "IDP_124_L_GRID_MIXED", "IDP_124_DENSITY_GRID_MIXED", "IDP_124_METADATA_REFMOD_GRID_MIXED"],
                            [str(IDP_124_MASS_GRID_MIXED_Truth), str(IDP_124_RADIUS_GRID_MIXED_Truth), str(IDP_124_AGE_GRID_MIXED_Truth), str(IDP_124_LOGG_GRID_MIXED_Truth), str(IDP_124_TEFF_GRID_MIXED_Truth), str(IDP_124_L_GRID_MIXED_Truth), str(IDP_124_DENSITY_GRID_MIXED_Truth), str(IDP_124_METADATA_REFMOD_GRID_MIXED_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.write("Grid Based Inference mixed modes completed.\n")
        file.close()
    """The function below performs Grid Based Inference Individual Frequencies operation"""
    def GridBasedInferenceIndividualFrequencies_14():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GridBasedInferenceIndividualFrequencies_14 = {'GridBasedInferenceIndividualFrequencies_14':['True']}
        Data_GridBasedInferenceIndividualFrequencies_14 = pd.DataFrame(Variable_GridBasedInferenceIndividualFrequencies_14)
        Data_GridBasedInferenceIndividualFrequencies_14.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceIndividualFrequencies_14.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("IDP_123_QUALITY_METADATA was set to True.\n")
        file.write("Mixed Modes was set to False.\n")
        file.write("Grid Based Inference Individual Frequencies intiated.\n")
        file.write("Reading Inputs.\n")
        file.write("MSAP5-1: DP3 Check Initialized.\n")
        file.write("MSAP5-1: DP3 Files available are as below.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=150)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: DP3 Check Completed.\n")
        file.write("MSAP5-1: PDP-C and IDP check.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__ALPHA_FE__SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "PDP_C_122_[M_H]_SAPP", "IDP_122_[M_H]_SAPP", "PDP_C_122_[ALPHA_FE]_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY_Truth", "PDP_C_122_L_SED", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_COVMAT_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth), str(PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth), str(PDP_C_122_L_SED_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_COVMAT_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: PDP-C and IDP Check Completed.\n")
        file.write("MSAP5-1: PDP-B Check Initiated.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-1: PDP-B Check Completed.\n")
        file.write("MSAP5-1: Input Check Completed.\n")
        file.write("Generating Outputs for Grid Based Inference Individual Frequencies.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_DP_124_MASS_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'DP_124_MASS_GRID_FREQS.csv')
        DP_124_MASS_GRID_FREQS_out = DP4PQRead.to_csv(Path_DP_124_MASS_GRID_FREQS, index = False)

        Path_IDP_124_RADIUS_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_GRID_FREQS.csv')
        IDP_124_RADIUS_GRID_FREQS_out = DP4PQRead.to_csv(Path_IDP_124_RADIUS_GRID_FREQS, index = False)

        Path_IDP_124_AGE_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_GRID_FREQS.csv')
        IDP_124_AGE_GRID_FREQS_out = DP4PQRead.to_csv(Path_IDP_124_AGE_GRID_FREQS, index = False)

        Path_IDP_124_LOGG_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_GRID_FREQS.csv')
        IDP_124_LOGG_GRID_FREQS_out = DP4PQRead.to_csv(Path_IDP_124_LOGG_GRID_FREQS, index = False)

        Path_IDP_124_TEFF_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_TEFF_GRID_FREQS.csv')
        IDP_124_TEFF_GRID_FREQS_out = DP4PQRead.to_csv(Path_IDP_124_TEFF_GRID_FREQS, index = False)

        Path_IDP_124_L_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_L_GRID_FREQS.csv')
        IDP_124_L_GRID_FREQS_out = DP4PQRead.to_csv(Path_IDP_124_L_GRID_FREQS, index = False)

        Path_IDP_124_DENSITY_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_GRID_FREQS.csv')
        IDP_124_DENSITY_GRID_FREQS_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_GRID_FREQS, index = False)

        Path_IDP_124_METADATA_REFMOD_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_REFMOD_GRID_FREQS.csv')
        IDP_124_METADATA_REFMOD_GRID_FREQS_out = DP4PQRead.to_csv(Path_IDP_124_METADATA_REFMOD_GRID_FREQS, index = False)

        DP_124_MASS_GRID_FREQS_Truth = os.path.isfile(Path_DP_124_MASS_GRID_FREQS)
        IDP_124_RADIUS_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_RADIUS_GRID_FREQS)
        IDP_124_AGE_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_AGE_GRID_FREQS)
        IDP_124_LOGG_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_LOGG_GRID_FREQS)
        IDP_124_TEFF_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_TEFF_GRID_FREQS)
        IDP_124_L_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_L_GRID_FREQS)
        IDP_124_DENSITY_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_DENSITY_GRID_FREQS)
        IDP_124_METADATA_REFMOD_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_METADATA_REFMOD_GRID_FREQS)
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t","t", "t", "t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_MASS_GRID_FREQS", "IDP_124_RADIUS_GRID_FREQS", "IDP_124_AGE_GRID_FREQS", "IDP_124_LOGG_GRID_FREQS", "IDP_124_TEFF_GRID_FREQS", "IDP_124_L_GRID_FREQS", "IDP_124_DENSITY_GRID_FREQS", "IDP_124_METADATA_REFMOD_GRID_FREQS"],
                            [str(DP_124_MASS_GRID_FREQS_Truth), str(IDP_124_RADIUS_GRID_FREQS_Truth), str(IDP_124_AGE_GRID_FREQS_Truth), str(IDP_124_LOGG_GRID_FREQS_Truth), str(IDP_124_TEFF_GRID_FREQS_Truth), str(IDP_124_L_GRID_FREQS_Truth), str(IDP_124_DENSITY_GRID_FREQS_Truth), str(IDP_124_METADATA_REFMOD_GRID_FREQS_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.write("\nGrid Based Inference Individual Frequencies completed.\n")
        file.close()
    """The function below performs Grid Based Inference Surface independent operation"""
    def GridBasedInferenceSurfaceIndependent_15():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GridBasedInferenceSurfaceIndependent_15 = {'GridBasedInferenceSurfaceIndependent_15':['True']}
        Data_GridBasedInferenceSurfaceIndependent_15 = pd.DataFrame(Variable_GridBasedInferenceSurfaceIndependent_15)
        Data_GridBasedInferenceSurfaceIndependent_15.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceSurfaceIndependent_15.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("IDP_123_QUALITY_METADATA was set to True.\n")
        file.write("Mixed Modes was set to False.\n")
        file.write("Grid Based Inference Surface Independent intiated.\n")
        file.write("Reading Inputs.\n")
        file.write("MSAP5-1: DP3 Check Initialized.\n")
        file.write("MSAP5-1: DP3 Files available are as below.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: DP3 Check Completed.\n")
        file.write("MSAP5-1: PDP-C and IDP check.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__ALPHA_FE__SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "PDP_C_122_[M_H]_SAPP", "IDP_122_[M_H]_SAPP", "PDP_C_122_[ALPHA_FE]_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY_Truth", "PDP_C_122_L_SED", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_COVMAT_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth), str(PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth), str(PDP_C_122_L_SED_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_COVMAT_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: PDP-C and IDP Check Completed.\n")
        file.write("MSAP5-1: PDP-B Check Initiated.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-1: PDP-B Check Completed.\n")
        file.write("MSAP5-1: Input Check Completed.\n")
        file.write("Generating Outputs for Grid Based Inference Surface Independent.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_MASS_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part1', 'IDP_124_MASS_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_MASS_GRID_SURFACE_INDEPENDENT_out = DP4PQRead.to_csv(Path_IDP_124_MASS_GRID_SURFACE_INDEPENDENT, index = False)

        Path_IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT_out = DP4PQRead.to_csv(Path_IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT, index = False)

        Path_IDP_124_AGE_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_AGE_GRID_SURFACE_INDEPENDENT_out = DP4PQRead.to_csv(Path_IDP_124_AGE_GRID_SURFACE_INDEPENDENT, index = False)

        Path_IDP_124_LOGG_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_LOGG_GRID_SURFACE_INDEPENDENT_out = DP4PQRead.to_csv(Path_IDP_124_LOGG_GRID_SURFACE_INDEPENDENT, index = False)

        Path_IDP_124_TEFF_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_TEFF_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_TEFF_GRID_SURFACE_INDEPENDENT_out = DP4PQRead.to_csv(Path_IDP_124_TEFF_GRID_SURFACE_INDEPENDENT, index = False)

        Path_IDP_124_L_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_L_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_L_GRID_SURFACE_INDEPENDENT_out = DP4PQRead.to_csv(Path_IDP_124_L_GRID_SURFACE_INDEPENDENT, index = False)

        Path_IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT, index = False)

        Path_IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT_out = DP4PQRead.to_csv(Path_IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT, index = False)

        IDP_124_MASS_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_MASS_GRID_SURFACE_INDEPENDENT)
        IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT)
        IDP_124_AGE_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_AGE_GRID_SURFACE_INDEPENDENT)
        IDP_124_LOGG_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_LOGG_GRID_SURFACE_INDEPENDENT)
        IDP_124_TEFF_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_TEFF_GRID_SURFACE_INDEPENDENT)
        IDP_124_L_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_L_GRID_SURFACE_INDEPENDENT)
        IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT)
        IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT)
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=180)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t","t", "t", "t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_MASS_GRID_SURFACE_INDEPENDENT", "IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT", "IDP_124_AGE_GRID_SURFACE_INDEPENDENT", "IDP_124_LOGG_GRID_SURFACE_INDEPENDENT", "IDP_124_TEFF_GRID_SURFACE_INDEPENDENT", "IDP_124_L_GRID_SURFACE_INDEPENDENT", "IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT", "IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT"],
                            [str(IDP_124_MASS_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_AGE_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_LOGG_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_TEFF_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_L_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.write("\nGrid Based Inference Surface Independent completed.\n")
        file.close()
    """The function below performs end operation from Grid Based Inference mixed modes"""
    def End_From13():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_From13 = {'End_From13':['True']}
        Data_End_From13 = pd.DataFrame(Variable_End_From13)
        Data_End_From13.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_From13.csv'), header=True, index=False)
        """Write the contents into the fourth child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_04.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Sofesticated Method was set to False.\n")
        file.close()
    """The function below performs the interpolation operation"""
    def Interpolation_131():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Interpolation_131 = {'Interpolation_131':['True']}
        Data_Interpolation_131 = pd.DataFrame(Variable_Interpolation_131)
        Data_Interpolation_131.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Interpolation_131.csv'), header=True, index=False)
        """Write the contents into the fourth child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_04.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Sofesticated Method was set to True.\n")
        file.write("Interpolation was set to True.\n")
        file.write("Reading Inputs.\n")
        file.write("Reading The following parameters.\n")
        file.write("1 - Interpolation Coefficients.\n")
        file.write("2 - Relevant model names/paths.\n")
        file.write("Producing Outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_STRUCT_MOD_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_STRUCT_MOD_MIXED.csv')
        IDP_124_STRUCT_MOD_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_STRUCT_MOD_MIXED, index = False)
        file.write("The following Output was produced.\n")
        IDP_124_STRUCT_MOD_MIXED_Truth = os.path.isfile(Path_IDP_124_STRUCT_MOD_MIXED)
        """Create texttable object"""
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_124_STRUCT_MOD_MIXED"],
                            [str(IDP_124_STRUCT_MOD_MIXED_Truth)]
                            ])
        file.write(tableObj.draw())
        file.close()
    """The function below performs the inversion operation"""
    def Inversion_132():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Inversion_132 = {'Inversion_132':['True']}
        Data_Inversion_132 = pd.DataFrame(Variable_Inversion_132)
        Data_Inversion_132.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part1', 'Inversion_132.csv'), header=True, index=False)
        """Write the contents into the fourth child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_04.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Interpolation was set to False.\n")
        file.write("Reading Inputs.\n")
        file.write("The following inputs are available.\n")
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        Path_IDP_124_STRUCT_MOD_MIXED = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part1', 'IDP_124_STRUCT_MOD_MIXED.csv')
        IDP_124_STRUCT_MOD_MIXED_Truth = os.path.isfile(Path_IDP_124_STRUCT_MOD_MIXED)
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=150)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t","t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA", "PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL", "IDP_124_STRUCT_MOD_MIXED"],
                            [str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth), str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth), str(IDP_124_STRUCT_MOD_MIXED_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nGenerating the outputs.\n")

        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_DENSITY_INV_MIXED = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_INV_MIXED.csv')
        IDP_124_DENSITY_INV_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_INV_MIXED, index = False)
        file.write("The following Output was produced.\n")
        IDP_124_DENSITY_INV_MIXED_Truth = os.path.isfile(Path_IDP_124_DENSITY_INV_MIXED)
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_124_DENSITY_INV_MIXED"],
                            [str(IDP_124_DENSITY_INV_MIXED_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.close()
    """The function below performs mixed mode fitting with inversion constraints operation"""
    def MixedModeFittingwithInversionConstraints_133():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_MixedModeFittingwithInversionConstraints_133 = {'MixedModeFittingwithInversionConstraints_133':['True']}
        Data_MixedModeFittingwithInversionConstraints_133 = pd.DataFrame(Variable_MixedModeFittingwithInversionConstraints_133)
        Data_MixedModeFittingwithInversionConstraints_133.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'MixedModeFittingwithInversionConstraints_133.csv'), header=True, index=False)
        """Write the contents into the fourth child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_04.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Mixed Mode Fitting with Inversion Constraints initiated.\n")
        file.write("Reading inputs.\n")
        file.write("MSAP5-1: DP3 Check Initialized.\n")
        file.write("MSAP5-1: DP3 Files available are as below.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: DP3 Check Completed.\n")
        file.write("MSAP5-1: PDP-C and IDP check.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__ALPHA_FE__SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "PDP_C_122_[M_H]_SAPP", "IDP_122_[M_H]_SAPP", "PDP_C_122_[ALPHA_FE]_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY_Truth", "PDP_C_122_L_SED", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_COVMAT_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth), str(PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth), str(PDP_C_122_L_SED_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_COVMAT_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: PDP-C and IDP Check Completed.\n")
        file.write("MSAP5-1: PDP-B Check Initiated.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-1: PDP-B Check Completed.\n")
        file.write("Reading the generated inputs.\n")
        Path_IDP_124_DENSITY_INV_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_INV_MIXED.csv')
        IDP_124_DENSITY_INV_MIXED_Truth = os.path.isfile(Path_IDP_124_DENSITY_INV_MIXED)
        Path_IDP_124_METADATA_REFMOD_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_REFMOD_GRID_MIXED.csv')
        IDP_124_METADATA_REFMOD_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_METADATA_REFMOD_GRID_MIXED)
        """Create texttable object"""
        tableObj4 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b"])
        """ Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_DENSITY_INV_MIXED", "IDP_124_METADATA_REFMOD_GRID_MIXED"],
                            [str(IDP_124_DENSITY_INV_MIXED_Truth), str(IDP_124_METADATA_REFMOD_GRID_MIXED_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.write("\nGenerating the outputs.\n")

        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_MASS_INV_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_MASS_INV_GRID_MIXED.csv')
        IDP_124_MASS_INV_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_MASS_INV_GRID_MIXED, index = False)
        Path_IDP_124_RADIUS_INV_GRID_MIXED = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_INV_GRID_MIXED.csv')
        IDP_124_RADIUS_INV_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_RADIUS_INV_GRID_MIXED, index = False)
        Path_IDP_124_AGE_INV_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_INV_GRID_MIXED.csv')
        IDP_124_AGE_INV_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_AGE_INV_GRID_MIXED, index = False)
        Path_IDP_124_LOGG_INV_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_INV_GRID_MIXED.csv')
        IDP_124_LOGG_INV_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_LOGG_INV_GRID_MIXED, index = False)
        Path_IDP_124_TEFF_INV_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_TEFF_INV_GRID_MIXED.csv')
        IDP_124_TEFF_INV_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_TEFF_INV_GRID_MIXED, index = False)
        Path_IDP_124_L_INV_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_L_INV_GRID_MIXED.csv')
        IDP_124_L_INV_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_L_INV_GRID_MIXED, index = False)
        Path_IDP_124_DENSITY_INV_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_INV_GRID_MIXED.csv')
        IDP_124_DENSITY_INV_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_INV_GRID_MIXED, index = False)
        Path_IDP_124_METADATA_INV_GRID_MIXED = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_INV_GRID_MIXED.csv')
        IDP_124_METADATA_INV_GRID_MIXED_out = DP4PQRead.to_csv(Path_IDP_124_METADATA_INV_GRID_MIXED, index = False)

        IDP_124_MASS_INV_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_MASS_INV_GRID_MIXED)
        IDP_124_RADIUS_INV_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_RADIUS_INV_GRID_MIXED)
        IDP_124_AGE_INV_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_AGE_INV_GRID_MIXED)
        IDP_124_LOGG_INV_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_LOGG_INV_GRID_MIXED)
        IDP_124_TEFF_INV_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_TEFF_INV_GRID_MIXED)
        IDP_124_L_INV_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_L_INV_GRID_MIXED)
        IDP_124_DENSITY_INV_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_DENSITY_INV_GRID_MIXED)
        IDP_124_METADATA_INV_GRID_MIXED_Truth = os.path.isfile(Path_IDP_124_METADATA_INV_GRID_MIXED)
        """Create texttable object"""
        tableObj5 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj5.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj5.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj5.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj5.add_rows([["IDP_124_MASS_INV_GRID_MIXED", "IDP_124_RADIUS_INV_GRID_MIXED", "IDP_124_AGE_INV_GRID_MIXED",
                            "IDP_124_LOGG_INV_GRID_MIXED", "IDP_124_TEFF_INV_GRID_MIXED", "IDP_124_L_INV_GRID_MIXED", "IDP_124_DENSITY_INV_GRID_MIXED", "IDP_124_METADATA_INV_GRID_MIXED"],
                            [str(IDP_124_MASS_INV_GRID_MIXED_Truth), str(IDP_124_RADIUS_INV_GRID_MIXED_Truth), str(IDP_124_AGE_INV_GRID_MIXED_Truth),
                            str(IDP_124_LOGG_INV_GRID_MIXED_Truth), str(IDP_124_TEFF_INV_GRID_MIXED_Truth), str(IDP_124_L_INV_GRID_MIXED_Truth),
                            str(IDP_124_DENSITY_INV_GRID_MIXED_Truth), str(IDP_124_METADATA_INV_GRID_MIXED_Truth)]
                            ])
        file.write(tableObj5.draw())
        file.close()
    """The function below performs end operation of the MSAP5 part1 datapipeline"""
    def End_MSAP5_1():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_MSAP5_1 = {'End_MSAP5_1':['True']}
        Data_End_MSAP5_1 = pd.DataFrame(Variable_End_MSAP5_1)
        Data_End_MSAP5_1.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_MSAP5_1.csv'), header=True, index=False)
        """Write the contents into the fifth child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_05.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5 Part 1 is completed.\n")
        file.write("--**--End-of-Report--**--\n")
        file.close()
        """Write the contents into the child branch of overall sas workflow"""
        Main_Report_file = open(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt'), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Write the Table in the Report"""
        Main_Report_file.write("\n")
        Main_Report_file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        Main_Report_file.write("\nThe summary of all the processes in MSAP5 Part 1 is:\n")
        """For each function that is described, we check if that function was run or not by reading the respective .csv files,
        we later delete these files"""
        Process_01 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'ScalingLaws_12.csv'))
        Process_02 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'ScalingLaws_12_End.csv'))
        Process_03 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GlobalParametersDirectMethod_11.csv'))
        Process_04 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GlobalParametersDirectMethod_11_End.csv'))
        Process_05 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceMixedModes_13.csv'))
        Process_06 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceIndividualFrequencies_14.csv'))
        Process_07 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceSurfaceIndependent_15.csv'))
        Process_08 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_From13.csv'))
        Process_09 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Interpolation_131.csv'))
        Process_10 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Inversion_132.csv'))
        Process_11 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'MixedModeFittingwithInversionConstraints_133.csv'))
        Process_12 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_From_MSAP5_1.csv'))
        Process_13 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_From_14_15.csv'))
        Process_14 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Glitches_16.csv'))
        Process_15 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceGlitch_161.csv'))
        Process_16 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_From_161.csv'))
        Process_17 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Interpolation_162.csv'))
        Process_18 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Inversion_163.csv'))
        Process_19 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceWithInversionConstraints_164.csv'))
        Process_20 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_MSAP5_1.csv'))
        """writing the summary into the child report of SAS overall workflow"""
        tableObj = texttable.Texttable(max_width=160)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["ScalingLaws_12", "GlobalParametersDirectMethod_11",
                            "GridBasedInferenceMixedModes_13", "GridBasedInferenceIndividualFrequencies_14",
                            "GridBasedInferenceSurfaceIndependent_15", "Interpolation_131",
                            "Inversion_132"],
                            [str(Process_01), str(Process_03),
                            str(Process_05), str(Process_06),
                            str(Process_07), str(Process_09),
                            str(Process_10)]
                            ])
        Main_Report_file.write(tableObj.draw())
        Main_Report_file.write("\nFurther\n")
        tableObj3 = texttable.Texttable(max_width=140)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c","c", "c", "c","c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t","t", "t", "t","t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b","b", "b", "b","b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["MixedModeFittingwithInversionConstraints_133",
                            "Glitches_16", "GridBasedInferenceGlitch_161",
                            "Interpolation_162", "Inversion_163",
                            "GridBasedInferenceWithInversionConstraints_164"],
                            [str(Process_11), str(Process_14), str(Process_15),
                            str(Process_17), str(Process_18), str(Process_19)]
                            ])
        Main_Report_file.write(tableObj3.draw())
        Main_Report_file.write("\nMSAP5 Part1 completed.\n")
        Main_Report_file.write("\n---**---MSAP5-Part1-END---**---\n")
        Main_Report_file.close()
        """Copying each of the child branch report into the main report specific to MSAP5 part1 and deleting the child report"""
        if os.path.isfile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_01.txt")) == True:
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_01.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_01.txt"))
        else:
            print("Report_MSAP5_Part1_01.txt does not exist")
        if os.path.isfile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_02.txt")) == True:
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_02.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_02.txt"))
        else:
            print("Report_MSAP5_Part1_02.txt does not exist")
        if os.path.isfile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt")) == True:
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"))
        else:
            print("Report_MSAP5_Part1_03.txt does not exist")
        if os.path.isfile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_04.txt")) == True:
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_04.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_04.txt"))
        else:
            print("Report_MSAP5_Part1_04.txt does not exist")
        if os.path.isfile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_05.txt")) == True:
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'Report_MSAP5_Part1.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part1_05.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_05.txt"))
        else:
            print("Report_MSAP5_Part1_05.txt does not exist")
        """Copying the contents from child report to the SAS overall workflow report"""
        if os.path.isfile(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part1.txt')) == True:
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part1.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part1.txt"))
        else:
            print("MainReportfile_MSAP5_Part1.txt does not exist")
    """The function below performs completion operation from both 14 and 15 operations of the datapipeline"""
    def End_From_14_15():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_From_14_15 = {'End_From_14_15':['True']}
        Data_End_From_14_15 = pd.DataFrame(Variable_End_From_14_15)
        Data_End_From_14_15.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_From_14_15.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Sofesticated Method was set to False.\n")
        file.close()
    """The function below performs Glitches operation of the MSAP5 part1 datapipeline"""
    def Glitches_16():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Glitches_16 = {'Glitches_16':['True']}
        Data_Glitches_16 = pd.DataFrame(Variable_Glitches_16)
        Data_Glitches_16.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Glitches_16.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Sofesticated Method was set to True.\n")
        file.write("Glitches operator initiated.\n")
        file.write("Reading Inputs.\n")
        file.write("Reading DP3 Inputs.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nReading Outputs generated from Grid-Based Inference - Surface Independent.\n")
        Path_IDP_124_MASS_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_MASS_GRID_SURFACE_INDEPENDENT.csv')
        Path_IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT.csv')
        Path_IDP_124_AGE_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_GRID_SURFACE_INDEPENDENT.csv')
        Path_IDP_124_LOGG_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_GRID_SURFACE_INDEPENDENT.csv')
        Path_IDP_124_TEFF_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_TEFF_GRID_SURFACE_INDEPENDENT.csv')
        Path_IDP_124_L_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_L_GRID_SURFACE_INDEPENDENT.csv')
        Path_IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT.csv')

        IDP_124_MASS_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_MASS_GRID_SURFACE_INDEPENDENT)
        IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT)
        IDP_124_AGE_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_AGE_GRID_SURFACE_INDEPENDENT)
        IDP_124_LOGG_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_LOGG_GRID_SURFACE_INDEPENDENT)
        IDP_124_TEFF_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_TEFF_GRID_SURFACE_INDEPENDENT)
        IDP_124_L_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_L_GRID_SURFACE_INDEPENDENT)
        IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT)
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t","t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_124_MASS_GRID_SURFACE_INDEPENDENT", "IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT", "IDP_124_AGE_GRID_SURFACE_INDEPENDENT", "IDP_124_LOGG_GRID_SURFACE_INDEPENDENT", "IDP_124_TEFF_GRID_SURFACE_INDEPENDENT", "IDP_124_L_GRID_SURFACE_INDEPENDENT", "IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT"],
                            [str(IDP_124_MASS_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_RADIUS_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_AGE_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_LOGG_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_TEFF_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_L_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_DENSITY_GRID_SURFACE_INDEPENDENT_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nReading Outputs generated from Grid-Based Inference - Individual Frequencies.\n")
        Path_DP_124_MASS_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'DP_124_MASS_GRID_FREQS.csv')
        Path_IDP_124_RADIUS_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_GRID_FREQS.csv')
        Path_IDP_124_AGE_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_GRID_FREQS.csv')
        Path_IDP_124_LOGG_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_GRID_FREQS.csv')
        Path_IDP_124_TEFF_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_TEFF_GRID_FREQS.csv')
        Path_IDP_124_L_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_L_GRID_FREQS.csv')
        Path_IDP_124_DENSITY_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_GRID_FREQS.csv')

        DP_124_MASS_GRID_FREQS_Truth = os.path.isfile(Path_DP_124_MASS_GRID_FREQS)
        IDP_124_RADIUS_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_RADIUS_GRID_FREQS)
        IDP_124_AGE_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_AGE_GRID_FREQS)
        IDP_124_LOGG_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_LOGG_GRID_FREQS)
        IDP_124_TEFF_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_TEFF_GRID_FREQS)
        IDP_124_L_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_L_GRID_FREQS)
        IDP_124_DENSITY_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_DENSITY_GRID_FREQS)
        """Create texttable object"""
        tableObj3 = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t", "t","t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["DP_124_MASS_GRID_FREQS", "IDP_124_RADIUS_GRID_FREQS", "IDP_124_AGE_GRID_FREQS", "IDP_124_LOGG_GRID_FREQS", "IDP_124_TEFF_GRID_FREQS", "IDP_124_L_GRID_FREQS", "IDP_124_DENSITY_GRID_FREQS"],
                            [str(DP_124_MASS_GRID_FREQS_Truth), str(IDP_124_RADIUS_GRID_FREQS_Truth), str(IDP_124_AGE_GRID_FREQS_Truth), str(IDP_124_LOGG_GRID_FREQS_Truth), str(IDP_124_TEFF_GRID_FREQS_Truth), str(IDP_124_L_GRID_FREQS_Truth), str(IDP_124_DENSITY_GRID_FREQS_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nGenerating Outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_Y_DEPTH_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_Y_DEPTH_GLITCHES.csv')
        IDP_124_Y_DEPTH_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_Y_DEPTH_GLITCHES, index = False)
        Path_IDP_124_Y_AMPLITUDE_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_Y_AMPLITUDE_GLITCHES.csv')
        IDP_124_Y_AMPLITUDE_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_Y_AMPLITUDE_GLITCHES, index = False)
        Path_IDP_124_BCE_DEPTH_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_BCE_DEPTH_GLITCHES.csv')
        IDP_124_BCE_DEPTH_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_BCE_DEPTH_GLITCHES, index = False)
        Path_IDP_124_BCE_AMPLITUDE_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_BCE_AMPLITUDE_GLITCHES.csv')
        IDP_124_BCE_AMPLITUDE_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_BCE_AMPLITUDE_GLITCHES, index = False)
        Path_IDP_124_CC_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_CC_GLITCHES.csv')
        IDP_124_CC_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_CC_GLITCHES, index = False)
        file.write("The Generated Outputs are as below.\n")

        IDP_124_Y_DEPTH_GLITCHES_Truth = os.path.isfile(Path_IDP_124_Y_DEPTH_GLITCHES)
        IDP_124_Y_AMPLITUDE_GLITCHES_Truth = os.path.isfile(Path_IDP_124_Y_AMPLITUDE_GLITCHES)
        IDP_124_BCE_DEPTH_GLITCHES_Truth = os.path.isfile(Path_IDP_124_BCE_DEPTH_GLITCHES)
        IDP_124_BCE_AMPLITUDE_GLITCHES_Truth = os.path.isfile(Path_IDP_124_BCE_AMPLITUDE_GLITCHES)
        IDP_124_CC_GLITCHES_Truth = os.path.isfile(Path_IDP_124_CC_GLITCHES)
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=140)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t","t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_Y_DEPTH_GLITCHES", "IDP_124_Y_AMPLITUDE_GLITCHES", "IDP_124_BCE_DEPTH_GLITCHES", "IDP_124_BCE_AMPLITUDE_GLITCHES", "IDP_124_CC_GLITCHES"],
                            [str(IDP_124_Y_DEPTH_GLITCHES_Truth), str(IDP_124_Y_AMPLITUDE_GLITCHES_Truth), str(IDP_124_BCE_DEPTH_GLITCHES_Truth), str(IDP_124_BCE_AMPLITUDE_GLITCHES_Truth), str(IDP_124_CC_GLITCHES_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.close()
    """The function below performs Grid based inference operation of the MSAP5 part1 datapipeline"""
    def GridBasedInferenceGlitch_161():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GridBasedInferenceGlitch_161 = {'GridBasedInferenceGlitch_161':['True']}
        Data_GridBasedInferenceGlitch_161 = pd.DataFrame(Variable_GridBasedInferenceGlitch_161)
        Data_GridBasedInferenceGlitch_161.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceGlitch_161.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Grid based inference Glitch operator initiated.\n")
        file.write("Reading the inputs.\n")
        file.write("MSAP5-1: DP3 Check Initialized.\n")
        file.write("MSAP5-1: DP3 Files available are as below.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: DP3 Check Completed.\n")
        file.write("MSAP5-1: PDP-C and IDP check.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__ALPHA_FE__SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "PDP_C_122_[M_H]_SAPP", "IDP_122_[M_H]_SAPP", "PDP_C_122_[ALPHA_FE]_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY_Truth", "PDP_C_122_L_SED", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_COVMAT_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth), str(PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth), str(PDP_C_122_L_SED_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_COVMAT_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: PDP-C and IDP Check Completed.\n")
        file.write("MSAP5-1: PDP-B Check Initiated.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-1: PDP-B Check Completed.\n")
        file.write("Reading outputs from the previous Glitches operation.\n")
        Path_IDP_124_Y_DEPTH_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_Y_DEPTH_GLITCHES.csv')
        Path_IDP_124_Y_AMPLITUDE_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_Y_AMPLITUDE_GLITCHES.csv')
        Path_IDP_124_BCE_DEPTH_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_BCE_DEPTH_GLITCHES.csv')
        Path_IDP_124_BCE_AMPLITUDE_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_BCE_AMPLITUDE_GLITCHES.csv')
        Path_IDP_124_CC_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_CC_GLITCHES.csv')

        IDP_124_Y_DEPTH_GLITCHES_Truth = os.path.isfile(Path_IDP_124_Y_DEPTH_GLITCHES)
        IDP_124_Y_AMPLITUDE_GLITCHES_Truth = os.path.isfile(Path_IDP_124_Y_AMPLITUDE_GLITCHES)
        IDP_124_BCE_DEPTH_GLITCHES_Truth = os.path.isfile(Path_IDP_124_BCE_DEPTH_GLITCHES)
        IDP_124_BCE_AMPLITUDE_GLITCHES_Truth = os.path.isfile(Path_IDP_124_BCE_AMPLITUDE_GLITCHES)
        IDP_124_CC_GLITCHES_Truth = os.path.isfile(Path_IDP_124_CC_GLITCHES)
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=120)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t","t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_Y_DEPTH_GLITCHES", "IDP_124_Y_AMPLITUDE_GLITCHES", "IDP_124_BCE_DEPTH_GLITCHES", "IDP_124_BCE_AMPLITUDE_GLITCHES", "IDP_124_CC_GLITCHES"],
                            [str(IDP_124_Y_DEPTH_GLITCHES_Truth), str(IDP_124_Y_AMPLITUDE_GLITCHES_Truth), str(IDP_124_BCE_DEPTH_GLITCHES_Truth), str(IDP_124_BCE_AMPLITUDE_GLITCHES_Truth), str(IDP_124_CC_GLITCHES_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.write("\nReading outputs from Grid Based inference - Individual Frequencies & Surface Independent.\n")
        Path_IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT)
        Path_IDP_124_METADATA_REFMOD_GRID_FREQS = os.path.join(FolderPathForReport, 'IDP_124_METADATA_REFMOD_GRID_FREQS.csv')
        IDP_124_METADATA_REFMOD_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_METADATA_REFMOD_GRID_FREQS)
        """Create texttable object"""
        tableObj4 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT", "IDP_124_METADATA_REFMOD_GRID_FREQS"],
                            [str(IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_METADATA_REFMOD_GRID_FREQS_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.write("\nInput Checks completed.\n")
        file.write("Generating Outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_MASS_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_MASS_GRID_GLITCHES.csv')
        IDP_124_MASS_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_MASS_GRID_GLITCHES, index = False)
        Path_IDP_124_RADIUS_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_GRID_GLITCHES.csv')
        IDP_124_RADIUS_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_RADIUS_GRID_GLITCHES, index = False)
        Path_IDP_124_LOGG_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_GRID_GLITCHES.csv')
        IDP_124_LOGG_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_LOGG_GRID_GLITCHES, index = False)
        Path_IDP_124_AGE_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_GRID_GLITCHES.csv')
        IDP_124_AGE_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_AGE_GRID_GLITCHES, index = False)
        Path_IDP_124_TEFF_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_TEFF_GRID_GLITCHES.csv')
        IDP_124_TEFF_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_TEFF_GRID_GLITCHES, index = False)
        Path_IDP_124_L_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_L_GRID_GLITCHES.csv')
        IDP_124_L_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_L_GRID_GLITCHES, index = False)
        Path_IDP_124_DENSITY_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_GRID_GLITCHES.csv')
        IDP_124_DENSITY_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_GRID_GLITCHES, index = False)
        Path_IDP_124_R_BCE_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_R_BCE_GRID_GLITCHES.csv')
        IDP_124_R_BCE_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_R_BCE_GRID_GLITCHES, index = False)
        Path_IDP_124_Y_S_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_Y_S_GRID_GLITCHES.csv')
        IDP_124_Y_S_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_Y_S_GRID_GLITCHES, index = False)
        Path_IDP_124_METADATA_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_GRID_GLITCHES.csv')
        IDP_124_METADATA_GRID_GLITCHES_out = DP4PQRead.to_csv(Path_IDP_124_METADATA_GRID_GLITCHES, index = False)
        file.write("\nThe Generated Outputs are.\n")
        IDP_124_MASS_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_MASS_GRID_GLITCHES)
        IDP_124_RADIUS_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_RADIUS_GRID_GLITCHES)
        IDP_124_LOGG_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_LOGG_GRID_GLITCHES)
        IDP_124_AGE_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_AGE_GRID_GLITCHES)
        IDP_124_TEFF_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_TEFF_GRID_GLITCHES)
        IDP_124_L_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_L_GRID_GLITCHES)
        IDP_124_DENSITY_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_DENSITY_GRID_GLITCHES)
        IDP_124_R_BCE_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_R_BCE_GRID_GLITCHES)
        IDP_124_Y_S_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_Y_S_GRID_GLITCHES)
        IDP_124_METADATA_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_METADATA_GRID_GLITCHES)
        """Create texttable object"""
        tableObj5 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj5.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj5.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj5.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj5.add_rows([["IDP_124_MASS_GRID_GLITCHES", "IDP_124_RADIUS_GRID_GLITCHES", "IDP_124_LOGG_GRID_GLITCHES",
                            "IDP_124_AGE_GRID_GLITCHES", "IDP_124_TEFF_GRID_GLITCHES", "IDP_124_L_GRID_GLITCHES_Truth",
                            "IDP_124_DENSITY_GRID_GLITCHES", "IDP_124_R_BCE_GRID_GLITCHES", "IDP_124_Y_S_GRID_GLITCHES", "IDP_124_METADATA_GRID_GLITCHES"],
                            [str(IDP_124_MASS_GRID_GLITCHES_Truth), str(IDP_124_RADIUS_GRID_GLITCHES_Truth), str(IDP_124_LOGG_GRID_GLITCHES_Truth),
                            str(IDP_124_AGE_GRID_GLITCHES_Truth), str(IDP_124_TEFF_GRID_GLITCHES_Truth), str(IDP_124_L_GRID_GLITCHES_Truth),
                            str(IDP_124_DENSITY_GRID_GLITCHES_Truth), str(IDP_124_R_BCE_GRID_GLITCHES_Truth), str(IDP_124_Y_S_GRID_GLITCHES_Truth), str(IDP_124_METADATA_GRID_GLITCHES_Truth)]
                            ])
        file.write(tableObj5.draw())
        file.close()
    """The function below performs completion operation from 161st operator of the MSAP5 part1 datapipeline"""
    def End_From_161():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_From_161 = {'End_From_161':['True']}
        Data_End_From_161 = pd.DataFrame(Variable_End_From_161)
        Data_End_From_161.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_From_161.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Inversion was set to False.\n")
        file.close()
    """The function below performs the interpolation operation of the MSAP5 part1 datapipeline"""
    def Interpolation_162():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Interpolation_162 = {'Interpolation_162':['True']}
        Data_Interpolation_162 = pd.DataFrame(Variable_Interpolation_162)
        Data_Interpolation_162.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Interpolation_162.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Interpolation was set to True.\n")
        file.write("Interpolation operation intiated.\n")
        file.write("Reading the following inputs.\n")
        file.write("1 - Interpolation Coefficients.\n")
        file.write("Also reading the following PDP_B inputs.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nGenerating the following output.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_STRUCT_MOD = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_STRUCT_MOD.csv')
        IDP_124_STRUCT_MOD_out = DP4PQRead.to_csv(Path_IDP_124_STRUCT_MOD, index = False)
        IDP_124_STRUCT_MOD_Truth = os.path.isfile(Path_IDP_124_STRUCT_MOD)
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["IDP_124_STRUCT_MOD"],
                            [str(IDP_124_STRUCT_MOD_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs the inversion operation of the MSAP5 part1 datapipeline"""
    def Inversion_163():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Inversion_163 = {'Inversion_163':['True']}
        Data_Inversion_163 = pd.DataFrame(Variable_Inversion_163)
        Data_Inversion_163.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'Inversion_163.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Interpolation was set to False.\n")
        file.write("Inversion intiated.\n")
        file.write("Reading the inputs.\n")
        file.write("Reading the following PDP_B inputs.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nReading the DP3, PDP-C and outputs generated from Interpolation.\n")
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        Path_IDP_124_STRUCT_MOD = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_STRUCT_MOD.csv')
        IDP_124_STRUCT_MOD_Truth = os.path.isfile(Path_IDP_124_STRUCT_MOD)
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_C_122_COVMAT_SAPP", "PDP_C_125_RADIUS_CLASSICAL", "IDP_124_STRUCT_MOD", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(PDP_C_122_COVMAT_SAPP_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(IDP_124_STRUCT_MOD_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nInput read completed.\n")
        file.write("Generating the following outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_DENSITY_INV = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_INV.csv')
        IDP_124_DENSITY_INV_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_INV, index = False)
        Path_IDP_124_ACOUSTIC_RADIUS_INV = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_ACOUSTIC_RADIUS_INV.csv')
        IDP_124_ACOUSTIC_RADIUS_INV_out = DP4PQRead.to_csv(Path_IDP_124_ACOUSTIC_RADIUS_INV, index = False)
        Path_IDP_124_CORE_IND_INV = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_CORE_IND_INV.csv')
        IDP_124_CORE_IND_INV_out = DP4PQRead.to_csv(Path_IDP_124_CORE_IND_INV, index = False)
        Path_IDP_124_ENV_IND_INV = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_ENV_IND_INV.csv')
        IDP_124_ENV_IND_INV_out = DP4PQRead.to_csv(Path_IDP_124_ENV_IND_INV, index = False)

        IDP_124_DENSITY_INV_Truth = os.path.isfile(Path_IDP_124_DENSITY_INV)
        IDP_124_ACOUSTIC_RADIUS_INV_Truth = os.path.isfile(Path_IDP_124_ACOUSTIC_RADIUS_INV)
        IDP_124_CORE_IND_INV_Truth = os.path.isfile(Path_IDP_124_CORE_IND_INV)
        IDP_124_ENV_IND_INV_Truth = os.path.isfile(Path_IDP_124_ENV_IND_INV)
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=100)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_124_DENSITY_INV", "IDP_124_ACOUSTIC_RADIUS_INV", "IDP_124_CORE_IND_INV", "IDP_124_ENV_IND_INV_Truth"],
                            [str(IDP_124_DENSITY_INV_Truth), str(IDP_124_ACOUSTIC_RADIUS_INV_Truth), str(IDP_124_CORE_IND_INV_Truth), str(IDP_124_ENV_IND_INV_Truth)]
                            ])
        file.write(tableObj.draw())
        file.close()
    """The function below performs the Grid based inference with inversion constraints operation of the MSAP5 part1 datapipeline"""
    def GridBasedInferenceWithInversionConstraints_164():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_GridBasedInferenceWithInversionConstraints_164 = {'GridBasedInferenceWithInversionConstraints_164':['True']}
        Data_GridBasedInferenceWithInversionConstraints_164 = pd.DataFrame(Variable_GridBasedInferenceWithInversionConstraints_164)
        Data_GridBasedInferenceWithInversionConstraints_164.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'GridBasedInferenceWithInversionConstraints_164.csv'), header=True, index=False)
        """Write the contents into the third child branch of report specific to MSAP5 part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1_03.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Grid Based Inference with Inversion Constraints initiated\n")
        file.write("Reading the inputs\n")
        file.write("MSAP5-1: DP3 Check Initialized.\n")
        file.write("MSAP5-1: DP3 Files available are as below.\n")
        """If IDP_128_DETECTION_FLAG is False then the following DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_DELTA_NU_AV_METADATA_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
            DP3_128_NU_MAX_METADATA_Truth = "False"
        else:
            DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
            DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """If Peak_Bagging_Flag is False then the following DP3 outputs are not produced"""
        Peak_Bagging_Flag = UserInputsData.loc[0, 'Peak_Bagging_Flag']
        if Peak_Bagging_Flag == "False":
            DP3_128_OSC_FREQ_Truth = "False"
            DP3_128_OSC_FREQ_COVMAT_Truth = "False"
            DP3_128_OSC_FREQ_METADATA_Truth = "False"
        else:
            DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ.csv'))
            DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
            DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=170)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX", "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT", "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth), str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth), str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-1: DP3 Check Completed.\n")
        file.write("MSAP5-1: PDP-C and IDP check.\n")
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        PDP_C_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__M_H__SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122__ALPHA_FE__SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_C_122_COVMAT_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_COVMAT_SAPP.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "PDP_C_122_[M_H]_SAPP", "IDP_122_[M_H]_SAPP", "PDP_C_122_[ALPHA_FE]_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY_Truth", "PDP_C_122_L_SED", "PDP_C_125_RADIUS_CLASSICAL", "PDP_C_122_COVMAT_SAPP"],
                            [str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(PDP_C_122_M_H_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth), str(PDP_C_122_ALPHA_FE_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth), str(PDP_C_122_L_SED_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(PDP_C_122_COVMAT_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-1: PDP-C and IDP Check Completed.\n")
        file.write("MSAP5-1: PDP-B Check Initiated.\n")
        PDP_B_121_MOD_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_OSC_FREQ.csv'))
        PDP_B_121_MOD_EVOL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_B_121_MOD_EVOL.csv'))
        """Create texttable object"""
        tableObj3 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_B_121_MOD_OSC_FREQ", "PDP_B_121_MOD_EVOL"],
                            [str(PDP_B_121_MOD_OSC_FREQ_Truth), str(PDP_B_121_MOD_EVOL_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nMSAP5-1: PDP-B Check Completed.\n")
        file.write("Reading outputs generated from Inversion.\n")

        Path_IDP_124_DENSITY_INV = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_INV.csv')
        Path_IDP_124_ACOUSTIC_RADIUS_INV = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_ACOUSTIC_RADIUS_INV.csv')
        Path_IDP_124_CORE_IND_INV = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_CORE_IND_INV.csv')
        Path_IDP_124_ENV_IND_INV = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_ENV_IND_INV.csv')

        IDP_124_DENSITY_INV_Truth = os.path.isfile(Path_IDP_124_DENSITY_INV)
        IDP_124_ACOUSTIC_RADIUS_INV_Truth = os.path.isfile(Path_IDP_124_ACOUSTIC_RADIUS_INV)
        IDP_124_CORE_IND_INV_Truth = os.path.isfile(Path_IDP_124_CORE_IND_INV)
        IDP_124_ENV_IND_INV_Truth = os.path.isfile(Path_IDP_124_ENV_IND_INV)
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=100)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["IDP_124_DENSITY_INV", "IDP_124_ACOUSTIC_RADIUS_INV", "IDP_124_CORE_IND_INV", "IDP_124_ENV_IND_INV_Truth"],
                            [str(IDP_124_DENSITY_INV_Truth), str(IDP_124_ACOUSTIC_RADIUS_INV_Truth), str(IDP_124_CORE_IND_INV_Truth), str(IDP_124_ENV_IND_INV_Truth)]
                            ])
        file.write(tableObj4.draw())
        Path_IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT.csv')
        IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT_Truth = os.path.isfile(Path_IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT)
        Path_IDP_124_METADATA_REFMOD_GRID_FREQS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_REFMOD_GRID_FREQS.csv')
        IDP_124_METADATA_REFMOD_GRID_FREQS_Truth = os.path.isfile(Path_IDP_124_METADATA_REFMOD_GRID_FREQS)
        Path_IDP_124_METADATA_GRID_GLITCHES = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_GRID_GLITCHES.csv')
        IDP_124_METADATA_GRID_GLITCHES_Truth = os.path.isfile(Path_IDP_124_METADATA_GRID_GLITCHES)
        file.write("\nReading outputs generated from Grid Based Inference - Individual Frequencies, Surface Independent and Glitch.\n")
        """Create texttable object"""
        tableObj5 = texttable.Texttable(max_width=80)
        """Set column alignment (center alignment)"""
        tableObj5.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj5.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj5.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj5.add_rows([["IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT", "IDP_124_METADATA_REFMOD_GRID_FREQS", "IDP_124_METADATA_GRID_GLITCHES"],
                            [str(IDP_124_METADATA_REFMOD_GRID_SURFACE_INDEPENDENT_Truth), str(IDP_124_METADATA_REFMOD_GRID_FREQS_Truth), str(IDP_124_METADATA_GRID_GLITCHES_Truth)]
                            ])
        file.write(tableObj5.draw())
        file.write("\nGenerating the following Outputs.\n")
        DP4PQRead = pd.read_csv(os.path.join(GeneratedOutputs_Path,'Corrected_Light_Curves.csv'))
        """Different Outputs are generated, have a look at the architecture diagram for clear understanding"""
        Path_IDP_124_MASS_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_MASS_INV_GRID.csv')
        IDP_124_MASS_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_MASS_INV_GRID, index = False)
        Path_IDP_124_RADIUS_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_RADIUS_INV_GRID.csv')
        IDP_124_RADIUS_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_RADIUS_INV_GRID, index = False)
        Path_IDP_124_AGE_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_AGE_INV_GRID.csv')
        IDP_124_AGE_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_AGE_INV_GRID, index = False)
        Path_IDP_124_LOGG_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_LOGG_INV_GRID.csv')
        IDP_124_LOGG_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_LOGG_INV_GRID, index = False)
        Path_IDP_124_TEFF_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_TEFF_INV_GRID.csv')
        IDP_124_TEFF_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_TEFF_INV_GRID, index = False)
        Path_IDP_124_L_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_L_INV_GRID.csv')
        IDP_124_L_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_L_INV_GRID, index = False)
        Path_IDP_124_DENSITY_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_DENSITY_INV_GRID.csv')
        IDP_124_DENSITY_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_DENSITY_INV_GRID, index = False)
        Path_IDP_124_Y_S_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_Y_S_INV_GRID.csv')
        IDP_124_Y_S_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_Y_S_INV_GRID, index = False)
        Path_IDP_124_R_BCE_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_R_BCE_INV_GRID.csv')
        IDP_124_R_BCE_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_R_BCE_INV_GRID, index = False)
        Path_IDP_124_METADATA_INV_GRID = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'IDP_124_METADATA_INV_GRID.csv')
        IDP_124_METADATA_INV_GRID_out = DP4PQRead.to_csv(Path_IDP_124_METADATA_INV_GRID, index = False)

        IDP_124_MASS_INV_GRID_Truth = os.path.isfile(Path_IDP_124_MASS_INV_GRID)
        IDP_124_RADIUS_INV_GRID_Truth = os.path.isfile(Path_IDP_124_RADIUS_INV_GRID)
        IDP_124_AGE_INV_GRID_Truth = os.path.isfile(Path_IDP_124_AGE_INV_GRID)
        IDP_124_LOGG_INV_GRID_Truth = os.path.isfile(Path_IDP_124_LOGG_INV_GRID)
        IDP_124_TEFF_INV_GRID_Truth = os.path.isfile(Path_IDP_124_TEFF_INV_GRID)
        IDP_124_L_INV_GRID_Truth = os.path.isfile(Path_IDP_124_L_INV_GRID)
        IDP_124_DENSITY_INV_GRID_Truth = os.path.isfile(Path_IDP_124_DENSITY_INV_GRID)
        IDP_124_Y_S_INV_GRID_Truth = os.path.isfile(Path_IDP_124_Y_S_INV_GRID)
        IDP_124_R_BCE_INV_GRID_Truth = os.path.isfile(Path_IDP_124_R_BCE_INV_GRID)
        IDP_124_METADATA_INV_GRID_Truth = os.path.isfile(Path_IDP_124_METADATA_INV_GRID)

        """Create texttable object"""
        tableObj6 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj6.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj6.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj6.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj6.add_rows([["IDP_124_MASS_INV_GRID", "IDP_124_RADIUS_INV_GRID", "IDP_124_AGE_INV_GRID",
                            "IDP_124_LOGG_INV_GRID", "IDP_124_TEFF_INV_GRID", "IDP_124_L_INV_GRID",
                            "IDP_124_DENSITY_INV_GRID", "IDP_124_Y_S_INV_GRID", "IDP_124_R_BCE_INV_GRID", "IDP_124_METADATA_INV_GRID"],
                            [str(IDP_124_MASS_INV_GRID_Truth), str(IDP_124_RADIUS_INV_GRID_Truth), str(IDP_124_AGE_INV_GRID_Truth),
                            str(IDP_124_LOGG_INV_GRID_Truth), str(IDP_124_TEFF_INV_GRID_Truth), str(IDP_124_L_INV_GRID_Truth),
                            str(IDP_124_DENSITY_INV_GRID_Truth), str(IDP_124_Y_S_INV_GRID_Truth), str(IDP_124_R_BCE_INV_GRID_Truth), str(IDP_124_METADATA_INV_GRID_Truth)]
                            ])
        file.write(tableObj6.draw())
        file.close()
    """The function below performs the end if IDP_123_QUALITY_METADATA flag was set to false operation of the MSAP5 part1 datapipeline"""
    def End_From_MSAP5_1():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_From_MSAP5_1 = {'End_From_MSAP5_1':['True']}
        Data_End_From_MSAP5_1 = pd.DataFrame(Variable_End_From_MSAP5_1)
        Data_End_From_MSAP5_1.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'End_From_MSAP5_1.csv'), header=True, index=False)
        """Writing in the report specific to MSAP5 Part1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part1.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("Individual Frequencies was set to False.\n")
        file.write("IDP_123_QUALITY_METADATA was set to False.\n")
        file.close()

if __name__ == '__Start_MSAP5_1__':
    Start_MSAP5_1()
if __name__ == '__InputsCheck_MSAP5_1__':
    InputsCheck_MSAP5_1()
if __name__ == '__ScalingLaws_12__':
    ScalingLaws_12()
if __name__ == '__ScalingLaws_12_End__':
    ScalingLaws_12_End()
if __name__ == '__GlobalParametersDirectMethod_11__':
    GlobalParametersDirectMethod_11()
if __name__ == '__GlobalParametersDirectMethod_11_End__':
    GlobalParametersDirectMethod_11_End()
if __name__ == '__GridBasedInferenceMixedModes_13__':
    GridBasedInferenceMixedModes_13()
if __name__ == '__GridBasedInferenceIndividualFrequencies_14__':
    GridBasedInferenceIndividualFrequencies_14()
if __name__ == '__GridBasedInferenceSurfaceIndependent_15__':
    GridBasedInferenceSurfaceIndependent_15()
if __name__ == '__End_From13__':
    End_From13()
if __name__ == '__Interpolation_131__':
    Interpolation_131()
if __name__ == '__Inversion_132__':
    Inversion_132()
if __name__ == '__End_From_14_15__':
    End_From_14_15()
if __name__ == '__End_MSAP5_1__':
    End_MSAP5_1()
if __name__ == '__End_From_161__':
    End_From_161()
if __name__ == '__MixedModeFittingwithInversionConstraints_133__':
    MixedModeFittingwithInversionConstraints_133()
if __name__ == '__GridBasedInferenceGlitch_161__':
    GridBasedInferenceGlitch_161()
if __name__ == '__GridBasedInferenceWithInversionConstraints_164__':
    GridBasedInferenceWithInversionConstraints_164()
if __name__ == '__Glitches_16__':
    Glitches_16()
if __name__ == '__Inversion_163__':
    Inversion_163()
if __name__ == '__Interpolation_162__':
    Interpolation_162()
if __name__ == '__End_From_MSAP5_1__':
    End_From_MSAP5_1()
