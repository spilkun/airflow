"""Importing libraries"""
import pandas as pd
import os
from pathlib import Path
from datetime import datetime
"""import texttable to define write Tables in .txt files which is generated as reports"""
import texttable
import Operations
"""Set the home directory path"""
HOME_DIR = str(Path.home())
"""We define the path for the input and output file location"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
FilePaths = pd.read_csv(os.path.join(UserDirectoryPath, 'FilePaths.csv'), delimiter = ',')
InputsPath = FilePaths.loc[0, 'InputsPath']#os.path.join(HOME_DIR, 'userdirectory', 'inputs') #'airflow',
MandatoryInputsPath = os.path.join(InputsPath, 'MandatoryInputs') #'airflow',
NonMandatoryInputsPath = os.path.join(InputsPath, 'NonMandatoryInputs') #'airflow',
GeneratedOutputsPath = FilePaths.loc[0, 'GeneratedOutputs_Path']#os.path.join(HOME_DIR, 'userdirectory', 'output', 'GeneratedOutputs') #'airflow',
"""Reading the name of the overall SAS data pipeline report name which is based on
the date and time at which the data pipeline was run"""
F = open(os.path.join(GeneratedOutputsPath, "ReportID.txt"), "r")
ReportName = F.readline()
F.close()
"""Reading the name of the overall SAS data pipeline folder name which is based on
the date and time at which the data pipeline was run"""
F2 = open(os.path.join(GeneratedOutputsPath, "FolderName.txt"), "r")
FolderName = F2.readline()
F2.close()
"""Defining the path to the folder where all outputs are stored, this is based on
the date and time at which the data pipeline was run"""
FolderPathForReport = os.path.join(GeneratedOutputsPath, FolderName)
"""Defining User inputs file"""
UserInputsFilePath = os.path.join(HOME_DIR, 'userdirectory', 'UserInputs.csv')#'airflow',
UserInputsData = pd.read_csv(UserInputsFilePath, index_col=None)
"""We use the current date to extract the timestamp for the report and also for the foldername"""
now = datetime.now()
dt_string = now.strftime("%B_%d_%Y_%H_%M")
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
"""The function checks if User has chosen Transits to be true or false for a given quarter,
based on this Transits true or tansits false is executed"""
def Transit_Y_N(i):
    TransitFilePath = os.path.join(HOME_DIR, 'userdirectory', 'Transit2.csv')#'airflow',
    TransitData = pd.read_csv(TransitFilePath, index_col=None)
    Transit = TransitData.iloc[i, 0]
    if Transit == True:
        Transits_like_Removal_01(i)
    else:
        Transit_No(i)
"""The function checks if User has chosen Flares to be true or false for a given
quarter, based on this Flares true or Flares false is executed"""
def Flares_Y_N(i):
    FlaresFilePath = os.path.join(HOME_DIR, 'userdirectory', 'Flares2.csv')#'airflow',
    FlaresData = pd.read_csv(FlaresFilePath, index_col=None)
    Flares = FlaresData.iloc[i, 0]
    if Flares == True:
        Flares_like_Removal_02(i)
    else:
        Flares_No(i)
"""If transit is true, the below funtion is executed"""
def Transits_like_Removal_01(i):
    Variable_Transits_like_Removal_01 = {'Transits_like_Removal_01':['True']}
    Data_Transits_like_Removal_01 = pd.DataFrame(Variable_Transits_like_Removal_01)
    Data_Transits_like_Removal_01.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1', 'Transits_like_Removal_01.csv'), header=True, index=False)
    """Reading the inputs for Transit to be true"""
    """For Quarter number == 1, there is no DP1 or DP2 produced"""
    if i == 1:
        DP1_Truth = "False"
        DP2_Truth = "False"
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=140)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c","c","c","c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t","t","t","t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b","b","b","b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_A_122_PREP_OBS_DATA.csv","PDP_A_122_PREP_OBS_METADATA.csv","PDP_C_122_TEFF_SAPP.csv",
        "PDP_C_125_AGE_CLASSICAL.csv","PDP_C_125_MASS_CLASSICAL.csv","PDP_C_125_RADIUS_CLASSICAL.csv"],
                            [str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth)]
                            ])
    else:
        DP1_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP1_Path'], 'DP1.csv'))
        DP2_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP2_Path'], 'DP2.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable()
        """define the number of columns (center alignment)"""
        tableObj.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP1_All_Quarters", "DP2_All_Quarters"],
                            [str(DP1_Truth), str(DP2_Truth)]
                            ])

    file = open(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), "a")
    file.write("\nMSAP1: Transit is True.\n")
    now = datetime.now()
    timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
    file.write("Time Stamp: " + str(timeStamp) + "\n")
    file.write("Preparing for Transit Removal.\n")
    file.write("MSAP1: The inputs available are as below:\n")
    file.write(tableObj.draw())
    file.write("\n")
    file.write("\nMSAP1_Transits_like_Removal_01: Reading the inputs:\n")
    Read_DP1 = pd.read_csv(os.path.join(FilePaths.loc[0, 'DP1_Path'], 'DP1.csv'))
    IDP_128_LC_NT_QI_Name = str('IDP_128_LC_NT_Q')+str(i)+str('.csv')
    IDP_128_LC_NT_QI = Read_DP1.to_csv(os.path.join(FolderPathForReport, 'MSAP1', IDP_128_LC_NT_QI_Name), index = False)
    line = str("MSAP1_Transits_like_Removal_01: Produces IDP_128_LC_NT_Q") + str(i) + str(".csv\n")
    file.write(line)
    file.write("MSAP1_Transits_like_Removal_01: Output available is:\n")
    """Checking for Output"""
    IDP_128_LC_NT_QI_Truth = os.path.isfile(os.path.join(FolderPathForReport, 'MSAP1', IDP_128_LC_NT_QI_Name))
    """Create texttable object"""
    tableObj2 = texttable.Texttable()
    """Set column alignment (center alignment)"""
    tableObj2.set_cols_align(["c"])
    """Set datatype of each column (text type)"""
    tableObj2.set_cols_dtype(["t"])
    """Adjust columns"""
    tableObj2.set_cols_valign(["b"])
    """Insert rows in the table"""
    IDP_128_LC_NT_QI = str("IDP_128_LC_NT_Q") + str(i) + str(".csv")
    tableObj2.add_rows([[IDP_128_LC_NT_QI],
                        [str(IDP_128_LC_NT_QI_Truth)]
                        ])
    """Write the Table in the Report"""
    file.write(tableObj2.draw())
    file.write("\n")
    file.close()

"""If transit is False, the below funtion is executed"""
def Transit_No(i):
    Variable_Transits_like_Removal_01 = {'Transits_like_Removal_01':['False']}
    Data_Transits_like_Removal_01 = pd.DataFrame(Variable_Transits_like_Removal_01)
    Data_Transits_like_Removal_01.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1', 'Transits_like_Removal_01.csv'), header=True, index=False)
    """Reading inputs for Transit False"""
    DP2_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP2_Path'], 'DP2.csv'))
    file = open(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), "a")
    file.write("\nMSAP1: Transit is False.\n")
    now = datetime.now()
    timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
    file.write("Time Stamp: " + str(timeStamp) + "\n")
    file.write("MSAP1: Transit is False because:\n")
    """Create texttable object"""
    tableObj = texttable.Texttable()
    """Set column alignment (center alignment)"""
    tableObj.set_cols_align(["c"])
    """Set datatype of each column (text type)"""
    tableObj.set_cols_dtype(["t"])
    """Adjust columns"""
    tableObj.set_cols_valign(["b"])
    """Insert rows in the table"""
    tableObj.add_rows([["Transit Flag as User Input Parameter"],
                        [str("False")]
                        ])
    """Write the Table in the Report"""
    file.write(tableObj.draw())
    file.write("\n")
    file.close()
"""If Flares is true, the below funtion is executed"""
def Flares_like_Removal_02(i):
    Variable_Flares_like_Removal_02 = {'Flares_like_Removal_02':['True']}
    Data_Flares_like_Removal_02 = pd.DataFrame(Variable_Flares_like_Removal_02)
    Data_Flares_like_Removal_02.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1', 'Flares_like_Removal_02.csv'), header=True, index=False)
    file = open(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), "a")
    file.write("\nMSAP1: Flares is set to True.\n")
    now = datetime.now()
    timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
    file.write("Time Stamp: " + str(timeStamp) + "\n")
    file.write("Preparing for Flares Removal.\n")
    file.write("\nMSAP1_Flares: Reading the inputs.\n")
    IDP_128_LC_NT_QI_Name = str('IDP_128_LC_NT_Q')+str(i)+str('.csv')
    IDP_128_LC_NT_QI_Truth = os.path.isfile(os.path.join(FolderPathForReport, 'MSAP1', IDP_128_LC_NT_QI_Name))
    DP1_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP1_Path'], 'DP1.csv'))
    """If output generated from transit is present then DP1 will not be used"""
    if IDP_128_LC_NT_QI_Truth == "True":
        DP1_Truth == "False"
    """For Quarter number == 1, there is no DP1 or DP4_123_PROT, DP4_123_DELTAPROT or
    DP4_123_PCYCLE, DP4_123_HARVEY1_AMPLITUDE produced"""
    if i == 1:
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=140)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c","c","c","c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t","t","t","t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b","b","b","b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_A_122_PREP_OBS_DATA.csv","PDP_A_122_PREP_OBS_METADATA.csv","PDP_C_122_TEFF_SAPP.csv",
        "PDP_C_125_AGE_CLASSICAL.csv","PDP_C_125_MASS_CLASSICAL.csv","PDP_C_125_RADIUS_CLASSICAL.csv"],
                            [str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth)]
                            ])
    else:
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        Path_DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_HARVEY1_AMPLITUDE.csv'))
        Path_DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_DELTAPROT.csv'))
        Path_DP4_123_PCYCLE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PCYCLE.csv'))
        Path_DP4_123_PROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PROT.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c","c","c","c","c","c","c","c","c","c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t","t","t","t","t","t","t","t","t","t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b","b","b","b","b","b","b","b","b","b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([[IDP_128_LC_NT_QI_Name,"DP1.csv","PDP_A_122_PREP_OBS_DATA.csv","PDP_A_122_PREP_OBS_METADATA.csv","PDP_C_122_TEFF_SAPP.csv",
        "PDP_C_125_AGE_CLASSICAL.csv","PDP_C_125_MASS_CLASSICAL.csv","PDP_C_125_RADIUS_CLASSICAL.csv","DP4_123_HARVEY1_AMPLITUDE.csv","DP4_123_DELTAPROT.csv",
        "DP4_123_PCYCLE.csv", "DP4_123_PROT.csv"],
                            [str(IDP_128_LC_NT_QI_Truth), str(DP1_Truth), str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth),
                            str(Path_DP4_123_HARVEY1_AMPLITUDE_Truth), str(Path_DP4_123_DELTAPROT_Truth), str(Path_DP4_123_PCYCLE_Truth), str(Path_DP4_123_PROT_Truth)]
                            ])
        """Write the Table in the Report"""
    file.write(tableObj.draw())
    file.write("\n")
    IDP_123_LC_NTF_QI = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
    IDP_123_LC_NTF_QI_Name = str("IDP_123_LC_NTF_Q")+str(i)+str(".csv")
    IDP_123_LC_NTF_QI.to_csv(os.path.join(FolderPathForReport,'MSAP1', IDP_123_LC_NTF_QI_Name), index = False)
    ADP_123_FLARES = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
    ADP_123_FLARES.to_csv(os.path.join(FolderPathForReport,'MSAP1','ADP_123_FLARES.csv'), index = False)
    ADP_123_FLARES_METADATA = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
    ADP_123_FLARES_METADATA.to_csv(os.path.join(FolderPathForReport, 'MSAP1','ADP_123_FLARES_METADATA.csv'), index = False)
    line1 = str("\nMSAP1_Flares: Producing Output IDP_123_LC_NTF_Q")+ IDP_123_LC_NTF_QI_Name + str("\n")
    file.write(line1)
    line2 = str("MSAP1_Flares: Produced Output IDP_123_LC_NTF_Q") + IDP_128_LC_NT_QI_Name + str("\n")
    file.write(line2)
    file.write("\nMSAP1_Flares: Producing Output ADP_123_FLARES.csv.\n")
    file.write("MSAP1_Flares: Produced Output ADP_123_FLARES.csv.\n")
    file.write("\nMSAP1_Flares: Producing Output ADP_123_FLARES_METADATA.csv.\n")
    file.write("MSAP1_Flares: Produced Output ADP_123_FLARES_METADATA.csv.\n")
    IDP_123_LC_NTF_QI_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1',IDP_123_LC_NTF_QI_Name))
    ADP_123_FLARES_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1','ADP_123_FLARES.csv'))
    ADP_123_FLARES_METADATA_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1','ADP_123_FLARES_METADATA.csv'))

    tableObj2 = texttable.Texttable()
    """Set column alignment (center alignment)"""
    tableObj2.set_cols_align(["c","c","c"])
    """Set datatype of each column (text type)"""
    tableObj2.set_cols_dtype(["t","t","t"])
    """Adjust columns"""
    tableObj2.set_cols_valign(["b","b","b"])
    """Insert rows in the table"""
    tableObj2.add_rows([[IDP_128_LC_NT_QI_Name,"ADP_123_FLARES.csv","ADP_123_FLARES_METADATA.csv"],
                        [str(IDP_123_LC_NTF_QI_Truth), str(ADP_123_FLARES_Truth), str(ADP_123_FLARES_METADATA_Truth)]
                        ])
    """Write the Table in the Report"""
    file.write(tableObj2.draw())
    file.write("\n")
    file.close()
"""If Flares is False, the below funtion is executed"""
def Flares_No(i):
    Variable_Flares_like_Removal_02 = {'Flares_like_Removal_02':['False']}
    Data_Flares_like_Removal_02 = pd.DataFrame(Variable_Flares_like_Removal_02)
    Data_Flares_like_Removal_02.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1', 'Flares_like_Removal_02.csv'), header=True, index=False)
    file = open(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), "a")
    file.write("\nMSAP1: Flares is set to False.\n")
    now = datetime.now()
    timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
    file.write("Time Stamp: " + str(timeStamp) + "\n")
    file.write("MSAP1: Flares is False because:\n")

    FlaresFilePath = os.path.join(HOME_DIR, 'userdirectory', 'Flares2.csv') #'airflow',
    FlaresData = pd.read_csv(FlaresFilePath, index_col=None)
    Flares = FlaresData.iloc[i-1, 0]
    """Create texttable object"""
    tableObj = texttable.Texttable()
    """Set column alignment (center alignment)"""
    tableObj.set_cols_align(["c"])
    """Set datatype of each column (text type)"""
    tableObj.set_cols_dtype(["t"])
    """Adjust columns"""
    tableObj.set_cols_valign(["b"])
    """Insert rows in the table"""
    tableObj.add_rows([["Flares Flag as User Input Parameter"],
                        [str("FALSE")]
                        ])
    """Write the Table in the Report"""
    file.write(tableObj.draw())
    file.write("\n")
    file.close()
"""Gaps Characterization and Filling funtion is executed"""
def Gaps_Charaterization_and_Filling_03(i):
    Variable_Gaps_Charaterization_and_Filling_03 = {'Gaps_Charaterization_and_Filling_03':['True']}
    Data_Gaps_Charaterization_and_Filling_03 = pd.DataFrame(Variable_Gaps_Charaterization_and_Filling_03)
    Data_Gaps_Charaterization_and_Filling_03.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1', 'Gaps_Charaterization_and_Filling_03.csv'), header=True, index=False)
    file = open(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), "a")
    file.write("\nMSAP1: Gaps Charaterization and Filling.\n")
    now = datetime.now()
    timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
    file.write("Time Stamp: " + str(timeStamp) + "\n")
    file.write("MSAP1: Gaps Charaterization and Filling Reading Inputs.\n")

    IDP_128_LC_NT_QI_Name = str('IDP_128_LC_NT_Q')+str(i)+str('.csv')
    IDP_123_LC_NTF_QI_Name = str("IDP_123_LC_NTF_Q")+str(i)+str(".csv")
    IDP_123_LC_NTF_QI_Truth = os.path.isfile(os.path.join(FolderPathForReport, 'MSAP1', IDP_123_LC_NTF_QI_Name))
    IDP_128_LC_NT_QI_Truth = os.path.isfile(os.path.join(FolderPathForReport, 'MSAP1',IDP_128_LC_NT_QI_Name))
    """If QuarterNumber is 1 then DP1 outputs are not produced"""
    QuarterNumber = UserInputsData.loc[0, 'QuarterNumber']
    if QuarterNumber == 1:
        DP1_Truth = "False"
    else:
        DP1_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP1_Path'], 'DP1.csv'))
    """Create texttable object"""
    tableObj = texttable.Texttable()
    """Set column alignment (center alignment)"""
    tableObj.set_cols_align(["c","c","c"])
    """Set datatype of each column (text type)"""
    tableObj.set_cols_dtype(["t","t","t"])
    """Adjust columns"""
    tableObj.set_cols_valign(["b","b","b"])
    """Insert rows in the table"""
    tableObj.add_rows([[IDP_123_LC_NTF_QI_Name, IDP_128_LC_NT_QI_Name, "DP1.csv"],
                        [str(IDP_123_LC_NTF_QI_Truth), str(IDP_128_LC_NT_QI_Truth), str(DP1_Truth)]
                        ])
    """Write the Table in the Report"""
    file.write(tableObj.draw())
    file.write("\n")
    file.write("\nMSAP1: Producing Output IDP_128_LC_NTF_COMPLETE_QI.\n")
    file.write("MSAP1: Produced Output IDP_128_LC_NTF_COMPLETE_QI.\n")
    IDP_128_LC_NTF_COMPLETE_QI = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
    IDP_128_LC_NTF_COMPLETE_QI.to_csv(os.path.join(FolderPathForReport,'MSAP1','IDP_128_LC_NTF_COMPLETE_QI.csv'), index = False)
    IDP_128_LC_NTF_COMPLETE_QI_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1','IDP_128_LC_NTF_COMPLETE_QI.csv'))
    tableObj2 = texttable.Texttable()
    """Set column alignment (center alignment)"""
    tableObj2.set_cols_align(["c"])
    """Set datatype of each column (text type)"""
    tableObj2.set_cols_dtype(["t"])
    """Adjust columns"""
    tableObj2.set_cols_valign(["b"])
    """Insert rows in the table"""
    tableObj2.add_rows([["IDP_128_LC_NTF_COMPLETE_QI.csv"],
                        [str(IDP_128_LC_NTF_COMPLETE_QI_Truth)]
                        ])
    """Write the Table in the Report"""
    file.write(tableObj2.draw())
    file.write("\n")
    file.close()
    Process_01 = pd.read_csv(os.path.join(FolderPathForReport, 'MSAP1', 'Transits_like_Removal_01.csv')).iloc[0,0]
    Process_02 = pd.read_csv(os.path.join(FolderPathForReport, 'MSAP1', 'Flares_like_Removal_02.csv')).iloc[0,0]
    Main_Report_file = open(os.path.join(FolderPathForReport, ReportName), "a")
    tableObj = texttable.Texttable()
    """Set column alignment (center alignment)"""
    tableObj.set_cols_align(["c","c"])
    """Set datatype of each column (text type)"""
    tableObj.set_cols_dtype(["t","t"])
    """Adjust columns"""
    tableObj.set_cols_valign(["b","b"])
    """Insert rows in the table"""
    tableObj.add_rows([["Transits_like_Removal_01", "Flares_like_Removal_02"],
                        [str(Process_01), str(Process_02)]
                        ])
    """Write the Table in the Report"""
    Main_Report_file.write(tableObj.draw())
    Main_Report_file.close()
    """Transit check, Flares check and Gaps Charaterization & Filling is executed
    for every quarter by executing the above functions described, however the quarter number is
    passed in the below functions described"""
class MSAP1:
    """The class defined is used to decribe the MSAP1 data pipeline, the different functions defined below
    represent the tasks performed in the different operators of the pipeline,
    in the current version of the code, we use these functions to produce a report"""
    def Start_MSAP1():
        """We generate 2 reports one for the overall work flow and one which is specific to MSAP1 data pipeline,
        thus in certain information is repeated in both the reports"""
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Writing the data in the report specific to MSAP1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP1_temp.txt"), "x")
        file.write("\n\n")
        file.write("-----**---**---**-----\n")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP1 is initialized.\n")
        file.close()
        """write the same content in the Overall SAS workflow report"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp.txt'))

    def NonMandatoryInputsCheck_MSAP1():
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        """Writing the data in the temporary report specific to MSAP1"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_A.txt"), "a")
        file.write("\n\nDP4 Inputs Check intialized.\n")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        """Checks for the Non MandatoryInputs - DP4"""
        """If QuarterNumber is 1 then DP4 outputs are not produced"""
        QuarterNumber = UserInputsData.loc[0, 'QuarterNumber']
        if QuarterNumber == 1:
            DP4_123_HARVEY1_AMPLITUDE_Truth = "False"
            DP4_123_DELTAPROT_Truth = "False"
            DP4_123_PCYCLE_Truth = "False"
            DP4_123_PROT_Truth = "False"
        else:
            DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_HARVEY1_AMPLITUDE.csv'))
            DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_DELTAPROT.csv'))
            DP4_123_PCYCLE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PCYCLE.csv'))
            DP4_123_PROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PROT.csv'))
        file.write("\nMSAP1: Reading DP4 inputs from Quarter 1.\n")
        if QuarterNumber != 1:
            if DP4_123_HARVEY1_AMPLITUDE_Truth ==True:
                file.write("MSAP1: DP4_123_HARVEY1_AMPLITUDE file exists.\n")
            else:
                file.write("MSAP1: DP4_123_HARVEY1_AMPLITUDE file does not exist.\n")
            if DP4_123_DELTAPROT_Truth==True:
                file.write("MSAP1: DP4_123_DELTAPROT file exists.\n")
            else:
                file.write("MSAP1: DP4_123_DELTAPROT file does not exist.\n")
            if DP4_123_PCYCLE_Truth ==True:
                file.write("MSAP1: DP4_123_PCYCLE file exists.\n")
            else:
                file.write("MSAP1: DP4_123_PCYCLE file does not exist.\n")
            if DP4_123_PROT_Truth ==True:
                file.write("MSAP1: DP4_123_PROT file exists.\n")
            else:
                file.write("MSAP1: DP4_123_PROT file does not exist.\n")
        file.write("\nDP4 Inputs check completed.\n")
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=90)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_HARVEY1_AMPLITUDE", "DP4_123_DELTAPROT", "DP4_123_PCYCLE", "DP4_123_PROT"],
                            [str(DP4_123_HARVEY1_AMPLITUDE_Truth), str(DP4_123_DELTAPROT_Truth), str(DP4_123_PCYCLE_Truth), str(DP4_123_PROT_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj.draw())
        file.close()
        """Writing the data in the report specific to MSAP1"""
        """write the content in the Overall SAS workflow report"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_A.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_A.txt'))

    def DP1Check_MSAP1():
        """Writing the data in the report specific to MSAP1 and Overall SAS workflow"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_B.txt"), "a")
        file.write("\n\n\nDP1 Check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        """If Mandatory inputs are not present then raise Airflow exceptions to quit the data pipeline"""

        """If QuarterNumber is 1 then DP1 outputs are not produced"""
        QuarterNumber = UserInputsData.loc[0, 'QuarterNumber']
        DP1_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP1_Path'], 'DP1.csv'))
        if DP1_Truth==True:
            file.write("MSAP1: DP1.csv file exists.\n")
        else:
            file.write("MSAP1: DP1.csv file does not exist, data pipeline quit.\n")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_B.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_B.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_B.txt'))
            raise ValueError("MSAP1: DP1.csv file does not exist, data pipeline quit.\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_B.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_B.txt'))

    def DP2Check_MSAP1():
        """Writing the data in the report specific to MSAP1 and Overall SAS workflow"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_C.txt"), "x")
        file.write("\n\nDP2 Check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        DP2_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP2_Path'], 'DP2.csv'))
        """If QuarterNumber is 1 then DP1 outputs are not produced"""
        QuarterNumber = UserInputsData.loc[0, 'QuarterNumber']
        if QuarterNumber == 1:
            DP2_Truth = "False"
        if os.path.isfile(os.path.join(FilePaths.loc[0, 'DP2_Path'], 'DP2.csv'))==True:
            file.write("MSAP1: DP2.csv file exists.\n")
            file.close()
        else:
            file.write("MSAP1: DP2.csv file does not exist.\n")
            file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_C.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_C.txt'))
    def PreparatoryDataCheck_MSAP1():
        """Writing the data in the report specific to MSAP1 and Overall SAS workflow"""
        """Checks for the MandatoryInputs - PDP-A and PDP-C"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"), "x")
        file.write("\n\nPreparatory data Inputs Check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")

        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        """If Mandatory inputs are not present then raise Airflow exceptions to quit the data pipeline"""
        if PDP_A_122_PREP_OBS_DATA_Truth ==True:
            file.write("MSAP1: PDP_A_122_PREP_OBS_DATA file exists.\n")
        else:
            file.write("MSAP1: PDP_A_122_PREP_OBS_DATA file does not exist, data pipeline quit.\n")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_D.txt'))
            raise ValueError("MSAP1: PDP_A_122_PREP_OBS_DATA file does not exist, data pipeline quit.")
        if PDP_A_122_PREP_OBS_METADATA_Truth ==True:
            file.write("MSAP1: PDP_A_122_PREP_OBS_METADATA file exists.\n")
        else:
            file.write("MSAP1: PDP_A_122_PREP_OBS_METADATA file does not exist, data pipeline quit.\n")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_D.txt'))
            raise ValueError("MSAP1: PDP_A_122_PREP_OBS_METADATA file does not exist, data pipeline quit.")
        if PDP_C_122_TEFF_SAPP_Truth ==True:
            file.write("MSAP1: PDP_C_122_TEFF_SAPP file exists.\n")
        else:
            file.write("MSAP1: PDP_C_122_TEFF_SAPP file does not exist, data pipeline quit.\n")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_D.txt'))
            raise ValueError("MSAP1: PDP_C_122_TEFF_SAPP file does not exist, data pipeline quit.")
        if PDP_C_125_AGE_CLASSICAL_Truth ==True:
            file.write("MSAP1: PDP_C_125_AGE_CLASSICAL file exists.\n")
        else:
            file.write("MSAP1: PDP_C_125_AGE_CLASSICAL file does not exist, data pipeline quit.\n")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_D.txt'))
            raise ValueError("MSAP1: PDP_C_125_AGE_CLASSICAL file does not exist, data pipeline quit.")
        if PDP_C_125_MASS_CLASSICAL_Truth==True:
            file.write("MSAP1: PDP_C_125_MASS_CLASSICAL file exists.\n")
        else:
            file.write("MSAP1: PDP_C_125_MASS_CLASSICAL file does not exist, data pipeline quit.\n")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_D.txt'))
            raise ValueError("MSAP1: PDP_C_125_MASS_CLASSICAL file does not exist, data pipeline quit.")
        if os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))==True:
            file.write("MSAP1: PDP_C_125_RADIUS_CLASSICAL file exists.\n")
        else:
            file.write("MSAP1: PDP_C_125_RADIUS_CLASSICAL file does not exist, data pipeline quit.\n")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_D.txt'))
            raise ValueError("MSAP1: PDP_C_125_RADIUS_CLASSICAL file does not exist, data pipeline quit.")
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=130)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_A_122_PREP_OBS_DATA", "PDP_A_122_PREP_OBS_METADATA", "PDP_C_122_TEFF_SAPP",
                            "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL", "PDP_C_125_RADIUS_CLASSICAL"],
                            [str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth), str(PDP_C_122_TEFF_SAPP_Truth),
                            str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth)]
                            ])
        """Write the Table in the Report"""
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp_Branch_D.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp_Branch_D.txt'))

    def InputCheck_MSAP1():
        """Once the input check is completed it prints the message in the report"""
        """Write the contents on the main report file too"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP1_temp.txt"), "x")
        file.write("\n\nMSAP1: Input Check Completed.\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_MSAP1_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp.txt'))

    def MSAP1_01_02_03_Looped():
        """The function runs in a loop for a given quarter chosen by the user,
        for each quarter it checks for transit removal, flares removal, and Gaps Characterization & filling,
        based on the user input it runs for True and False"""
        UserInputFilePath = os.path.join(HOME_DIR, 'userdirectory', 'UserInputs.csv') #'airflow',
        UserInputData = pd.read_csv(UserInputFilePath, index_col=None)
        """Read the Quarter number to produce the Flares.csv"""
        Quarter = UserInputData.loc[0, 'QuarterNumber']
        Quarter = int(Quarter)
        """Reading Flares User Input Data"""
        FlaresDataTrueFalsePath = os.path.join(HOME_DIR, 'userdirectory', 'Flares2.csv')
        """Check if Flares.csv is empty (This happens if for none of the quarters Flares was set to True)"""
        if os.stat(os.path.join(HOME_DIR, 'userdirectory', 'Flares.csv')).st_size == 0:
            FlaresDataTrueFalse = pd.DataFrame({'FlareQuarterSelectionData':['False']})
            FlaresDataTrueFalse.to_csv(path_or_buf = FlaresDataTrueFalsePath, header=True, index=False)
        else:
            FlareQuarterSelect = pd.read_csv(os.path.join(HOME_DIR, 'userdirectory', 'Flares.csv'), index_col=None, header=None, sep=",")
            """Create Flares True False Data"""
            FlaresDataTrueFalse = []
            for i in range(0,Quarter+1):
                FlaresDataTrueFalse.append("False")
            for i in range(len(FlareQuarterSelect)):
                FlaresDataTrueFalse[int(FlareQuarterSelect.iloc[i, 0])] = "True"
            FlaresDataTrueFalse = {'FlareQuarterSelectionData':FlaresDataTrueFalse}
            FlaresDataTrueFalseTable = pd.DataFrame(FlaresDataTrueFalse)
            FlaresDataTrueFalseTable.to_csv(path_or_buf = FlaresDataTrueFalsePath, header=True, index=False)
        """Reading Transit User Input Data"""
        TransitDataTrueFalsePath = os.path.join(HOME_DIR, 'userdirectory', 'Transit2.csv')
        """Check if Transit.csv is empty (This happens if for none of the quarters Transit was set to True)"""
        if os.stat(os.path.join(HOME_DIR, 'userdirectory', 'Transit.csv')). st_size == 0:
            TransitDataTrueFalse = pd.DataFrame({'TransitQuarterSelectionData':['False']})
            TransitDataTrueFalse.to_csv(path_or_buf = TransitDataTrueFalsePath, header=True, index=False)
        else:
            TransitQuarterSelect = pd.read_csv(os.path.join(HOME_DIR, 'userdirectory', 'Transit.csv'), index_col=None, header=None)
            """Create Transit True False Data"""
            TransitDataTrueFalse = []
            for i in range(0,Quarter+1):
                TransitDataTrueFalse.append("False")
            for i in range(len(TransitQuarterSelect)):
                TransitDataTrueFalse[int(TransitQuarterSelect.iloc[i, 0])] = "True"
            TransitDataTrueFalse = {'TransitQuarterSelectionData':TransitDataTrueFalse}
            TransitDataTrueFalseTable = pd.DataFrame(TransitDataTrueFalse)
            TransitDataTrueFalseTable.to_csv(path_or_buf = TransitDataTrueFalsePath, header=True, index=False)
        """For every quarter we will perform Transit Check, Quarter Check and Gaps Characterization & Filling using a FOR loop"""
        for i in range(1, Quarter+1):
            now = datetime.now()
            timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
            file = open(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), "a")
            file.write("\nTime Stamp: " + str(timeStamp) + "\n")
            file.write("MSAP1: The Loop is run for Quarter Number: " + str(i) + ".\n")
            file.write("\n")
            file.close()
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP1.txt"))
            #Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'Report_MSAP1_temp.txt'))
            """The If part of the loop below is used if Flares/Transits are not selected in the UI"""
            if os.stat(os.path.join(HOME_DIR, 'userdirectory', 'Transit.csv')).st_size == 0 or os.stat(os.path.join(HOME_DIR, 'userdirectory', 'Flares.csv')).st_size == 0:
                Transit_Y_N(0)
                Flares_Y_N(0)
                Gaps_Charaterization_and_Filling_03(0)
            else:
                if(i >= 1):
                    Transit_Y_N(i)
                    Flares_Y_N(i)
                    Gaps_Charaterization_and_Filling_03(i)
    """The function below performs Stitching 1"""
    def Stitching1_05():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Stitching1_05 = {'Stitching1_05':['True']}
        Data_Stitching1_05 = pd.DataFrame(Variable_Stitching1_05)
        Data_Stitching1_05.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1', 'Stitching1_05.csv'), header=True, index=False)
        """Whenever 2 functions are executed parallely in the data pipeline, we write
        the results in seperate reports, and then put them together in its parent report
        this is done below:
        Report_Stitching1_05.txt is child report
        Report_MSAP1.txt is parent report"""
        file = open(os.path.join(FolderPathForReport, "Report_Stitching1_05.txt"), "a")
        file.write("\nMSAP1: Stictching1 is Initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP1_Stictching1: Reading Inputs.\n")
        file.write("MSAP1_Stictching1: IDP_128_LC_NTF_COMPLETE_QI exists.\n")
        IDP_128_LC_NTF_COMPLETE_QI_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1','IDP_128_LC_NTF_COMPLETE_QI.csv'))
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_LC_NTF_COMPLETE_QI.csv"],
                            [str(IDP_128_LC_NTF_COMPLETE_QI_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj.draw())
        file.write("\n")
        file.write("\nMSAP1_Stictching1: Producing IDP_128_LC_STITCHED.\n")

        IDP_128_LC_STITCHED = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
        IDP_128_LC_STITCHED.to_csv(os.path.join(FolderPathForReport, 'MSAP1', 'IDP_128_LC_STITCHED.csv'), index = False)
        IDP_128_LC_STITCHED_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_128_LC_STITCHED.csv'))

        file.write("MSAP1_Stictching1: IDP_128_LC_STITCHED exists.\n")
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_128_LC_STITCHED.csv"],
                            [str(IDP_128_LC_STITCHED_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj2.draw())
        file.write("\n")
        file.close()

    """The function below executes KASOC Filter"""
    def KASOCFilter_07():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_KASOCFilter_07 = {'KASOCFilter_07':['True']}
        Data_KASOCFilter_07 = pd.DataFrame(Variable_KASOCFilter_07)
        Data_KASOCFilter_07.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1', 'KASOCFilter_07.csv'), header=True, index=False)
        """Whenever 2 functions are executed parallely in the data pipeline, we write
        the results in seperate reports, and then put them together in its parent report
        this is done below:
        Report_Stitching1_05.txt is child report
        Report_MSAP1.txt is parent report"""
        file = open(os.path.join(FolderPathForReport, "Report_Stitching1_05.txt"), "a")
        file.write("\nMSAP1: KASOC Filter is Initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP1_KASOCFilter_07: Reading Inputs.\n")
        file.write("MSAP1_KASOCFilter_07: IDP_128_LC_STITCHED.csv exists.\n")
        IDP_128_LC_STITCHED_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_128_LC_STITCHED.csv'))
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_LC_STITCHED.csv"],
                            [str(IDP_128_LC_STITCHED_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj.draw())
        file.write("\nMSAP1_KASOCFilter_07: Producing IDP_128_AARLC.csv.\n")
        file.write("MSAP1_KASOCFilter_07: Producing IDP_128_AARLC_METADATA.csv.\n")
        IDP_128_AARLC = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
        IDP_128_AARLC_METADATA = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
        IDP_128_AARLC.to_csv(os.path.join(FolderPathForReport,'MSAP1', 'IDP_128_AARLC.csv'), index = False)
        IDP_128_AARLC_METADATA.to_csv(os.path.join(FolderPathForReport, 'MSAP1', 'IDP_128_AARLC_METADATA.csv'), index = False)
        IDP_128_AARLC_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_128_AARLC.csv'))
        IDP_128_AARLC_METADATA_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_128_AARLC_METADATA.csv'))
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_128_AARLC.csv", "IDP_128_AARLC_METADATA.csv"],
                            [str(IDP_128_AARLC_Truth), str(IDP_128_AARLC_METADATA_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj2.draw())
        file.write("\n")
        file.close()
        """Writing the details of Outputs produced from KASOC Filter into the Overall SAS workflow Report,
        we do this because KASOC filter produces DP outputs"""
        Overall_Report_file = open(os.path.join(FolderPathForReport, ReportName), "a")
        Overall_Report_file.write("\nMSAP1: Implementation of KASOC Filter in MSAP1, produces the following outputs:")
        Overall_Report_file.write("\n")
        Overall_Report_file.write(tableObj2.draw())
        Overall_Report_file.close()
        """Writing the details from child report - Report_Stitching1_05.txt to the parent report Report_MSAP1.txt"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), os.path.join(FolderPathForReport, "Report_Stitching1_05.txt"))

    """The function below executes Stitching2"""
    def Stitching2_06():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Stitching2_06 = {'Stitching2_06':['True']}
        Data_Stitching2_06 = pd.DataFrame(Variable_Stitching2_06)
        Data_Stitching2_06.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1', 'Stitching2_06.csv'), header=True, index=False)
        """Whenever 2 functions are executed parallely in the data pipeline, we write
        the results in seperate reports, and then put them together in its parent report
        this is done below:
        Report_Stitching1_06.txt is child report
        Report_MSAP1.txt is parent report"""
        file = open(os.path.join(FolderPathForReport, "Report_Stitching2_06.txt"), "a")
        file.write("\n\nMSAP1: Stictching2 is Initiated.\n")
        file.write("MSAP1_Stictching2: Reading Inputs.\n")
        file.write("MSAP1_Stictching2: IDP_128_LC_NTF_COMPLETE_QI exists.\n")
        file.write("MSAP1_Stictching2: DP4_123_HARVEY1_AMPLITUDE exists.\n")
        file.write("MSAP1_Stictching2: DP4_123_DELTAPROT exists.\n")
        file.write("MSAP1_Stictching2: DP4_123_PCYCLE exists.\n")
        file.write("MSAP1_Stictching2: DP4_123_PROT exists.\n")
        IDP_128_LC_NTF_COMPLETE_QI_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1','IDP_128_LC_NTF_COMPLETE_QI.csv'))
        """If QuarterNumber is 1 then DP4 outputs are not produced"""
        QuarterNumber = UserInputsData.loc[0, 'QuarterNumber']
        if QuarterNumber == 1:
            DP4_123_HARVEY1_AMPLITUDE_Truth = "False"
            DP4_123_DELTAPROT_Truth = "False"
            DP4_123_PCYCLE_Truth = "False"
            DP4_123_PROT_Truth = "False"
        else:
            DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_HARVEY1_AMPLITUDE.csv'))
            DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_DELTAPROT.csv'))
            DP4_123_PCYCLE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PCYCLE.csv'))
            DP4_123_PROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PROT.csv'))
        tableObj = texttable.Texttable(max_width=120)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c","c","c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t","t","t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b","b","b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_LC_NTF_COMPLETE_QI.csv", "DP4_123_HARVEY1_AMPLITUDE.csv", "DP4_123_DELTAPROT.csv", "DP4_123_PCYCLE.csv", "DP4_123_PROT.csv"],
                            [str(IDP_128_LC_NTF_COMPLETE_QI_Truth), str(DP4_123_HARVEY1_AMPLITUDE_Truth), str(DP4_123_DELTAPROT_Truth), str(DP4_123_PCYCLE_Truth), str(DP4_123_PROT_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj.draw())
        file.write("\n")
        file.write("\nMSAP1_Stictching2: Producing IDP_128_LC_STITCHED2.csv.\n")
        IDP_128_LC_STITCHED2 = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
        IDP_128_LC_STITCHED2.to_csv(os.path.join(FolderPathForReport,'MSAP1','IDP_128_LC_STITCHED2.csv'), index = False)
        IDP_128_LC_STITCHED2_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_128_LC_STITCHED2.csv'))
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_128_LC_STITCHED2.csv"],
                            [str(IDP_128_LC_STITCHED2_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj2.draw())
        file.write("\n")
        file.close()

    """The function below executes Detrending of Instrumental Residual Effects"""
    def Detrending_Of_Instrumental_Residual_Effects_04():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Detrending_Of_Instrumental_Residual_Effects_04 = {'Detrending_Of_Instrumental_Residual_Effects_04':['True']}
        Data_Detrending_Of_Instrumental_Residual_Effects_04 = pd.DataFrame(Variable_Detrending_Of_Instrumental_Residual_Effects_04)
        Data_Detrending_Of_Instrumental_Residual_Effects_04.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1','Detrending_Of_Instrumental_Residual_Effects_04.csv'), header=True, index=False)
        """Whenever 2 functions are executed parallely in the data pipeline, we write
        the results in seperate reports, and then put them together in its parent report
        this is done below:
        Report_Stitching1_06.txt is child report
        Report_MSAP1.txt is parent report"""
        file = open(os.path.join(FolderPathForReport, "Report_Stitching2_06.txt"), "a")
        file.write("\nMSAP1: Detrending Of Instrumental Residual Effects is Initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04 reading inputs.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: DP4_123_HARVEY1_AMPLITUDE exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: DP4_123_DELTAPROT exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: DP4_123_PCYCLE exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: DP4_123_PROT exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: PDP_A_122_PREP_OBS_DATA exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: PDP_A_122_PREP_OBS_METADATA exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: PDP_C_122_TEFF_SAPP exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: PDP_C_125_AGE_CLASSICAL exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: PDP_C_125_MASS_CLASSICAL exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: PDP_C_125_RADIUS_CLASSICAL exists.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: IDP_128_LC_STITCHED2 exists.\n")
        """If QuarterNumber is 1 then DP4 outputs are not produced"""
        QuarterNumber = UserInputsData.loc[0, 'QuarterNumber']
        if QuarterNumber == 1:
            DP4_123_HARVEY1_AMPLITUDE_Truth = "False"
            DP4_123_DELTAPROT_Truth = "False"
            DP4_123_PCYCLE_Truth = "False"
            DP4_123_PROT_Truth = "False"
        else:
            DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_HARVEY1_AMPLITUDE.csv'))
            DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_DELTAPROT.csv'))
            DP4_123_PCYCLE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PCYCLE.csv'))
            DP4_123_PROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PROT.csv'))
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        IDP_128_LC_STITCHED2_Truth = os.path.isfile(os.path.join(FolderPathForReport, 'MSAP1', 'IDP_128_LC_STITCHED2.csv'))

        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c","c","c","c","c","c","c","c","c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t","t","t","t","t","t","t","t","t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b","b","b","b","b","b","b","b","b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_HARVEY1_AMPLITUDE", "DP4_123_DELTAPROT", "DP4_123_PCYCLE",
                            "DP4_123_PROT", "PDP_A_122_PREP_OBS_DATA", "PDP_A_122_PREP_OBS_METADATA",
                            "PDP_C_122_TEFF_SAPP", "PDP_C_125_AGE_CLASSICAL", "PDP_C_125_MASS_CLASSICAL",
                            "PDP_C_125_RADIUS_CLASSICAL", "IDP_128_LC_STITCHED2"],
                            [str(DP4_123_HARVEY1_AMPLITUDE_Truth), str(DP4_123_DELTAPROT_Truth), str(DP4_123_PCYCLE_Truth),
                            str(DP4_123_PROT_Truth), str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth),
                            str(PDP_C_122_TEFF_SAPP_Truth), str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth),
                            str(PDP_C_125_RADIUS_CLASSICAL_Truth), str(IDP_128_LC_STITCHED2_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj.draw())
        file.write("\n")
        file.write("\nMSAP1_Detrending_Of_Instrumental_Residual_Effects_04: Producing IDP_123_VARLC.csv.\n")
        file.write("MSAP1_Detrending_Of_Instrumental_Residual_Effects_04: Producing IDP_123_VARLC_METADATA.csv.\n")
        IDP_123_VARLC = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
        IDP_123_VARLC.to_csv(os.path.join(FolderPathForReport,'MSAP1','IDP_123_VARLC.csv'), index = False)
        IDP_123_VARLC_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_123_VARLC.csv'))
        IDP_123_VARLC_METADATA = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
        IDP_123_VARLC_METADATA.to_csv(os.path.join(FolderPathForReport, 'MSAP1','IDP_123_VARLC_METADATA.csv'), index = False)
        IDP_123_VARLC_METADATA_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_123_VARLC_METADATA.csv'))
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c","c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t","t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b","b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_123_VARLC", "IDP_123_VARLC_METADATA"],
                            [str(IDP_123_VARLC_Truth), str(IDP_123_VARLC_METADATA_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj2.draw())
        file.write("\n")
        file.close()

    """The function below executes Binning"""
    def Binning_08():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Binning_08 = {'Binning_08':['True']}
        Data_Binning_08 = pd.DataFrame(Variable_Binning_08)
        Data_Binning_08.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP1','Binning_08.csv'), header=True, index=False)
        """Whenever 2 functions are executed parallely in the data pipeline, we write
        the results in seperate reports, and then put them together in its parent report
        this is done below:
        Report_Stitching1_06.txt is child report
        Report_MSAP1.txt is parent report"""
        file = open(os.path.join(FolderPathForReport, "Report_Stitching2_06.txt"), "a")
        file.write("\nMSAP1: Binning_08 is Initiated.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP1_Binning_08 reading inputs.\n")
        file.write("MSAP1_Binning_08 reading inputs.\n")
        file.write("MSAP1_Binning_08: PDP_A_122_PREP_OBS_DATA exists.\n")
        file.write("MSAP1_Binning_08: PDP_A_122_PREP_OBS_METADATA exists.\n")
        file.write("MSAP1_Binning_08: PDP_C_122_TEFF_SAPP exists.\n")
        file.write("MSAP1_Binning_08: PDP_C_125_AGE_CLASSICAL exists.\n")
        file.write("MSAP1_Binning_08: PDP_C_125_MASS_CLASSICAL exists.\n")
        file.write("MSAP1_Binning_08: PDP_C_125_RADIUS_CLASSICAL exists.\n")
        file.write("MSAP1_Binning_08: IDP_123_VARLC exists.\n")
        file.write("MSAP1_Binning_08: IDP_123_VARLC_METADATA exists.\n")

        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        PDP_C_125_AGE_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_AGE_CLASSICAL.csv'))
        PDP_C_125_MASS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_MASS_CLASSICAL.csv'))
        PDP_C_125_RADIUS_CLASSICAL_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'PDP_C_125_RADIUS_CLASSICAL.csv'))
        IDP_123_VARLC_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_123_VARLC.csv'))
        IDP_123_VARLC_METADATA_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_123_VARLC_METADATA.csv'))
        """If QuarterNumber is 1 then DP4 outputs are not produced"""
        QuarterNumber = UserInputsData.loc[0, 'QuarterNumber']
        if QuarterNumber == 1:
            DP4_123_DELTAPROT_Truth = "False"
            DP4_123_PROT_Truth = "False"
        else:
            DP4_123_PROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_PROT.csv'))
            DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'DP4_Path'], 'DP4_123_DELTAPROT.csv'))

        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c","c","c","c","c","c","c","c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t","t","t","t","t","t","t","t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b","b","b","b","b","b","b","b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_A_122_PREP_OBS_DATA", "PDP_A_122_PREP_OBS_METADATA.csv", "PDP_C_122_TEFF_SAPP.csv",
                            "PDP_C_125_AGE_CLASSICAL.csv", "PDP_C_125_MASS_CLASSICAL.csv", "PDP_C_125_RADIUS_CLASSICAL.csv",
                            "IDP_123_VARLC.csv", "IDP_123_VARLC_METADATA.csv", "DP4_123_PROT.csv",
                            "DP4_123_DELTAPROT.csv"],
                            [str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth), str(PDP_C_122_TEFF_SAPP_Truth),
                            str(PDP_C_125_AGE_CLASSICAL_Truth), str(PDP_C_125_MASS_CLASSICAL_Truth), str(PDP_C_125_RADIUS_CLASSICAL_Truth),
                            str(IDP_123_VARLC_Truth), str(IDP_123_VARLC_METADATA_Truth), str(DP4_123_PROT_Truth),
                            str(DP4_123_DELTAPROT_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj.draw())
        file.write("\n")
        file.write("\nMSAP1_Binning_08: Producing IDP_123_VARLC_BINNED.csv.\n")
        file.write("MSAP1_Binning_08: Producing IDP_123_VARLC_BINNED_METADATA.csv.\n")
        IDP_123_VARLC_BINNED = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
        IDP_123_VARLC_BINNED.to_csv(os.path.join(FolderPathForReport, 'MSAP1', 'IDP_123_VARLC_BINNED.csv'), index = False)
        IDP_123_VARLC_BINNED_METADATA = pd.read_csv(os.path.join(MandatoryInputsPath, 'DP1','DP1.csv'))
        IDP_123_VARLC_BINNED_METADATA.to_csv(os.path.join(FolderPathForReport,'MSAP1', 'IDP_123_VARLC_BINNED_METADATA.csv'), index = False)
        IDP_123_VARLC_BINNED_METADATA_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_123_VARLC_BINNED_METADATA.csv'))
        IDP_123_VARLC_BINNED_Truth = os.path.isfile(os.path.join(FolderPathForReport,'MSAP1', 'IDP_123_VARLC_METADATA.csv'))
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c","c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t","t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b","b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_123_VARLC_BINNED.csv", "IDP_123_VARLC_BINNED_METADATA.csv"],
                            [str(IDP_123_VARLC_BINNED_Truth), str(IDP_123_VARLC_BINNED_METADATA_Truth)]
                            ])
        """Write the Table in the Report"""
        file.write(tableObj2.draw())
        file.write("\n")
        file.close()
        """Writing the details of Outputs produced from Binning into the Overall Report because the IDP's generated is
        used in the next MSAP's"""
        Overall_Report_file = open(os.path.join(FolderPathForReport, ReportName), "a")
        Overall_Report_file.write("\nMSAP1: Implementation of Binning in MSAP1, produces the following outputs:")
        Overall_Report_file.write("\n")
        Overall_Report_file.write(tableObj2.draw())
        Overall_Report_file.close()
        """The function below executes Removes the seperate reports for Stitching1 and Stitching2 and copies the information into the main report"""
        with open(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), "a") as Main_Report_file, open(os.path.join(FolderPathForReport, "Report_Stitching2_06.txt")) as f:
            f1_lines = f.readlines()
            Main_Report_file.write(f1_lines[0].strip())
            Main_Report_file.writelines(f1_lines[1:])
        Main_Report_file.close()

    def End():
        """For each function that was described above, we check if that function was run or not by reading the respective .csv files,
        we later delete these files"""
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'MSAP1', 'Transits_like_Removal_01.csv'))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'MSAP1', 'Flares_like_Removal_02.csv'))
        Process_03 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP1',  'Gaps_Charaterization_and_Filling_03.csv'))
        Process_04 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP1', 'Detrending_Of_Instrumental_Residual_Effects_04.csv'))
        Process_05 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport,'MSAP1', 'Stitching1_05.csv'))
        Process_06 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport,'MSAP1', 'Stitching2_06.csv'))
        Process_07 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP1', 'KASOCFilter_07.csv'))
        Process_08 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP1', 'Binning_08.csv'))
        file = open(os.path.join(FolderPathForReport, "Report_MSAP1.txt"), "a")
        file.write("\nMSAP1 Completed.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("-----**-----")
        file.close()
        """We remove the reports that were generated when the data pipelines runs operators parallely, if they exist"""
        os.remove(os.path.join(FolderPathForReport, "Report_Stitching2_06.txt"))
        os.remove(os.path.join(FolderPathForReport, "Report_Stitching1_05.txt"))

        """Writing the operations summary on Main report"""
        Main_Report_file = open(os.path.join(FolderPathForReport, ReportName), "a")
        tableObj = texttable.Texttable(max_width=130)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c", "c","c", "c","c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t", "t","t", "t","t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b", "b","b", "b","b"])
        """Insert rows in the table"""
        tableObj.add_rows([["Gaps_Charaterization_and_Filling_03", "Stitching1_05",
                            "KASOCFilter_07", "Stitching2_06",
                            "Detrending_Of_Instrumental_Residual_Effects_04", "Binning_08"],
                            [str(Process_03), str(Process_05),
                            str(Process_07), str(Process_06),
                            str(Process_04), str(Process_08)]
                            ])
        """Write the Table in the Report"""
        Main_Report_file.write("\n")
        Main_Report_file.write(tableObj.draw())
        Main_Report_file.write("\nMSAP1 completed.\n")
        Main_Report_file.write("\n---**---MSAP1-END---**---\n")
        Main_Report_file.close()
if __name__ == '__Start_MSAP1__':
    Start_MSAP1()
if __name__ == '__NonMandatoryInputsCheck_MSAP1__':
    NonMandatoryInputsCheck_MSAP1()
if __name__ == '__DP1Check_MSAP1__':
    DP1Check_MSAP1()
if __name__ == '__DP2Check_MSAP1__':
    DP1Check_MSAP1()
if __name__ == '__PreparatoryDataCheck_MSAP1__':
    PreparatoryDataCheck_MSAP1()
if __name__ == '__InputCheck_MSAP1__':
    InputCheck_MSAP1()
if __name__ == '__Transits_like_Removal_01__':
    Transits_like_Removal_01()
if __name__ == '__Transit_No__':
    Transit_No()
if __name__ == '__Flares_like_Removal_02__':
    Flares_like_Removal_02()
if __name__ == '__Flares_No__':
    Flares_No()
if __name__ == '__Gaps_Charaterization_and_Filling_03__':
    Gaps_Charaterization_and_Filling_03()
if __name__ == '__MSAP1_01_02_03_Looped__':
    MSAP1_01_02_03_Looped()
if __name__ == '__Stitching1_05__':
    Stitching1_05()
if __name__ == '__KASOCFilter_07__':
    KASOCFilter_07()
if __name__ == '__Stitching2_06__':
    Stitching2_06()
if __name__ == '__Detrending_Of_Instrumental_Residual_Effects_04__':
    Detrending_Of_Instrumental_Residual_Effects_04()
if __name__ == '__Binning_08__':
    Binning_08()
if __name__ == '__End__':
    End()
