"""Importing libraries"""
import pandas as pd
import os
import texttable
from pathlib import Path
from datetime import datetime
import Operations
"""Define the home directory"""
HOME_DIR = str(Path.home())
"""We define the path for the input and output file location"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
FilePaths = pd.read_csv(os.path.join(UserDirectoryPath, 'FilePaths.csv'), delimiter = ',')
"""Defining the path to the folder where all outputs are stored, this is based on
the date and time at which the data pipeline was run"""
FolderPathForReport = FilePaths.loc[0, 'FolderPathForReport']
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
class MSAP5_Part3_class:
    """The class defined is used to decribe the MSAP5_Part3 data pipeline, the different functions defined below
    represent the tasks performed in the different operators of the pipeline,
    in the current version of the code, we use these functions to produce a report"""
    def Start_MSAP5_3():
        """We generate one report for the overall work flow, one which is specific to overall MSAP5 and one specific to MSAP5 Part2 data pipeline,
        thus in certain information is repeated in both the reports"""

        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_MSAP5_3_Selection_and_Validation = {'MSAP5_3_Selection_and_Validation':['True']}
        Data_MSAP5_3_Selection_and_Validation = pd.DataFrame(Variable_MSAP5_3_Selection_and_Validation)
        Data_MSAP5_3_Selection_and_Validation.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_3_Selection_and_Validation.csv'), header=True, index=False)

        Variable_Start_MSAP5_3 = {'Start_MSAP5_3':['True']}
        Data_Start_MSAP5_3 = pd.DataFrame(Variable_Start_MSAP5_3)
        Data_Start_MSAP5_3.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part3', 'Start_MSAP5_3.csv'), header=True, index=False)
        """Writing the content in a temporary report specific to MSAP5 Part3"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\n\n")
        file.write("_____*__*_____\n")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-3 Selection and Validation initialized.\n")
        file.close()
        """Writing the content in the report specific to MSAP5 Part3"""
        """Writing the content in the child report of Overall SAS workflow"""
        """Writing the content in the report specific to Overall MSAP5"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"))
        # Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part3.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"))

    """The function below performs the input checks"""
    def InputCheck_MSAP5_3():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_InputCheck_MSAP5_3 = {'InputCheck_MSAP5_3':['True']}
        Data_InputCheck_MSAP5_3 = pd.DataFrame(Variable_InputCheck_MSAP5_3)
        Data_InputCheck_MSAP5_3.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part3', 'InputCheck_MSAP5_3.csv'), header=True, index=False)
        """Writing the content in the temporary report specific to MSAP5 Part2"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-3 Inputs check initialized.\n")
        file.write("MSAP5-3 The following inputs are read.\n")

        IDP_125_AGE_GYRO_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_GYRO.csv'))
        IDP_125_AGE_ACTIVITY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_ACTIVITY.csv'))
        IDP_125_MASS_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_GRANULATION_CGBM.csv'))
        IDP_125_RADIUS_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_RADIUS_GRANULATION_CGBM.csv'))
        IDP_125_AGE_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_GRANULATION_CGBM.csv'))
        IDP_125_MASS_RHO_TRANSIT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_RHO_TRANSIT.csv'))
        IDP_125_MASS_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_RHO_TRANSIT_CGBM.csv'))
        IDP_125_RADIUS_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_RADIUS_RHO_TRANSIT_CGBM.csv'))
        IDP_125_AGE_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_RHO_TRANSIT_CGBM.csv'))
        IDP_125_MASS_GRANULATION_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_GRANULATION.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_125_AGE_GYRO", "IDP_125_AGE_ACTIVITY", "IDP_125_MASS_GRANULATION_CGBM", "IDP_125_RADIUS_GRANULATION_CGBM",
                            "IDP_125_AGE_GRANULATION_CGBM", "IDP_125_MASS_RHO_TRANSIT", "IDP_125_MASS_RHO_TRANSIT_CGBM", "IDP_125_RADIUS_RHO_TRANSIT_CGBM",
                            "IDP_125_AGE_RHO_TRANSIT_CGBM", "IDP_125_MASS_GRANULATION"],
                            [str(IDP_125_AGE_GYRO_Truth), str(IDP_125_AGE_ACTIVITY_Truth), str(IDP_125_MASS_GRANULATION_CGBM_Truth), str(IDP_125_RADIUS_GRANULATION_CGBM_Truth),
                            str(IDP_125_AGE_GRANULATION_CGBM_Truth), str(IDP_125_MASS_RHO_TRANSIT_Truth), str(IDP_125_MASS_RHO_TRANSIT_CGBM_Truth), str(IDP_125_RADIUS_RHO_TRANSIT_CGBM_Truth),
                            str(IDP_125_AGE_RHO_TRANSIT_CGBM_Truth), str(IDP_125_MASS_GRANULATION_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-3 Input Check completed.\n")
        file.close()
        """Writing the content in the report specific to MSAP5 Part2"""
        """Writing the content in the Overall SAS workflow child report"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"))
        # Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part3.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP5_Part3_temp.txt"))

    """The function below performs the Consistency checks operation"""
    def Consistency_Checks_31():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Consistency_Checks_31 = {'Consistency_Checks_31':['True']}
        Data_Consistency_Checks_31 = pd.DataFrame(Variable_Consistency_Checks_31)
        Data_Consistency_Checks_31.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part3', 'Consistency_Checks_31.csv'), header=True, index=False)
        """Writing the content in the report specific to MSAP5 Part3"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-3 Consistency checks initialized.\n")
        file.write("MSAP5-3 The following inputs are read.\n")
        IDP_125_AGE_GYRO_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_GYRO.csv'))
        IDP_125_AGE_ACTIVITY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_ACTIVITY.csv'))
        IDP_125_MASS_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_GRANULATION_CGBM.csv'))
        IDP_125_RADIUS_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_RADIUS_GRANULATION_CGBM.csv'))
        IDP_125_AGE_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_GRANULATION_CGBM.csv'))
        IDP_125_MASS_RHO_TRANSIT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_RHO_TRANSIT.csv'))
        IDP_125_MASS_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_RHO_TRANSIT_CGBM.csv'))
        IDP_125_RADIUS_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_RADIUS_RHO_TRANSIT_CGBM.csv'))
        IDP_125_AGE_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_RHO_TRANSIT_CGBM.csv'))
        IDP_125_MASS_GRANULATION_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_GRANULATION.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_125_AGE_GYRO", "IDP_125_AGE_ACTIVITY", "IDP_125_MASS_GRANULATION_CGBM", "IDP_125_RADIUS_GRANULATION_CGBM",
                            "IDP_125_AGE_GRANULATION_CGBM", "IDP_125_MASS_RHO_TRANSIT", "IDP_125_MASS_RHO_TRANSIT_CGBM", "IDP_125_RADIUS_RHO_TRANSIT_CGBM",
                            "IDP_125_AGE_RHO_TRANSIT_CGBM", "IDP_125_MASS_GRANULATION"],
                            [str(IDP_125_AGE_GYRO_Truth), str(IDP_125_AGE_ACTIVITY_Truth), str(IDP_125_MASS_GRANULATION_CGBM_Truth), str(IDP_125_RADIUS_GRANULATION_CGBM_Truth),
                            str(IDP_125_AGE_GRANULATION_CGBM_Truth), str(IDP_125_MASS_RHO_TRANSIT_Truth), str(IDP_125_MASS_RHO_TRANSIT_CGBM_Truth), str(IDP_125_RADIUS_RHO_TRANSIT_CGBM_Truth),
                            str(IDP_125_AGE_RHO_TRANSIT_CGBM_Truth), str(IDP_125_MASS_GRANULATION_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nConsistency Checks input check completed, producing output.")
        CorrectedLightCurvespath = os.path.join(FolderPathForReport,'Corrected_Light_Curves.csv')
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        CorrectedLightCurvespath_Create = DP4PQRead.to_csv(CorrectedLightCurvespath, index = False)
        DP4PQRead = pd.read_csv(CorrectedLightCurvespath)
        """Producing the Output"""
        Path_Consistency_Flags = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'Consistency_Flags.csv')
        Consistency_Flags_out = DP4PQRead.to_csv(Path_Consistency_Flags, index = False)
        file.write("\nConsistency Flags are produced.\n")
        ConsistencyFlagsTruth = os.path.isfile(Path_Consistency_Flags)
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["Consistency Flags"],
                            [str(ConsistencyFlagsTruth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nConsistency checks completed.")
        file.close()
    """The function below performs the weighing of datas and bayesian decision operation"""
    def WeighingOfDatasandBayesianDecision_32():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_WeighingOfDatasandBayesianDecision_32 = {'WeighingOfDatasandBayesianDecision_32':['True']}
        Data_WeighingOfDatasandBayesianDecision_32 = pd.DataFrame(Variable_WeighingOfDatasandBayesianDecision_32)
        Data_WeighingOfDatasandBayesianDecision_32.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part3', 'WeighingOfDatasandBayesianDecision_32.csv'), header=True, index=False)
        """Writing the content in the report specific to MSAP5 Part3"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-3 IDP_125_CONSISTENCY_FLAG is set to True.\n")
        file.write("MSAP5-3 Weighing of Datas and Bayesian Decision initialized.\n")
        file.write("MSAP5-3 The following inputs are read.\n")
        IDP_125_AGE_GYRO_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_GYRO.csv'))
        IDP_125_AGE_ACTIVITY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_ACTIVITY.csv'))
        IDP_125_MASS_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_GRANULATION_CGBM.csv'))
        IDP_125_RADIUS_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_RADIUS_GRANULATION_CGBM.csv'))
        IDP_125_AGE_GRANULATION_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_GRANULATION_CGBM.csv'))
        IDP_125_MASS_RHO_TRANSIT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_RHO_TRANSIT.csv'))
        IDP_125_MASS_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_RHO_TRANSIT_CGBM.csv'))
        IDP_125_RADIUS_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_RADIUS_RHO_TRANSIT_CGBM.csv'))
        IDP_125_AGE_RHO_TRANSIT_CGBM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_AGE_RHO_TRANSIT_CGBM.csv'))
        IDP_125_MASS_GRANULATION_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP5_Part2_Output_Path'], 'IDP_125_MASS_GRANULATION.csv'))

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_125_AGE_GYRO", "IDP_125_AGE_ACTIVITY", "IDP_125_MASS_GRANULATION_CGBM", "IDP_125_RADIUS_GRANULATION_CGBM",
                            "IDP_125_AGE_GRANULATION_CGBM", "IDP_125_MASS_RHO_TRANSIT", "IDP_125_MASS_RHO_TRANSIT_CGBM", "IDP_125_RADIUS_RHO_TRANSIT_CGBM",
                            "IDP_125_AGE_RHO_TRANSIT_CGBM"],
                            [str(IDP_125_AGE_GYRO_Truth), str(IDP_125_AGE_ACTIVITY_Truth), str(IDP_125_MASS_GRANULATION_CGBM_Truth), str(IDP_125_RADIUS_GRANULATION_CGBM_Truth),
                            str(IDP_125_AGE_GRANULATION_CGBM_Truth), str(IDP_125_MASS_RHO_TRANSIT_Truth), str(IDP_125_MASS_RHO_TRANSIT_CGBM_Truth), str(IDP_125_RADIUS_RHO_TRANSIT_CGBM_Truth),
                            str(IDP_125_AGE_RHO_TRANSIT_CGBM_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-3 Input read completed.\n")
        file.write("MSAP5-3 Producing the outputs.\n")
        file.write("MSAP5-3 The following outputs are produced.\n")
        CorrectedLightCurvespath = os.path.join(FolderPathForReport,'Corrected_Light_Curves.csv')
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        CorrectedLightCurvespath_Create = DP4PQRead.to_csv(CorrectedLightCurvespath, index = False)
        DP4PQRead = pd.read_csv(CorrectedLightCurvespath)
        """Producing the Output"""
        Path_IDP_MASS_BAYESIAN = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'IDP_MASS_BAYESIAN.csv')
        IDP_MASS_BAYESIAN_out = DP4PQRead.to_csv(Path_IDP_MASS_BAYESIAN, index = False)
        IDP_MASS_BAYESIAN_Truth = os.path.isfile(Path_IDP_MASS_BAYESIAN)
        Path_IDP_RADIUS_BAYESIAN = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'IDP_RADIUS_BAYESIAN.csv')
        IDP_RADIUS_BAYESIAN_out = DP4PQRead.to_csv(Path_IDP_RADIUS_BAYESIAN, index = False)
        IDP_RADIUS_BAYESIAN_Truth = os.path.isfile(Path_IDP_RADIUS_BAYESIAN)
        Path_IDP_AGE_BAYESIAN = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'IDP_AGE_BAYESIAN.csv')
        IDP_AGE_BAYESIAN_out = DP4PQRead.to_csv(Path_IDP_AGE_BAYESIAN, index = False)
        IDP_AGE_BAYESIAN_Truth = os.path.isfile(Path_IDP_AGE_BAYESIAN)
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_MASS_BAYESIAN", "IDP_RADIUS_BAYESIAN", "IDP_AGE_BAYESIAN"],
                            [str(IDP_MASS_BAYESIAN_Truth), str(IDP_RADIUS_BAYESIAN_Truth), str(IDP_AGE_BAYESIAN_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nWeighing of datas and bayesian decision completed.")
        file.close()
    """The function below performs the Alert operation"""
    def Alert_33():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Alert_33 = {'Alert_33':['True']}
        Data_Alert_33 = pd.DataFrame(Variable_Alert_33)
        Data_Alert_33.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part3', 'Alert_33.csv'), header=True, index=False)
        """Writing the content in the report specific to MSAP5 Part3"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-3 IDP_125_CONSISTENCY_FLAG is set to False.\n")
        file.write("MSAP5-3 Alert: Manual check is initialized.\n")
        file.close()
    """The function below performs the Validation operation"""
    def Validation_34():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_Validation_34 = {'Validation_34':['True']}
        Data_Validation_34 = pd.DataFrame(Variable_Validation_34)
        Data_Validation_34.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part3', 'Validation_34.csv'), header=True, index=False)
        """Writing the content in the report specific to MSAP5 Part3"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5-3 Validation is initialized.\n")
        file.write("MSAP5-3 Reading the outputs produced from weighing of datas and bayesian decision.\n")
        CorrectedLightCurvespath = os.path.join(FolderPathForReport,'Corrected_Light_Curves.csv')
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        CorrectedLightCurvespath_Create = DP4PQRead.to_csv(CorrectedLightCurvespath, index = False)
        DP4PQRead = pd.read_csv(CorrectedLightCurvespath)

        Path_IDP_MASS_BAYESIAN = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'IDP_MASS_BAYESIAN.csv')
        IDP_MASS_BAYESIAN_out = DP4PQRead.to_csv(Path_IDP_MASS_BAYESIAN, index = False)
        IDP_MASS_BAYESIAN_Truth = os.path.isfile(Path_IDP_MASS_BAYESIAN)
        Path_IDP_RADIUS_BAYESIAN = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'IDP_RADIUS_BAYESIAN.csv')
        IDP_RADIUS_BAYESIAN_out = DP4PQRead.to_csv(Path_IDP_RADIUS_BAYESIAN, index = False)
        IDP_RADIUS_BAYESIAN_Truth = os.path.isfile(Path_IDP_RADIUS_BAYESIAN)
        Path_IDP_AGE_BAYESIAN = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'IDP_AGE_BAYESIAN.csv')
        IDP_AGE_BAYESIAN_out = DP4PQRead.to_csv(Path_IDP_AGE_BAYESIAN, index = False)
        IDP_AGE_BAYESIAN_Truth = os.path.isfile(Path_IDP_AGE_BAYESIAN)
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_MASS_BAYESIAN", "IDP_RADIUS_BAYESIAN", "IDP_AGE_BAYESIAN"],
                            [str(IDP_MASS_BAYESIAN_Truth), str(IDP_RADIUS_BAYESIAN_Truth), str(IDP_AGE_BAYESIAN_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nMSAP5-3 Input read completed.\n")
        file.write("MSAP5-3 Producing the outputs.\n")
        file.write("MSAP5-3 The following Data Product outputs are produced.\n")
        CorrectedLightCurvespath = os.path.join(FolderPathForReport,'Corrected_Light_Curves.csv')
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0, 'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        CorrectedLightCurvespath_Create = DP4PQRead.to_csv(CorrectedLightCurvespath, index = False)
        DP4PQRead = pd.read_csv(CorrectedLightCurvespath)
        """Producing the Output"""
        Path_DP5_125_MASS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'DP5_125_MASS.csv')
        DP5_125_MASS_out = DP4PQRead.to_csv(Path_DP5_125_MASS, index = False)
        Path_DP5_125_MASS_METADATA = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'DP5_125_MASS_METADATA.csv')
        DP5_125_MASS_METADATA_out = DP4PQRead.to_csv(Path_DP5_125_MASS_METADATA, index = False)
        Path_DP5_125_RADIUS = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'DP5_125_RADIUS.csv')
        DP5_125_RADIUS_out = DP4PQRead.to_csv(Path_DP5_125_RADIUS, index = False)
        Path_DP5_125_RADIUS_METADATA = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'DP5_125_RADIUS_METADATA.csv')
        DP5_125_RADIUS_METADATA_out = DP4PQRead.to_csv(Path_DP5_125_RADIUS_METADATA, index = False)
        Path_DP5_125_AGE = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'DP5_125_AGE.csv')
        DP5_125_AGE_out = DP4PQRead.to_csv(Path_DP5_125_AGE, index = False)
        Path_DP5_125_AGE_METADATA = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'DP5_125_AGE_METADATA.csv')
        DP5_125_AGE_METADATA_out = DP4PQRead.to_csv(Path_DP5_125_AGE_METADATA, index = False)

        DP5_125_MASS_Truth = os.path.isfile(Path_DP5_125_MASS)
        DP5_125_MASS_METADATA_Truth = os.path.isfile(Path_DP5_125_MASS_METADATA)
        DP5_125_RADIUS_Truth = os.path.isfile(Path_DP5_125_RADIUS)
        DP5_125_RADIUS_METADATA_Truth = os.path.isfile(Path_DP5_125_RADIUS_METADATA)
        DP5_125_AGE_Truth = os.path.isfile(Path_DP5_125_AGE)
        DP5_125_AGE_METADATA_Truth = os.path.isfile(Path_DP5_125_AGE_METADATA)
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP5_125_MASS", "DP5_125_MASS_METADATA", "DP5_125_RADIUS",
                            "DP5_125_RADIUS_METADATA", "DP5_125_AGE", "DP5_125_AGE_METADATA"],
                            [str(DP5_125_MASS_Truth), str(DP5_125_MASS_METADATA_Truth), str(DP5_125_RADIUS_Truth),
                            str(DP5_125_RADIUS_METADATA_Truth), str(DP5_125_AGE_Truth), str(DP5_125_AGE_METADATA_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5-3 Validation completed.\n")
        file.close()
        """Writing the outputs to the SAS overall workflow child report"""
        # Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "MainReportfile_MSAP5_Part3.txt"), os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"))

    """The function below performs the completion operation of MSAP5 Part3"""
    def End_MSAP5_Part3():
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_End_MSAP5_Part3 = {'End_MSAP5_Part3':['True']}
        Data_End_MSAP5_Part3 = pd.DataFrame(Variable_End_MSAP5_Part3)
        Data_End_MSAP5_Part3.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP5', 'MSAP5_Part3', 'End_MSAP5_Part3.csv'), header=True, index=False)
        """For each function that is described, we check if that function was run or not by reading the respective .csv files,
        we later delete these files"""
        Process_01 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'Start_MSAP5_3.csv'))
        Process_02 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'InputCheck_MSAP5_3.csv'))
        Process_03 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'Consistency_Checks_31.csv'))
        Process_04 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'WeighingOfDatasandBayesianDecision_32.csv'))
        Process_05 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'Alert_33.csv'))
        Process_06 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'Validation_34.csv'))
        Process_07 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part3', 'End_MSAP5_Part3.csv'))
        """Write the Table in the Report"""
        """Writing the content in the report specific to MSAP5 Part3"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"), "a")
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["InputCheck_MSAP5_3", "Consistency_Checks_31",
                            "Weighing of Datas and Bayesian Decision 32", "Alert_33", "Validation_34"],
                            [str(Process_02), str(Process_03),
                            str(Process_04), str(Process_05), str(Process_06)]
                            ])
        file.write("\n")
        file.write("\nThe summary of all the processes in MSAP5 Part 2 is:\n")
        file.write(tableObj.draw())
        file.write("\nMSAP5 Part 3 completed.\n")
        file.write("\n---**---MSAP5-Part3-END---**---\n")
        file.close()
        """Writing the outputs to the SAS overall workflow child report"""
        # Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part3.txt'), os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt"))
        """Copying the contents from child report to the SAS overall workflow report"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "Report_MSAP5_Part3.txt")) #MainReportfile_MSAP5_Part3
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, 'MainReportfile_MSAP5_Part3.txt'))
