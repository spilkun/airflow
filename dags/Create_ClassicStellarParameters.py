"""import libraries"""
import pandas as pd
import os
"""import texttable to define write Tables in .txt files which is generated as reports"""
import texttable
from pathlib import Path
from datetime import datetime
import Operations
"""Set the home directory path"""
HOME_DIR = str(Path.home())
"""Defining User inputs file"""
UserInputsData = pd.read_csv(os.path.join(HOME_DIR, 'userdirectory', 'UserInputs.csv'), index_col=None)
"""Defining Input Paths"""
FilePaths = pd.read_csv(os.path.join(HOME_DIR, 'userdirectory', 'FilePaths.csv'), delimiter = ',')
"""Defining the path for various files and folders"""
GeneratedOutputsPath = FilePaths.loc[0,'GeneratedOutputs_Path']
ReportName = FilePaths.loc[0,'ReportName']
"""Defining the path to the folder where all outputs are stored, this is based on
the date and time at which the data pipeline was run"""
FolderPathForReport = FilePaths.loc[0,'FolderPathForReport']
"""We use the current date to extract the timestamp for the report and also for the foldername"""
now = datetime.now()
dt_string = now.strftime("%B_%d_%Y_%H_%M")
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
class MSAP2:
    """The class defined is used to decribe the MSAP2 data pipeline, the different functions defined below
    represent the tasks performed in the different operators of the pipeline,
    in the current version of the code, we use these functions to produce a report"""
    def Start_MSAP2():
        """We generate 2 reports one for the overall work flow and one which is specific to MSAP2 data pipeline,
        thus in certain information is repeated in both the reports"""
        now = datetime.now()
        file = open(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"), "a")
        file.write("\n.\n")
        file.write("*** --- Start of Report - MSAP2 --- ***\n\n")
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP2 is initialized.\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
        """write the same content in the Overall SAS workflow report"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
        """Delete the Temporary file"""
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))

    """The function below performs the input checks"""
    def PDP_IDP_DP3_Checks_MSAP2():
        now = datetime.now()
        file = open(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"), "a")
        file.write("\n.\nChecking for preparatory data:\n")
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("The Following preparatory data exists.\n")
        PDP_B_122_MOD_STELLAR_SPECTRA_1D_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_B_122_MOD_STELLAR_SPECTRA_1D.csv'))
        PDP_B_122_MOD_STELLAR_SPECTRA_3D_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_B_122_MOD_STELLAR_SPECTRA_3D.csv'))
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        IDP_128_LOGG_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'MSAP2_Output_Path'], 'IDP_128_LOGG_VARLC.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        if PDP_B_122_MOD_STELLAR_SPECTRA_1D_Truth == "False":
            file.write("PDP_B_122_MOD_STELLAR_SPECTRA_1D is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_B_122_MOD_STELLAR_SPECTRA_1D is absent, Data Pipeline failed.")
        if PDP_B_122_MOD_STELLAR_SPECTRA_3D_Truth == "False":
            file.write("PDP_B_122_MOD_STELLAR_SPECTRA_3D is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_B_122_MOD_STELLAR_SPECTRA_3D is absent, Data Pipeline failed.")
        if PDP_A_122_PREP_OBS_DATA_Truth == "False":
            file.write("PDP_A_122_PREP_OBS_DATA is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_A_122_PREP_OBS_DATA is absent, Data Pipeline failed.")
        if PDP_A_122_PREP_OBS_METADATA_Truth == "False":
            file.write("PDP_A_122_PREP_OBS_METADATA is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_A_122_PREP_OBS_METADATA is absent, Data Pipeline failed.")
        if PDP_C_122_TEFF_SAPP_Truth == "False":
            file.write("PDP_C_122_TEFF_SAPP is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_TEFF_SAPP is absent, Data Pipeline failed.")

        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["PDP_B_122_MOD_STELLAR_SPECTRA_1D", "PDP_B_122_MOD_STELLAR_SPECTRA_3D", "PDP_A_122_PREP_OBS_DATA", "PDP_A_122_PREP_OBS_METADATA", "IDP_128_LOGG_VARLC", "PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP"],
                            [str(PDP_B_122_MOD_STELLAR_SPECTRA_1D_Truth), str(PDP_B_122_MOD_STELLAR_SPECTRA_3D_Truth), str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth), str(IDP_128_LOGG_VARLC_Truth), str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nThe Following DP3 exists.\n")
        """If IDP_128_DETECTION_FLAG is False then DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
        else:
            DP3_128_DATA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_NU_MAX"],
                            [str(DP3_128_DATA_NU_AV_Truth), str(DP3_128_NU_MAX_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nThe Following Preparatory Data exists.\n")
        PDP_C_122_EXTINCTION_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_EXTINCTION.csv'))
        PDP_C_122_F_BOL_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_F_BOL_SED.csv'))
        PDP_C_122_F_IR_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_F_IR_SED.csv'))
        PDP_C_122_LD_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_LD_INTERFEROMETRY.csv'))
        PDP_C_122_LOGG_PHOTOMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_LOGG_PHOTOMETRY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_122_METADATA_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_INTERFEROMETRY.csv'))
        PDP_C_122_METADATA_IRFM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_IRFM.csv'))
        PDP_C_122_METADATA_PHOTOMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_PHOTOMETRY.csv'))
        PDP_C_122_METADATA_SBCR_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_SBCR.csv'))
        PDP_C_122_METADATA_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_SED.csv'))
        PDP_C_122_RADIUS_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_RADIUS_INTERFEROMETRY.csv'))
        """If Mandatory inputs are not present then raise Airflow exceptions to quit the data pipeline"""
        if PDP_C_122_EXTINCTION_Truth == "False":
            file.write("PDP_C_122_EXTINCTION is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_EXTINCTION is absent, Data Pipeline failed.")
        if PDP_C_122_F_BOL_SED_Truth == "False":
            file.write("PDP_C_122_F_BOL_SED is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_F_BOL_SED is absent, Data Pipeline failed.")
        if PDP_C_122_F_IR_SED_Truth == "False":
            file.write("PDP_C_122_F_IR_SED is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_F_IR_SED is absent, Data Pipeline failed.")
        if PDP_C_122_LD_INTERFEROMETRY_Truth == "False":
            file.write("PDP_C_122_LD_INTERFEROMETRY is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_LD_INTERFEROMETRY is absent, Data Pipeline failed.")
        if PDP_C_122_LOGG_PHOTOMETRY_Truth == "False":
            file.write("PDP_C_122_LOGG_PHOTOMETRY is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_LOGG_PHOTOMETRY is absent, Data Pipeline failed.")
        if PDP_C_122_L_SED_Truth == "False":
            file.write("PDP_C_122_L_SED is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_L_SED is absent, Data Pipeline failed.")
        if PDP_C_122_METADATA_INTERFEROMETRY_Truth == "False":
            file.write("PDP_C_122_METADATA_INTERFEROMETRY is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_METADATA_INTERFEROMETRY is absent, Data Pipeline failed.")
        if PDP_C_122_METADATA_IRFM_Truth == "False":
            file.write("PDP_C_122_METADATA_IRFM is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_METADATA_IRFM is absent, Data Pipeline failed.")
        if PDP_C_122_METADATA_PHOTOMETRY_Truth == "False":
            file.write("PDP_C_122_METADATA_PHOTOMETRY is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_METADATA_PHOTOMETRY is absent, Data Pipeline failed.")
        if PDP_C_122_METADATA_SBCR_Truth == "False":
            file.write("PDP_C_122_METADATA_SBCR is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_METADATA_SBCR is absent, Data Pipeline failed.")
        if PDP_C_122_METADATA_SED_Truth == "False":
            file.write("PDP_C_122_METADATA_SED is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_METADATA_SED is absent, Data Pipeline failed.")
        if PDP_C_122_RADIUS_INTERFEROMETRY_Truth == "False":
            file.write("PDP_C_122_RADIUS_INTERFEROMETRY is absent, Data Pipeline failed.")
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
            raise ValueError("PDP_C_122_RADIUS_INTERFEROMETRY is absent, Data Pipeline failed.")

        """Create texttable object"""
        tableObj3 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c", "c", "c","c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["PDP_C_122_EXTINCTION", "PDP_C_122_F_BOL_SED", "PDP_C_122_F_IR_SED",
                            "PDP_C_122_LD_INTERFEROMETRY", "PDP_C_122_LOGG_PHOTOMETRY", "PDP_C_122_L_SED_Truth",
                            "PDP_C_122_METADATA_INTERFEROMETRY", "PDP_C_122_METADATA_IRFM", "PDP_C_122_METADATA_PHOTOMETRY",
                            "PDP_C_122_METADATA_SBCR", "PDP_C_122_METADATA_SED", "PDP_C_122_RADIUS_INTERFEROMETRY"],
                            [str(PDP_C_122_EXTINCTION_Truth), str(PDP_C_122_F_BOL_SED_Truth), str(PDP_C_122_F_IR_SED_Truth),
                            str(PDP_C_122_LD_INTERFEROMETRY_Truth), str(PDP_C_122_LOGG_PHOTOMETRY_Truth), str(PDP_C_122_L_SED_Truth),
                            str(PDP_C_122_METADATA_INTERFEROMETRY_Truth), str(PDP_C_122_METADATA_IRFM_Truth), str(PDP_C_122_METADATA_PHOTOMETRY_Truth),
                            str(PDP_C_122_METADATA_SBCR_Truth), str(PDP_C_122_METADATA_SED_Truth), str(PDP_C_122_RADIUS_INTERFEROMETRY_Truth)]
                            ])
        file.write(tableObj3.draw())
        PDP_C_122_RADIUS_IRFM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_RADIUS_IRFM.csv'))
        PDP_C_122_RADIUS_SBCR_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_RADIUS_SBCR.csv'))
        PDP_C_122_RADIUS_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_RADIUS_SED.csv'))
        PDP_C_122_TEFF_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_INTERFEROMETRY.csv'))
        PDP_C_122_TEFF_IRFM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_IRFM.csv'))
        PDP_C_122_TEFF_PHOTOMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_PHOTOMETRY.csv'))
        PDP_C_122_TEFF_SBCR_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_SBCR.csv'))
        PDP_C_122_TEFF_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_SED.csv'))
        PDP_C_122_THETA_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_THETA_INTERFEROMETRY.csv'))
        PDP_C_122_THETA_IRFM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_THETA_IRFM.csv'))
        PDP_C_122_THETA_SBCR_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_THETA_SBCR.csv'))
        PDP_C_122_THETA_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_THETA_SED.csv'))
        file.write("\nFurther.\n")
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c","c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["PDP_C_122_RADIUS_IRFM", "PDP_C_122_RADIUS_SBCR", "PDP_C_122_RADIUS_SED",
                            "PDP_C_122_TEFF_INTERFEROMETRY", "PDP_C_122_TEFF_IRFM", "PDP_C_122_TEFF_PHOTOMETRY",
                            "PDP_C_122_TEFF_SBCR", "PDP_C_122_TEFF_SED", "PDP_C_122_THETA_INTERFEROMETRY",
                            "PDP_C_122_THETA_IRFM", "PDP_C_122_THETA_SBCR", "PDP_C_122_THETA_SED"],
                            [str(PDP_C_122_RADIUS_IRFM_Truth), str(PDP_C_122_RADIUS_SBCR_Truth), str(PDP_C_122_RADIUS_SED_Truth),
                            str(PDP_C_122_TEFF_INTERFEROMETRY_Truth), str(PDP_C_122_TEFF_IRFM_Truth), str(PDP_C_122_TEFF_PHOTOMETRY_Truth),
                            str(PDP_C_122_TEFF_SBCR_Truth), str(PDP_C_122_TEFF_SED_Truth), str(PDP_C_122_THETA_INTERFEROMETRY_Truth),
                            str(PDP_C_122_THETA_IRFM_Truth), str(PDP_C_122_THETA_SBCR_Truth), str(PDP_C_122_THETA_SED_Truth)]
                            ])
        file.write(tableObj4.draw())
        file.write("\nInputs check completed.")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, ReportName), os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_MSAP2_temp.txt"))
    """The function below performs seismic logg computation and logg selection operation"""
    def SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01():
        now = datetime.now()
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01 = {'SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01':['True']}
        Data_SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01 = pd.DataFrame(Variable_SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01)
        Data_SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP2', 'SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), "a")
        file.write("\n.\nMSAP2: Performing Seismic Logg Computation and Logg Selection.\n")
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The Inputs available are.\n")
        IDP_128_LOGG_VARLC_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'MSAP2_Output_Path'], 'IDP_128_LOGG_VARLC.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        """If IDP_128_DETECTION_FLAG is False then DP3 outputs are not produced"""
        IDP_128_DETECTION_FLAG = UserInputsData.loc[0, 'Oscillations']
        if IDP_128_DETECTION_FLAG == "False":
            DP3_128_DATA_NU_AV_Truth = "False"
            DP3_128_NU_MAX_Truth = "False"
        else:
            DP3_128_DATA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
            DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_128_LOGG_VARLC", "PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP", "DP3_128_DATA_NU_AV", "DP3_128_NU_MAX"],
                            [str(IDP_128_LOGG_VARLC_Truth), str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth), str(DP3_128_DATA_NU_AV_Truth), str(DP3_128_NU_MAX_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nSuccessfully read inputs.\n")
        file.write("Producing IDP_122_LOGG_SAPP_TMP.\n")

        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0,'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))

        Path_IDP_122_LOGG_SAPP_TMP = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_LOGG_SAPP_TMP.csv')
        IDP_122_LOGG_SAPP_TMP_out = DP4PQRead.to_csv(Path_IDP_122_LOGG_SAPP_TMP, index = False)
        file.write("Successfully Produced IDP_122_LOGG_SAPP_TMP.\n")
        IDP_122_LOGG_SAPP_TMP_Truth = os.path.isfile(Path_IDP_122_LOGG_SAPP_TMP)
        """Create texttable object"""
        tableObj2 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_122_LOGG_SAPP_TMP"],
                            [str(IDP_122_LOGG_SAPP_TMP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.close()
    """The function below performs spectroscopy operation"""
    def SPECTROSCOPY_MSAP2_02():
        now = datetime.now()
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_SPECTROSCOPY_MSAP2_02 = {'SPECTROSCOPY_MSAP2_02':['True']}
        Data_SPECTROSCOPY_MSAP2_02 = pd.DataFrame(Variable_SPECTROSCOPY_MSAP2_02)
        Data_SPECTROSCOPY_MSAP2_02.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP2', 'SPECTROSCOPY_MSAP2_02.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), "a")
        file.write("\n.\nMSAP2: Performing Spectroscopy studies.\n")
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The Inputs available are.\n")
        Path_IDP_122_LOGG_SAPP_TMP = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_LOGG_SAPP_TMP.csv')
        IDP_122_LOGG_SAPP_TMP_Truth = os.path.isfile(Path_IDP_122_LOGG_SAPP_TMP)
        PDP_B_122_MOD_STELLAR_SPECTRA_1D_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_B_122_MOD_STELLAR_SPECTRA_1D.csv'))
        PDP_B_122_MOD_STELLAR_SPECTRA_3D_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_B_122_MOD_STELLAR_SPECTRA_3D.csv'))
        PDP_A_122_PREP_OBS_DATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_DATA.csv'))
        PDP_A_122_PREP_OBS_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_A_122_PREP_OBS_METADATA.csv'))
        PDP_C_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_SAPP.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'IDP_122_TEFF_SAPP.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_122_LOGG_SAPP_TMP", "PDP_B_122_MOD_STELLAR_SPECTRA_1D", "PDP_B_122_MOD_STELLAR_SPECTRA_3D", "PDP_A_122_PREP_OBS_DATA", "PDP_A_122_PREP_OBS_METADATA", "PDP_C_122_TEFF_SAPP", "IDP_122_TEFF_SAPP"],
                            [str(IDP_122_LOGG_SAPP_TMP_Truth), str(PDP_B_122_MOD_STELLAR_SPECTRA_1D_Truth), str(PDP_B_122_MOD_STELLAR_SPECTRA_3D_Truth), str(PDP_A_122_PREP_OBS_DATA_Truth), str(PDP_A_122_PREP_OBS_METADATA_Truth), str(PDP_C_122_TEFF_SAPP_Truth), str(IDP_122_TEFF_SAPP_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nReading the Inputs.\n")
        file.write("Producing the Intermediate data products.\n")
        file.write("The following Intermediate data products have been produced.\n")
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0,'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        Path_IDP_122_TEFF_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_TEFF_SPECTROSCOPY.csv')
        IDP_122_TEFF_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_IDP_122_TEFF_SPECTROSCOPY, index = False)
        Path_IDP_122_LOGG_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_LOGG_SPECTROSCOPY.csv')
        IDP_122_LOGG_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_IDP_122_LOGG_SPECTROSCOPY, index = False)
        Path_IDP_122_VSINI_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_VSINI_SPECTROSCOPY.csv')
        IDP_122_VSINI_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_IDP_122_VSINI_SPECTROSCOPY, index = False)
        Path_IDP_122_ELEM1_ELEM2_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_ELEM1_ELEM2_SPECTROSCOPY.csv')
        IDP_122_ELEM1_ELEM2_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_IDP_122_ELEM1_ELEM2_SPECTROSCOPY, index = False)
        Path_IDP_122_ALPHA_FE_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv')
        IDP_122_ALPHA_FE_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_IDP_122_ALPHA_FE_SPECTROSCOPY, index = False)
        IDP_122_TEFF_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_TEFF_SPECTROSCOPY)
        IDP_122_LOGG_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_LOGG_SPECTROSCOPY)
        IDP_122_VSINI_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_VSINI_SPECTROSCOPY)
        IDP_122_ELEM1_ELEM2_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_ELEM1_ELEM2_SPECTROSCOPY)
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_ALPHA_FE_SPECTROSCOPY)
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_122_TEFF_SPECTROSCOPY", "IDP_122_LOGG_SPECTROSCOPY", "IDP_122_VSINI_SPECTROSCOPY", "IDP_122_ELEM1_ELEM2_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY"],
                            [str(IDP_122_TEFF_SPECTROSCOPY_Truth), str(IDP_122_LOGG_SPECTROSCOPY_Truth), str(IDP_122_VSINI_SPECTROSCOPY_Truth), str(IDP_122_ELEM1_ELEM2_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nProducing Additional data products.\n")
        file.write("The Following Additional data products have been produced.\n")

        Path_ADP_122_MICROTURBULENCE_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_MICROTURBULENCE_SPECTROSCOPY.csv')
        ADP_122_MICROTURBULENCE_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_ADP_122_MICROTURBULENCE_SPECTROSCOPY, index = False)
        Path_ADP_122_MACROTURBULENCE_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_MACROTURBULENCE_SPECTROSCOPY.csv')
        ADP_122_MACROTURBULENCE_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_ADP_122_MACROTURBULENCE_SPECTROSCOPY, index = False)
        Path_ADP_122_FLAGS_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_FLAGS_SPECTROSCOPY.csv')
        ADP_122_FLAGS_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_ADP_122_FLAGS_SPECTROSCOPY, index = False)
        Path_ADP_122_METADATA_STAT_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_METADATA_STAT_SPECTROSCOPY.csv')
        ADP_122_METADATA_STAT_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_ADP_122_METADATA_STAT_SPECTROSCOPY, index = False)
        Path_ADP_122_MOD_BESTFIT_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_MOD_BESTFIT_SPECTROSCOPY.csv')
        ADP_122_MOD_BESTFIT_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_ADP_122_MOD_BESTFIT_SPECTROSCOPY, index = False)
        Path_ADP_122_RADIAL_VELOCITY_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_RADIAL_VELOCITY_SPECTROSCOPY.csv')
        ADP_122_RADIAL_VELOCITY_SPECTROSCOPY_out = DP4PQRead.to_csv(Path_ADP_122_RADIAL_VELOCITY_SPECTROSCOPY, index = False)

        ADP_122_MICROTURBULENCE_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_MICROTURBULENCE_SPECTROSCOPY)
        ADP_122_MACROTURBULENCE_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_MACROTURBULENCE_SPECTROSCOPY)
        ADP_122_FLAGS_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_FLAGS_SPECTROSCOPY)
        ADP_122_METADATA_STAT_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_METADATA_STAT_SPECTROSCOPY)
        ADP_122_MOD_BESTFIT_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_MOD_BESTFIT_SPECTROSCOPY)
        ADP_122_RADIAL_VELOCITY_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_RADIAL_VELOCITY_SPECTROSCOPY)
        """Create texttable object"""
        tableObj3 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["ADP_122_MICROTURBULENCE_SPECTROSCOPY", "ADP_122_MACROTURBULENCE_SPECTROSCOPY", "ADP_122_FLAGS_SPECTROSCOPY", "ADP_122_METADATA_STAT_SPECTROSCOPY", "ADP_122_MOD_BESTFIT_SPECTROSCOPY", "ADP_122_RADIAL_VELOCITY_SPECTROSCOPY"],
                            [str(ADP_122_MICROTURBULENCE_SPECTROSCOPY_Truth), str(ADP_122_MACROTURBULENCE_SPECTROSCOPY_Truth), str(ADP_122_FLAGS_SPECTROSCOPY_Truth), str(ADP_122_METADATA_STAT_SPECTROSCOPY_Truth), str(ADP_122_MOD_BESTFIT_SPECTROSCOPY_Truth), str(ADP_122_RADIAL_VELOCITY_SPECTROSCOPY_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.close()
    """The function below performs bayesian inference operation"""
    def BAYESIAN_INFERENCE_MSAP2_03():
        now = datetime.now()
        """For each process we need to check if that function is executed or not,
        to do this we produce a csv file which sets to True if a function is executed"""
        Variable_BAYESIAN_INFERENCE_MSAP2_03 = {'BAYESIAN_INFERENCE_MSAP2_03':['True']}
        Data_BAYESIAN_INFERENCE_MSAP2_03 = pd.DataFrame(Variable_BAYESIAN_INFERENCE_MSAP2_03)
        Data_BAYESIAN_INFERENCE_MSAP2_03.to_csv(path_or_buf = os.path.join(FolderPathForReport,'MSAP2', 'BAYESIAN_INFERENCE_MSAP2_03.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), "a")
        file.write("\n.\nMSAP2: Performing Bayesian Inference.\n")
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("Reading Inputs.\n")
        file.write("The Inputs available are as shown below.\n")
        Path_IDP_122_TEFF_SPECTROSCOPY = os.path.join(FolderPathForReport,'MSAP2', 'IDP_122_TEFF_SPECTROSCOPY.csv')
        Path_IDP_122_LOGG_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_LOGG_SPECTROSCOPY.csv')
        Path_IDP_122_VSINI_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_VSINI_SPECTROSCOPY.csv')
        Path_IDP_122_ELEM1_ELEM2_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_ELEM1_ELEM2_SPECTROSCOPY.csv')
        Path_IDP_122_ALPHA_FE_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv')

        IDP_122_TEFF_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_TEFF_SPECTROSCOPY)
        IDP_122_LOGG_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_LOGG_SPECTROSCOPY)
        IDP_122_VSINI_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_VSINI_SPECTROSCOPY)
        IDP_122_ELEM1_ELEM2_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_ELEM1_ELEM2_SPECTROSCOPY)
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(Path_IDP_122_ALPHA_FE_SPECTROSCOPY)
        file.write("The Intermediate Data Products as inputs are:\n")
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["IDP_122_TEFF_SPECTROSCOPY", "IDP_122_LOGG_SPECTROSCOPY", "IDP_122_VSINI_SPECTROSCOPY", "IDP_122_ELEM1_ELEM2_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY"],
                            [str(IDP_122_TEFF_SPECTROSCOPY_Truth), str(IDP_122_LOGG_SPECTROSCOPY_Truth), str(IDP_122_VSINI_SPECTROSCOPY_Truth), str(IDP_122_ELEM1_ELEM2_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nThe Additional Data Products as inputs are:\n")
        Path_ADP_122_MICROTURBULENCE_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_MICROTURBULENCE_SPECTROSCOPY.csv')
        Path_ADP_122_MACROTURBULENCE_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_MACROTURBULENCE_SPECTROSCOPY.csv')
        Path_ADP_122_FLAGS_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_FLAGS_SPECTROSCOPY.csv')
        Path_ADP_122_METADATA_STAT_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_METADATA_STAT_SPECTROSCOPY.csv')
        Path_ADP_122_MOD_BESTFIT_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_MOD_BESTFIT_SPECTROSCOPY.csv')
        Path_ADP_122_RADIAL_VELOCITY_SPECTROSCOPY = os.path.join(FolderPathForReport, 'MSAP2', 'ADP_122_RADIAL_VELOCITY_SPECTROSCOPY.csv')

        ADP_122_MICROTURBULENCE_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_MICROTURBULENCE_SPECTROSCOPY)
        ADP_122_MACROTURBULENCE_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_MACROTURBULENCE_SPECTROSCOPY)
        ADP_122_FLAGS_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_FLAGS_SPECTROSCOPY)
        ADP_122_METADATA_STAT_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_METADATA_STAT_SPECTROSCOPY)
        ADP_122_MOD_BESTFIT_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_MOD_BESTFIT_SPECTROSCOPY)
        ADP_122_RADIAL_VELOCITY_SPECTROSCOPY_Truth = os.path.isfile(Path_ADP_122_RADIAL_VELOCITY_SPECTROSCOPY)
        """Create texttable object"""
        tableObj3 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj3.set_cols_align(["c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj3.set_cols_dtype(["t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj3.set_cols_valign(["b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj3.add_rows([["ADP_122_MICROTURBULENCE_SPECTROSCOPY", "ADP_122_MACROTURBULENCE_SPECTROSCOPY", "ADP_122_FLAGS_SPECTROSCOPY", "ADP_122_METADATA_STAT_SPECTROSCOPY", "ADP_122_MOD_BESTFIT_SPECTROSCOPY", "ADP_122_RADIAL_VELOCITY_SPECTROSCOPY"],
                            [str(ADP_122_MICROTURBULENCE_SPECTROSCOPY_Truth), str(ADP_122_MACROTURBULENCE_SPECTROSCOPY_Truth), str(ADP_122_FLAGS_SPECTROSCOPY_Truth), str(ADP_122_METADATA_STAT_SPECTROSCOPY_Truth), str(ADP_122_MOD_BESTFIT_SPECTROSCOPY_Truth), str(ADP_122_RADIAL_VELOCITY_SPECTROSCOPY_Truth)]
                            ])
        file.write(tableObj3.draw())
        file.write("\nIDP_122_LOGG_SAPP_TMP as input is:\n")
        Path_IDP_122_LOGG_SAPP_TMP = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_LOGG_SAPP_TMP.csv')
        IDP_122_LOGG_SAPP_TMP_Truth = os.path.isfile(Path_IDP_122_LOGG_SAPP_TMP)
        """Create texttable object"""
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_122_LOGG_SAPP_TMP"],
                            [str(IDP_122_LOGG_SAPP_TMP_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nThe Preparatory Data as inputs are:\n")

        PDP_C_122_EXTINCTION_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_EXTINCTION.csv'))
        PDP_C_122_F_BOL_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_F_BOL_SED.csv'))
        PDP_C_122_F_IR_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_F_IR_SED.csv'))
        PDP_C_122_LD_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_LD_INTERFEROMETRY.csv'))
        PDP_C_122_LOGG_PHOTOMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_LOGG_PHOTOMETRY.csv'))
        PDP_C_122_L_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_L_SED.csv'))
        PDP_C_122_METADATA_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_INTERFEROMETRY.csv'))
        PDP_C_122_METADATA_IRFM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_IRFM.csv'))
        PDP_C_122_METADATA_PHOTOMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_PHOTOMETRY.csv'))
        PDP_C_122_METADATA_SBCR_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_SBCR.csv'))
        PDP_C_122_METADATA_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_METADATA_SED.csv'))
        PDP_C_122_RADIUS_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_RADIUS_INTERFEROMETRY.csv'))
        """Create texttable object"""
        tableObj4 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj4.set_cols_align(["c", "c", "c", "c", "c","c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj4.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj4.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj4.add_rows([["PDP_C_122_EXTINCTION", "PDP_C_122_F_BOL_SED", "PDP_C_122_F_IR_SED",
                            "PDP_C_122_LD_INTERFEROMETRY", "PDP_C_122_LOGG_PHOTOMETRY", "PDP_C_122_L_SED_Truth",
                            "PDP_C_122_METADATA_INTERFEROMETRY", "PDP_C_122_METADATA_IRFM", "PDP_C_122_METADATA_PHOTOMETRY",
                            "PDP_C_122_METADATA_SBCR", "PDP_C_122_METADATA_SED", "PDP_C_122_RADIUS_INTERFEROMETRY"],
                            [str(PDP_C_122_EXTINCTION_Truth), str(PDP_C_122_F_BOL_SED_Truth), str(PDP_C_122_F_IR_SED_Truth),
                            str(PDP_C_122_LD_INTERFEROMETRY_Truth), str(PDP_C_122_LOGG_PHOTOMETRY_Truth), str(PDP_C_122_L_SED_Truth),
                            str(PDP_C_122_METADATA_INTERFEROMETRY_Truth), str(PDP_C_122_METADATA_IRFM_Truth), str(PDP_C_122_METADATA_PHOTOMETRY_Truth),
                            str(PDP_C_122_METADATA_SBCR_Truth), str(PDP_C_122_METADATA_SED_Truth), str(PDP_C_122_RADIUS_INTERFEROMETRY_Truth)]
                            ])
        file.write(tableObj4.draw())
        PDP_C_122_RADIUS_IRFM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_RADIUS_IRFM.csv'))
        PDP_C_122_RADIUS_SBCR_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_RADIUS_SBCR.csv'))
        PDP_C_122_RADIUS_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_RADIUS_SED.csv'))
        PDP_C_122_TEFF_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_INTERFEROMETRY.csv'))
        PDP_C_122_TEFF_IRFM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_IRFM.csv'))
        PDP_C_122_TEFF_PHOTOMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_PHOTOMETRY.csv'))
        PDP_C_122_TEFF_SBCR_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_SBCR.csv'))
        PDP_C_122_TEFF_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_TEFF_SED.csv'))
        PDP_C_122_THETA_INTERFEROMETRY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_THETA_INTERFEROMETRY.csv'))
        PDP_C_122_THETA_IRFM_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_THETA_IRFM.csv'))
        PDP_C_122_THETA_SBCR_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_THETA_SBCR.csv'))
        PDP_C_122_THETA_SED_Truth = os.path.isfile(os.path.join(FilePaths.loc[0,'PreparatoryData_Path'], 'PDP_C_122_THETA_SED.csv'))
        file.write("\nFurther.\n")
        """Create texttable object"""
        tableObj5 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj5.set_cols_align(["c", "c", "c", "c", "c","c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj5.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj5.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj5.add_rows([["PDP_C_122_RADIUS_IRFM", "PDP_C_122_RADIUS_SBCR", "PDP_C_122_RADIUS_SED",
                            "PDP_C_122_TEFF_INTERFEROMETRY", "PDP_C_122_TEFF_IRFM", "PDP_C_122_TEFF_PHOTOMETRY",
                            "PDP_C_122_TEFF_SBCR", "PDP_C_122_TEFF_SED", "PDP_C_122_THETA_INTERFEROMETRY",
                            "PDP_C_122_THETA_IRFM", "PDP_C_122_THETA_SBCR", "PDP_C_122_THETA_SED"],
                            [str(PDP_C_122_RADIUS_IRFM_Truth), str(PDP_C_122_RADIUS_SBCR_Truth), str(PDP_C_122_RADIUS_SED_Truth),
                            str(PDP_C_122_TEFF_INTERFEROMETRY_Truth), str(PDP_C_122_TEFF_IRFM_Truth), str(PDP_C_122_TEFF_PHOTOMETRY_Truth),
                            str(PDP_C_122_TEFF_SBCR_Truth), str(PDP_C_122_TEFF_SED_Truth), str(PDP_C_122_THETA_INTERFEROMETRY_Truth),
                            str(PDP_C_122_THETA_IRFM_Truth), str(PDP_C_122_THETA_SBCR_Truth), str(PDP_C_122_THETA_SED_Truth)]
                            ])
        file.write(tableObj5.draw())
        file.write("\nInputs Read Successfully.\n")
        file.write("The Outputs produced are as shown below:\n")
        DP4PQRead = pd.read_csv(os.path.join(FilePaths.loc[0,'GeneratedOutputs_Path'],'Corrected_Light_Curves.csv'))
        Path_IDP_122_TEFF_SAPP = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_TEFF_SAPP.csv')
        IDP_122_TEFF_SAPP_out = DP4PQRead.to_csv(Path_IDP_122_TEFF_SAPP, index = False)

        Path_IDP_122_LOGG_SAPP = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_LOGG_SAPP.csv')
        IDP_122_LOGG_SAPP_out = DP4PQRead.to_csv(Path_IDP_122_LOGG_SAPP, index = False)

        Path_IDP_122_M_H_SAPP = os.path.join(FolderPathForReport, 'MSAP2', 'IDP_122_M_H_SAPP.csv')
        IDP_122_M_H_SAPP_out = DP4PQRead.to_csv(Path_IDP_122_M_H_SAPP, index = False)

        IDP_122_TEFF_SAPP_Truth = os.path.isfile(Path_IDP_122_TEFF_SAPP)
        IDP_122_LOGG_SAPP_Truth = os.path.isfile(Path_IDP_122_LOGG_SAPP)
        IDP_122_M_H_SAPP_Truth = os.path.isfile(Path_IDP_122_M_H_SAPP)

        """Create texttable object"""
        tableObj6 = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj6.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj6.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj6.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj6.add_rows([["IDP_122_TEFF_SAPP", "IDP_122_LOGG_SAPP", "IDP_122_M_H_SAPP"],
                            [str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_LOGG_SAPP_Truth), str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj6.draw())
        """Writing the details of Outputs produced from Bayesian Inference into the Overall SAS workflow Report"""
        Overall_Report_file = open(os.path.join(FolderPathForReport, ReportName), "a")
        Overall_Report_file.write("\nMSAP2: Implementation of Bayesian Inference in MSAP2, produces the following outputs:")
        Overall_Report_file.write("\n")
        Overall_Report_file.write(tableObj6.draw())
        Overall_Report_file.close()
        file.close()
    """The function below performs completion of MSAP2 datapipeline"""
    def Stop_MSAP2():
        """For each function that was described above, we check if that function was run or not by reading the respective .csv files,
        we later delete these files"""
        Process_01 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP2', 'SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01.csv'))
        Process_02 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP2', 'SPECTROSCOPY_MSAP2_02.csv'))
        Process_03 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP2', 'BAYESIAN_INFERENCE_MSAP2_03.csv'))

        """Writing in the file"""
        file = open(os.path.join(FolderPathForReport, "Report_MSAP2.txt"), "a")
        file.write("\n.\nMSAP2 Completed:\n")
        file.write("***--- End of Report ---***\n")
        file.close()
        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c","c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t","t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b","b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01", "SPECTROSCOPY_MSAP2_02",
                            "BAYESIAN_INFERENCE_MSAP2_03"],
                            [str(Process_01), str(Process_02),
                            str(Process_03)]
                            ])
        """Write the Table in the Report"""
        """Writing the operations summary on Main report"""
        Main_Report_file = open(os.path.join(FolderPathForReport, ReportName), "a")
        Main_Report_file.write("\n")
        Main_Report_file.write("\nThe summary of all the processes in MSAP2 is:\n")
        Main_Report_file.write(tableObj.draw())
        Main_Report_file.write("\nMSAP2 completed.\n")
        Main_Report_file.write("\n---**---MSAP2-END---**---\n")
        Main_Report_file.close()

if __name__ == '__Start_MSAP2__':
    Start_MSAP2()
if __name__ == '__PDP_IDP_DP3_Checks_MSAP2__':
    PDP_IDP_DP3_Checks_MSAP2()
if __name__ == '__SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01__':
    SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01()
if __name__ == '__SPECTROSCOPY_MSAP2_02__':
    SPECTROSCOPY_MSAP2_02()
if __name__ == '__BAYESIAN_INFERENCE_MSAP2_03__':
    BAYESIAN_INFERENCE_MSAP2_03()
if __name__ == '__Stop_MSAP2__':
    Stop_MSAP2()
