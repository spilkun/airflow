"""Importing libraries"""
import pandas as pd
import os
import texttable
from pathlib import Path
from datetime import datetime
import Operations
"""Define the home directory"""
HOME_DIR = str(Path.home())
"""We define the path for the input and output file location"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
FilePaths = pd.read_csv(os.path.join(UserDirectoryPath, 'FilePaths.csv'), delimiter = ',')
"""Defining the path to the folder where all outputs are stored, this is based on
the date and time at which the data pipeline was run"""
FolderPathForReport = FilePaths.loc[0, 'FolderPathForReport']#os.path.join(GeneratedOutputs_Path, FolderName)
"""We call the Operations class that will be used in case one wants to delete a temporary file,
or if one wants to copy contents from one file to another"""
Operations_Class = Operations.OperationsClass
class Overall_MSAP5:
    """The class defined is used to decribe the Overall MSAP5 data pipeline, the different functions defined below
    represent the tasks performed in the different operators of the pipeline,
    in the current version of the code, we use these functions to produce a report"""
    def Start_MSAP5():
        """We generate one report for the overall work flow and one which is specific to overall MSAP5 data pipeline,
        thus in certain information is repeated in both the reports"""
        now = datetime.now()
        """Writing the content in the report specific to Overall MSAP5"""
        file = open(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"), "a")
        file.write("\n.\n")
        file.write("*** --- Start of Report - MSAP5 --- ***\n\n")
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        file.write("MSAP5 is initialized.\n")
        file.close()
        """Writing the content in the child report of Overall SAS workflow"""
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))

    """The function below performs the input checks - DP2"""
    def InitialInputCheckup_MSAP5_DP2():
        """Writing the content in the report specific to Overall MSAP5"""
        file = open(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_A.txt"), "a")
        file.write("\n.\nInputs Check MSAP5: DP2 Check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        if os.path.isfile(os.path.join(FilePaths.loc[0, 'DP2_Path'], 'DP2.csv'))==True:
            file.write("MSAP5_InputsCheck: DP2.csv file exists.\n")
        else:
            file.write("MSAP5_InputsCheck: DP2.csv does not file exist.\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_A.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_A.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_A.txt"))

    """The function below performs the input checks - DP4"""
    def InitialInputCheckup_MSAP5_DP4():
        """Writing the content in the report specific to Overall MSAP5"""
        file = open(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_B.txt"), "a")
        file.write("\n.\nInputs Check MSAP5: DP4 Check.\n")
        file.write("\nInputs Check MSAP5: The following DP4 products are checked.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        DP4_123_DELTAPROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_DELTAPROT.csv'))
        DP4_123_DELTAPROT_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_DELTAPROT_METADATA.csv'))
        DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_HARVEY1_AMPLITUDE.csv'))
        DP4_123_HARVEY_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_HARVEY_METADATA.csv'))
        DP4_123_PCYCLE_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PCYCLE.csv'))
        DP4_123_PCYCLE_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PCYCLE_METADATA.csv'))
        DP4_123_PROT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP4_123_PROT.csv'))
        DP4_123_PROT_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'DP_123_PROT_METADATA.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP4_123_DELTAPROT", "DP4_123_DELTAPROT_METADATA", "DP4_123_HARVEY1_AMPLITUDE",
                            "DP4_123_HARVEY_METADATA", "DP4_123_PCYCLE", "DP4_123_PCYCLE_METADATA",
                            "DP4_123_PROT", "DP4_123_PROT_METADATA"],
                            [str(DP4_123_DELTAPROT_Truth), str(DP4_123_DELTAPROT_METADATA_Truth), str(DP4_123_HARVEY1_AMPLITUDE_Truth),
                            str(DP4_123_HARVEY_METADATA_Truth), str(DP4_123_PCYCLE_Truth), str(DP4_123_PCYCLE_METADATA_Truth),
                            str(DP4_123_PROT_Truth), str(DP4_123_PROT_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_B.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_B.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_B.txt"))

    """The function below performs the input checks - DP3"""
    def InitialInputCheckup_MSAP5_DP3():
        """Writing the content in the report specific to Overall MSAP5"""
        file = open(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_C.txt"), "a")
        file.write("\n.\nInputs Check MSAP5: DP3 Check.\n")
        file.write("\nInputs Check MSAP5: The following DP3 Products are present.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        DP3_128_DELTA_NU_AV_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV.csv'))
        DP3_128_DELTA_NU_AV_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_DELTA_NU_AV_METADATA.csv'))
        DP3_128_NU_MAX_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX.csv'))
        DP3_128_NU_MAX_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP3_Output_Path'], 'DP3_128_NU_MAX_METADATA.csv'))
        DP3_128_OSC_FREQ_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ.csv'))
        DP3_128_OSC_FREQ_COVMAT_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ_COVMAT.csv'))
        DP3_128_OSC_FREQ_METADATA_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'PreparatoryData_Path'], 'DP3_128_OSC_FREQ_METADATA.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["DP3_128_DELTA_NU_AV", "DP3_128_DELTA_NU_AV_METADATA", "DP3_128_NU_MAX",
                            "DP3_128_NU_MAX_METADATA", "DP3_128_OSC_FREQ", "DP3_128_OSC_FREQ_COVMAT",
                            "DP3_128_OSC_FREQ_METADATA"],
                            [str(DP3_128_DELTA_NU_AV_Truth), str(DP3_128_DELTA_NU_AV_METADATA_Truth), str(DP3_128_NU_MAX_Truth),
                            str(DP3_128_NU_MAX_METADATA_Truth), str(DP3_128_OSC_FREQ_Truth), str(DP3_128_OSC_FREQ_COVMAT_Truth),
                            str(DP3_128_OSC_FREQ_METADATA_Truth)]
                            ])
        file.write(tableObj.draw())
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_C.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_C.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_C.txt"))

    """The function below performs the input checks - IDP_123_LOGG_VARLC"""
    def InitialInputCheckup_MSAP5_IDP_123_LOGG_VARLC():
        """Writing the content in the report specific to Overall MSAP5"""
        file = open(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_D.txt"), "a")
        file.write("\n.\nInputs Check MSAP5: IDP_123_LOGG_VARLC Check.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")
        if os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP4_Output_Path'], 'IDP_123_LOGG_VARLC.csv'))==True:
            file.write("MSAP5_IDP_Input_Check: IDP_123_LOGG_VARLC.csv file exists.\n")
        else:
            file.write("MSAP5_IDP_Input_Check: IDP_123_LOGG_VARLC.csv does not file exist.\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_D.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_D.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp_branch_D.txt"))

    """The function below performs the input checks - Outputs from MSAP2"""
    def InitialInputCheckup_MSAP5_OP_MSAP2():
        """Writing the content in the report specific to Overall MSAP5"""
        file = open(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"), "a")
        file.write("\n.\nInputs Check MSAP5: Check for outputs produced from MSAP2.\n")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("Time Stamp: " + str(timeStamp) + "\n")

        IDP_122_LOGG_SAPP_TMP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_LOGG_SAPP_TMP.csv'))
        IDP_122_TEFF_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SPECTROSCOPY.csv'))
        IDP_122_LOGG_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_LOGG_SPECTROSCOPY.csv'))
        IDP_122_VSINI_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_VSINI_SPECTROSCOPY.csv'))
        IDP_122_ELEM1_ELEM2_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ELEM1_ELEM2_SPECTROSCOPY.csv'))
        IDP_122_ALPHA_FE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_ALPHA_FE_SPECTROSCOPY.csv'))
        ADP_122_MICROTURBULENCE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'ADP_122_MICROTURBULENCE_SPECTROSCOPY.csv'))
        ADP_122_MACROTURBULENCE_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'ADP_122_MACROTURBULENCE_SPECTROSCOPY.csv'))
        ADP_122_FLAGS_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'ADP_122_FLAGS_SPECTROSCOPY.csv'))
        ADP_122_METADATA_STAT_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'ADP_122_METADATA_STAT_SPECTROSCOPY.csv'))
        ADP_122_RADIAL_VELOCITY_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'ADP_122_RADIAL_VELOCITY_SPECTROSCOPY.csv'))
        ADP_122_MOD_BESTFIT_SPECTROSCOPY_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'ADP_122_MOD_BESTFIT_SPECTROSCOPY.csv'))
        IDP_122_TEFF_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_TEFF_SAPP.csv'))
        IDP_122_LOGG_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_LOGG_SAPP.csv'))
        IDP_122_M_H_SAPP_Truth = os.path.isfile(os.path.join(FilePaths.loc[0, 'MSAP2_Output_Path'], 'IDP_122_M_H_SAPP.csv'))
        """Create texttable object"""
        tableObj = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["IDP_122_LOGG_SAPP_TMP", "IDP_122_TEFF_SPECTROSCOPY", "IDP_122_LOGG_SPECTROSCOPY",
                            "IDP_122_VSINI_SPECTROSCOPY", "IDP_122_ELEM1_ELEM2_SPECTROSCOPY", "IDP_122_ALPHA_FE_SPECTROSCOPY",
                            "ADP_122_MICROTURBULENCE_SPECTROSCOPY", "ADP_122_MACROTURBULENCE_SPECTROSCOPY"],
                            [str(IDP_122_LOGG_SAPP_TMP_Truth), str(IDP_122_TEFF_SPECTROSCOPY_Truth), str(IDP_122_LOGG_SPECTROSCOPY_Truth),
                            str(IDP_122_VSINI_SPECTROSCOPY_Truth), str(IDP_122_ELEM1_ELEM2_SPECTROSCOPY_Truth), str(IDP_122_ALPHA_FE_SPECTROSCOPY_Truth),
                            str(ADP_122_MICROTURBULENCE_SPECTROSCOPY_Truth), str(ADP_122_MACROTURBULENCE_SPECTROSCOPY_Truth)]
                            ])
        file.write(tableObj.draw())
        file.write("\nFurther.\n")
        """Create texttable object"""
        tableObj2 = texttable.Texttable(max_width=190)
        """Set column alignment (center alignment)"""
        tableObj2.set_cols_align(["c", "c", "c", "c", "c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj2.set_cols_dtype(["t", "t", "t", "t", "t", "t", "t"])
        """Adjust columns"""
        tableObj2.set_cols_valign(["b", "b", "b", "b", "b", "b", "b"])
        """Insert rows in the table"""
        tableObj2.add_rows([["ADP_122_FLAGS_SPECTROSCOPY", "ADP_122_METADATA_STAT_SPECTROSCOPY", "ADP_122_RADIAL_VELOCITY_SPECTROSCOPY",
                            "ADP_122_MOD_BESTFIT_SPECTROSCOPY", "IDP_122_TEFF_SAPP", "IDP_122_LOGG_SAPP",
                            "IDP_122_M_H_SAPP"],
                            [str(ADP_122_FLAGS_SPECTROSCOPY_Truth), str(ADP_122_METADATA_STAT_SPECTROSCOPY_Truth), str(ADP_122_RADIAL_VELOCITY_SPECTROSCOPY_Truth),
                            str(ADP_122_MOD_BESTFIT_SPECTROSCOPY_Truth), str(IDP_122_TEFF_SAPP_Truth), str(IDP_122_LOGG_SAPP_Truth),
                            str(IDP_122_M_H_SAPP_Truth)]
                            ])
        file.write(tableObj2.draw())
        file.write("\nMSAP5 Inputs check completed.")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))

    def MSAP5_3_Selection_and_Validation():
        Variable_MSAP5_3_Selection_and_Validation = {'MSAP5_3_Selection_and_Validation':['True']}
        Data_MSAP5_3_Selection_and_Validation = pd.DataFrame(Variable_MSAP5_3_Selection_and_Validation)
        Data_MSAP5_3_Selection_and_Validation.to_csv(path_or_buf = os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_3_Selection_and_Validation.csv'), header=True, index=False)

        file = open(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\n.\nTime Stamp: " + str(timeStamp) + "\n")
        file.write("IDP_128_DETECTION_FLAG is set to False.\n")
        file.write("MSAP5-3 Selection and Validation initialized.\n")
        file.close()
    """The function below performs the completion operation of Overall MSAP5"""
    def Stop_MSAP5():
        """Writing the content in the report specific to Overall MSAP5"""
        file = open(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"), "a")
        now = datetime.now()
        timeStamp = now.strftime("%b-%d-%Y %H-%M-%S")
        file.write("\n.\nTime Stamp: " + str(timeStamp) + "\n")
        """For each function that is described, we check if that function was run or not by reading the respective .csv files,
        we later delete these files"""
        Process_01 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_Part1', 'MSAP5_1_MRA_Determination_From_Seismic_Data.csv'))
        Process_02 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_2_MRA_determination_from_Non_Seismic_data.csv'))
        Process_03 = Operations_Class.ReturnProcessValue(os.path.join(FolderPathForReport, 'MSAP5', 'MSAP5_3_Selection_and_Validation.csv'))

        tableObj = texttable.Texttable()
        """Set column alignment (center alignment)"""
        tableObj.set_cols_align(["c", "c", "c"])
        """Set datatype of each column (text type)"""
        tableObj.set_cols_dtype(["t", "t", "t"])
        """Adjust columns"""
        tableObj.set_cols_valign(["b", "b", "b"])
        """Insert rows in the table"""
        tableObj.add_rows([["MSAP5_1_MRA_Determination_From_Seismic_Data", "MSAP5_2_MRA_determination_from_Non_Seismic_data", "MSAP5_3_Selection_and_Validation"],
                            [str(Process_01), str(Process_02), str(Process_03)]
                            ])
        """Write the Table in the Report"""
        file.write("\n")
        file.write("\nThe summary of all the processes in MSAP5 is:\n")
        file.write(tableObj.draw())
        file.write("\nMSAP5 Ends.\n")
        file.write("---**END--OF--REPORT**---\n")
        file.close()
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, "Report_Overall_MSAP5.txt"), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))
        Operations_Class.CopyFile1_File2(os.path.join(FolderPathForReport, FilePaths.loc[0, 'ReportName']), os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))
        Operations_Class.RemoveFile(os.path.join(FolderPathForReport, "Report_Overall_MSAP5_temp.txt"))

if __name__ == '__Start_MSAP5__':
    Start_MSAP5()
if __name__ == '__InitialInputCheckup_MSAP5_DP2__':
    InitialInputCheckup_MSAP5_DP2()
if __name__ == '__InitialInputCheckup_MSAP5_DP4__':
    InitialInputCheckup_MSAP5_DP4()
if __name__ == '__InitialInputCheckup_MSAP5_DP3__':
    InitialInputCheckup_MSAP5_DP3()
if __name__ == '__InitialInputCheckup_MSAP5_IDP_123_LOGG_VARLC__':
    InitialInputCheckup_MSAP5_IDP_123_LOGG_VARLC()
if __name__ == '__InitialInputCheckup_MSAP5_OP_MSAP2__':
    InitialInputCheckup_MSAP5_OP_MSAP2()
if __name__ == '__MSAP5_3_Selection_and_Validation__':
    MSAP5_3_Selection_and_Validation()
