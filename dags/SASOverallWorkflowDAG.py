"""Import the libraries"""
from pathlib import Path
import os
import pandas as pd
import texttable
from airflow import DAG
from datetime import datetime
"""These are the different operators we use"""
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.sensors.file_sensor import FileSensor
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
"""We then import the Trigger Rule that defines how/under what condition should an operator be executed"""
from airflow.utils.trigger_rule import TriggerRule
"""Each of the MSAP1 upto MSAP5 is implemented using a task group"""
from airflow.utils.task_group import TaskGroup
"""MSAP1 to MSAP5 are python files created manually to read data from a given input"""
import CheckInputs, ProduceCorrectedLightCurves, Create_ClassicStellarParameters
import Create_DP3Out, Create_DP4Out, Create_DP5Out
import MSAP5_Part1, MSAP5_Part2, MSAP5_Part3
"""Defining the various paths"""
"""Home Path"""
HOME_DIR = str(Path.home())
"""Path to user directory, containing information about the different user inputs"""
UserDirectoryPath = os.path.join(HOME_DIR, 'userdirectory')
OutputPath = os.path.join(UserDirectoryPath, 'output') #'airflow',
InputsPath = os.path.join(UserDirectoryPath, 'inputs') #'airflow',
PathToGeneratedOutputs = os.path.join(OutputPath, 'GeneratedOutputs') #'airflow',
"""Config file is a csv file containing user input parameters to run the data pipelines"""
UserInputFilePath = os.path.join(HOME_DIR, 'userdirectory', 'UserInputs.csv') #'airflow',
UserInputData = pd.read_csv(UserInputFilePath, index_col=None)
"""The 4 flags, are extracted below from the config file"""
RotationPeriod = UserInputData.loc[0, 'RotationPeriod']
ActivityLevel = UserInputData.loc[0, 'ActivityLevel']
LoggGranulation = UserInputData.loc[0, 'LoggGranulation']
OscillationDetectionFlag = UserInputData.loc[0, 'Oscillations']
"""We note doen the current date and time, this is important because
based on the date and time a folder is generated in the GeneratedOutputs folder
This folder contains all the outputs from the data-pipelines"""
now = datetime.now()
dt_string = now.strftime("%B_%d_%Y_%H_%M")
"""ReportID.txt contains the name of the report describing the overall data flow
This report ID information is based on the date and time at which the data pipeline was run"""
if os.path.isfile(os.path.join(PathToGeneratedOutputs, "ReportID.txt"))==False:
    F = open(os.path.join(PathToGeneratedOutputs, "ReportID.txt"), "w")
    F.close()
else:
    print("ReportID.txt exists")
"""FolderName.txt contains the name of the folder based on the date and time at which the data pipeline was run"""
if os.path.isfile(os.path.join(PathToGeneratedOutputs, "FolderName.txt"))==False:
    F2 = open(os.path.join(PathToGeneratedOutputs, "FolderName.txt"), "w")
    F2.close()
else:
    print("FolderName.txt exists")
"""Defining the path for the new folder created using time stamp"""
F = open(os.path.join(PathToGeneratedOutputs, "ReportID.txt"), "r")
"""Reads the first line of the report file"""
ReportName = F.readline()
F.close()
"""Reads the foldername"""
F2 = open(os.path.join(PathToGeneratedOutputs, "FolderName.txt"), "r")
"""Reads the first line of the report file"""
FolderName = F2.readline()
F2.close()
"""Creates the path to the Folder name where all outputs are stored"""
FolderPathForReport = os.path.join(PathToGeneratedOutputs, FolderName)

"""Functions and Operations Specific to MSAP3 Begins
Defining the conditional statements
If IDP_125_DETECTION_FLAG is true we will go ahead with Global parameters extraction"""
def IDP_125_DETECTION_FLAG_Yes_N0_MSAP3_T_F():
    if bool(OscillationDetectionFlag) == True:
        return 'MSAP3.GlobalParametersExtraction02_MSAP3'
    else:
        return 'MSAP3.IDP_125_DETECTION_FLAG_MSAP3_False'
"""If Peak Bagging Flag is true we will perform Peak Bagging"""
def PEAK_BAGGING_FLAG_Yes_N0_T_F_MSAP3():
    # PeakBaggingFlagDataPath = os.path.join(UserDirectoryPath, 'IDP_128_PEAK_BAGGING_FLAG.csv') #'airflow',
    # PeakBaggingFlagData = pd.read_csv(PeakBaggingFlagDataPath, index_col=None)
    IDP_128_PEAK_BAGGING_FLAG = UserInputData.loc[0, 'Peak_Bagging_Flag']
    if bool(IDP_128_PEAK_BAGGING_FLAG) == True:
        return 'MSAP3.PEAK_BAGGING_MSAP3_04'
    else:
        return 'MSAP3.PEAK_BAGGING_FLAG_FALSE_MSAP3'
"""Functions and Operations Specific to MSAP3 Ends"""

"""Functions Specific to MSAP4 Begins"""
"""Reading the Spot Modelling Flag"""
# SpotModelling05Path = os.path.join(UserDirectoryPath, 'SpotModelling.csv')#'airflow',
# SpotModelling05Data = pd.read_csv(SpotModelling05Path, index_col=None)
SpotModelling05Flag = UserInputData.loc[0, 'SpotModelling_Flag']
"""Defining the conditional statements for MSAP4"""
def SpotModelling_Y_N_MSAP4():
    if bool(SpotModelling05Flag) == True:
        Main_Report_file = open(os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"), "a")
        Main_Report_file.write("\nSpot Modelling Flag was set to True.")
        Main_Report_file.write("\nSpot Modelling Produces the following outputs.")
        Main_Report_file.close()
        return 'MSAP4.SpotModelling05'
    else:
        Main_Report_file = open(os.path.join(FolderPathForReport, "Main_Report_MSAP4.txt"), "a")
        Main_Report_file.write("\nSpot Modelling Flag was set to False.")
        Main_Report_file.close()
        return 'MSAP4.SpotModelling05_False_MSAP4'
"""Functions Specific to MSAP4 Ends"""

""""Function Specific to MSAP2 Begins"""
def IDP_128_DetectionFlag_OR_LoggGranulation_T_F_MSAP2():
    if (OscillationDetectionFlag or LoggGranulation) == True:
        return 'MSAP2.Start_MSAP2'
    else:
        return 'IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2'
"""Function Specific to MSAP2 ends"""

"""Function Specific to Overall MSAP5 Begins"""
def IDP_128_DETECTION_FLAG_Y_N_MSAP5():
    """IDP128_DETECTION_FLAG is same as Oscillations"""
    # IDP_128_DETECTION_FLAG_Path = os.path.join(UserDirectoryPath, 'config.csv') #'airflow',
    # IDP_128_DETECTION_FLAG_Data = pd.read_csv(IDP_128_DETECTION_FLAG_Path, index_col=None)
    IDP_128_DETECTION_FLAG = UserInputData.loc[0, 'Oscillations']
    if bool(IDP_128_DETECTION_FLAG) == True:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Start_MSAP5_1'
    else:
        return 'MSAP5.MSAP5_3_Selection_and_Validation_TaskGroup.Start_MSAP5_3'

"""Function Specific to Overall MSAP5 Ends"""

"""Function Specific to MSAP5 Part1 Begins"""
def IndividualFrequencies_Y_N_MSAP5_1():
    """Individual Frequencies/IDP_128_QUALITY_METADATA is same as the IDP_128_PEAK_BAGGING_FLAG"""
    # IDP_128_QUALITY_METADATA_Path = os.path.join(UserDirectoryPath, 'IDP_128_PEAK_BAGGING_FLAG.csv') #'airflow',
    # IDP_128_QUALITY_METADATA_Data = pd.read_csv(IDP_128_QUALITY_METADATA_Path, index_col=None)
    IDP_128_QUALITY_METADATA = UserInputData.loc[0, 'Peak_Bagging_Flag']
    if bool(IDP_128_QUALITY_METADATA) == True:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Mixed_Modes_Yes_No_MSAP5_1'
    else:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.End_From_MSAP5_1'
def Mixed_Modes_Y_N_MSAP5_1():
    # Mixed_Modes_Path = os.path.join(UserDirectoryPath, 'Mixed_Modes.csv')
    # Mixed_Modes_Data = pd.read_csv(Mixed_Modes_Path, index_col=None)
    Mixed_Modes_FLAG = UserInputData.loc[0, 'Mixed_Modes_Flag']
    if bool(Mixed_Modes_FLAG) == True:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.GridBasedInferenceMixedModes_13'
    else:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.DummyOperatorConnecting_14_15'
def SofesticatedMethod_Y_N_MSAP5_1_From13():
    # Sofesticated_Method_Mixed_ModesPath = os.path.join(UserDirectoryPath, 'Sofesticated_Method_Mixed_Modes.csv')
    # Sofesticated_Method_Mixed_Modes_Data = pd.read_csv(Sofesticated_Method_Mixed_ModesPath, index_col=None)
    Sofesticated_Method_Mixed_Modes_FLAG = UserInputData.loc[0, 'Sofesticated_Method_Mixed_Modes']
    if bool(Sofesticated_Method_Mixed_Modes_FLAG) == True:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Interpolation_Yes_No_From13'
    else:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.End_From13'
def Interpolation_Y_N_From13():
    # Interpolation_Mixed_Modes_Path = os.path.join(UserDirectoryPath, 'Interpolation_Mixed_Modes.csv')
    # Interpolation_Mixed_Modes_Data = pd.read_csv(Interpolation_Mixed_Modes_Path, index_col=None)
    Interpolation_Mixed_Modes_FLAG = UserInputData.loc[0, 'Interpolation_Mixed_Modes']
    if bool(Interpolation_Mixed_Modes_FLAG) == True:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Interpolation_131'
    else:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Inversion_132'
def SofesticatedMethod_Y_N_MSAP5_1_From_14_15():
    # Sofesticated_Method_P_Modes_Path = os.path.join(UserDirectoryPath, 'Sofesticated_Method_P_Modes.csv')
    # Sofesticated_Method_P_Modes_Data = pd.read_csv(Sofesticated_Method_P_Modes_Path, index_col=None)
    Sofesticated_Method_P_Modes_FLAG = UserInputData.loc[0, 'Sofesticated_Method_P_Modes']
    if bool(Sofesticated_Method_P_Modes_FLAG) == True:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Glitches_16'
    else:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.End_From_14_15'
def Inversion_Y_N_MSAP5_1():
    # InversionFlag_Path = os.path.join(UserDirectoryPath, 'InversionFlag.csv')
    # InversionFlag_Data = pd.read_csv(InversionFlag_Path, index_col=None)
    InversionFlag = UserInputData.loc[0, 'InversionFlag']
    if bool(InversionFlag) == True:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Interpolation_Yes_No_MSAP5_1_From_161'
    else:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.End_From_161'
def Interpolation_Y_N_MSAP5_1_From_161():
    # Interpolation_P_Modes_Path = os.path.join(UserDirectoryPath, 'Interpolation_P_Modes.csv')
    # Interpolation_P_Modes_Data = pd.read_csv(Interpolation_P_Modes_Path, index_col=None)
    Interpolation_P_Modes_FLAG = UserInputData.loc[0, 'Interpolation_P_Modes']
    if bool(Interpolation_P_Modes_FLAG) == True:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Interpolation_162'
    else:
        return 'MSAP5.MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup.Inversion_163'

"""Function Specific to MSAP5 Part 1 Ends"""

"""Functions Specific to MSAP5 Part2 Begins"""
def Gyrochronocology_21_Y_N_MSAP5_2():
    PATH_MSAP4 = os.path.join(FolderPathForReport, 'MSAP4')
    PATH_DP4_123_PROT = os.path.join(PATH_MSAP4, 'DP4_123_PROT.csv')
    PATH_DP4_123_PROT_METADATA = os.path.join(PATH_MSAP4, 'DP4_123_PROT_METADATA.csv')
    DP4_123_PROT_Truth = os.path.isfile(PATH_DP4_123_PROT)
    DP4_123_PROT_METADATA_Truth = os.path.isfile(PATH_DP4_123_PROT_METADATA)
    if DP4_123_PROT_Truth or DP4_123_PROT_METADATA_Truth == True:
        return 'MSAP5.MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup.Gyrochronocology_21'
    else:
        return 'MSAP5.MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup.End_From_MSAP5_Part2_21'
def Activity_Age_22_Y_N_MSAP5_2():
    # PATH_MSAP4 = os.path.join(FolderPathForReport, 'MSAP4')
    # PATH_DP4_123_HARVEY1_AMPLITUDE = os.path.join(PATH_MSAP4, 'DP4_123_HARVEY1_AMPLITUDE.csv')
    # DP4_123_HARVEY1_AMPLITUDE_Truth = os.path.isfile(PATH_DP4_123_HARVEY1_AMPLITUDE)
    ActivityLevel_Flag = UserInputData.loc[0, 'ActivityLevel']
    if bool(ActivityLevel_Flag) == True:
        return 'MSAP5.MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup.Activity_Age_22'
    else:
        return 'MSAP5.MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup.End_From_MSAP5_Part2_22'
def IDP_123_LOGG_VARLC_Y_N_MSAP5_2():
    PATH_MSAP4 = os.path.join(FolderPathForReport, 'MSAP4')
    Path_IDP_123_LOGG_VARLC = os.path.join(PATH_MSAP4, 'IDP_123_LOGG_VARLC.csv')
    IDP_123_LOGG_VARLC_Truth = os.path.isfile(Path_IDP_123_LOGG_VARLC)
    if bool(IDP_123_LOGG_VARLC_Truth) == True:
        return 'MSAP5.MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup.Dummy_Operator_MSAP5_Part2_23_24'
    else:
        return 'MSAP5.MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup.End_From_MSAP5_Part2_23_24'
def TransitMeanDensity_Y_N_MSAP5_2():
    TransitMeanDensity_Path = os.path.join(InputsPath, 'NonMandatoryInputs', 'DP2', 'DP2_RHO_TRANSIT.csv')
    TransitMeanDensity_Truth = os.path.isfile(TransitMeanDensity_Path)
    if bool(TransitMeanDensity_Truth) == True:
        return 'MSAP5.MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup.Dummy_Operator_MSAP5_Part2_25_26'
    else:
        return 'MSAP5.MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup.End_From_MSAP5_Part2_25_26'
"""Functions Specific to MSAP5 Part2 Ends"""

"""Functions Specific to MSAP5 Part3 Begins"""
def Flags_Y_N_MSAP5_3():
    Flags_Path = os.path.join(UserDirectoryPath, 'IDP_125_CONSISTENCY_FLAG.csv')
    Flags_Data = pd.read_csv(Flags_Path, index_col=None)
    FLAGS = Flags_Data.loc[0, 'IDP_125_CONSISTENCY_FLAG']
    if bool(FLAGS) == True:
        return 'MSAP5.MSAP5_3_Selection_and_Validation_TaskGroup.WeighingOfDatasandBayesianDecision_32'
    else:
        return 'MSAP5.MSAP5_3_Selection_and_Validation_TaskGroup.Alert_33'
"""Functions Specific to MSAP5 Part3 Ends"""

"""defining default arguements"""
default_args = {
    "start_date" :datetime(2020, 11, 11),
    "owner" : "airflow"
    }

"""instantiate the DAG (Directed Acyclic Graph / The data pipeline architecture defined) object"""
with DAG(dag_id = "SASOverallWorkflowDAG", schedule_interval ="@daily", default_args = default_args, catchup =False) as dag:
    Start = PythonOperator(task_id="Start", python_callable=CheckInputs.start)
    """Define a SubDAG/TaskGroup called MSAP1"""
    with TaskGroup("MSAP1") as MSAP1:
        Class_MSAP1 = ProduceCorrectedLightCurves.MSAP1
        """The start function calls another function in MSAP1 class that creates the Report file
        it also creates the path for the new folder created based on the timestamp"""
        Start_MSAP1 = PythonOperator(task_id="Start_MSAP1", python_callable=Class_MSAP1.Start_MSAP1, trigger_rule="none_failed_or_skipped")
        """we Create a taskgroup/subdag that performs initial input checks"""
        with TaskGroup ("InitialInputCheckup_MSAP1") as InitialInputCheckup_MSAP1:
            """Non mandatory input check, checks for the different DP4's"""
            NonMandatoryInputsCheck_MSAP1 = PythonOperator(task_id="NonMandatoryInputsCheck_MSAP1", python_callable=Class_MSAP1.NonMandatoryInputsCheck_MSAP1, trigger_rule="none_failed_or_skipped")
            """Check for DP1"""
            DP1Check_MSAP1 = PythonOperator(task_id="DP1Check_MSAP1", python_callable=Class_MSAP1.DP1Check_MSAP1, trigger_rule="none_failed_or_skipped")
            """Checks for DP2"""
            DP2Check_MSAP1 = PythonOperator(task_id="DP2Check_MSAP1", python_callable=Class_MSAP1.DP2Check_MSAP1, trigger_rule="none_failed_or_skipped")
            """Check for PDP-A and PDP-C"""
            PreparatoryDataCheck_MSAP1 = PythonOperator(task_id="PreparatoryDataCheck_MSAP1", python_callable=Class_MSAP1.PreparatoryDataCheck_MSAP1, trigger_rule="none_failed_or_skipped")
            """Prints in the report indicating the completion of the input checks"""
            InputCheck_MSAP1 = PythonOperator(task_id="InputCheck_MSAP1", python_callable=Class_MSAP1.InputCheck_MSAP1, trigger_rule="none_failed_or_skipped")
            """Defining Dependencies of the Task group that performs input check"""
            [NonMandatoryInputsCheck_MSAP1, DP1Check_MSAP1, DP2Check_MSAP1, PreparatoryDataCheck_MSAP1] >> InputCheck_MSAP1
        """The below function includes Flares check, transit check and Gaps Charaterization & Filling,
        which is looped for the different Flares, one can understand this better while having a look at the
        "ProduceCorrectedLightCurves.py" file"""
        MSAP1_01_02_03_Looped = PythonOperator(task_id="MSAP1_01_02_03_Looped", python_callable = Class_MSAP1.MSAP1_01_02_03_Looped, trigger_rule="none_failed_or_skipped")
        """The below function performs Stitching1"""
        Stitching1_05 = PythonOperator(task_id="Stitching1_05", python_callable=Class_MSAP1.Stitching1_05, trigger_rule="none_failed_or_skipped")
        """The below function executes KASOC filter after Stitching1 that was performed above"""
        KASOCFilter_07 = PythonOperator(task_id="KASOCFilter_07", python_callable=Class_MSAP1.KASOCFilter_07, trigger_rule="none_failed_or_skipped")
        """The below function performs Stitching2"""
        Stitching2_06 = PythonOperator(task_id="Stitching2_06", python_callable=Class_MSAP1.Stitching2_06, trigger_rule="none_failed_or_skipped")
        """The below function executes Detrending of instrumental residual effects after Stitching2"""
        Detrending_Of_Instrumental_Residual_Effects_04 = PythonOperator(task_id="Detrending_Of_Instrumental_Residual_Effects_04", python_callable=Class_MSAP1.Detrending_Of_Instrumental_Residual_Effects_04, trigger_rule="none_failed_or_skipped")
        """The below function executes Binning after Detrending of instrumental residual effects"""
        Binning_08 = PythonOperator(task_id="Binning_08", python_callable=Class_MSAP1.Binning_08, trigger_rule="none_failed_or_skipped")
        End = PythonOperator(task_id="End", python_callable=Class_MSAP1.End, trigger_rule="none_failed_or_skipped")

        """Defining the Dependencies for the MSAP1 task group"""
        Start_MSAP1 >> InitialInputCheckup_MSAP1
        InitialInputCheckup_MSAP1 >> MSAP1_01_02_03_Looped
        MSAP1_01_02_03_Looped >> [Stitching1_05, Stitching2_06]
        Stitching1_05 >> KASOCFilter_07
        Stitching2_06 >> Detrending_Of_Instrumental_Residual_Effects_04 >> Binning_08
        Binning_08 >> End
        KASOCFilter_07 >> End

    """Define SubDAG/TaskGroup for MSAP3 data pipeline"""
    with TaskGroup("MSAP3") as MSAP3:
        Class_MSAP3 = Create_DP3Out.MSAP3
        """The start function generates the report for MSAP3"""
        Start_MSAP3 = PythonOperator(task_id="Start_MSAP3", python_callable=Class_MSAP3.Start_MSAP3, trigger_rule = "none_failed_or_skipped")
        """Defining SubDAG/TaskGroup that performs input check for MSAP3"""
        with TaskGroup("InitialInputCheckup_MSAP3") as InitialInputCheckup_MSAP3:
            """Perform the input checks for Preparatory Data, Intermediate Data Products and DP3"""
            PrepDataCheck_MSAP3 = PythonOperator(task_id="PrepDataCheck_MSAP3", python_callable=Class_MSAP3.PrepDataCheck_MSAP3, trigger_rule="none_failed_or_skipped")
            IDP_dataCheck_MSAP3 = PythonOperator(task_id="IDP_dataCheck_MSAP3", python_callable=Class_MSAP3.IDP_dataCheck_MSAP3, trigger_rule="none_failed_or_skipped")
            DP3_dataCheck_MSAP3 = PythonOperator(task_id="DP3_dataCheck_MSAP3", python_callable=Class_MSAP3.DP3_dataCheck_MSAP3, trigger_rule="none_failed_or_skipped")
            """Defining Dependencies for TaskGroup to perform input check"""
            [PrepDataCheck_MSAP3, IDP_dataCheck_MSAP3, DP3_dataCheck_MSAP3]
        """Checking for Oscillation Detection True or False"""
        DETECTION_OF_OSCILLATIONS_01 = PythonOperator(task_id="DETECTION_OF_OSCILLATIONS_01", python_callable=Class_MSAP3.DETECTION_OF_OSCILLATIONS_01, trigger_rule="none_failed_or_skipped")
        """Check if IDP_125_DETECTION_FLAG is set to True or False"""
        IDP_125_DETECTION_FLAG_Yes_N0_MSAP3 = BranchPythonOperator(task_id='IDP_125_DETECTION_FLAG_Yes_N0_MSAP3', python_callable=IDP_125_DETECTION_FLAG_Yes_N0_MSAP3_T_F, provide_context=True, trigger_rule="none_failed_or_skipped")
        """If IDP_125_DETECTION_FLAG is True, perform Global Parameters Extraction"""
        GlobalParametersExtraction02_MSAP3 = PythonOperator(task_id="GlobalParametersExtraction02_MSAP3", python_callable=Class_MSAP3.GlobalParametersExtraction02_MSAP3, trigger_rule="none_failed_or_skipped")
        IDP_125_DETECTION_FLAG_MSAP3_False = PythonOperator(task_id="IDP_125_DETECTION_FLAG_MSAP3_False", python_callable=Class_MSAP3.IDP_125_DETECTION_FLAG_MSAP3_False, trigger_rule="none_failed_or_skipped")
        """Prepare the Data for Peak Bagging"""
        PREPARATION_FOR_PEAK_BAGGING_MSAP3_03 = PythonOperator(task_id="PREPARATION_FOR_PEAK_BAGGING_MSAP3_03", python_callable=Class_MSAP3.PREPARATION_FOR_PEAK_BAGGING_MSAP3_03, trigger_rule="none_failed_or_skipped")
        """We check for Peak Bagging, if Peak Bagging is set to True we perform Peak Bagging"""
        PEAK_BAGGING_FLAG_Yes_N0_MSAP3 = BranchPythonOperator(task_id='PEAK_BAGGING_FLAG_Yes_N0_MSAP3', python_callable=PEAK_BAGGING_FLAG_Yes_N0_T_F_MSAP3, provide_context=True, trigger_rule="none_failed_or_skipped")
        PEAK_BAGGING_MSAP3_04 = PythonOperator(task_id="PEAK_BAGGING_MSAP3_04", python_callable=Class_MSAP3.PEAK_BAGGING_MSAP3_04, trigger_rule="none_failed_or_skipped")
        PEAK_BAGGING_FLAG_FALSE_MSAP3 = PythonOperator(task_id="PEAK_BAGGING_FLAG_FALSE_MSAP3", python_callable=Class_MSAP3.PEAK_BAGGING_FLAG_FALSE_MSAP3, trigger_rule="none_failed_or_skipped")
        """End of MSAP3 SubDAG/TaskGroup"""
        Stop_MSAP3 = PythonOperator(task_id="Stop_MSAP3", python_callable=Class_MSAP3.Stop_MSAP3, trigger_rule="none_failed_or_skipped")

        """Defining the Dependencies for MSAP3 SubDAG/TaskGroup"""
        Start_MSAP3 >> InitialInputCheckup_MSAP3
        InitialInputCheckup_MSAP3 >> DETECTION_OF_OSCILLATIONS_01
        DETECTION_OF_OSCILLATIONS_01 >> IDP_125_DETECTION_FLAG_Yes_N0_MSAP3
        IDP_125_DETECTION_FLAG_Yes_N0_MSAP3 >> [GlobalParametersExtraction02_MSAP3, IDP_125_DETECTION_FLAG_MSAP3_False]
        GlobalParametersExtraction02_MSAP3 >> PREPARATION_FOR_PEAK_BAGGING_MSAP3_03
        PREPARATION_FOR_PEAK_BAGGING_MSAP3_03 >> PEAK_BAGGING_FLAG_Yes_N0_MSAP3
        PEAK_BAGGING_FLAG_Yes_N0_MSAP3 >> [PEAK_BAGGING_MSAP3_04, PEAK_BAGGING_FLAG_FALSE_MSAP3]
        PEAK_BAGGING_MSAP3_04 >> Stop_MSAP3
        PEAK_BAGGING_FLAG_FALSE_MSAP3 >> Stop_MSAP3
        IDP_125_DETECTION_FLAG_MSAP3_False >> Stop_MSAP3

    """Define the SubDAG/TaskGroup for MSAP4 datapipeline"""
    with TaskGroup("MSAP4") as MSAP4:
        Class_MSAP4 = Create_DP4Out.MSAP4
        """The start function generates the report for MSAP4"""
        Start_MSAP4 = PythonOperator(task_id="Start_MSAP4", python_callable=Class_MSAP4.Start_MSAP4, trigger_rule = "none_failed_or_skipped")
        """Defining the SubDAG/TaskGroup for input checks for MSAP4 datapipeline"""
        with TaskGroup("InitialInputCheckup_MSAP4") as InitialInputCheckup_MSAP4:
            """Checking for intermediate data products, DP4 of previous quarters and Preparatory Data products"""
            IntermediateDataProductsCheck_MSAP4 = PythonOperator(task_id="IntermediateDataProductsCheck_MSAP4", python_callable=Class_MSAP4.IntermediateDataProductsCheck_MSAP4, trigger_rule = "none_failed_or_skipped")
            DP4PQCheck_MSAP4 = PythonOperator(task_id="DP4PQCheck_MSAP4", python_callable=Class_MSAP4.DP4PQCheck_MSAP4, trigger_rule="none_failed_or_skipped")
            PDP_Check_MSAP4 = PythonOperator(task_id="PDP_Check_MSAP4", python_callable=Class_MSAP4.PDP_Check_MSAP4, trigger_rule="none_failed_or_skipped")
            """Defining Dependencies for MSAP4 input checks"""
            [IntermediateDataProductsCheck_MSAP4, DP4PQCheck_MSAP4, PDP_Check_MSAP4]
        """Now we define the different operators in the MSAP4 Datapipeline"""
        """Fourier analysis and Time series analysis are performed parallely"""
        FourierAnalysis01_MSAP4 = PythonOperator(task_id="FourierAnalysis01_MSAP4", python_callable=Class_MSAP4.FourierAnalysis01_MSAP4, trigger_rule = "none_failed_or_skipped")
        TimeSeriesAnalysis02_MSAP4 = PythonOperator(task_id="TimeSeriesAnalysis02_MSAP4", python_callable=Class_MSAP4.TimeSeriesAnalysis02_MSAP4, trigger_rule = "none_failed_or_skipped")
        """The Dummy_MSAP4 does nothing, its there to just connect the two operators described above"""
        Dummy_MSAP4 = BashOperator(task_id = "Dummy_MSAP4", bash_command = 'echo "This Operator does nothing !!"', trigger_rule = "none_failed_or_skipped")
        """Rotation Period determination and Gravity from flicker are performed parallely"""
        RotationPeriodDetermination03_MSAP4 = PythonOperator(task_id="RotationPeriodDetermination03_MSAP4", python_callable=Class_MSAP4.RotationPeriodDetermination03_MSAP4, trigger_rule = "none_failed_or_skipped")
        GravityFromFlicker_FliPer04_MSAP4 = PythonOperator(task_id="GravityFromFlicker_FliPer04_MSAP4", python_callable=Class_MSAP4.GravityFromFlicker_FliPer04_MSAP4, trigger_rule = "none_failed_or_skipped")
        """Conditional operator to check on spot modelling flag is defined below"""
        SpotModelling_Yes_No_MSAP4 = BranchPythonOperator(task_id='SpotModelling_Yes_No_MSAP4', python_callable=SpotModelling_Y_N_MSAP4, provide_context=True, trigger_rule = "none_failed_or_skipped")
        SpotModelling05 = PythonOperator(task_id="SpotModelling05", python_callable=Class_MSAP4.SpotModelling05, trigger_rule = "none_failed_or_skipped")
        SpotModelling05_False_MSAP4 = PythonOperator(task_id="SpotModelling05_False_MSAP4", python_callable=Class_MSAP4.SpotModelling05_False_MSAP4, trigger_rule = "none_failed_or_skipped")
        """The Dummy_MSAP4 does nothing, its there to just connect the two operators described above"""
        Dummy2_MSAP4 = BashOperator(task_id = "Dummy2_MSAP4", bash_command = 'echo "This Operator does nothing !!"', trigger_rule = 'none_failed_or_skipped')
        """Fix Rotation period and Activity cycle determination are performed parallely"""
        FixRotationPeriod07_MSAP4 = PythonOperator(task_id="FixRotationPeriod07_MSAP4", python_callable=Class_MSAP4.FixRotationPeriod07_MSAP4, trigger_rule = "none_failed_or_skipped")
        ActivityCycleDetermination06_MSAP4 = PythonOperator(task_id="ActivityCycleDetermination06_MSAP4", python_callable=Class_MSAP4.ActivityCycleDetermination06_MSAP4, trigger_rule = "none_failed_or_skipped")
        """End of MSAP4 data pipeline is defined with Stop_MSAP4 operator"""
        Stop_MSAP4 = PythonOperator(task_id="Stop_MSAP4", python_callable=Class_MSAP4.Stop_MSAP4, trigger_rule = "none_failed_or_skipped")
        """Defining the Dependencies of the MSAP4 datapipeline"""
        Start_MSAP4 >> InitialInputCheckup_MSAP4
        InitialInputCheckup_MSAP4 >> [FourierAnalysis01_MSAP4, TimeSeriesAnalysis02_MSAP4]
        [FourierAnalysis01_MSAP4, TimeSeriesAnalysis02_MSAP4] >> Dummy_MSAP4
        Dummy_MSAP4 >> [RotationPeriodDetermination03_MSAP4, GravityFromFlicker_FliPer04_MSAP4]
        [RotationPeriodDetermination03_MSAP4, GravityFromFlicker_FliPer04_MSAP4] >> SpotModelling_Yes_No_MSAP4
        SpotModelling_Yes_No_MSAP4 >> [SpotModelling05, SpotModelling05_False_MSAP4]
        [SpotModelling05, SpotModelling05_False_MSAP4] >> Dummy2_MSAP4
        Dummy2_MSAP4 >> [FixRotationPeriod07_MSAP4, ActivityCycleDetermination06_MSAP4]
        [FixRotationPeriod07_MSAP4, ActivityCycleDetermination06_MSAP4] >> Stop_MSAP4

    """Print out a message based on MSAP3 and MSAP4 data pipeline completion"""
    MSAP3_MSAP4_Completed = PythonOperator(task_id ="MSAP3_MSAP4_Completed", python_callable=CheckInputs.MSAP3_MSAP4_Completed, trigger_rule = 'none_failed_or_skipped')
    """We then carry out the conditional Operation, here the condition true or false is executed
    Based on if IDP_128_DetectionFlag OR LoggGranulation be True"""
    IDP_128_DetectionFlag_OR_LoggGranulation_True_False_MSAP2 = BranchPythonOperator(task_id='IDP_128_DetectionFlag_OR_LoggGranulation_True_False_MSAP2', python_callable=IDP_128_DetectionFlag_OR_LoggGranulation_T_F_MSAP2, provide_context=True, trigger_rule = "none_failed_or_skipped")
    """Create SubDAG/TaskGroup for the MSAP2 datapipeline"""
    with TaskGroup("MSAP2") as MSAP2:
        """Calling the MSAP2 Class"""
        Class_MSAP2 = Create_ClassicStellarParameters.MSAP2
        """The start function generates the report for MSAP2"""
        Start_MSAP2 = PythonOperator(task_id="Start_MSAP2", python_callable=Class_MSAP2.Start_MSAP2, trigger_rule = "none_failed_or_skipped")
        """Check for the inputs - PDP, IDP and DP3"""
        PDP_IDP_DP3_Checks_MSAP2 = PythonOperator(task_id="PDP_IDP_DP3_Checks_MSAP2", python_callable=Class_MSAP2.PDP_IDP_DP3_Checks_MSAP2, trigger_rule="none_failed_or_skipped")
        """Defining the different functions for MSAP2 datapipeline"""
        SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01 = PythonOperator(task_id="SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01", python_callable=Class_MSAP2.SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01, trigger_rule="none_failed_or_skipped")
        SPECTROSCOPY_MSAP2_02 = PythonOperator(task_id="SPECTROSCOPY_MSAP2_02", python_callable=Class_MSAP2.SPECTROSCOPY_MSAP2_02, trigger_rule="none_failed_or_skipped")
        BAYESIAN_INFERENCE_MSAP2_03 = PythonOperator(task_id="BAYESIAN_INFERENCE_MSAP2_03", python_callable=Class_MSAP2.BAYESIAN_INFERENCE_MSAP2_03, trigger_rule="none_failed_or_skipped")
        """End MSAP2 with Stop_MSAP2 operator"""
        Stop_MSAP2 = PythonOperator(task_id="Stop_MSAP2", python_callable=Class_MSAP2.Stop_MSAP2, trigger_rule="none_failed_or_skipped")
        """Defining the Dependencies specific to MSAP2"""
        Start_MSAP2 >> PDP_IDP_DP3_Checks_MSAP2
        PDP_IDP_DP3_Checks_MSAP2 >> SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01
        SEISMIC_LOGG_COMPUTATION_LOGG_SELECTION_MSAP2_01 >> SPECTROSCOPY_MSAP2_02
        SPECTROSCOPY_MSAP2_02 >> BAYESIAN_INFERENCE_MSAP2_03
        BAYESIAN_INFERENCE_MSAP2_03 >> Stop_MSAP2

    IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2 = PythonOperator(task_id ="IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2", python_callable=CheckInputs.IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2, trigger_rule="none_failed_or_skipped")

    with TaskGroup("MSAP5") as MSAP5:
        """Defining the Class for the overall MSAP5"""
        Class_Overall_MSAP5 = Create_DP5Out.Overall_MSAP5
        """The start function generates the report for Overall MSAP5"""
        Start_MSAP5 = PythonOperator(task_id="Start_MSAP5", python_callable=Class_Overall_MSAP5.Start_MSAP5, trigger_rule="none_failed_or_skipped")
        """Checking for DP2, DP3, DP4 and outputs generated from MSAP2 to be used as inputs in this Datapipeline"""
        with TaskGroup("InitialInputCheckup_MSAP5") as InitialInputCheckup_MSAP5:
            InitialInputCheckup_MSAP5_DP2 = PythonOperator(task_id="InitialInputCheckup_MSAP5_DP2", python_callable=Class_Overall_MSAP5.InitialInputCheckup_MSAP5_DP2, trigger_rule="none_failed_or_skipped")
            InitialInputCheckup_MSAP5_DP4 = PythonOperator(task_id="InitialInputCheckup_MSAP5_DP4", python_callable=Class_Overall_MSAP5.InitialInputCheckup_MSAP5_DP4, trigger_rule="none_failed_or_skipped")
            InitialInputCheckup_MSAP5_DP3 = PythonOperator(task_id="InitialInputCheckup_MSAP5_DP3", python_callable=Class_Overall_MSAP5.InitialInputCheckup_MSAP5_DP3, trigger_rule="none_failed_or_skipped")
            InitialInputCheckup_MSAP5_IDP_123_LOGG_VARLC = PythonOperator(task_id="InitialInputCheckup_MSAP5_IDP_123_LOGG_VARLC", python_callable=Class_Overall_MSAP5.InitialInputCheckup_MSAP5_IDP_123_LOGG_VARLC, trigger_rule="none_failed_or_skipped")
            InitialInputCheckup_MSAP5_OP_MSAP2 = PythonOperator(task_id="InitialInputCheckup_MSAP5_OP_MSAP2", python_callable=Class_Overall_MSAP5.InitialInputCheckup_MSAP5_OP_MSAP2, trigger_rule="none_failed_or_skipped")
            """defining the Dependencies for input checks in MSAP5"""
            [InitialInputCheckup_MSAP5_DP2, InitialInputCheckup_MSAP5_DP4, InitialInputCheckup_MSAP5_DP3, InitialInputCheckup_MSAP5_DP3, InitialInputCheckup_MSAP5_OP_MSAP2]
        """Defining the SubDAG/TaskGroup for MSAP5 Part2"""
        with TaskGroup("MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup") as MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup:
            """Defining the class for MSAP5 Part2"""
            Class_MSAP5_Part2 = MSAP5_Part2.MSAP5_Part2_class
            """The start function generates the report for MSAP5 Part 2"""
            Start_MSAP5_2 = PythonOperator(task_id="Start_MSAP5_2", python_callable=Class_MSAP5_Part2.Start_MSAP5_2, trigger_rule="none_failed_or_skipped")
            """Checking for the inputs to be read in MSAP5 part 2"""
            InputsCheck_MSAP5_2 = PythonOperator(task_id="InputsCheck_MSAP5_2", python_callable=Class_MSAP5_Part2.InputsCheck_MSAP5_2, trigger_rule="none_failed_or_skipped")
            Gyrochronocology_21_Yes_No_MSAP5_2 = BranchPythonOperator(task_id='Gyrochronocology_21_Yes_No_MSAP5_2', python_callable=Gyrochronocology_21_Y_N_MSAP5_2, provide_context=True, trigger_rule="none_failed_or_skipped")
            Gyrochronocology_21 = PythonOperator(task_id="Gyrochronocology_21", python_callable=Class_MSAP5_Part2.Gyrochronocology_21, trigger_rule="none_failed_or_skipped")
            End_From_MSAP5_Part2_21 = PythonOperator(task_id="End_From_MSAP5_Part2_21", python_callable=Class_MSAP5_Part2.End_From_MSAP5_Part2_21, trigger_rule="none_failed_or_skipped")
            Activity_Age_22_Yes_No_MSAP5_2 = BranchPythonOperator(task_id='Activity_Age_22_Yes_No_MSAP5_2', python_callable=Activity_Age_22_Y_N_MSAP5_2, provide_context=True, trigger_rule="none_failed_or_skipped")
            Activity_Age_22 = PythonOperator(task_id="Activity_Age_22", python_callable=Class_MSAP5_Part2.Activity_Age_22, trigger_rule="none_failed_or_skipped")
            End_From_MSAP5_Part2_22 = PythonOperator(task_id="End_From_MSAP5_Part2_22", python_callable=Class_MSAP5_Part2.End_From_MSAP5_Part2_22, trigger_rule="none_failed_or_skipped")
            IDP_123_LOGG_VARLC_Yes_No_MSAP5_2 = BranchPythonOperator(task_id='IDP_123_LOGG_VARLC_Yes_No_MSAP5_2', python_callable=IDP_123_LOGG_VARLC_Y_N_MSAP5_2, provide_context=True, trigger_rule="none_failed_or_skipped")
            End_From_MSAP5_Part2_23_24 = PythonOperator(task_id="End_From_MSAP5_Part2_23_24", python_callable=Class_MSAP5_Part2.End_From_MSAP5_Part2_23_24, trigger_rule="none_failed_or_skipped")
            Dummy_Operator_MSAP5_Part2_23_24 = BashOperator(task_id = "Dummy_Operator_MSAP5_Part2_23_24", bash_command = 'echo "This Operator does nothing !!"', trigger_rule = 'none_failed_or_skipped')
            ComputeMfromLOGG_Flick_R_Phot_23 = PythonOperator(task_id="ComputeMfromLOGG_Flick_R_Phot_23", python_callable=Class_MSAP5_Part2.ComputeMfromLOGG_Flick_R_Phot_23, trigger_rule="none_failed_or_skipped")
            GridBasedModellingUsingClassicalCode_24 = PythonOperator(task_id="GridBasedModellingUsingClassicalCode_24", python_callable=Class_MSAP5_Part2.GridBasedModellingUsingClassicalCode_24, trigger_rule="none_failed_or_skipped")
            End2_From_MSAP5_Part2_23_24 = PythonOperator(task_id="End2_From_MSAP5_Part2_23_24", python_callable=Class_MSAP5_Part2.End2_From_MSAP5_Part2_23_24, trigger_rule="none_failed_or_skipped")
            TransitMeanDensity_Yes_No_MSAP5_2 = BranchPythonOperator(task_id='TransitMeanDensity_Yes_No_MSAP5_2', python_callable=TransitMeanDensity_Y_N_MSAP5_2, provide_context=True, trigger_rule="none_failed_or_skipped")
            End_From_MSAP5_Part2_25_26 = PythonOperator(task_id="End_From_MSAP5_Part2_25_26", python_callable=Class_MSAP5_Part2.End_From_MSAP5_Part2_25_26, trigger_rule="none_failed_or_skipped")
            Dummy_Operator_MSAP5_Part2_25_26 = BashOperator(task_id = "Dummy_Operator_MSAP5_Part2_25_26", bash_command = 'echo "This Operator does nothing !!"', trigger_rule = 'none_failed_or_skipped')
            ComputeMfromRHO_Transit_R_Phot_25 = PythonOperator(task_id="ComputeMfromRHO_Transit_R_Phot_25", python_callable=Class_MSAP5_Part2.ComputeMfromRHO_Transit_R_Phot_25, trigger_rule="none_failed_or_skipped")
            GridBasedModellingUsingClassicalCode_26 = PythonOperator(task_id="GridBasedModellingUsingClassicalCode_26", python_callable=Class_MSAP5_Part2.GridBasedModellingUsingClassicalCode_26, trigger_rule="none_failed_or_skipped")
            End2_From_MSAP5_Part2_25_26 = PythonOperator(task_id="End2_From_MSAP5_Part2_25_26", python_callable=Class_MSAP5_Part2.End2_From_MSAP5_Part2_25_26, trigger_rule="none_failed_or_skipped")
            """End MSAP5 Part 2 with Stop_MSAP5_2 operator"""
            Stop_MSAP5_2 = PythonOperator(task_id = "Stop_MSAP5_2", python_callable=Class_MSAP5_Part2.Stop_MSAP5_2, trigger_rule = 'none_failed_or_skipped')
            """Defining the Dependencies for MSAP5 Part 2"""
            Start_MSAP5_2 >> InputsCheck_MSAP5_2
            InputsCheck_MSAP5_2 >> [Gyrochronocology_21_Yes_No_MSAP5_2, Activity_Age_22_Yes_No_MSAP5_2, IDP_123_LOGG_VARLC_Yes_No_MSAP5_2, TransitMeanDensity_Yes_No_MSAP5_2]
            Gyrochronocology_21_Yes_No_MSAP5_2 >> [Gyrochronocology_21, End_From_MSAP5_Part2_21]
            Activity_Age_22_Yes_No_MSAP5_2 >> [Activity_Age_22, End_From_MSAP5_Part2_22]
            IDP_123_LOGG_VARLC_Yes_No_MSAP5_2 >> [Dummy_Operator_MSAP5_Part2_23_24, End_From_MSAP5_Part2_23_24]
            TransitMeanDensity_Yes_No_MSAP5_2 >> [Dummy_Operator_MSAP5_Part2_25_26, End_From_MSAP5_Part2_25_26]
            Dummy_Operator_MSAP5_Part2_23_24 >> [ComputeMfromLOGG_Flick_R_Phot_23, GridBasedModellingUsingClassicalCode_24]
            [ComputeMfromLOGG_Flick_R_Phot_23, GridBasedModellingUsingClassicalCode_24] >> End2_From_MSAP5_Part2_23_24
            Dummy_Operator_MSAP5_Part2_25_26 >> [ComputeMfromRHO_Transit_R_Phot_25, GridBasedModellingUsingClassicalCode_26]
            [ComputeMfromRHO_Transit_R_Phot_25, GridBasedModellingUsingClassicalCode_26] >> End2_From_MSAP5_Part2_25_26
            Gyrochronocology_21 >> End_From_MSAP5_Part2_21
            Activity_Age_22 >> End_From_MSAP5_Part2_22

            [End_From_MSAP5_Part2_21, End_From_MSAP5_Part2_22, End_From_MSAP5_Part2_23_24, End2_From_MSAP5_Part2_23_24, End_From_MSAP5_Part2_25_26, End2_From_MSAP5_Part2_25_26] >> Stop_MSAP5_2

        IDP_128_DETECTION_FLAG_Yes_No_MSAP5 = BranchPythonOperator(task_id='IDP_128_DETECTION_FLAG_Yes_No_MSAP5', python_callable=IDP_128_DETECTION_FLAG_Y_N_MSAP5, provide_context=True, trigger_rule="none_failed_or_skipped")
        """Defining the SubDAG/TaskGroup for MSAP5 Part1"""
        with TaskGroup("MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup") as MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup:
            """Defining the Class for MSAP5 part1"""
            Class_MSAP5_Part1 = MSAP5_Part1.MSAP5_Part1_class
            """The start function generates the report for MSAP5 Part 1"""
            Start_MSAP5_1 = PythonOperator(task_id="Start_MSAP5_1", python_callable=Class_MSAP5_Part1.Start_MSAP5_1, trigger_rule="none_failed_or_skipped")
            """Performing inputs check for MSAP5 Part1"""
            InputsCheck_MSAP5_1 = PythonOperator(task_id="InputsCheck_MSAP5_1", python_callable=Class_MSAP5_Part1.InputsCheck_MSAP5_1, trigger_rule="none_failed_or_skipped")
            IndividualFrequencies_Yes_No_MSAP5_1 = BranchPythonOperator(task_id='IndividualFrequencies_Yes_No_MSAP5_1', python_callable=IndividualFrequencies_Y_N_MSAP5_1, provide_context=True, trigger_rule="none_failed_or_skipped")
            End_From_MSAP5_1 = PythonOperator(task_id="End_From_MSAP5_1", python_callable=Class_MSAP5_Part1.End_From_MSAP5_1, trigger_rule="none_failed_or_skipped")
            ScalingLaws_12 = PythonOperator(task_id="ScalingLaws_12", python_callable=Class_MSAP5_Part1.ScalingLaws_12, trigger_rule="none_failed_or_skipped")
            ScalingLaws_12_End = PythonOperator(task_id="ScalingLaws_12_End", python_callable=Class_MSAP5_Part1.ScalingLaws_12_End, trigger_rule="none_failed_or_skipped")
            GlobalParametersDirectMethod_11 = PythonOperator(task_id="GlobalParametersDirectMethod_11", python_callable=Class_MSAP5_Part1.GlobalParametersDirectMethod_11, trigger_rule="none_failed_or_skipped")
            GlobalParametersDirectMethod_11_End = PythonOperator(task_id="GlobalParametersDirectMethod_11_End", python_callable=Class_MSAP5_Part1.GlobalParametersDirectMethod_11_End, trigger_rule="none_failed_or_skipped")
            Mixed_Modes_Yes_No_MSAP5_1 = BranchPythonOperator(task_id='Mixed_Modes_Yes_No_MSAP5_1', python_callable=Mixed_Modes_Y_N_MSAP5_1, provide_context=True, trigger_rule="none_failed_or_skipped")
            GridBasedInferenceMixedModes_13 = PythonOperator(task_id="GridBasedInferenceMixedModes_13", python_callable=Class_MSAP5_Part1.GridBasedInferenceMixedModes_13, trigger_rule="none_failed_or_skipped")
            DummyOperatorConnecting_14_15 = BashOperator(task_id = "DummyOperatorConnecting_14_15", bash_command = 'echo "This Operator does nothing !!"', trigger_rule = 'none_failed_or_skipped')
            GridBasedInferenceIndividualFrequencies_14 = PythonOperator(task_id="GridBasedInferenceIndividualFrequencies_14", python_callable=Class_MSAP5_Part1.GridBasedInferenceIndividualFrequencies_14, trigger_rule="none_failed_or_skipped")
            GridBasedInferenceSurfaceIndependent_15 = PythonOperator(task_id="GridBasedInferenceSurfaceIndependent_15", python_callable=Class_MSAP5_Part1.GridBasedInferenceSurfaceIndependent_15, trigger_rule="none_failed_or_skipped")
            SofesticatedMethod_Yes_No_MSAP5_1_From13 = BranchPythonOperator(task_id='SofesticatedMethod_Yes_No_MSAP5_1_From13', python_callable=SofesticatedMethod_Y_N_MSAP5_1_From13, provide_context=True, trigger_rule="none_failed_or_skipped")
            Interpolation_Yes_No_From13 = BranchPythonOperator(task_id='Interpolation_Yes_No_From13', python_callable=Interpolation_Y_N_From13, provide_context=True, trigger_rule="none_failed_or_skipped")
            End_From13 = PythonOperator(task_id="End_From13", python_callable=Class_MSAP5_Part1.End_From13, trigger_rule="none_failed_or_skipped")
            Interpolation_131 = PythonOperator(task_id="Interpolation_131", python_callable=Class_MSAP5_Part1.Interpolation_131, trigger_rule="none_failed_or_skipped")
            Inversion_132 = PythonOperator(task_id="Inversion_132", python_callable=Class_MSAP5_Part1.Inversion_132, trigger_rule="none_failed_or_skipped")
            MixedModeFittingwithInversionConstraints_133 = PythonOperator(task_id="MixedModeFittingwithInversionConstraints_133", python_callable=Class_MSAP5_Part1.MixedModeFittingwithInversionConstraints_133, trigger_rule="none_failed_or_skipped")
            SofesticatedMethod_Yes_No_MSAP5_1_From_14_15 = BranchPythonOperator(task_id='SofesticatedMethod_Yes_No_MSAP5_1_From_14_15', python_callable=SofesticatedMethod_Y_N_MSAP5_1_From_14_15, provide_context=True, trigger_rule="none_failed_or_skipped")
            End_From_14_15 = PythonOperator(task_id="End_From_14_15", python_callable=Class_MSAP5_Part1.End_From_14_15, trigger_rule="none_failed_or_skipped")
            Glitches_16 = PythonOperator(task_id="Glitches_16", python_callable=Class_MSAP5_Part1.Glitches_16, trigger_rule="none_failed_or_skipped")
            GridBasedInferenceGlitch_161 = PythonOperator(task_id="GridBasedInferenceGlitch_161", python_callable=Class_MSAP5_Part1.GridBasedInferenceGlitch_161, trigger_rule="none_failed_or_skipped")
            Inversion_Yes_No_MSAP5_1 = BranchPythonOperator(task_id='Inversion_Yes_No_MSAP5_1', python_callable=Inversion_Y_N_MSAP5_1, provide_context=True, trigger_rule="none_failed_or_skipped")
            End_From_161 = PythonOperator(task_id="End_From_161", python_callable=Class_MSAP5_Part1.End_From_161, trigger_rule="none_failed_or_skipped")
            Interpolation_Yes_No_MSAP5_1_From_161 = BranchPythonOperator(task_id='Interpolation_Yes_No_MSAP5_1_From_161', python_callable=Interpolation_Y_N_MSAP5_1_From_161, provide_context=True, trigger_rule="none_failed_or_skipped")
            Interpolation_162 = PythonOperator(task_id="Interpolation_162", python_callable=Class_MSAP5_Part1.Interpolation_162, trigger_rule="none_failed_or_skipped")
            Inversion_163 = PythonOperator(task_id="Inversion_163", python_callable=Class_MSAP5_Part1.Inversion_163, trigger_rule="none_failed_or_skipped")
            GridBasedInferenceWithInversionConstraints_164 = PythonOperator(task_id="GridBasedInferenceWithInversionConstraints_164", python_callable=Class_MSAP5_Part1.GridBasedInferenceWithInversionConstraints_164, trigger_rule="none_failed_or_skipped")
            """End MSAP5 Part 1 with Stop_MSAP5_1 operator"""
            End_MSAP5_1 = PythonOperator(task_id="End_MSAP5_1", python_callable=Class_MSAP5_Part1.End_MSAP5_1, trigger_rule="none_failed_or_skipped")

            """Defining the Dependencies for MSAP5 Part1"""
            Start_MSAP5_1 >> InputsCheck_MSAP5_1
            InputsCheck_MSAP5_1 >> [IndividualFrequencies_Yes_No_MSAP5_1, GlobalParametersDirectMethod_11, ScalingLaws_12]
            GlobalParametersDirectMethod_11 >> GlobalParametersDirectMethod_11_End
            ScalingLaws_12 >> ScalingLaws_12_End

            IndividualFrequencies_Yes_No_MSAP5_1 >> Mixed_Modes_Yes_No_MSAP5_1
            IndividualFrequencies_Yes_No_MSAP5_1 >> End_From_MSAP5_1

            Mixed_Modes_Yes_No_MSAP5_1 >> [GridBasedInferenceMixedModes_13, DummyOperatorConnecting_14_15]
            DummyOperatorConnecting_14_15 >> [GridBasedInferenceIndividualFrequencies_14, GridBasedInferenceSurfaceIndependent_15]

            GridBasedInferenceMixedModes_13 >> SofesticatedMethod_Yes_No_MSAP5_1_From13
            SofesticatedMethod_Yes_No_MSAP5_1_From13 >> End_From13

            SofesticatedMethod_Yes_No_MSAP5_1_From13 >> Interpolation_Yes_No_From13
            Interpolation_Yes_No_From13 >> [Interpolation_131, Inversion_132]
            Interpolation_131 >> Inversion_132
            Inversion_132 >> MixedModeFittingwithInversionConstraints_133
            MixedModeFittingwithInversionConstraints_133 >> End_MSAP5_1

            [GridBasedInferenceIndividualFrequencies_14, GridBasedInferenceSurfaceIndependent_15] >> SofesticatedMethod_Yes_No_MSAP5_1_From_14_15
            SofesticatedMethod_Yes_No_MSAP5_1_From_14_15 >> End_From_14_15
            SofesticatedMethod_Yes_No_MSAP5_1_From_14_15 >> Glitches_16
            Glitches_16 >> GridBasedInferenceGlitch_161
            GridBasedInferenceGlitch_161 >> Inversion_Yes_No_MSAP5_1
            Inversion_Yes_No_MSAP5_1 >> End_From_161
            Inversion_Yes_No_MSAP5_1 >> Interpolation_Yes_No_MSAP5_1_From_161
            Interpolation_Yes_No_MSAP5_1_From_161 >> Interpolation_162
            Interpolation_Yes_No_MSAP5_1_From_161 >> Inversion_163
            Interpolation_162 >> Inversion_163
            Inversion_163 >> GridBasedInferenceWithInversionConstraints_164
            GridBasedInferenceWithInversionConstraints_164 >> End_MSAP5_1

            """Ending everything with End_MSAP5_1"""
            [ScalingLaws_12_End, GlobalParametersDirectMethod_11_End, End_From_MSAP5_1, End_From13, End_From_14_15, End_From_161] >> End_MSAP5_1
        """Defining the TaskGroup/SubDAG for MSAP5 Part3"""
        with TaskGroup("MSAP5_3_Selection_and_Validation_TaskGroup") as MSAP5_3_Selection_and_Validation_TaskGroup:
            """Defining the class for MSAP5 Part3"""
            Class_MSAP5_Part3 = MSAP5_Part3.MSAP5_Part3_class
            """The start function generates the report for MSAP5 Part 3"""
            Start_MSAP5_3 = PythonOperator(task_id="Start_MSAP5_3", python_callable=Class_MSAP5_Part3.Start_MSAP5_3, trigger_rule="none_failed_or_skipped")
            """Performing input checks for MSAP5 Part3"""
            InputCheck_MSAP5_3 = PythonOperator(task_id="InputCheck_MSAP5_3", python_callable=Class_MSAP5_Part3.InputCheck_MSAP5_3, trigger_rule="none_failed_or_skipped")
            Consistency_Checks_31 = PythonOperator(task_id="Consistency_Checks_31", python_callable=Class_MSAP5_Part3.Consistency_Checks_31, trigger_rule="none_failed_or_skipped")
            Flags_Yes_No_MSAP5_3 = BranchPythonOperator(task_id='Flags_Yes_No_MSAP5_3', python_callable=Flags_Y_N_MSAP5_3, provide_context=True, trigger_rule="none_failed_or_skipped")
            WeighingOfDatasandBayesianDecision_32 = PythonOperator(task_id="WeighingOfDatasandBayesianDecision_32", python_callable=Class_MSAP5_Part3.WeighingOfDatasandBayesianDecision_32, trigger_rule="none_failed_or_skipped")
            Alert_33 = PythonOperator(task_id="Alert_33", python_callable=Class_MSAP5_Part3.Alert_33, trigger_rule="none_failed_or_skipped")
            Validation_34 = PythonOperator(task_id="Validation_34", python_callable=Class_MSAP5_Part3.Validation_34, trigger_rule="none_failed_or_skipped")
            """End MSAP5 Part 3 with End_MSAP5_Part3 operator"""
            End_MSAP5_Part3 = PythonOperator(task_id="End_MSAP5_Part3", python_callable=Class_MSAP5_Part3.End_MSAP5_Part3, trigger_rule="none_failed_or_skipped")
            """Defining the dependencies for MSAP5 Part3"""
            Start_MSAP5_3 >> InputCheck_MSAP5_3
            InputCheck_MSAP5_3 >> Consistency_Checks_31
            Consistency_Checks_31 >> Flags_Yes_No_MSAP5_3
            Flags_Yes_No_MSAP5_3 >> [WeighingOfDatasandBayesianDecision_32, Alert_33]
            WeighingOfDatasandBayesianDecision_32 >> Validation_34
            Validation_34 >> End_MSAP5_Part3
            Alert_33 >> End_MSAP5_Part3
        """Ending the overall MSAP5 with Stop_MSAP5"""
        Stop_MSAP5 = PythonOperator(task_id="Stop_MSAP5", python_callable=Class_Overall_MSAP5.Stop_MSAP5, trigger_rule="none_failed_or_skipped")
        """Defining the dependencies for MSAP5"""
        Start_MSAP5 >> InitialInputCheckup_MSAP5
        InitialInputCheckup_MSAP5 >> [MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup, IDP_128_DETECTION_FLAG_Yes_No_MSAP5]
        IDP_128_DETECTION_FLAG_Yes_No_MSAP5 >> [MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup, MSAP5_3_Selection_and_Validation_TaskGroup]
        MSAP5_2_MRA_determination_from_Non_Seismic_data_TaskGroup >> MSAP5_3_Selection_and_Validation_TaskGroup
        MSAP5_1_MRA_Determination_From_Seismic_Data_TaskGroup >> MSAP5_3_Selection_and_Validation_TaskGroup
        MSAP5_3_Selection_and_Validation_TaskGroup >> Stop_MSAP5
    """Stopping the overall SAS workflow with Stop"""
    Stop = PythonOperator(task_id = "Stop", python_callable=CheckInputs.Stop, trigger_rule = 'none_failed_or_skipped')

    """defining the Overall Dependencies"""
    Start >> MSAP1

    MSAP1 >> MSAP3 >> MSAP3_MSAP4_Completed
    MSAP1 >> MSAP4 >> MSAP3_MSAP4_Completed

    MSAP3_MSAP4_Completed >> IDP_128_DetectionFlag_OR_LoggGranulation_True_False_MSAP2
    IDP_128_DetectionFlag_OR_LoggGranulation_True_False_MSAP2 >> [MSAP2, IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2]

    MSAP2 >> MSAP5
    IDP_128_DetectionFlag_OR_LoggGranulation_False_MSAP2 >> MSAP5

    MSAP5 >> Stop
