import texttable
import os
from pathlib import Path
import pandas as pd
"""Set the home directory path"""
HOME_DIR = str(Path.home())

"""This class contains functions that are used to deleted temporary files,
copy content between 2 files"""
class OperationsClass:
    """The function below deletes a file in a given path"""
    def RemoveFile(PathToFile):
        if os.path.exists(PathToFile):
            os.remove(PathToFile)
        else:
            print("The file does not exist")
    """The function below deletes a file in a given path and returns True
    or False based on if a file exists"""
    def ReturnProcessValue(PathToFile):
        if os.path.isfile(PathToFile)==True:
            Process_Value = pd.read_csv(PathToFile).iloc[0,0]
            os.remove(PathToFile)
        else:
            Process_Value = "False"
        return Process_Value

    """The function below copies data from file1 to file 2"""
    def CopyFile1_File2(PathFile1, PathFile2):
        with open(PathFile1, "a") as FileWhereDataisCopied, open(PathFile2, "r") as FileWhereDataisExtracted:
            f1_lines = FileWhereDataisExtracted.readlines()
            FileWhereDataisCopied.write(f1_lines[0].strip())
            FileWhereDataisCopied.writelines(f1_lines[1:])
        FileWhereDataisCopied.close()
        FileWhereDataisExtracted.close()
